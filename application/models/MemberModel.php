<?php
if(!defined('BASEPATH')) exit;


class MemberModel extends CI_Model {


	public function __construct(){

		parent::__construct();


		$this->load->database();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('User');

	}



	public function SetSignupInsert($array){	// 회원 가입 2019-05-13


		$member_email	= $array['member_email_1'] . "@" . $array['member_email_2'];

		
		$query = "";
		$query .= " insert into ";
		$query .= " admin_member ";
		$query .= " ( ";
		$query .= " member_id ";
		$query .= " , member_password ";
		$query .= " , member_name ";
		$query .= " , member_email ";
		$query .= " , member_level ";
		$query .= " , member_profile ";
		$query .= " , member_formal ";
		$query .= " , member_signup_date ";
		$query .= " ) ";
		$query .= " values ";
		$query .= " ( ";
		$query .= " " . $this->db->escape($array['member_id']) . " ";
		$query .= " , " . $this->db->escape($array['member_password']) . " ";
		$query .= " , " . $this->db->escape($array['member_name']) . " ";
		$query .= " , " . $this->db->escape($member_email) . " ";
		$query .= " , '1' ";
		$query .= " , " . $this->db->escape($array['member_profile']) . " ";
		$query .= " , '1' ";
		$query .= " , now() ";
		$query .= " ) ";
		//echo $query;

		$this->db->query($query);

	}


    public function CheckDuplicatedID($array){	// 회원아이디 중복 체크 2019-09-06

        $query	= "";
        $query	.= " select ";
        $query	.= " * ";
        $query	.= " from ";
        $query	.= " admin_member ";
        $query	.= " where ";
        $query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";
        //echo $query;

        $result	= $this->db->query($query);

        return $result;

    }


    public function CheckDuplicatedName($array){	// 회원이름 중복 체크 2019-09-09

        $query	= "";
        $query	.= " select ";
        $query	.= " * ";
        $query	.= " from ";
        $query	.= " admin_member ";
        $query	.= " where ";
        $query	.= " member_name = " . $this->db->escape($this->common->GetEncrypt(@$array['member_name'], @$secret_key, @$secret_iv)) . " ";
        //echo $query;

        $result	= $this->db->query($query);

        return $result;

    }


    public function CheckDuplicatedEmail($array){	// 회원이메일 중복 체크 2019-09-06

        $query	= "";
        $query	.= " select ";
        $query	.= " * ";
        $query	.= " from ";
        $query	.= " admin_member ";
        $query	.= " where ";
        $query	.= " member_email = '" . $array['member_email_1'] . "@" . $array['member_email_2'] . "' ";
        //echo $query;

        $result	= $this->db->query($query);

        return $result;

    }


	public function CheckMemberDuplicate($array){	// 회원아이디, 회원이메일 중복 체크 2019-05-13


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";

		if(!empty($array['member_id'])){

			$query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";
			$query	.= " and ";
			$query	.= " member_password = " . $this->db->escape($array['member_password']) . " ";

		}
		
		if(!empty($array['member_email_1']) && !empty($array['member_email_2'])){	// member_email 에 대한 파라미터를 받아야 할 때 2019-05-13

			$query	.= " or ";
			$query	.= " member_email = '" . $array['member_email_1'] . "@" . $array['member_email_2'] . "' ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetMember($array){	// 회원정보 가져오기 2019-05-13

		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";
		//echo $query;
		
		$result	= $this->db->query($query);

		return $result;

	}


	public function SetMemberUpdate($array){	// 회원정보 수정 2019-05-13

		$query	= "";
		$query	.= " update ";
		$query	.= " admin_member ";
		$query	.= " set ";
		$query	.= " member_password = " . $this->db->escape($array['member_password']) . " ";
		$query	.= " , member_name = " . $this->db->escape($array['member_name']) . " ";
		$query	.= " , member_profile = " . $this->db->escape($array['member_profile']) . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";
		$query	.= " and ";
		$query	.= " member_email = " . $this->db->escape($array['member_email']) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetPasswordUpdate($array){	// 비밀번호 변경 로그 2019-07-02

		
		$query  = "";
		$query  .= " insert into ";
		$query  .= " admin_lost_password ";
		$query  .= " ( ";
		$query  .= " member_id ";
		$query  .= " , lo_password ";
		$query  .= " , lo_ip ";
		$query  .= " , lo_date ";
		$query  .= " ) ";
		$query  .= " values ";
		$query  .= " ( ";
		$query  .= " " . $this->db->escape($array['member_id']) . " ";
		$query  .= " , " . $this->db->escape($array['member_password']) . " ";
		$query  .= " , '" . $this->input->ip_address() . "' ";
		$query  .= " , now() ";
		$query  .= " ) ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetMemberDelete($member_id, $member_email){	// 회원 탈퇴 2019-05-13


		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		$query	.= " and ";
		$query	.= " member_email = " . $this->db->escape($member_email) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function CheckMemberAccounts($array){	// 아이디찾기 및 비밀번호찾기용 메서드 2019-06-11

		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if(!empty($array['member_id'])){

			$query	.= " and ";
			$query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";

		}
		
		if(!empty($array['member_email_1']) && !empty($array['member_email_2'])){

			$query	.= " and ";
			$query	.= " member_email = '" . $array['member_email_1'] . "@" . $array['member_email_2'] . "' ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetInstalledBoardList(){	// 어드민에 설치 된 게시판 목록의 이름을 가져옴 2019-07-11

		$query	= "";
		$query	.= " select ";
		$query	.= " con_name ";
		$query	.= " from ";
		$query	.= " admin_board ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetMembersBoardList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " ( ";
				$query .= " select ";
				$query .= " * ";
				$query .= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query .= " from ";
				$query .= " " . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					$query .= " union ";
					
				}
				
			}
			
			$query .= " limit " . $start . " , " . $end . " ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersBoardCount($member_id, $board_count, $board_result){


		$query	= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " select ";
				$query .= " * ";
				$query .= " from ";
				$query .= " " . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					$query .= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}
	
	
	public function GetMembersFileList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " ( ";
				$query .= " select ";
				$query .= " * ";
				$query .= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query .= " from ";
				$query .= " file_" . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					$query .= " union ";
					
				}
				
			}
			
			$query .= " limit " . $start . " , " . $end . " ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersFileCount($member_id, $board_count, $board_result){


		$query	= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " select ";
				$query .= " * ";
				$query .= " from ";
				$query .= " file_" . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					$query .= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}


	public function GetMembersReplyList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " ( ";
				$query .= " select ";
				$query .= " * ";
				$query .= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query .= " from ";
				$query .= " reply_" . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					$query .= " union ";
					
				}
				
			}
			
			$query .= " limit " . $start . " , " . $end . " ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersReplyCount($member_id, $board_count, $board_result){


		$query	= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " select ";
				$query .= " * ";
				$query .= " from ";
				$query .= " reply_" . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					$query .= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}


	public function GetMembersVotedList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " ( ";
				$query .= " select ";
				$query .= " * ";
				$query .= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query .= " from ";
				$query .= " vote_" . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					//$query	.= " limit " . $start . " , " . $end . " ";
					$query .= " ) ";
					$query .= " union ";
					
				}
				
			}
			
			$query .= " limit " . $start . " , " . $end . " ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersVotedCount($member_id, $board_count, $board_result){


		$query	= "";
		
		if($board_count >= 1) {
			
			foreach ($board_result as $no => $print) {
				
				$query .= " select ";
				$query .= " * ";
				$query .= " from ";
				$query .= " vote_" . $print['con_name'] . " ";
				
				if ($no % $board_count == $board_count - 1) {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					
				} else {
					
					$query .= " where ";
					$query .= " 1 = 1 ";
					$query .= " and ";
					$query .= " member_id = " . $this->db->escape($member_id) . " ";
					$query .= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}

}