<?php
if(!defined('BASEPATH')) exit;


class HomeModel extends CI_Model {


	public function __construct(){

		parent::__construct();


		$this->load->database();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('User');

	}
	
	
	public function GetPageContent($page_name){     // 페이지 본문 2019-09-23
		
		$query  = "";
		$query  .= " select ";
		$query  .= " * ";
		$query  .= " from ";
		$query  .= " admin_page ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		$query  .= " and ";
		$query  .= " page_name = " . $this->db->escape($page_name) . " ";
		//echo $query;
		
		$result = $this->db->query($query);
		
		return $result;
		
	}

}