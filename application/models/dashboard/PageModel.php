<?php
if(!defined('BASEPATH')) exit;


class PageModel extends CI_Model{


	public function __construct(){

		parent::__construct();

		$this->load->database();

		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');

	}
	
	
	public function GetMenu(){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " order by menu_grandparent , menu_parent, menu_child ";
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}

	
	
	public function GetGDMaximum($array){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " max(menu_grandparent) as max_gd ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_grandparent = " . $this->db->escape($array) . " ";
		//echo $query . "<br>";
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function GetParentMaximum($array){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " max(menu_parent) as max_par ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_grandparent = " . $this->db->escape($array) . " ";
		//echo $query . "<br>";
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function CheckMenu($array){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if(isset($array['menu_no'])){
			
			$query	.= " and ";
			$query	.= " menu_no = " . $this->db->escape($array['menu_no']) . " ";
			
		}
		
		if(isset($array['menu_grandparent'])){
			
			$query	.= " and ";
			$query	.= " menu_grandparent = " . $this->db->escape($array['menu_grandparent']) . " ";
			
		}
		
		if(isset($array['menu_parent'])){
			
			$query	.= " and ";
			$query	.= " menu_parent = " . $this->db->escape($array['menu_parent']) . " ";
		}
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function SetGDMenuInsert($array){	// 메뉴 추가 2019-04-29

		
		$query = "";
		$query .= " insert into ";
		$query .= " admin_menu ";
		$query .= " ( ";
		$query .= " menu_grandparent ";
		$query .= " , menu_parent ";
		$query .= " , menu_child ";
		$query .= " , menu_name ";
		$query .= " , menu_url ";
		$query .= " , menu_target ";
		$query .= " , menu_date ";
		$query .= " ) ";
		$query .= " values ";
		$query .= " ( ";
		$query .= " " . $this->db->escape($array['menu_grandparent']) . " ";
		$query .= " , " . $this->db->escape($array['menu_parent']) . " ";
		$query .= " , " . $this->db->escape($array['menu_child']) . " ";
		$query .= " , " . $this->db->escape($array['menu_name']) . " ";
		$query .= " , " . $this->db->escape($array['menu_url']) . " ";
		$query .= " , " . $this->db->escape($array['menu_target']) . " ";
		$query .= " , now() ";
		$query .= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	
	public function SetMenuUpdate($array){	// 메뉴 수정 2019-04-29
		
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_menu ";
		$query	.= " set ";
		$query	.= " menu_grandparent = " . $this->db->escape($array['menu_grandparent']) . " ";
		$query	.= " , menu_parent = " . $this->db->escape($array['menu_parent']) . " ";
		$query	.= " , menu_child = " . $this->db->escape($array['menu_child']) . " ";
		$query	.= " , menu_name = " . $this->db->escape($array['menu_name']) . " ";
		$query	.= " , menu_url = " . $this->db->escape($array['menu_url']) . " ";
		$query	.= " , menu_target = " . $this->db->escape($array['menu_target']) . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_no = " . $this->db->escape($array['menu_no']) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	
	public function SetMenuDelete($array){	// 메뉴 삭제 2019-04-30

		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_no = ".$this->db->escape($array['menu_no'])." ";
		//echo $query;

		$this->db->query($query);

	}


}