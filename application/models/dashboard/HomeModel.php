<?php
if(!defined('BASEPATH')) exit;


class HomeModel extends CI_Model{
	
	
	public function __construct(){
		
		parent::__construct();
		
		$this->load->database();
		
		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');
		
	}
	
	
	public function GetConfig(){	// 어드민-기본환경설정 출력 2019-01-31
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_config ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;
		
		$result		= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function SetConfigInsert($array){	// 어드민-기본환경설정 인서트 2019-02-21
		
		$query	= "";
		$query	.= " insert into ";
		$query	.= " admin_config ";
		$query	.= " ( ";
		$query	.= " home_no ";
		$query	.= " , home_title ";
		$query	.= " , home_layout ";
		$query  .= " , member_skin ";
		$query  .= " , home_id_blocks ";
		$query  .= " , home_name_blocks ";
		$query	.= " , home_date ";
		$query	.= " ) ";
		$query	.= " values ";
		$query	.= " ( ";
		$query	.= " " . $this->db->escape($array['home_no']) . " ";
		$query	.= " , " . $this->db->escape($array['home_title']) . " ";
		$query	.= " , " . $this->db->escape($array['home_layout']) . " ";
		$query  .= " , " . $this->db->escape($array['member_skin']) . " ";
		$query  .= " , " . $this->db->escape($array['home_id_blocks']) . " ";
		$query  .= " , " . $this->db->escape($array['home_name_blocks']) . " ";
		$query	.= " , now() ";
		$query	.= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetConfigUpdate($array){	// 어드민-기본환경설정 수정 2019-01-31
		
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_config ";
		$query	.= " set ";
		$query	.= " home_title = " . $this->db->escape($array['home_title']) . " ";
		$query	.= " , home_layout = " . $this->db->escape($array['home_layout']) . " ";
		$query  .= " , member_skin = " . $this->db->escape($array['member_skin']) . " ";
		$query  .= " , home_id_blocks = " . $this->db->escape($array['home_id_blocks']) . " ";
		$query  .= " , home_name_blocks = " . $this->db->escape($array['home_name_blocks']) . " ";
		$query	.= " , home_date = now() ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " home_no = " . $this->db->escape($array['home_no']) . " ";	// 시퀀스를 난수로 생성하려 했으나 불필요 할 거 같아 정수 1을 인덱스로 삼는다.
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetImageUpdate($array){	// 어드민-기본환경설정 이미지업로드 2020-03-04
		
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_config ";
		$query	.= " set ";
		
		if(!empty($array['home_logo'])){
			
			$query	.= " home_logo = " . $this->db->escape($array['home_logo']) . " ";
			
		} else if(!empty($array['home_icon'])){
			
			$query	.= " home_icon = " . $this->db->escape($array['home_icon']) . " ";
			
		}
		
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " home_no = " . $this->db->escape($array['home_no']) . " ";	// 시퀀스를 난수로 생성하려 했으나 불필요 할 거 같아 정수 1을 인덱스로 삼는다.
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetSmtpUpdate($array){	// 어드민-기본환경설정 smtp 2020-03-04
		
		
		$home_email	= $array['home_email_1'] . "@" . $array['home_email_2'];
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_config ";
		$query	.= " set ";
		$query	.= " home_email = " . $this->db->escape($home_email) . " ";
		$query	.= " , home_email_password = " . $this->db->escape($array['home_email_password']) . " ";
		$query	.= " , home_email_host = " . $this->db->escape($array['home_email_host']) . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " home_no = " . $this->db->escape($array['home_no']) . " ";	// 시퀀스를 난수로 생성하려 했으나 불필요 할 거 같아 정수 1을 인덱스로 삼는다.
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetMetaUpdate($array){	// 어드민-기본환경설정 메타 2020-03-04
		
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_config ";
		$query	.= " set ";
		
		if($array['type'] == "naver"){
			
			$query	.= " home_naver = " . $this->db->escape($array['home_naver']) . " ";
			
		} else if($array['type'] == "google"){
			
			$query	.= " home_google = " . $this->db->escape($array['home_google']) . " ";
			
		}
		
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " home_no = " . $this->db->escape($array['home_no']) . " ";	// 시퀀스를 난수로 생성하려 했으나 불필요 할 거 같아 정수 1을 인덱스로 삼는다.
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function GetCompany(){	// 어드민-회사정보 출력 2019-02-15
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_company ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;
		
		$result		= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function SetCompanyInsert($array){	// 어드민-기본환경설정 인서트 2019-02-21
		
		
		$com_rotary			= $array['com_rotary_1'] . "-" . $array['com_rotary_2'] . "-" . $array['com_rotary_3'];
		$com_fax			= $array['com_fax_1'] . "-" . $array['com_fax_2'] . "-" . $array['com_fax_3'];
		$com_manager_email	= $array['com_manager_email_1'] . "@" . $array['com_manager_email_2'];
		
		
		$query	= "";
		$query	.= " insert into ";
		$query	.= " admin_company ";
		$query	.= " ( ";
		$query	.= " com_no ";
		$query	.= " , com_name ";
		$query	.= " , com_owner ";
		$query	.= " , com_rotary ";
		$query	.= " , com_fax ";
		$query	.= " , com_zip_1 ";
		$query	.= " , com_zip_2 ";
		$query	.= " , com_address_1 ";
		$query	.= " , com_address_2 ";
		$query	.= " , com_manager ";
		$query	.= " , com_manager_email ";
		$query	.= " , com_date ";
		$query	.= " ) ";
		$query	.= " values ";
		$query	.= " ( ";
		$query .= " " . $this->db->escape($array['com_no']) . " ";
		$query .= " , " . $this->db->escape($array['com_name']) . " ";
		$query .= " , " . $this->db->escape($array['com_owner']) . " ";
		$query .= " , " . $this->db->escape($com_rotary) . " ";
		$query .= " , " . $this->db->escape($com_fax) . " ";
		$query .= " , " . $this->db->escape($array['com_zip_1']) . " ";
		$query .= " , " . $this->db->escape($array['com_zip_2']) . " ";
		$query .= " , " . $this->db->escape($array['com_address_1']) . " ";
		$query .= " , " . $this->db->escape($array['com_address_2']) . " ";
		$query .= " , " . $this->db->escape($array['com_manager']) . " ";
		$query .= " , " . $this->db->escape($com_manager_email) . " ";
		$query .= " , now() ";
		$query .= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	
	public function SetCompanyUpdate($array){	// 어드민-회사정보 수정 2019-02-15
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_company ";
		$query	.= " set ";
		
		if(!empty($array['com_name'])){
			
			$query	.= " com_name = " . $this->db->escape($array['com_name']) . " , ";
			
		}
		
		if(!empty($array['com_owner'])){
			
			$query	.= " com_owner = " . $this->db->escape($array['com_owner']) . " , ";
			
		}
		
		if(!empty($array['com_rotary_1']) && !empty($array['com_rotary_2']) && !empty($array['com_rotary_3'])){
			
			$com_rotary		= $array['com_rotary_1'] . "-" . $array['com_rotary_2'] . "-" . $array['com_rotary_3'];
			$query			.= " com_rotary = " . $this->db->escape($com_rotary) . " , ";
			
		}
		
		if(!empty($array['com_fax_1']) && !empty($array['com_fax_2']) && !empty($array['com_fax_3'])){
			
			$com_fax		= $array['com_fax_1'] . "-" . $array['com_fax_2'] . "-" . $array['com_fax_3'];
			$query			.= " com_fax = " . $this->db->escape($com_fax) . " , ";
			
		}
		
		if(!empty($array['com_zip_1'])){
			
			$query		.= " com_zip_1 = " . $this->db->escape($array['com_zip_1']) . " , ";
			
		}
		
		if(!empty($array['com_zip_2'])){
			
			$query		.= " com_zip_2 = " . $this->db->escape($array['com_zip_2']) . " , ";
			
		}
		
		if(!empty($array['com_address_1'])){
			
			$query		.= " com_address_1 = " . $this->db->escape($array['com_address_1']) . " , ";
			
		}
		
		if(!empty($array['com_address_2'])){
			
			$query		.= " com_address_2 = " . $this->db->escape($array['com_address_2']) . " , ";
			
		}
		
		if(!empty($array['com_manager'])){
			
			$query		.= " com_manager = " . $this->db->escape($array['com_manager']) . " , ";
			
		}
		
		if(!empty($array['com_manager_email_1']) && !empty($array['com_manager_email_2'])){
			
			$com_manager_email	= $array['com_manager_email_1'] . "@" . $array['com_manager_email_2'];
			$query				.= " com_manager_email = " . $this->db->escape($com_manager_email) . " , ";
			
		}
		
		$query	.= " com_date = now() ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " com_no = " . $this->db->escape($array['com_no']) . " ";	// 시퀀스를 난수로 생성하려 했으나 불필요 할 거 같아 정수 1을 인덱스로 삼는다.
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function GetInstalledBoard(){	// 어드민에 설치 된 게시판 목록의 이름을 가져옴 2019-07-11
		
		$query	= "";
		$query	.= " select ";
		$query	.= " con_name ";
		$query	.= " from ";
		$query	.= " admin_board ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestArticles($board_count, $board_result){    // 최신 게시물 5개 2019-11-06
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , member_name ";
				$query  .= " , bbs_title ";
				$query  .= " , bbs_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " " . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
			$query	.= " order by bbs_no desc limit 5 ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query . "----" . $board_count;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestArticlesCount($board_count, $board_result){    // 최신 게시물 5개 2019-11-07
		
		
		/**
		리팩토링 구간
		union 에 대한 sum 을 모두 구하시오.
		 **/
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , member_name ";
				$query  .= " , bbs_title ";
				$query  .= " , bbs_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " " . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestReplies($board_count, $board_result){    // 최신 댓글 5개 2019-11-06
		
		
		/**
		리팩토링 구간
		union 에 대한 sum 을 모두 구하시오.
		 **/
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " rep_no ";
				$query  .= " , bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , member_name ";
				$query  .= " , rep_content ";
				$query  .= " , rep_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " reply_" . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
			$query	.= " order by rep_no desc limit 5 ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestRepliesCount($board_count, $board_result){    // 최신 댓글 5개 2019-11-06
		
		
		/**
		리팩토링 구간
		union 에 대한 sum 을 모두 구하시오.
		 **/
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " rep_no ";
				$query  .= " , bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , member_name ";
				$query  .= " , rep_content ";
				$query  .= " , rep_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " reply_" . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestFiles($board_count, $board_result){    // 최신 첨부파일 5개 2019-11-06
		
		
		/**
		리팩토링 구간
		union 에 대한 sum 을 모두 구하시오.
		 **/
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " file_no ";
				$query  .= " , bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , file_real_name ";
				$query  .= " , file_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " file_" . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
			$query	.= " order by file_no desc limit 5 ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestFilesCount($board_count, $board_result){    // 최신 첨부파일 5개 2019-11-06
		
		
		/**
		리팩토링 구간
		union 에 대한 sum 을 모두 구하시오.
		 **/
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " file_no ";
				$query  .= " , bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , file_real_name ";
				$query  .= " , file_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " file_" . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestVoted($board_count, $board_result){    // 최근 추천/비추천수 2019-11-06
		
		
		/**
		리팩토링 구간
		union 에 대한 sum 을 모두 구하시오.
		 **/
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " vo_no ";
				$query  .= " , bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , vo_like ";
				$query  .= " , vo_dislike ";
				$query  .= " , vo_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " vote_" . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
			$query	.= " order by vo_no desc limit 5 ";
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetLatestVotedCount($board_count, $board_result){    // 최근 추천/비추천수 2019-11-06
		
		
		/**
		리팩토링 구간
		union 에 대한 sum 을 모두 구하시오.
		 **/
		
		
		$query		= "";
		
		if($board_count >= 1){
			
			foreach($board_result as $no => $print){
				
				$query	.= " ( ";
				$query	.= " select ";
				$query	.= " vo_no ";
				$query  .= " , bbs_no ";
				$query  .= " , member_id ";
				$query  .= " , vo_like ";
				$query  .= " , vo_dislike ";
				$query  .= " , vo_date ";
				$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
				$query	.= " from ";
				$query	.= " vote_" . $print['con_name'] . " ";
				
				if($no % $board_count == $board_count - 1){
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					
				} else {
					
					$query	.= " where ";
					$query	.= " 1 = 1 ";
					$query	.= " ) ";
					$query	.= " union ";
					
				}
				
			}
			
		} else {
			
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " admin_board ";
			$query	.= " where ";
			$query	.= " 1 = 1 ";
			
		}
		//echo $query;
		
		$result			= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetJoinMembers($board_count, $board_result){    // 최근 가입 회원 목록 5개 2019-11-06
		
		
		$query	= "";
		$query	.= " select ";
		$query	.= " member_no ";
		$query  .= " , member_id ";
		$query  .= " , member_name ";
		$query  .= " , member_signup_date ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " order by member_no desc limit 5";
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetJoinMembersCount($board_count, $board_result){    // 최근 가입 회원 목록 5개 2019-11-06
		
		
		$query	= "";
		$query	.= " select ";
		$query	.= " member_no ";
		$query  .= " , member_id ";
		$query  .= " , member_name ";
		$query  .= " , member_signup_date ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
}