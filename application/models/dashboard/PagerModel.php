<?php
if(!defined('BASEPATH')) exit;


class PagerModel extends CI_Model{


	public function __construct(){

		parent::__construct();

		$this->load->database();

		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');

	}
	
	
	public function GetMenu(){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " order by menu_grandparent , menu_parent, menu_child ";
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}

	
	
	public function GetGDMaximum($array){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " max(menu_grandparent) as max_gd ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_grandparent = " . $this->db->escape($array) . " ";
		//echo $query . "<br>";
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function GetParentMaximum($array){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " max(menu_parent) as max_par ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_grandparent = " . $this->db->escape($array) . " ";
		//echo $query . "<br>";
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function CheckMenu($array){
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if(isset($array['menu_no'])){
			
			$query	.= " and ";
			$query	.= " menu_no = " . $this->db->escape($array['menu_no']) . " ";
			
		}
		
		if(isset($array['menu_grandparent'])){
			
			$query	.= " and ";
			$query	.= " menu_grandparent = " . $this->db->escape($array['menu_grandparent']) . " ";
			
		}
		
		if(isset($array['menu_parent'])){
			
			$query	.= " and ";
			$query	.= " menu_parent = " . $this->db->escape($array['menu_parent']) . " ";
		}
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function SetGDMenuInsert($array){	// 메뉴 추가 2019-04-29

		
		$query = "";
		$query .= " insert into ";
		$query .= " admin_menu ";
		$query .= " ( ";
		$query .= " menu_grandparent ";
		$query .= " , menu_parent ";
		$query .= " , menu_child ";
		$query .= " , menu_name ";
		$query .= " , menu_url ";
		$query .= " , menu_target ";
		$query .= " , menu_date ";
		$query .= " ) ";
		$query .= " values ";
		$query .= " ( ";
		$query .= " " . $this->db->escape($array['menu_grandparent']) . " ";
		$query .= " , " . $this->db->escape($array['menu_parent']) . " ";
		$query .= " , " . $this->db->escape($array['menu_child']) . " ";
		$query .= " , " . $this->db->escape($array['menu_name']) . " ";
		$query .= " , " . $this->db->escape($array['menu_url']) . " ";
		$query .= " , " . $this->db->escape($array['menu_target']) . " ";
		$query .= " , now() ";
		$query .= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	
	public function SetMenuUpdate($array){	// 메뉴 수정 2019-04-29
		
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_menu ";
		$query	.= " set ";
		$query	.= " menu_grandparent = " . $this->db->escape($array['menu_grandparent']) . " ";
		$query	.= " , menu_parent = " . $this->db->escape($array['menu_parent']) . " ";
		$query	.= " , menu_child = " . $this->db->escape($array['menu_child']) . " ";
		$query	.= " , menu_name = " . $this->db->escape($array['menu_name']) . " ";
		$query	.= " , menu_url = " . $this->db->escape($array['menu_url']) . " ";
		$query	.= " , menu_target = " . $this->db->escape($array['menu_target']) . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_no = " . $this->db->escape($array['menu_no']) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	
	public function SetMenuDelete($array){	// 메뉴 삭제 2019-04-30

		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " menu_no = ".$this->db->escape($array['menu_no'])." ";
		//echo $query;

		$this->db->query($query);

	}
	
	
	public function GetPageList($search_type, $search_name, $page, $start, $end){   // 페이지 목록 2019-09-18
		
		if(isset($page)){
			
			$num            = 1 + (($end * $page) - $end);
			
		} else {
			
			$num            = 1 + (($end * 1) - $end);
			
		}
		
		if(isset($page)){
			
			$id             = $page;
			$start          = ($id - 1) * $end;
			
		} else {
			
			$id             = 1;
			$start          = ($id - 1) * $end;
			
		}
		
		$query  = "";
		$query  .= " select ";
		$query  .= " * ";
		$query  .= " from ";
		$query  .= " admin_page ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		
		if($search_type == "page_title"){
			
			$query  .= " and ";
			$query  .= " page_title like '&" . $search_name . "&' ";
			
		} else if($search_type == "page_name"){
			
			$query  .= " and ";
			$query  .= " page_name like '&" . $search_name . "&' ";
			
		}
		
		$query  .= " order by page_no desc limit " . $start . " , " . $end . " ";
		//echo $query;
		
		$result     = $this->db->query($query);
		
		return array("num"=>$num, "id"=>$id, "result"=>$result);
		
	}
	
	
	public function GetPageCount($search_type, $search_name){ // 페이지 목록 갯수 2019-09-18
		
		$query  = "";
		$query  .= " select ";
		$query  .= " * ";
		$query  .= " from ";
		$query  .= " admin_page ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		
		if($search_type == "page_title") {
			
			$query .= " and ";
			$query .= " page_title like '&" . $search_type . "&' ";
			
		} else if($search_type == "page_name"){
			
			$query  .= " and ";
			$query  .= " page_name like '&" . $search_type . "&' ";
			
		}
		//echo $query;
		
		$result     = $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetPageContent($array){     // 페이지 설정 본문 2019-09-19
		
		$query  = "";
		$query  .= " select ";
		$query  .= " * ";
		$query  .= " from ";
		$query  .= " admin_page ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		$query  .= " and ";
		$query  .= " page_name = " . $this->db->escape($array['page_name']) . " ";
		//echo $query;
		
		$result = $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function SetPageInsert($array){
		
		$query  = "";
		$query  .= " insert ";
		$query  .= " admin_page ";
		$query  .= " ( ";
		$query  .= " member_id ";
		$query  .= " , page_title ";
		$query  .= " , page_name ";
		$query  .= " , page_content ";
		$query  .= " , page_layout ";
		$query  .= " , page_include ";
		$query  .= " , page_ip ";
		$query  .= " , page_date ";
		$query  .= " ) ";
		$query  .= " values ";
		$query  .= " ( ";
		$query  .= " " . $this->db->escape($array['member_id']) . " ";
		$query  .= " , " . $this->db->escape($array['page_title']) . " ";
		$query  .= " , " . $this->db->escape($array['page_name']) . " ";
		$query  .= " , " . $this->db->escape($array['page_content']) . " ";
		$query  .= " , " . $this->db->escape($array['page_layout']) . " ";
		$query  .= " , " . $this->db->escape($array['page_include']) . " ";
		$query	.= " , '" . $this->input->ip_address() . "' ";
		$query  .= " , now() ";
		$query  .= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetPageUpdate($array){	// 페이지설정 수정 2019-09-20
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_page ";
		$query	.= " set ";
		$query	.= " page_title = " . $this->db->escape($array['page_title']) . " ";
		$query	.= " , page_name = " . $this->db->escape($array['page_name']) . " ";
		$query	.= " , page_content = " . $this->db->escape($array['page_content']) . " ";
		$query	.= " , page_layout = " . $this->db->escape($array['page_layout']) . " ";
		$query	.= " , page_include = " . $this->db->escape($array['page_include']) . " ";
		$query	.= " , page_ip = '" . $this->input->ip_address() . "' ";
		$query	.= " , page_date = now() ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " page_name = " . $this->db->escape($array['page_name']) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetPageFilesInsert($page_no, $member_id, $file_encrypt_name, $file_name, $file_width, $file_height, $file_type){	// 페이지 파일 업로드 2019-09-23
		
		$query	= "";
		$query	.= " insert into ";
		$query	.= " admin_page_file ";
		$query	.= " ( ";
		$query	.= " page_no ";
		$query	.= " , member_id ";
		$query	.= " , file_encrypt_name ";
		$query	.= " , file_real_name ";
		$query	.= " , file_width ";
		$query	.= " , file_height ";
		$query	.= " , file_type ";
		$query	.= " , file_ip ";
		$query	.= " , file_date ";
		$query	.= " ) ";
		$query	.= " values ";
		$query	.= " ( ";
		$query	.= " " . $this->db->escape($page_no) . " ";
		$query	.= " , " . $this->db->escape($member_id) . " ";
		$query	.= " , " . $this->db->escape($file_encrypt_name) . " ";
		$query	.= " , " . $this->db->escape($file_name) . " ";
		$query	.= " , " . $file_width . " ";
		$query	.= " , " . $file_height ." ";
		$query	.= " , " . $this->db->escape($file_type) . " ";
		$query	.= " , '" . $this->input->ip_address() . "' ";
		$query	.= " , now() ";
		$query	.= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function GetPageFiles($page_no, $file_no){	// 페이지 파일들 가져오기 2019-09-23
		
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_page_file ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " page_no = " . $this->db->escape($page_no) . " ";
		
		if(!empty($file_no)){
			
			$query	.= " and ";
			$query	.= " file_no = " . $this->db->escape($file_no) . " ";
			
		}
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function SetPageFileDelete($file_no, $page_no){     // 페이지 파일 삭제 2019-09-23
		
		$query  = "";
		$query  .= " delete ";
		$query  .= " from ";
		$query  .= " admin_page_file ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		$query  .= " and ";
		$query  .= " file_no = " . $this->db->escape($file_no) . " ";
		$query  .= " and ";
		$query  .= " page_no = " . $this->db->escape($page_no) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetPageDelete($page_name){     // 페이지 삭제 2019-11-20
		
		$query  = "";
		$query  .= " delete ";
		$query  .= " from ";
		$query  .= " admin_page ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		$query  .= " and ";
		$query  .= " page_name = " . $this->db->escape($page_name) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function GetPopupList($search_type, $search_name, $page, $start, $end){   // 팝업 목록 2019-09-24
		
		if(isset($page)){
			
			$num            = 1 + (($end * $page) - $end);
			
		} else {
			
			$num            = 1 + (($end * 1) - $end);
			
		}
		
		if(isset($page)){
			
			$id             = $page;
			$start          = ($id - 1) * $end;
			
		} else {
			
			$id             = 1;
			$start          = ($id - 1) * $end;
			
		}
		
		$query  = "";
		$query  .= " select ";
		$query  .= " * ";
		$query  .= " from ";
		$query  .= " admin_popup ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		
		if($search_type == "popup_title"){
			
			$query  .= " and ";
			$query  .= " popup_title like '&" . $search_name . "&' ";
			
		} else if($search_type == "popup_name"){
			
			$query  .= " and ";
			$query  .= " popup_name like '&" . $search_name . "&' ";
			
		}
		
		$query  .= " order by popup_no desc limit " . $start . " , " . $end . " ";
		//echo $query;
		
		$result     = $this->db->query($query);
		
		return array("num"=>$num, "id"=>$id, "result"=>$result);
		
	}
	
	
	public function GetPopupCount($search_type, $search_name){ // 팝업 목록 갯수 2019-09-18
		
		$query  = "";
		$query  .= " select ";
		$query  .= " * ";
		$query  .= " from ";
		$query  .= " admin_popup ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		
		if($search_type == "popup_title") {
			
			$query .= " and ";
			$query .= " popup_title like '&" . $search_type . "&' ";
			
		} else if($search_type == "popup_name"){
			
			$query  .= " and ";
			$query  .= " popup_name like '&" . $search_type . "&' ";
			
		}
		//echo $query;
		
		$result     = $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function SetPopupInsert($array){     // 팝업 추가 2019-09-24
		
		$query  = "";
		$query  .= " insert ";
		$query  .= " admin_popup ";
		$query  .= " ( ";
		$query  .= " member_id ";
		$query  .= " , popup_title ";
		$query  .= " , popup_name ";
		$query  .= " , popup_content ";
		$query  .= " , popup_ip ";
		$query  .= " , popup_date ";
		$query  .= " ) ";
		$query  .= " values ";
		$query  .= " ( ";
		$query  .= " " . $this->db->escape($array['member_id']) . " ";
		$query  .= " , " . $this->db->escape($array['popup_title']) . " ";
		$query  .= " , " . $this->db->escape($array['popup_name']) . " ";
		$query  .= " , " . $this->db->escape($array['popup_content']) . " ";
		$query	.= " , '" . $this->input->ip_address() . "' ";
		$query  .= " , now() ";
		$query  .= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetPopupUpdate($array){	// 팝업설정 수정 2019-09-20
		
		$query	= "";
		$query	.= " update ";
		$query	.= " admin_popup ";
		$query	.= " set ";
		$query	.= " popup_title = " . $this->db->escape($array['popup_title']) . " ";
		$query	.= " , popup_name = " . $this->db->escape($array['popup_name']) . " ";
		$query	.= " , popup_content = " . $this->db->escape($array['popup_content']) . " ";
		$query	.= " , popup_ip = '" . $this->input->ip_address() . "' ";
		$query	.= " , popup_date = now() ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " popup_name = " . $this->db->escape($array['popup_name']) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function GetPopupContent($array){     // 팝업 설정 본문 2019-09-24
		
		$query  = "";
		$query  .= " select ";
		$query  .= " * ";
		$query  .= " from ";
		$query  .= " admin_popup ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		$query  .= " and ";
		$query  .= " popup_name = " . $this->db->escape($array['popup_name']) . " ";
		//echo $query;
		
		$result = $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetPopupFiles($popup_no, $file_no){	// 팝업 파일들 가져오기 2019-09-23
		
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_popup_file ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " popup_no = " . $this->db->escape($popup_no) . " ";
		
		if(!empty($file)){
			
			$query	.= " and ";
			$query	.= " file_no = " . $this->db->escape($file_no) . " ";
			
		}
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	public function SetPopupFilesInsert($popup_no, $member_id, $file_encrypt_name, $file_name, $file_width, $file_height, $file_type){	// 팝업 파일 업로드 2019-09-23
		
		$query	= "";
		$query	.= " insert into ";
		$query	.= " admin_popup_file ";
		$query	.= " ( ";
		$query	.= " popup_no ";
		$query	.= " , member_id ";
		$query	.= " , file_encrypt_name ";
		$query	.= " , file_real_name ";
		$query	.= " , file_width ";
		$query	.= " , file_height ";
		$query	.= " , file_type ";
		$query	.= " , file_ip ";
		$query	.= " , file_date ";
		$query	.= " ) ";
		$query	.= " values ";
		$query	.= " ( ";
		$query	.= " " . $this->db->escape($popup_no) . " ";
		$query	.= " , " . $this->db->escape($member_id) . " ";
		$query	.= " , " . $this->db->escape($file_encrypt_name) . " ";
		$query	.= " , " . $this->db->escape($file_name) . " ";
		$query	.= " , " . $file_width . " ";
		$query	.= " , " . $file_height ." ";
		$query	.= " , " . $this->db->escape($file_type) . " ";
		$query	.= " , '" . $this->input->ip_address() . "' ";
		$query	.= " , now() ";
		$query	.= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetPopupFileDelete($file_no, $popup_no){     // 팝업 파일 삭제 2019-09-23
		
		$query  = "";
		$query  .= " delete ";
		$query  .= " from ";
		$query  .= " admin_popup_file ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		$query  .= " and ";
		$query  .= " file_no = " . $this->db->escape($file_no) . " ";
		$query  .= " and ";
		$query  .= " popup_no = " . $this->db->escape($popup_no) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	public function SetPopupDelete($popup_name){     // 페이지 삭제 2019-11-21
		
		$query  = "";
		$query  .= " delete ";
		$query  .= " from ";
		$query  .= " admin_popup ";
		$query  .= " where ";
		$query  .= " 1 = 1 ";
		$query  .= " and ";
		$query  .= " popup_name = " . $this->db->escape($popup_name) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}


}