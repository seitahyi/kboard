<?php
if(!defined('BASEPATH')) exit;


class BoardModel extends CI_Model {


	
	public function __construct(){


		parent::__construct();

		$this->load->database();

		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');


	}


	public function SetCreateBoard($array){	// 게시판 추가 create schema 2019-04-30

		$query	= "";
		$query	.= " CREATE TABLE `" . $array['con_name'] . "` ";
		$query	.= " ( ";
		$query	.= " `bbs_no` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ";
		$query	.= " , `member_id` VARCHAR(50) NOT NULL COMMENT '회원 아이디' ";
		$query	.= " , `member_name` VARCHAR(50) NOT NULL COMMENT '회원 이름' ";
		$query	.= " , `bbs_title` VARCHAR(255) NOT NULL COMMENT '제목' ";
		$query	.= " , `bbs_category` VARCHAR(100) NOT NULL COMMENT '카테고리' ";
		$query	.= " , `bbs_content` TEXT NOT NULL COMMENT '본문' ";
		$query  .= " , `bbs_secret` enum('Y', 'N') NOT NULL DEFAULT 'N' COMMENT '비밀글' ";
		$query	.= " , `bbs_readed` MEDIUMINT(8) UNSIGNED NOT NULL COMMENT '조회수' ";
		$query	.= " , `bbs_ip` VARCHAR(16) NOT NULL COMMENT '아이피' ";
		$query	.= " , `bbs_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일' ";
		$query	.= " , PRIMARY KEY (`bbs_no`) ";
		$query  .= " , `ext_desc_1` varchar(30) NOT NULL COMMENT '여분필드 본문 1' ";
		$query  .= " , `ext_desc_2` varchar(30) NOT NULL COMMENT '여분필드 본문 2' ";
		$query  .= " , `ext_desc_3` varchar(30) NOT NULL COMMENT '여분필드 본문 3' ";
		$query  .= " , `ext_desc_4` varchar(30) NOT NULL COMMENT '여분필드 본문 4' ";
		$query  .= " , `ext_desc_5` varchar(30) NOT NULL COMMENT '여분필드 본문 5' ";
		$query	.= " ) ";
		$query	.= " COLLATE='utf8_general_ci' ";
		$query	.= " ENGINE=InnoDB; ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetCreateVote($array){	// 추천/비추천 추가 create schema 2019-05-19

		$query	= "";
		$query	.= " CREATE TABLE `vote_" . $array['con_name'] . "` ";
		$query	.= " ( ";
		$query	.= " `vo_no` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ";
		$query	.= " , `bbs_no` INT(11) UNSIGNED NOT NULL COMMENT '게시판 PK' ";
		$query	.= " , `member_id` VARCHAR(50) NOT NULL COMMENT '회원 아이디' ";
		$query	.= " , `vo_like` TINYINT(1) UNSIGNED NOT NULL COMMENT '추천수' ";
		$query	.= " , `vo_dislike` TINYINT(1) UNSIGNED NOT NULL COMMENT '비추천수' ";
		$query	.= " , `vo_ip` VARCHAR(16) NOT NULL COMMENT '아이피' ";
		$query	.= " , `vo_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일' ";
		$query	.= " , PRIMARY KEY (`vo_no`) ";
		$query	.= " ) ";
		$query	.= " COLLATE='utf8_general_ci' ";
		$query	.= " ENGINE=InnoDB; ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetCreateReply($array){	// 댓글 추가 create schema 2019-05-19

		$query	= "";
		$query	.= " CREATE TABLE `reply_" . $array['con_name'] . "` ";
		$query	.= " ( ";
		$query	.= " `rep_no` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ";
		$query	.= " , `rep_parent_no` INT(11) UNSIGNED NOT NULL ";
		$query	.= " , `rep_child_no` INT(11) UNSIGNED NOT NULL ";
		$query	.= " , `bbs_no` INT(11) UNSIGNED NOT NULL COMMENT '게시판 PK' ";
		$query	.= " , `member_id` VARCHAR(50) NOT NULL COMMENT '회원 아이디' ";
		$query	.= " , `member_name` VARCHAR(50) NOT NULL COMMENT '회원 이름' ";
		$query	.= " , `rep_content` VARCHAR(255) NOT NULL COMMENT '댓글' ";
		$query	.= " , `rep_ip` VARCHAR(16) NOT NULL COMMENT '아이피' ";
		$query	.= " , `rep_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일' ";
		$query	.= " , PRIMARY KEY (`rep_no`) ";
		$query	.= " ) ";
		$query	.= " COLLATE='utf8_general_ci' ";
		$query	.= " ENGINE=InnoDB; ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetCreateFile($array){	// 파일 추가 create schema 2019-05-19

		$query	= "";
		$query	.= " CREATE TABLE `file_" . $array['con_name'] . "` ";
		$query	.= " ( ";
		$query	.= " `file_no` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ";
		$query	.= " , `bbs_no` INT(11) UNSIGNED NOT NULL COMMENT '게시판 PK' ";
		$query	.= " , `member_id` VARCHAR(50) NOT NULL COMMENT '회원 아이디' ";
		$query	.= " , `file_encrypt_name` VARCHAR(255) NOT NULL COMMENT '암호화 파일명' ";
		$query	.= " , `file_real_name` VARCHAR(255) NOT NULL COMMENT '원래 파일명' ";
		$query	.= " , `file_width` SMALLINT(10) UNSIGNED NOT NULL COMMENT '넓이값' ";
		$query	.= " , `file_height` SMALLINT(10) UNSIGNED NOT NULL COMMENT '길이값' ";
		$query	.= " , `file_type` VARCHAR(16) NOT NULL COMMENT '파일 형식' ";
		$query	.= " , `file_ip` VARCHAR(16) NOT NULL COMMENT '아이피' ";
		$query	.= " , `file_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '첨부일자' ";
		$query	.= " , PRIMARY KEY (`file_no`) ";
		$query	.= " ) ";
		$query	.= " COLLATE='utf8_general_ci' ";
		$query	.= " ENGINE=InnoDB; ";
		//echo $query;

		$this->db->query($query);

	}


	public function GetBoardCount($search_type, $search_name){	// 게시판 목록 갯수 2019-05-02

		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_board ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if($search_type == "con_title"){

			$query	.= " and ";
			$query	.= " con_title like '%".$search_name."%' ";

		} else if($search_type == "con_name"){

			$query	.= " and ";
			$query	.= " con_name like '%".$search_name."%' ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}



	public function GetBoardList($search_type, $search_name, $page, $start, $end){	// 게시판 목록 2019-04-30

		if(isset($page)){

			$num				= 1 + (($end * $page) - $end);

		} else {

			$num				= 1 + (($end * 1) - $end);

		}

		if(isset($page)){

			$id					= $page;
			$start				= ($id - 1) * $end;

		} else {

			$id					= 1;
			$start				= ($id - 1) * $end;

		}

		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_board ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "con_title"){

			$query	.= " and ";
			$query	.= " con_title like '%".$search_name."%' ";

		} else if($search_type == "con_name"){

			$query	.= " and ";
			$query	.= " con_name like '%".$search_name."%' ";

		}

		$query	.= " order by con_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetBoard($array){	// 게시판 설정 본문 2019-04-30

		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_board ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " con_name = " . $this->db->escape($array['con_name']) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}
	
	
	public function SetBoardInsert($array){	// 게시판 설정 추가 2019-05-03
		
		$query	= "";
		$query	.= " insert into ";
		$query	.= " admin_board ";
		$query	.= " ( ";
		$query	.= " member_id ";
		$query	.= " , con_title ";
		$query	.= " , con_name ";
		$query	.= " , con_skin ";
		$query	.= " , con_category ";
		$query	.= " , con_layout ";
		$query	.= " , con_thumb_filter ";
		$query	.= " , con_diff ";
		$query  .= " , con_list_level ";
		$query  .= " , con_content_level ";
		$query  .= " , con_write_level ";
		$query  .= " , con_reply_level ";
		$query	.= " , con_ip ";
		$query	.= " , con_date ";
		$query	.= " , ext_title_1 ";
		$query	.= " , ext_title_2 ";
		$query	.= " , ext_title_3 ";
		$query	.= " , ext_title_4 ";
		$query	.= " , ext_title_5 ";
		$query	.= " ) ";
		$query	.= " values ";
		$query	.= " ( ";
		$query	.= " " . $this->db->escape($array['member_id']) . " ";
		$query	.= " , " . $this->db->escape($array['con_title']) . " ";
		$query	.= " , " . $this->db->escape($array['con_name']) . " ";
		$query	.= " , " . $this->db->escape($array['con_skin']) . " ";
		$query	.= " , " . $this->db->escape($array['con_category']) . " ";
		$query	.= " , " . $this->db->escape($array['con_layout']) . " ";
		$query	.= " , " . $this->db->escape($array['con_thumb_filter']) . " ";
		$query	.= " , " . $this->db->escape($array['con_diff']) . " ";
		$query  .= " , " . $this->db->escape($array['con_list_level']) . " ";
		$query  .= " , " . $this->db->escape($array['con_content_level']) . " ";
		$query  .= " , " . $this->db->escape($array['con_write_level']) . " ";
		$query  .= " , " . $this->db->escape($array['con_reply_level']) . " ";
		$query	.= " , '" . $this->input->ip_address() . "' ";
		$query	.= " , now() ";
		$query  .= " , " . $this->db->escape($array['ext_title_1']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_title_2']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_title_3']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_title_4']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_title_5']) . " ";
		$query	.= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}


	public function SetBoardUpdate($array){	// 게시판 설정 수정 2019-05-02

		$query	= "";
		$query	.= " update ";
		$query	.= " admin_board ";
		$query	.= " set ";
		$query	.= " con_title = " . $this->db->escape($array['con_title']) . " ";
		$query	.= " , con_skin = " . $this->db->escape($array['con_skin']) . " ";
		$query	.= " , con_category = " . $this->db->escape($array['con_category']) . " ";
		$query	.= " , con_layout = " . $this->db->escape($array['con_layout']) . " ";
		$query	.= " , con_thumb_filter = " . $this->db->escape($array['con_thumb_filter']) . " ";
		$query	.= " , con_diff = " . $this->db->escape($array['con_diff']) . " ";
		$query  .= " , con_list_level = " . $this->db->escape($array['con_list_level']) . " ";
		$query  .= " , con_content_level = " . $this->db->escape($array['con_content_level']) . " ";
		$query  .= " , con_write_level = " . $this->db->escape($array['con_write_level']) . " ";
		$query  .= " , con_reply_level = " . $this->db->escape($array['con_reply_level']) . " ";
		$query  .= " , ext_title_1 = " . $this->db->escape($array['ext_title_1']) . " ";
		$query  .= " , ext_title_2 = " . $this->db->escape($array['ext_title_2']) . " ";
		$query  .= " , ext_title_3 = " . $this->db->escape($array['ext_title_3']) . " ";
		$query  .= " , ext_title_4 = " . $this->db->escape($array['ext_title_4']) . " ";
		$query  .= " , ext_title_5 = " . $this->db->escape($array['ext_title_5']) . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " con_name = " . $this->db->escape($array['con_name']) . " ";
		//echo $query;

		$this->db->query($query);

	}

	public function GetCustomBoardList($con_name, $search_type, $search_name, $page, $start, $end){	// 사용자단 게시판 목록 2019-05-30

		if(isset($page)){

			$num				= 1 + (($end * $page) - $end);

		} else {

			$num				= 1 + (($end * 1) - $end);

		}

		if(isset($page)){

			$id					= $page;
			$start				= ($id - 1) * $end;

		} else {

			$id					= 1;
			$start				= ($id - 1) * $end;

		}


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " " . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name = " . $this->db->escape($search_name) . " ";

		} else if($search_type == "bbs_title"){

			$query	.= " and ";
			$query	.= " bbs_title like '%" . $search_name . "%' ";

		} else if($search_type == "bbs_content"){

			$query	.= " and ";
			$query	.= " bbs_content like '%" . $search_name . "%' ";

		}

		$query	.= " order by bbs_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetCustomBoardCount($con_name, $search_type, $search_name){	// 사용자단 게시판 목록 2019-05-30


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " " . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name = " . $this->db->escape($search_name) . " ";

		} else if($search_type == "bbs_title"){

			$query	.= " and ";
			$query	.= " bbs_title like '%" . $search_name . "%' ";

		} else if($search_type == "bbs_content"){

			$query	.= " and ";
			$query	.= " bbs_content like '%" . $search_name . "%' ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetCustomCommentList($con_name, $search_type, $search_name, $page, $start, $end){	// 사용자단 댓글 목록 2019-06-03


		if(isset($page)){

			$num				= 1 + (($end * $page) - $end);

		} else {

			$num				= 1 + (($end * 1) - $end);

		}

		if(isset($page)){

			$id					= $page;
			$start				= ($id - 1) * $end;

		} else {

			$id					= 1;
			$start				= ($id - 1) * $end;

		}


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name = " . $this->db->escape($search_name) . " ";

		} else if($search_type == "rep_content"){

			$query	.= " and ";
			$query	.= " rep_content like '%" . $search_name . "%' ";

		}

		$query	.= " order by bbs_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetCustomCommentCount($con_name, $search_type, $search_name){	// 사용자단 댓글 목록 2019-06-03

		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name = " . $this->db->escape($search_name) . " ";

		} else if($search_type == "rep_content"){

			$query	.= " and ";
			$query	.= " rep_content like '%" . $search_name . "%' ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetCustomVotedList($con_name, $search_type, $search_name, $page, $start, $end){	// 사용자단 추천/비추천 목록 2019-06-03


		if(isset($page)){

			$num				= 1 + (($end * $page) - $end);

		} else {

			$num				= 1 + (($end * 1) - $end);

		}

		if(isset($page)){

			$id					= $page;
			$start				= ($id - 1) * $end;

		} else {

			$id					= 1;
			$start				= ($id - 1) * $end;

		}

		$query	= "";
		$query	.= " select ";
		$query	.= " T1.vo_no ";
		$query	.= " , T1.vo_like ";
		$query	.= " , T1.vo_dislike ";
		$query	.= " , T1.vo_ip ";
		$query	.= " , T1.vo_date ";
		$query	.= " , T2.bbs_no ";
		$query	.= " , T2.member_name ";
		$query	.= " , T2.bbs_title ";
		$query	.= " from ";
		$query	.= " vote_" . $con_name . " ";
		$query	.= " T1 left join " . $con_name . " T2 on (T1.bbs_no = T2.bbs_no) ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " order by vo_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetCustomVotedCount($con_name, $search_type, $search_name){	// 사용자단 추천/비추천 목록 2019-06-03

		$query	= "";
		$query	.= " select ";
		$query	.= " T1.vo_no ";
		$query	.= " , T1.vo_like ";
		$query	.= " , T1.vo_dislike ";
		$query	.= " , T1.vo_ip ";
		$query	.= " , T1.vo_date ";
		$query	.= " , T2.bbs_no ";
		$query	.= " , T2.member_name ";
		$query	.= " , T2.bbs_title ";
		$query	.= " from ";
		$query	.= " vote_" . $con_name . " ";
		$query	.= " T1 left join " . $con_name . " T2 on (T1.bbs_no = T2.bbs_no) ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetCustomVoted($con_name, $bbs_no){	// 사용자단 게시물 추천수 가져오기 2019-05-30

		
		$query	= "";
		$query	.= " select ";
		$query	.= " sum(vo_like) as sum_like ";
		$query	.= " , sum(vo_dislike) as sum_dislike ";
		$query	.= " from ";
		$query	.= " vote_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetCustomFilesList($con_name, $search_type, $search_name, $page, $start, $end){	// 사용자단 파일 목록 2019-06-07


		if(isset($page)){

			$num				= 1 + (($end * $page) - $end);

		} else {

			$num				= 1 + (($end * 1) - $end);

		}

		if(isset($page)){

			$id					= $page;
			$start				= ($id - 1) * $end;

		} else {

			$id					= 1;
			$start				= ($id - 1) * $end;

		}

		$query	= "";
		$query	.= " select ";
		$query	.= " T1.file_no ";
		$query	.= " , T1.file_encrypt_name ";
		$query	.= " , T1.file_real_name ";
		$query  .= " , T1.file_type ";
		$query	.= " , T1.file_ip ";
		$query	.= " , T1.file_date ";
		$query	.= " , T2.bbs_no ";
		$query	.= " , T2.member_name ";
		$query	.= " , T2.bbs_title ";
		$query	.= " from ";
		$query	.= " file_" . $con_name . " ";
		$query	.= " T1 left join " . $con_name . " T2 on (T1.bbs_no = T2.bbs_no) ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " order by file_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetCustomFilesCount($con_name, $search_type, $search_name){	// 사용자단 파일 2019-06-07

		$query	= "";
		$query	.= " select ";
		$query	.= " T1.file_no ";
		$query	.= " , T1.file_encrypt_name ";
		$query	.= " , T1.file_real_name ";
		$query  .= " , T1.file_type ";
		$query	.= " , T1.file_ip ";
		$query	.= " , T1.file_date ";
		$query	.= " , T2.bbs_no ";
		$query	.= " , T2.member_name ";
		$query	.= " , T2.bbs_title ";
		$query	.= " from ";
		$query	.= " file_" . $con_name . " ";
		$query	.= " T1 left join " . $con_name . " T2 on (T1.bbs_no = T2.bbs_no) ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function SetCommonCount($name, $column, $no){	// common result method 2019-06-04

		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " " . $name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " " . $column . " = " . $this->db->escape($no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function SetCommonDelete($name, $column, $no){	// common delete method 2019-06-04

		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " " . $name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " " . $column . " = " . $this->db->escape($no) . " ";
		//echo $query;

		$this->db->query($query);

	}

}