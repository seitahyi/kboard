<?php
if(!defined('BASEPATH')) exit;


class MemberModel extends CI_Model{



	public function __construct(){


		parent::__construct();


		$this->load->database();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');


	}


	public function SetSigninSuccess($array){	// 로그인 확인


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";
		$query	.= " and ";
		$query	.= " member_password = " . $this->db->escape($array['member_password']) . " ";
		$query	.= " and ";
		$query	.= " member_formal = '1' ";
		//echo $query;
	
		$result	= $this->db->query($query);
		
		return $result;


	}


	public function GetMemberCount($search_type, $search_name){


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "member_id"){

			$query	.= " and ";
			$query	.= " member_id like '%".$search_name."%' ";

		} else if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name like '%".$this->common->GetEncrypt($search_name, @$secret_key, @$secret_iv)."%' ";
		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;


	}


	public function GetMemberList($search_type, $search_name, $page, $start, $end){	// 회원 목록 2019-02-28


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "member_id"){

			$query	.= " and ";
			$query	.= " member_id like '%".$search_name."%' ";

		} else if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name like '%".$this->common->GetEncrypt($search_name, @$secret_key, @$secret_iv)."%' ";

		}

		$query	.= " order by member_no desc limit ".$start." , ".$end." ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}
	

	public function GetMember($member_id){	// 회원 조회 2019-03-19


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_member ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = ".$this->db->escape($member_id)." ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;


	}
	

	public function SetMemberUpdate($array){	// 회원 수정 2019-03-20


		$member_email		= $array['member_email_1'] . "@" . $array['member_email_2'];


		$query	= "";
		$query	.= " update ";
		$query	.= " admin_member ";
		$query	.= " set ";
		$query	.= " member_password = " . $this->db->escape($array['member_password']) . " ";
		$query	.= " , member_name = " . $this->db->escape($array['member_name']) . " ";
		$query	.= " , member_email = " . $this->db->escape($member_email) . " ";
		$query	.= " , member_level = " . $this->db->escape($array['member_level']) . " ";
		$query	.= " , member_profile = " . $this->db->escape($array['member_profile']) . " ";
		$query	.= " , member_formal = " . $this->db->escape($array['member_formal']) . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";
		//echo $query;

		$this->db->query($query);

	}
	

	public function SetMemberDelete($array){	// 회원 삭제 2019-03-21


		$query = "";
		$query .= " delete ";
		$query .= " from ";
		$query .= " admin_member ";
		$query .= " where ";
		$query .= " 1 = 1 ";
		$query .= " and ";
		$query .= " member_no = " . $this->db->escape($array['member_no']) . " ";
		$query .= " and ";
		$query .= " member_id = " . $this->db->escape($array['member_id']) . " ";
		$query .= " and ";
		$query .= " member_email = " . $this->db->escape($array['member_email']) . " ";
		//echo $query;

		$this->db->query($query);

	}
	

	public function SetMemberInsert($array){	// 회원 추가 2019-03-22


		$member_email	= $array['member_email_1'] . "@" . $array['member_email_2'];

		
		$query = "";
		$query .= " insert into ";
		$query .= " admin_member ";
		$query .= " ( ";
		$query .= " member_id ";
		$query .= " , member_password ";
		$query .= " , member_name ";
		$query .= " , member_email ";
		$query .= " , member_level ";
		$query .= " , member_profile ";
		$query .= " , member_formal ";
		$query .= " , member_signup_date ";
		$query .= " ) ";
		$query .= " values ";
		$query .= " ( ";
		$query .= " " . $this->db->escape($array['member_id']) . " ";
		$query .= " , " . $this->db->escape($array['member_password']) . " ";
		$query .= " , " . $this->db->escape($array['member_name']) . " ";
		$query .= " , " . $this->db->escape($member_email) . " ";
		$query .= " , " . $this->db->escape($array['member_level']) . " ";
		$query .= " , " . $this->db->escape($array['member_profile']) . " ";
		$query .= " , " . $this->db->escape($array['member_formal']) . " ";
		$query .= " , now() ";
		$query .= " ) ";
		//echo $query;

		$this->db->query($query);

	}
	
	
	
	public function CheckMemberPermission($perm_member_id, $NULL){		// 퍼미션 상태 2019-04-04
		
		$query = "";
		$query .= " select ";
		$query .= " * ";
		$query .= " from ";
		$query .= " admin_permission ";
		$query .= " where ";
		$query .= " 1 = 1 ";
		$query .= " and ";
		$query .= " perm_member_id = " . $this->db->escape($perm_member_id) . " ";
		$query .= " and ";
		$query .= " perm_member_id not in ";
		$query .= " ( ";
		$query .= " 'admin' ";
		$query .= " ) ";
		
		if(!empty($NULL)){
			
			$query .= " and ";
			$query .= " perm_page = " . $this->db->escape($NULL) . " ";
			
		}
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
	}
	
	
	
	public function SetMemberPermissionInsert($array){	// 퍼미션 추가 2019-04-05
		
		$query = "";
		$query .= " insert into ";
		$query .= " admin_permission ";
		$query .= " ( ";
		$query .= " member_id ";
		$query .= " , perm_member_id ";
		$query .= " , perm_page ";
		$query .= " , perm_name ";
		$query .= " , perm_date ";
		$query .= " ) ";
		$query .= " values ";
		$query .= " ( ";
		$query .= " " . $this->db->escape($array['member_id']) . " ";
		$query .= " , " . $this->db->escape($array['perm_member_id']) . " ";
		$query .= " , " . $this->db->escape($array['perm_page']) . " ";
		$query .= " , " . $this->db->escape($array['perm_name']) . " ";
		$query .= " , now() ";
		$query .= " ) ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	
	public function SetMemberPermissionDelete($array){	// 퍼미션 삭제 2019-04-10
		
		$query = "";
		$query .= " delete ";
		$query .= " from ";
		$query .= " admin_permission ";
		$query .= " where ";
		$query .= " 1 = 1 ";
		$query .= " and ";
		$query .= " member_id = " . $this->db->escape($array['member_id']) . " ";
		$query .= " and ";
		$query .= " perm_member_id = " . $this->db->escape($array['perm_member_id']) . " ";
		$query .= " and ";
		$query .= " perm_page = " . $this->db->escape($array['perm_page']) . " ";
		$query .= " and ";
		$query .= " perm_name = " . $this->db->escape($array['perm_name']) . " ";
		//echo $query;
		
		$this->db->query($query);
		
	}
	
	
	
	public function GetMemberPermissionCount($perm_member_id){
		
		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_permission ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";			
		$query	.= " and ";
		$query	.= " perm_member_id = " . $this->db->escape($perm_member_id) . " ";
		$query	.= " and ";
		$query	.= " perm_page = 'member_perm_ok' ";
		//echo $query;
		
		$result	= $this->db->query($query);
		
		return $result;
		
		
	}


	public function GetMemberGenPwCount($search_type, $search_name){	// 회원 비밀번호 발급 갯수 2019-07-04


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_lost_password ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "member_id"){

			$query	.= " and ";
			$query	.= " member_id like '%".$search_name."%' ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;


	}


	public function GetMemberGenPw($search_type, $search_name, $page, $start, $end){	// 회원 비밀번호 발급 목록 2019-07-04


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_lost_password ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "member_id"){

			$query	.= " and ";
			$query	.= " member_id like '%".$search_name."%' ";

		}

		$query	.= " order by lo_no desc limit ".$start." , ".$end." ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMemberGenTkCount(){	// 회원 토큰 발급 갯수 2019-07-04


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_sessions ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;


	}


	public function GetMemberGenTk($page, $start, $end){	// 회원 토큰 발급 목록 2019-07-04


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_sessions ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " order by no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetAdminBoardList(){	// 어드민에 설치 된 게시판 목록의 이름을 가져옴 2019-07-05

		$query	= "";
		$query	.= " select ";
		$query	.= " con_name ";
		$query	.= " from ";
		$query	.= " admin_board ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetMembersBoardList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		foreach($board_result as $no => $print){

			$query	.= " ( ";
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
			$query	.= " from ";
			$query	.= " " . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";
				$query	.= " union ";

			}

		}

		$query	.= " order by bbs_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersBoardCount($member_id, $board_count, $board_result){


		$query	= "";
		foreach($board_result as $no => $print){

			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " " . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				$query	.= " union ";

			}

		}

		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}


	public function GetMembersFileList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		foreach($board_result as $no => $print){

			$query	.= " ( ";
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
			$query	.= " from ";
			$query	.= " file_" . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";
				$query	.= " union ";

			}

		}

		$query	.= " order by file_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersFileCount($member_id, $board_count, $board_result){


		$query	= "";
		foreach($board_result as $no => $print){

			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " file_" . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				$query	.= " union ";

			}

		}

		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}


	public function GetMembersReplyList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		foreach($board_result as $no => $print){

			$query	.= " ( ";
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
			$query	.= " from ";
			$query	.= " reply_" . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";
				$query	.= " union ";

			}

		}

		$query	.= " order by rep_no desc limit " . $start . " , " . $end . " ";

		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersReplyCount($member_id, $board_count, $board_result){


		$query	= "";
		foreach($board_result as $no => $print){

			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " reply_" . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				$query	.= " union ";

			}

		}

		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}


	public function GetMembersVotedList($page, $start, $end, $member_id, $board_count, $board_result){


		if(isset($page)){

			$num			= 1 + (($end * $page) - $end);

		} else {

			$num			= 1 + (($end * 1) - $end);

		}


		if(isset($page)){

			$id		= $page;
			$start	= ($id - 1) * $end;

		} else {

			$id		= 1;
			$start	= ($id - 1) * $end;

		}


		$query		= "";
		foreach($board_result as $no => $print){

			$query	.= " ( ";
			$query	.= " select ";
			$query	.= " * ";
			$query	.= " , (select con_name from admin_board where 1 = 1 and con_name = " . $this->db->escape($print['con_name']) . ") as con_name ";
			$query	.= " from ";
			$query	.= " vote_" . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				//$query	.= " limit " . $start . " , " . $end . " ";
				$query	.= " ) ";
				$query	.= " union ";

			}

		}

		$query	.= " order by vo_no desc limit " . $start . " , " . $end . " ";

		//echo $query;

		$result			= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetMembersVotedCount($member_id, $board_count, $board_result){


		$query	= "";
		foreach($board_result as $no => $print){

			$query	.= " select ";
			$query	.= " * ";
			$query	.= " from ";
			$query	.= " vote_" . $print['con_name'] . " ";

			if($no % $board_count == $board_count - 1){

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";

			} else {

				$query	.= " where ";
				$query	.= " 1 = 1 ";
				$query	.= " and ";
				$query	.= " member_id = " . $this->db->escape($member_id) . " ";
				$query	.= " union ";

			}

		}

		//echo $query;

		$result			= $this->db->query($query);

		return $result;

	}


}