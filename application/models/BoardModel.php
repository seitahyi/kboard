<?php
if(!defined('BASEPATH')) exit;


class BoardModel extends CI_Model {


	public function __construct(){

		parent::__construct();

		$this->load->database();

		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('User');

	}


	public function GetBoardList($con_name, $search_type, $search_name, $page, $start, $end){	// 게시판 목록 2019-05-15


		if(isset($page)){

			$num				= 1 + (($end * $page) - $end);

		} else {

			$num				= 1 + (($end * 1) - $end);

		}

		if(isset($page)){

			$id					= $page;
			$start				= ($id - 1) * $end;

		} else {

			$id					= 1;
			$start				= ($id - 1) * $end;

		}


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " " . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name = " . $this->db->escape($search_name) . " ";

		} else if($search_type == "bbs_title"){

			$query	.= " and ";
			$query	.= " bbs_title like '%".$search_name."%' ";

		} else if($search_type == "bbs_content"){

			$query	.= " and ";
			$query	.= " bbs_content like '%".$search_name."%' ";

		}

		$query	.= " order by bbs_no desc limit " . $start . " , " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("num"=>$num, "id"=>$id, "result"=>$result);

	}


	public function GetBoardCount($con_name, $search_type, $search_name){	// 게시판 목록 갯수 2019-05-02


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " " . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if($search_type == "member_name"){

			$query	.= " and ";
			$query	.= " member_name = " . $this->db->escape($search_name) . " ";

		} else if($search_type == "bbs_title"){

			$query	.= " and ";
			$query	.= " bbs_title like '%".$search_name."%' ";

		} else if($search_type == "bbs_content"){

			$query	.= " and ";
			$query	.= " bbs_content like '%".$search_name."%' ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetBoardContent($con_name, $bbs_no){	// 게시판 본문 2019-05-16

		$query  = "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " " . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function SetContentDelete($con_name, $bbs_no, $member_id){	// 게시판 본문 삭제 2019-05-16


		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " " . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetLikeInsert($con_name, $bbs_no, $member_id){	// 게시물 추천 2019-05-16

		$query  = "";
		$query  .= " insert into ";
		$query	.= " vote_" . $con_name . " ";
		$query  .= " ( ";
		$query  .= " bbs_no ";
		$query  .= " , member_id ";
		$query  .= " , vo_like ";
		$query  .= " , vo_ip ";
		$query  .= " , vo_date ";
		$query  .= " ) ";
		$query  .= " values ";
		$query  .= " ( ";
		$query  .= " " . $this->db->escape($bbs_no) . " ";
		$query  .= " , " . $this->db->escape($member_id) . " ";
		$query  .= " , '1' ";
		$query  .= " , '" . $this->input->ip_address() . "' ";
		$query  .= " , now() ";
		$query  .= " ) ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetDislikeInsert($con_name, $bbs_no, $member_id){	// 게시물 비추천 2019-05-17

		$query  = "";
		$query  .= " insert into ";
		$query	.= " vote_" . $con_name . " ";
		$query  .= " ( ";
		$query  .= " bbs_no ";
		$query  .= " , member_id ";
		$query  .= " , vo_dislike ";
		$query  .= " , vo_ip ";
		$query  .= " , vo_date ";
		$query  .= " ) ";
		$query  .= " values ";
		$query  .= " ( ";
		$query  .= " " . $this->db->escape($bbs_no) . " ";
		$query  .= " , " . $this->db->escape($member_id) . " ";
		$query  .= " , '1' ";
		$query  .= " , '" . $this->input->ip_address() . "' ";
		$query  .= " , now() ";
		$query  .= " ) ";
		//echo $query;

		$this->db->query($query);

	}


	public function CheckVoted($con_name, $bbs_no, $member_id){	// 게시물 추천수 확인 2019-05-18

		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " vote_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetVoted($con_name, $bbs_no){	// 게시물 추천수 가져오기 2019-05-18

		
		$query	= "";
		$query	.= " select ";
		$query	.= " sum(vo_like) as sum_like ";
		$query	.= " , sum(vo_dislike) as sum_dislike ";
		$query	.= " from ";
		$query	.= " vote_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetVotedRow($con_name, $bbs_no){	// 게시물 추천수 확인 2019-05-23

		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " vote_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function SetVotedDelete($con_name, $bbs_no){	// 게시물의 추천수 삭제 2019-05-23


		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " vote_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetReadUpdate($con_name, $bbs_no, $count){	// 게시판 조회수 2019-05-17

		$query	= "";
		$query	.= " update ";
		$query	.= " " . $con_name . " ";
		$query	.= " set ";
		$query	.= " bbs_readed = '" . ($count+1) . "' ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetBoardInsert($con_name, $array){	// 게시물 작성 2019-05-20

		$query  = "";
		$query  .= " insert into ";
		$query  .= " " . $con_name . " ";
		$query  .= " ( ";
		$query  .= " member_id ";
		$query  .= " , member_name ";
		$query  .= " , bbs_title ";
		$query  .= " , bbs_category ";
		$query  .= " , bbs_content ";
		$query  .= " , bbs_secret ";
		$query  .= " , bbs_readed ";
		$query  .= " , bbs_ip ";
		$query  .= " , bbs_date ";
		$query  .= " , ext_desc_1 ";
		$query  .= " , ext_desc_2 ";
		$query  .= " , ext_desc_3 ";
		$query  .= " , ext_desc_4 ";
		$query  .= " , ext_desc_5 ";
		$query  .= " ) ";
		$query  .= " values ";
		$query  .= " ( ";
		$query  .= " " . $this->db->escape($array['member_id']) . " ";
		$query  .= " , " . $this->db->escape($array['member_name']) . " ";
		$query  .= " , " . $this->db->escape($array['bbs_title']) . " ";
		$query  .= " , " . $this->db->escape($array['bbs_category']) . " ";
		$query  .= " , " . $this->db->escape($array['bbs_content']) . " ";
		$query  .= " , " . $this->db->escape($array['bbs_secret']) . " ";
		$query  .= " , '0' ";
		$query  .= " , '" . $this->input->ip_address() . "' ";
		$query  .= " , now() ";
		$query  .= " , " . $this->db->escape($array['ext_desc_1']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_desc_2']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_desc_3']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_desc_4']) . " ";
		$query  .= " , " . $this->db->escape($array['ext_desc_5']) . " ";
		$query  .= " ) ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetBoardModify($con_name, $array){	// 게시물 수정 2019-05-17

		$query	= "";
		$query	.= " update ";
		$query	.= " " . $con_name . " ";
		$query	.= " set ";
		$query	.= " bbs_title = '" . ($array['bbs_title']) . "' ";
		$query	.= " , bbs_category = '" . ($array['bbs_category']) . "' ";
		$query	.= " , bbs_content = '" . ($array['bbs_content']) . "' ";
		$query  .= " , bbs_secret = '" . ($array['bbs_secret']) . "' ";
		$query	.= " , bbs_ip = '" . $this->input->ip_address() . "' ";
		$query	.= " , bbs_date = now() ";
		$query  .= " , ext_desc_1 = '" . ($array['ext_desc_1']) . "' ";
		$query  .= " , ext_desc_2 = '" . ($array['ext_desc_2']) . "' ";
		$query  .= " , ext_desc_3 = '" . ($array['ext_desc_3']) . "' ";
		$query  .= " , ext_desc_4 = '" . ($array['ext_desc_4']) . "' ";
		$query  .= " , ext_desc_5 = '" . ($array['ext_desc_5']) . "' ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($array['bbs_no']) . " ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($array['member_id']) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function GetParentCommentMaxCount($con_name, $bbs_no){	// 댓글 부모 갯수 2019-05-24

		$query	= "";
		$query	.= " select ";
		$query	.= " max(rep_parent_no) as max_parent ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetChildCommentMaxCount($con_name, $bbs_no, $parent_no){	// 댓글 부모 갯수 2019-05-24

		$query	= "";
		$query	.= " select ";
		$query	.= " max(rep_child_no) as max_child ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " and ";
		$query	.= " rep_parent_no = " . $this->db->escape($parent_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function SetCommentInsert($con_name, $parent_no, $child_no, $bbs_no, $member_id, $member_name, $rep_content){	// 댓글 인서트 2019-05-24

		$query = "";
		$query .= " insert into ";
		$query	.= " reply_" . $con_name . " ";
		$query .= " ( ";
		$query .= " rep_parent_no ";
		$query .= " , rep_child_no ";
		$query .= " , bbs_no ";
		$query .= " , member_id ";
		$query .= " , member_name ";
		$query .= " , rep_content ";
		$query .= " , rep_ip ";
		$query .= " , rep_date ";
		$query .= " ) ";
		$query .= " values ";
		$query .= " ( ";

		if(!empty($child_no)){	// 댓글에 댓글을 쓸때

			$query .= " " . $this->db->escape($parent_no) . " ";
			$query .= " , " . $this->db->escape($child_no) . " ";

		} else {	// 댓글만 쓸때

			$query .= " " . $this->db->escape($parent_no) . " ";
			$query .= " , '0' ";

		}

		$query .= " , " . $this->db->escape($bbs_no) . " ";
		$query .= " , " . $this->db->escape($member_id) . " ";
		$query .= " , " . $this->db->escape($member_name) . " ";
		$query .= " , " . $this->db->escape($rep_content) . " ";
		$query .= " , '" . $this->input->ip_address() . "' ";
		$query .= " , now() ";
		$query .= " ) ";
		//echo $child_no . "----" . $query;

		$this->db->query($query); 

	}


	public function SetCommentModify($con_name, $rep_no, $bbs_no, $member_id, $rep_content){	// 댓글 수정 2019-05-26

		$query	= "";
		$query	.= " update ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " set ";
		$query	.= " rep_content = " . $this->db->escape($rep_content) . " ";
		$query	.= " , rep_ip = '" . $this->input->ip_address() . "' ";
		$query	.= " , rep_date = now() ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " rep_no = " . $this->db->escape($rep_no) . " ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function SetCommentRemove($con_name, $rep_no, $bbs_no, $member_id){	// 댓글 삭제 2019-05-26

		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " rep_no = " . $this->db->escape($rep_no) . " ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function GetCommentList($con_name, $bbs_no, $page, $start, $end){	// 댓글 목록 2019-05-24


		if(isset($page)){

			$num				= 1 + (($end * $page) - $end);

		} else {

			$num				= 1 + (($end * 1) - $end);

		}

		if(isset($page)){

			$id					= $page;
			$start				= ($id - 1) * $end;

		} else {

			$id					= 1;
			$start				= ($id - 1) * $end;

		}


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " order by rep_parent_no, rep_child_no limit " . $start . ", " . $end . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return array("reply_num"=>$num, "reply_id"=>$id, "reply_result"=>$result);

	}


	public function GetCommentCount($con_name, $bbs_no){	// 특정 게시물의 댓글 갯수 2019-05-28

		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function GetParentCommentCount($con_name, $bbs_no, $parent_no){	// 특정 댓글의 댓글 갯수 2019-06-13

		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " and ";
		$query	.= " rep_parent_no = " . $this->db->escape($parent_no) . " ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function SetFilesInsert($con_name, $bbs_no, $member_id, $file_encrypt_name, $file_real_name, $file_width, $file_height, $file_type){	// 첨부파일 인서트 2019-05-22

		$query = "";
		$query	.= " insert into ";
		$query	.= " file_" . $con_name . " ";
		$query	.= " ( ";
		$query	.= " bbs_no ";
		$query	.= " , member_id ";
		$query	.= " , file_encrypt_name ";
		$query	.= " , file_real_name ";
		$query	.= " , file_width ";
		$query	.= " , file_height ";
		$query	.= " , file_type ";
		$query	.= " , file_ip ";
		$query	.= " , file_date ";
		$query	.= " ) ";
		$query	.= " values ";
		$query	.= " ( ";
		$query	.= " " . $this->db->escape($bbs_no) . " ";
		$query	.= " , " . $this->db->escape($member_id) . " ";
		$query	.= " , " . $this->db->escape($file_encrypt_name) . " ";
		$query	.= " , " . $this->db->escape($file_real_name) . " ";
		$query	.= " , " . $file_width . " ";
		$query	.= " , " . $file_height . " ";
		$query	.= " , " . $this->db->escape($file_type) . " ";
		$query	.= " , '" . $this->input->ip_address() . "' ";
		$query	.= " , now() ";
		$query	.= " ) ";
		//echo $query;

		$this->db->query($query);

	}


	public function GetBoardFiles($con_name, $no, $file){	// 게시물 파일들 가져오기 2019-05-22

		
		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " file_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($no) . " ";

		if(!empty($file)){

			$query	.= " and ";
			$query	.= " file_no = " . $this->db->escape($file) . " ";

		}
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function SetFileDelete($con_name, $file, $bbs_no, $member_id){	// 게시물의 파일 삭제 2019-05-23


		$query	= "";
		$query	.= " delete ";
		$query	.= " from ";
		$query	.= " file_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " file_no = " . $this->db->escape($file) . " ";
		$query	.= " and ";
		$query	.= " bbs_no = " . $this->db->escape($bbs_no) . " ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		//echo $query;

		$this->db->query($query);

	}


	public function CheckContDiff($con_name, $member_id){	// 게시물 작성 가능 시간차를 구한다 2019-08-16

		
		$query	= "";
		$query	.= " select ";
		$query	.= " bbs_no ";
		$query	.= " , member_id ";
		$query	.= " , bbs_date ";
		$query	.= " from ";
		$query	.= " " . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		$query	.= " order by bbs_no desc ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}


	public function CheckComDiff($con_name, $member_id){	// 댓글 작성 가능 시간차를 구한다 2019-08-20

		
		$query	= "";
		$query	.= " select ";
		$query	.= " rep_no ";
		$query	.= " , member_id ";
		$query	.= " , rep_date ";
		$query	.= " from ";
		$query	.= " reply_" . $con_name . " ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " member_id = " . $this->db->escape($member_id) . " ";
		$query	.= " order by rep_no desc ";
		//echo $query;

		$result	= $this->db->query($query);

		return $result;

	}



}