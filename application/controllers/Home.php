<?php
if(!defined('BASEPATH')) exit;


class Home extends CI_Controller {


	public function __construct(){

		parent::__construct();

		$this->load->database();

		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('User');

		$this->load->helper(array('form', 'url'));

		$this->load->model('HomeModel');

	}

	

	public function index(){


		$array['get_parameter']			= $this->user->GetParameter();

		
		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']			= $this->user->GetConfig();
		$array['config_count']			= $array['get_config']['config']->num_rows();
		$array['config']			    = $array['get_config']['config']->row_array();
		$array['company_count']			= $array['get_config']['company']->num_rows();
		$array['company']			    = $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/


		$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
		$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HomeView");
		$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);

	}
	
	
	public function page_con(){
		
		
		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']			= $this->user->GetConfig();
		$array['config_count']			= $array['get_config']['config']->num_rows();
		$array['config']			    = $array['get_config']['config']->row_array();
		$array['company_count']			= $array['get_config']['company']->num_rows();
		$array['company']			    = $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/
		
		
		/** 페이지 본문 시작 **/
		$rows['get_page_content']       = $this->HomeModel->GetPageContent($array['get_parameter']['pager_array']['request_page_name']);
		$rows['page_count']             = $rows['get_page_content']->num_rows();
		$rows['page_print']             = $rows['get_page_content']->row_array();
		/** 페이지 본문 끝 **/
		
		
		if($rows['page_count'] >= 1) {
			
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/layouts/" . $rows['page_print']['page_layout'] . "/PageContentView", $rows);
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
			
		} else {
			
			echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST);
			
		}
		
	}


}