<?php
if(!defined('BASEPATH')) exit;


class Member extends CI_Controller {


	public function __construct(){

		parent::__construct();

		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('User');

		$this->load->helper(array('form', 'url'));

		$this->load->model('MemberModel');

	}


	public function index(){	// member 인덱스는 쓰지 않는다 오류가 나는 것을 방지하기 위해 메서드 추가 2019-05-13

		echo $this->common->move_error(KO_WRONG, "http://" . HTTP_HOST);

	}



	public function signup(){	// 회원가입 2019-05-13


		$array['get_parameter']			= $this->user->GetParameter();


		if(empty($array['get_parameter']['session_array']['sess_member_id'])){


			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']			= $this->user->GetConfig();
			$array['config_count']			= $array['get_config']['config']->num_rows();
			$array['config']    			= $array['get_config']['config']->row_array();
			$array['company_count']			= $array['get_config']['company']->num_rows();
			$array['company']   			= $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/


			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/SignupView");
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);

		} else {

			echo $this->common->move_error(KO_LOGGED, PROTOCOLS . HTTP_HOST);

		}

	}


	
	public function signup_ok(){	// 회원가입 성공 2019-05-13


		$array['get_parameter']			= $this->user->GetParameter();

		if(empty($array['get_parameter']['session_array']['sess_member_id'])){

			$parameters						=	array(
													"member_id"=>strtolower(@$array['get_parameter']['member_array']['request_member_id'])
													, "member_password"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_password'], @$secret_key, @$secret_iv)
													, "member_name"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_name'], @$secret_key, @$secret_iv)
													, "member_email_1"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_1'], @$secret_key, @$secret_iv)
													, "member_email_2"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_2'], @$secret_key, @$secret_iv)
													, "member_level"=>@$array['get_parameter']['member_array']['member_level']
													, "member_profile"=>@$array['get_parameter']['member_array']['request_member_profile']
													, "member_formal"=>@$array['get_parameter']['member_array']['request_member_formal']
													, "check_agree"=>@$array['get_parameter']['member_array']['request_check_agree']
												);


			if($array['get_parameter']['member_array']['request_check_agree'] == "on"){

				$result					= $this->MemberModel->CheckMemberDuplicate($parameters);
				$count					= $result->num_rows();

				if($count <= 0){

					$this->MemberModel->SetSignupInsert($parameters);

					$rows				= $this->db->affected_rows();


					if($rows >= 1){

						echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST);

					} else {

						echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/member/signup");

					}


				} else {

					echo $this->common->move_error(KO_DUPLICATED, PROTOCOLS . HTTP_HOST . "/member/signup");

				}

			} else {

				echo $this->common->move_error(KO_INVALID, PROTOCOLS . HTTP_HOST . "/member/signup");

			}

		} else {

			echo $this->common->move_error(KO_LOGGED, PROTOCOLS . HTTP_HOST);

		}

	}


    public function check_id(){     // 회원 아이디 중복 체크 2019-09-06


        $array['get_parameter']			= $this->user->GetParameter();


        /** 기본환경설정 및 회사정보 시작 **/
        $array['get_config']		= $this->user->GetConfig();
        $array['config_count']		= $array['get_config']['config']->num_rows();
        $array['config']    		= $array['get_config']['config']->row_array();
        $array['company_count']		= $array['get_config']['company']->num_rows();
        $array['company']   		= $array['get_config']['company']->row_array();
        /** 기본환경설정 및 회사정보 끝 **/


        $blocks                     = explode(",", trim($array['config']['home_id_blocks']));


        for($i = 0; $i < count($blocks); $i++){

            $arr[]    = $blocks[$i];

        }


        $parameters				        =	array(
                                                "member_id"=>strtolower(@$array['get_parameter']['member_array']['request_member_id'])
                                            );


        $result                         = $this->MemberModel->CheckDuplicatedID($parameters);
        $count                          = $result->num_rows();
        $print                          = $result->row_array();


        if(in_array($array['get_parameter']['member_array']['request_member_id'], $arr)){

            $call_back['return_code']   = "9998";

        } else {

            if($count >= 1){

                $call_back['return_code']   = "9999";

            } else {

                $call_back['return_code']   = "0000";

            }

        }


        echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴

    }


    public function check_name(){     // 회원 이름 중복 체크 2019-09-06

        $array['get_parameter'] 		= $this->user->GetParameter();


        /** 기본환경설정 및 회사정보 시작 **/
        $array['get_config']		= $this->user->GetConfig();
        $array['config_count']		= $array['get_config']['config']->num_rows();
        $array['config']    		= $array['get_config']['config']->row_array();
        $array['company_count']		= $array['get_config']['company']->num_rows();
        $array['company']   		= $array['get_config']['company']->row_array();
        /** 기본환경설정 및 회사정보 끝 **/


        $blocks                     = explode(",", trim($array['config']['home_name_blocks']));


        for($i = 0; $i < count($blocks); $i++){

            $arr[]    = $blocks[$i];

        }


        $parameters				        =	array(
                                                "member_name"=>@$array['get_parameter']['member_array']['request_member_name']
                                            );


        $result                          = $this->MemberModel->CheckDuplicatedName($parameters);
        $count                           = $result->num_rows();
        $print                           = $result->row_array();


        if(in_array($array['get_parameter']['member_array']['request_member_name'], $arr)){

            $call_back['return_code']     = "9998";

        } else {

            if ($count >= 1) {

                $call_back['return_code'] = "9999";

            } else {

                $call_back['return_code'] = "0000";

            }

        }

        echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴

    }


    public function check_email(){     // 회원 이메일 중복 체크 2019-09-06

        $array['get_parameter'] 		= $this->user->GetParameter();

        $parameters					    =	array(
                                                "member_email_1"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_1'], @$secret_key, @$secret_iv)
                                                , "member_email_2"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_2'], @$secret_key, @$secret_iv)
                                            );

        $result                          = $this->MemberModel->CheckDuplicatedEmail($parameters);
        $count                           = $result->num_rows();
        $print                           = $result->row_array();

        if($count >= 1){

            $call_back['return_code']   = "9999";

        } else {

            $call_back['return_code']   = "0000";

        }

        echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴

    }


	public function signin(){	// 로그인 2019-05-13


		$array['get_parameter']			= $this->user->GetParameter();


		if(empty($array['get_parameter']['session_array']['sess_member_id'])){


			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']    		= $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']   		= $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/


			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/SigninView");
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);

		} else {

			echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST);

		}

	}



	public function signin_ok(){	// 로그인 성공 2019-05-13


		$array['get_parameter']			= $this->user->GetParameter();


		if(empty($array['get_parameter']['session_array']['sess_member_id'])){


			$parameters					=	array(
												"member_id"=>strtolower(@$array['get_parameter']['member_array']['request_member_id'])
												, "member_password"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_password'], @$secret_key, @$secret_iv)
											);


			$result						= $this->MemberModel->CheckMemberDuplicate($parameters);
			$count						= $result->num_rows();


			if($count >= 1){

				$member					= $result->row_array();
				
				if($member['member_formal'] == 1) {
					
					$sess_member_id         = $this->session->set_userdata('member_id', $member['member_id']);
					$sess_member_name       = $this->session->set_userdata('member_name', $member['member_name']);
					$sess_member_email      = $this->session->set_userdata('member_email', $member['member_email']);
					$sess_member_level      = $this->session->set_userdata('member_level', $member['member_level']);
					$sess_member_profile    = $this->session->set_userdata('member_profile', $member['member_profile']);
					$sess_member_formal     = $this->session->set_userdata('member_formal', $member['member_formal']);
					
					echo $this->common->move_error(KO_LOGGED, PROTOCOLS . HTTP_HOST);
					
				} else {
					
					echo $this->common->move_error(KO_CANTLOGIN, PROTOCOLS . HTTP_HOST);
					
				}

			} else {

				echo $this->common->move_error(KO_CANTLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");

			}


		} else {

			echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST);
		}

	}



	public function signout(){		// signout method 2019-05-13


		$array['get_parameter']			= $this->user->GetParameter();


		if(isset($array['get_parameter']['session_array']['sess_member_id'])){	// 여기서는 isset 을 쓴다. 로그인 여부 및 권한 체크 2019-02-08


			$this->session->sess_destroy();

			echo $this->common->move_error(KO_LOGOUT, PROTOCOLS . HTTP_HOST);


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");


		}


	}



	public function modify(){	// 회원수정 2019-05-13

		$array['get_parameter']			= $this->user->GetParameter();


		if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){


			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']    		= $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']		    = $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/


			$parameters					=	array(
												"member_id"=>$array['get_parameter']['session_array']['sess_member_id']
											);


			$rows['get_member']			= $this->MemberModel->GetMember($parameters);
			$rows['member_count']		= $rows['get_member']->num_rows();
			$rows['member_print']		= $rows['get_member']->row_array();


			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/ModifyView", $rows);
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");

		}

	}



	public function modify_ok(){	// 회원수정 완료 2019-05-13


		$array['get_parameter']			= $this->user->GetParameter();
		

		if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){


			$parameters					=	array(
												"member_id"=>@$array['get_parameter']['session_array']['sess_member_id']
											);


			$rows['get_member']			= $this->MemberModel->GetMember($parameters);
			$count						= $rows['get_member']->num_rows();
			$print						= $rows['get_member']->row_array();


			if($count >= 1){

				$parameters				=	array(
												"member_id"=>$print['member_id']
												, "member_password"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_password'], @$secret_key, @$secret_iv)
												, "member_name"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_name'], @$secret_key, @$secret_iv)
												, "member_email"=>$print['member_email']
												, "member_profile"=>$array['get_parameter']['member_array']['request_member_profile']
											);


				$this->MemberModel->SetMemberUpdate($parameters);
				
				$rows					= $this->db->affected_rows();


				if($rows >= 1){

					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/member/modify/" . $array['get_parameter']['session_array']['sess_member_id']);

				} else {

					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/member/modify/" . $array['get_parameter']['session_array']['sess_member_id']);

				}


			} else {

				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/member/modify/" . $array['get_parameter']['session_array']['sess_member_id']);

			}

		} else {

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST);

		}

	}



	public function drop(){		// 회원 탈퇴 2019-05-14


		$array['get_parameter']			= $this->user->GetParameter();


		if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){	// 본인만 가능 2019-05-13


			/**
			ModifyView 에서 선언한
			member_id 와 member_id 의 변수가 각각 따로 선언 된 이유는 페이크 검증용.
			**/


			$parameters					=	array(
												"member_id"=>@$array['get_parameter']['session_array']['sess_member_id']
											);


			$rows['get_member']			= $this->MemberModel->GetMember($parameters);
			$count						= $rows['get_member']->num_rows();
			$print						= $rows['get_member']->row_array();


			if($count >= 1){

				$this->MemberModel->SetMemberDelete($print['member_id'], $print['member_email']);
				$rows					= $this->db->affected_rows();

				if($rows >= 1){

					$this->session->sess_destroy();

					$call_back['return_code']	= "0000";

				} else {

					$call_back['return_code']	= "9999";

				}

				echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴


			} else {

				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/member/modify/" . $array['get_parameter']['session_array']['sess_member_id']);

			}


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");


		}


	}


	public function lost_id(){	// 아이디 찾기 2019-06-10


		$array['get_parameter']		= $this->user->GetParameter();

		
		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']		= $this->user->GetConfig();
		$array['config_count']		= $array['get_config']['config']->num_rows();
		$array['config']    		= $array['get_config']['config']->row_array();
		$array['company_count']		= $array['get_config']['company']->num_rows();
		$array['company']		    = $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/
		
		
		if(empty($array['get_parameter']['session_array']['sess_member_id'])) {
			
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/ForgotIDView");
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
			
		} else {
			
			echo $this->common->move_error("이미  " . KO_LOGGED, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function lost_id_ok(){	// 아이디 찾기 2019-06-10


		$array['get_parameter']			= $this->user->GetParameter();
		
		
		if(empty($array['get_parameter']['session_array']['sess_member_id'])) {

		
			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']    		= $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']   		= $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/
	
	
			$parameters					=	array(
												"member_email_1"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_1'], @$secret_key, @$secret_iv)
												, "member_email_2"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_2'], @$secret_key, @$secret_iv)
											);
	
	
			$rows['check_member']		= $this->MemberModel->CheckMemberAccounts($parameters);
			$rows['member_count']		= $rows['check_member']->num_rows();
			$rows['member_print']		= $rows['check_member']->row_array();
	
	
			if($rows['member_count'] >= 1){
	
	
				$rows['set_explode_email']	= explode("@", $rows['member_print']['member_email']);
	
	
				$rows['set_id']				= substr($rows['member_print']['member_id'], 0, -2);
				$rows['set_email']			= substr($this->common->GetDecrypt($rows['set_explode_email']['0'], @$secret_key, @$secret_iv), 0, -2);
				
				
				$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
				$this->load->view("skin/members/" . $array['config']['member_skin'] . "/FoundIDView", $rows);
				$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
	
	
			} else {
	
				echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST . "/member/lost_id");
	
			}
		
		
		} else {
		
			echo $this->common->move_error("이미  " . KO_LOGGED, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function lost_password(){	// 비밀번호 찾기 2019-06-10


		$array['get_parameter']			= $this->user->GetParameter();

		
		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']		= $this->user->GetConfig();
		$array['config_count']		= $array['get_config']['config']->num_rows();
		$array['config']		    = $array['get_config']['config']->row_array();
		$array['company_count']		= $array['get_config']['company']->num_rows();
		$array['company']		    = $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/
		
		
		if(!empty($array['config']['home_email'])) {
			
			if(empty($array['get_parameter']['session_array']['sess_member_id'])) {
				
				$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
				$this->load->view("skin/members/" . $array['config']['member_skin'] . "/ForgotPasswordView");
				$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
				
			} else {
				
				echo $this->common->move_error("이미  " . KO_LOGGED, PROTOCOLS . HTTP_HOST);
				
			}
			
		} else {
			
			echo $this->common->move_error("SMTP 계정이 없습니다.", PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function lost_password_ok(){	// 비밀번호 찾기 2019-06-10


		$array['get_parameter']			= $this->user->GetParameter();
		
		
		if(empty($array['get_parameter']['session_array']['sess_member_id'])) {

		
			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']		    = $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']		    = $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/
	
	
			$parameters					=	array(
												"member_id"=>$array['get_parameter']['member_array']['request_member_id']
												, "member_email_1"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_1'], @$secret_key, @$secret_iv)
												, "member_email_2"=>$this->common->GetEncrypt(@$array['get_parameter']['member_array']['request_member_email_2'], @$secret_key, @$secret_iv)
											);
	
	
			$rows['check_member']		= $this->MemberModel->CheckMemberAccounts($parameters);
			$rows['member_count']		= $rows['check_member']->num_rows();
			$rows['member_print']		= $rows['check_member']->row_array();
			
			
			if(!empty($array['config']['home_email'])){
				
				
				if($rows['member_count'] >= 1) {
					
					
					/** 메일 보내기 메서드 시작 **/
					$set_token = $this->common->SetIntToken();
					
					
					$rows['get_explode_email'] = explode("@", $array['config']['home_email']);
					$rows['set_explode_email'] = explode("@", $rows['member_print']['member_email']);
					
					
					$to = $this->common->GetDecrypt($rows['get_explode_email']['0'], @$secret_key, @$secret_iv) . "@" . $this->common->GetDecrypt($rows['get_explode_email']['1'], @$secret_key, @$secret_iv);
					
					$password = $this->common->GetDecrypt($array['config']['home_email_password'], @$secret_key, @$secret_iv);
					
					$from = $this->common->GetDecrypt($rows['set_explode_email']['0'], @$secret_key, @$secret_iv) . "@" . $this->common->GetDecrypt($rows['set_explode_email']['1'], @$secret_key, @$secret_iv);
					
					$host = $this->common->GetDecrypt($array['config']['home_email_host'], @$secret_key, @$secret_iv);
					
					
					$body = "";
					$body .= "<b>비밀번호 : </b>" . $set_token . "<br>";
					$body .= "<b>링    크 : </b>" . "<a href=\"http://" . HTTP_HOST . "/member/signin/\" target=\"_blank\">" . PROTOCOLS . HTTP_HOST . "/member/signin/" . "</a>";
					
					
					$this->common->SendEmail($to, $password, $host, $from, $array['config']['home_title'] . " 임시 비밀번호", $body);
					/** 메일 보내기 메서드 끝 **/
					
					
					$parameters = array("member_id" => $rows['member_print']['member_id'], "member_email" => $rows['member_print']['member_email'], "member_password" => $this->common->GetEncrypt($set_token, @$secret_key, @$secret_iv), "member_name" => $rows['member_print']['member_name'], "member_profile" => $rows['member_print']['member_profile']);
					
					$this->MemberModel->SetMemberUpdate($parameters);      // 비밀번호 변경 메서드
					$this->MemberModel->SetPasswordUpdate($parameters);    // 비밀번호 변경 로그 메서드
					
					
					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/member/lost_password");
					
				} else {
					
					echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST . "/member/lost_password");
					
				}
			
			} else {
	
				echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST . "/member/lost_password");
	
			}
			
		} else {
			
			echo $this->common->move_error("이미  " . KO_LOGGED, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function board_list(){	// 글모음 2019-07-11

		$array['get_parameter']			= $this->user->GetParameter();


		if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){


			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']		    = $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']   		= $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/


			$rows['get_admin_board_list']		= $this->MemberModel->GetInstalledBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();

			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['get_parameter']['search_array']['page'];

			$rows['get_member_board_list']		= $this->MemberModel->GetMembersBoardList($rows['page'], $rows['start'], $rows['end'], $array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_board_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_board_list']['result']->result_array();

			$rows['num']						= $rows['get_member_board_list']['num'];
			$rows['id']							= $rows['get_member_board_list']['id'];

			$rows['get_members_board_count']	= $this->MemberModel->GetMembersBoardCount($array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/BoardListView", $rows);
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");

		}

	}


	public function file_list(){	// 첨부파일모음 2019-07-12

		$array['get_parameter']			= $this->user->GetParameter();


		if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){


			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']    		= $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']   		= $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/


			$rows['get_admin_board_list']		= $this->MemberModel->GetInstalledBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();

			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['get_parameter']['search_array']['page'];

			$rows['get_member_file_list']		= $this->MemberModel->GetMembersFileList($rows['page'], $rows['start'], $rows['end'], $array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_file_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_file_list']['result']->result_array();

			$rows['num']						= $rows['get_member_file_list']['num'];
			$rows['id']							= $rows['get_member_file_list']['id'];

			$rows['get_members_file_count']	= $this->MemberModel->GetMembersFileCount($array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/FileListView", $rows);
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");

		}

	}


	public function reply_list(){	// 댓글모음 2019-07-12
		

		$array['get_parameter']			= $this->user->GetParameter();

		if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){


			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']		    = $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']		    = $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/


			$rows['get_admin_board_list']		= $this->MemberModel->GetInstalledBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();

			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['get_parameter']['search_array']['page'];

			$rows['get_member_reply_list']		= $this->MemberModel->GetMembersReplyList($rows['page'], $rows['start'], $rows['end'], $array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_reply_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_reply_list']['result']->result_array();

			$rows['num']						= $rows['get_member_reply_list']['num'];
			$rows['id']							= $rows['get_member_reply_list']['id'];

			$rows['get_members_reply_count']	= $this->MemberModel->GetMembersReplyCount($array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/ReplyListView", $rows);
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");

		}

	}


	public function voted_list(){	// 추천/비추천 모음 2019-07-12

		$array['get_parameter']			= $this->user->GetParameter();


		if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){


			/** 기본환경설정 및 회사정보 시작 **/
			$array['get_config']		= $this->user->GetConfig();
			$array['config_count']		= $array['get_config']['config']->num_rows();
			$array['config']    		= $array['get_config']['config']->row_array();
			$array['company_count']		= $array['get_config']['company']->num_rows();
			$array['company']   		= $array['get_config']['company']->row_array();
			/** 기본환경설정 및 회사정보 끝 **/


			$rows['get_admin_board_list']		= $this->MemberModel->GetInstalledBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();

			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['get_parameter']['search_array']['page'];

			$rows['get_member_voted_list']		= $this->MemberModel->GetMembersVotedList($rows['page'], $rows['start'], $rows['end'], $array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_voted_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_voted_list']['result']->result_array();

			$rows['num']						= $rows['get_member_voted_list']['num'];
			$rows['id']							= $rows['get_member_voted_list']['id'];

			$rows['get_members_voted_count']	= $this->MemberModel->GetMembersVotedCount($array['get_parameter']['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
			$this->load->view("skin/members/" . $array['config']['member_skin'] . "/VotedListView", $rows);
			$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/member/signin");

		}

	}


}