<?php
if(!defined('BASEPATH')) exit;



class Library extends CI_Controller {


	
	public function __construct(){


		parent::__construct();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');


		$this->load->helper(array('form', 'url', 'download'));


		$this->load->model('dashboard/BoardModel');
        $this->load->model('dashboard/HomeModel');


	}


	public function lib_list(){	// 라이브러리 목록 2020-03-09

		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-05-03


		    /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$rows['get_curl']               = $this->admin->GetCURL("http://promotion.korwn.com/support/req_library.json");
			
			$rows['set_json']               = json_decode($rows['get_curl'], true);
			

			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/LibraryListView', $rows);
			$this->load->view('dashboard/FooterView');

		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}

	}

}