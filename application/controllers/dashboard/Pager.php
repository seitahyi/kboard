<?php
if(!defined('BASEPATH')) exit;


class Pager extends CI_Controller {



	public function __construct(){


		parent::__construct();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');


		$this->load->helper(array('form', 'url'));


		$this->load->model('dashboard/PagerModel');
		$this->load->model('dashboard/HomeModel');


	}



	public function menus(){	// 메뉴 설정 2019-02-21


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$rows['get_menu']			= $this->PagerModel->GetMenu();
			$rows['menu_count']			= $rows['get_menu']->num_rows();
			$rows['menu']			    = $rows['get_menu']->result_array();


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MenusView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}
	
	
	
	public function menus_gd_add(){	// 1차메뉴 추가 2019-04-17
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$parameters				=	array(
											"menu_grandparent"=>$array['menu_array']['post_menu_grandparent']
											, "menu_parent"=>$array['menu_array']['post_menu_parent']
											, "menu_child"=>$array['menu_array']['post_menu_child']
											, "menu_name"=>$array['menu_array']['post_menu_name']
											, "menu_url"=>$array['menu_array']['post_menu_url']
											, "menu_target"=>$array['menu_array']['post_menu_target']
										);
			
			
			$rows['check_menu']			= $this->PagerModel->CheckMenu($parameters);
			$count						= $rows['check_menu']->num_rows();
			$print						= $rows['check_menu']->row_array();
			
			
			if($count <= 0){
				
				$this->PagerModel->SetGDMenuInsert($parameters);
				
				$rows					= $this->db->affected_rows();
				
				if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
					
					$call_back['return_code']	= "0000";
					
				} else {	// 문제가 있을때
					
					$call_back['return_code']	= "9999";
					
				}
				
			} else {	// 1개 이상일 때
				
				$call_back['return_code']	= "9998";
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else {
			
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
			
		}
		
		
	}
	
	
	
	public function menus_pa_add(){	// 2차메뉴 추가 2019-04-17
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$parameters					=	array(
												"menu_grandparent"=>$array['menu_array']['post_menu_grandparent']
												, "menu_parent"=>$array['menu_array']['post_menu_parent']
												, "menu_child"=>$array['menu_array']['post_menu_child']
												, "menu_name"=>$array['menu_array']['post_menu_name']
												, "menu_url"=>$array['menu_array']['post_menu_url']
												, "menu_target"=>$array['menu_array']['post_menu_target']
											);
			
			
			$rows['check_menu']			= $this->PagerModel->CheckMenu($parameters);
			$count						= $rows['check_menu']->num_rows();
			$print						= $rows['check_menu']->row_array();
			
			
			if($count <= 0){
				
				$this->PagerModel->SetGDMenuInsert($parameters);
				
				$rows					= $this->db->affected_rows();
				
				if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
					
					$call_back['return_code']	= "0000";
					
				} else {	// 문제가 있을때
					
					$call_back['return_code']	= "9999";
					
				}
				
			} else {	// 1개 이상일 때
				
				$call_back['return_code']	= "9998";
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else {
			
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
			
		}
		
		
	}



	public function menus_mod_add(){	// 메뉴 수정 2019-04-17
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$parameters				=	array(
											"menu_no"=>$array['menu_array']['post_menu_no']
											, "menu_grandparent"=>$array['menu_array']['post_menu_grandparent']
											, "menu_parent"=>$array['menu_array']['post_menu_parent']
											, "menu_child"=>$array['menu_array']['post_menu_child']
											, "menu_name"=>$array['menu_array']['post_menu_name']
											, "menu_url"=>$array['menu_array']['post_menu_url']
											, "menu_target"=>$array['menu_array']['post_menu_target']
										);
			
			
			$rows['check_menu']			= $this->PagerModel->CheckMenu($parameters);
			$count						= $rows['check_menu']->num_rows();
			$print						= $rows['check_menu']->row_array();
			
			
			if($count == 1){
				
				$this->PagerModel->SetMenuUpdate($parameters);
				
				$rows					= $this->db->affected_rows();
				
				if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
					
					$call_back['return_code']	= "0000";
					
				} else {	// 문제가 있을때
					
					$call_back['return_code']	= "9999";
					
				}
				
			} else if($count <= 0) {	// 1개 미만일 때
				
				$call_back['return_code']	= "9998";
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
		
	}
	
	
	
	public function menus_remove(){	// 메뉴 설정 2019-04-17
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$parameters					=	array(
												"menu_no"=>$array['menu_array']['post_menu_no']
											);
			
			
			$rows['check_menu']			= $this->PagerModel->CheckMenu($parameters);
			$count						= $rows['check_menu']->num_rows();
			$print						= $rows['check_menu']->row_array();
			
			
			$this->PagerModel->SetMenuDelete($parameters);
				
			$rows					= $this->db->affected_rows();
				
			if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
				
				$call_back['return_code']	= "0000";
				
			} else {	// 문제가 있을때
					
				$call_back['return_code']	= "9999";
					
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else {
			
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
			
		}
		
		
	}



	public function page_list(){	// 페이지 목록 2019-02-21


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/
            
            
            $rows['start']                  = 0;
            $rows['end']                    = 15;
            $rows['page']                   = $array['search_array']['page'];
            
            $rows['page_list']              = $this->PagerModel->GetPageList(@$array['search_array']['search_type'], @$array['search_array']['search_name'], $rows['page'], $rows['start'], $rows['end']);
            $rows['count']                  = $rows['page_list']['result']->num_rows();
            $rows['result']                 = $rows['page_list']['result']->result_array();
            
            $rows['num']                    = $rows['page_list']['num'];
            
            $rows['id']                     = $rows['page_list']['id'];
            $rows['page_count']             = $this->PagerModel->GetPageCount(@$array['search_array']['search_type'], @$array['search_array']['search_name']);


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/PageListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}
	
	
	
	public function page_add(){     // 페이지 추가 2019-09-18
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));

		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
			$rows['config_count']			= $rows['get_config']->num_rows();
			$rows['config'] 				= $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			$rows['layout']		            = $this->common->GetDesign("layouts", "10");
			
			
			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/PageAddView', $rows);
			$this->load->view('dashboard/FooterView');
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	public function page_add_ok(){      // 페이지 추가 완료 2019-09-18
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$parameters         =   array(
			                            "member_id"=>$array['session_array']['sess_member_id']
			                            , "member_name"=>$this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
			                            , "page_title"=>$array['pager_array']['post_page_title']
			                            , "page_name"=>$array['pager_array']['post_page_name']
			                            , "page_content"=>preg_replace($this->common->CheckFilter(), "", $array['pager_array']['post_page_content'])
			                            , "page_layout"=>$array['pager_array']['post_page_layout']
			                            , "page_include"=>$array['pager_array']['post_page_include']
									);
			
			//$rows['page_content']       = $this->PagerModel->GetPageContent($parameters);
			
			$this->PagerModel->SetPageInsert($parameters);
			
			$rows                       = $this->db->affected_rows();
			
			if($rows >= 1){
				
				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/page_mod/" . $array['pager_array']['post_page_name']);
				
			} else {
				
				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/page_add/");
				
			}
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	public function tmp_page_add_ok(){
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			if(empty($array['pager_array']['post_page_title'])){
				
				$tmp_page_title             = $this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv) . "님의 임시 게시물";
				
			} else {
				
				$tmp_page_title             = $array['pager_array']['post_page_title'];
				
			}
			
			
			if(empty($array['pager_array']['post_page_content'])){
				
				$tmp_page_content           = $this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv) . "님의 임시 게시물";
				
			} else {
				
				$tmp_page_content           = preg_replace($this->common->CheckFilter(), "", $array['pager_array']['post_page_content']);
				
			}
			
			
			$tmp_page_name                  = $this->common->SetStringToken();
			
			
			$parameters                     =   array(
													  "member_id"=>$array['session_array']['sess_member_id']
													, "member_name"=>$this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
													, "page_title"=>$tmp_page_title
													, "page_name"=>$tmp_page_name
													, "page_content"=>$tmp_page_content
													, "page_layout"=>"basic"
													, "page_include"=>""
												);
			
						
			$this->PagerModel->SetPageInsert($parameters);
			
			$rows                           = $this->db->affected_rows();
			
			if($rows >= 1){
				
				$call_back['return_code']   = "0000";
				$call_back['page_name']     = $tmp_page_name;
				
			} else {
				
				$call_back['return_code']   = "9999";
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	public function page_mod(){     // 페이지 수정 2019-09-19
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1) {    // 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
			$rows['config_count']			= $rows['get_config']->num_rows();
			$rows['config'] 				= $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$parameters                     =   array(
			                                        "page_name"=>$array['pager_array']['request_page_name']
												);
			
			$rows['board_content']          = $this->PagerModel->GetPageContent($parameters);
			$rows['count']                  = $rows['board_content']->num_rows();
			$rows['print']                  = $rows['board_content']->row_array();
			
			
			if($rows['count'] >= 1) {
				
				$rows['layout'] = $this->common->GetDesign("layouts", "10");
				
				
				/** 첨부파일 가져오기 시작 **/
				$array['get_page_files']    = $this->PagerModel->GetPageFiles($rows['print']['page_no'], NULL);
				$array['file_count']        = $array['get_page_files']->num_rows();
				$array['file_result']       = $array['get_page_files']->result_array();
				/** 첨부파일 가져오기 끝 **/
				
				
				$this->load->view('dashboard/HeaderView', $array);
				$this->load->view('dashboard/PageModView', $rows);
				$this->load->view('dashboard/FooterView');
				
			} else {
				
				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/pager/page_list");
				
			}
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	
	public function page_mod_ok(){     // 페이지수정 완료 2019-09-19
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1) {    // 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
			$rows['config_count']			= $rows['get_config']->num_rows();
			$rows['config'] 				= $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$parameters                     =   array(
			                                        "page_title"=>$array['pager_array']['post_page_title']
			                                        , "page_name"=>$array['pager_array']['request_page_name']
			                                        , "page_content"=>$array['pager_array']['post_page_content']
			                                        , "page_layout"=>$array['pager_array']['post_page_layout']
			                                        , "page_include"=>@$array['pager_array']['post_page_include']
												);
			
			
			$this->PagerModel->SetPageUpdate($parameters);
			
			
			$rows                           = $this->db->affected_rows();
			
			if($rows >= 1){
				
				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/page_mod/" . $array['pager_array']['request_page_name']);
				
			} else {
				
				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/page_mod/" . $array['pager_array']['request_page_name']);
				
			}
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
		
	}
	
	
	public function page_remove(){
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1) {    // 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']     = $this->HomeModel->GetConfig();
			$rows['config_count']   = $rows['get_config']->num_rows();
			$rows['config']         = $rows['get_config']->row_array();
			
			$array['config']        = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$parameters                     =   array(
			                                        "page_name"=>$array['pager_array']['request_page_name']
												);
			
			
			$rows['page_content']           = $this->PagerModel->GetPageContent($parameters);
			$rows['count']                  = $rows['page_content']->num_rows();
			$rows['print']                  = $rows['page_content']->row_array();
			
			
			/** 첨부파일 가져오기 시작 **/
			$rows['get_page_files']		    = $this->PagerModel->GetPageFiles($rows['print']['page_no'], NULL);
			$rows['file_count']			    = $rows['get_page_files']->num_rows();
			$rows['file_result']			= $rows['get_page_files']->result_array();
			
			
			if($rows['count'] >= 1){
				
				if($rows['file_count'] >= 1) {
					
					foreach ($rows['file_result'] as $files) {
						
						$this->common->SetBoardFileDelete("pager", $files['page_no'], $files['file_encrypt_name']);
						$this->PagerModel->SetPageFileDelete($files['file_no'], $files['page_no']);
						
					}
					
				}
				
				
				$this->PagerModel->SetPageDelete($rows['print']['page_name']);
				
				$rows                       = $this->db->affected_rows();
				
				if($rows >= 1) {
					
					$call_back['return_code']   = "0000";
					
				} else {
					
					$call_back['return_code']   = "9999";
					
				}
				
			} else {
				
				$call_back['return_code']	= "9998";
				
			}
			/** 첨부파일 가져오기 끝 **/
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
		
	}
	
	
	public function page_insert_files(){
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			/** 첨부파일 가져오기 시작 **/
			$array['get_page_files']		= $this->PagerModel->GetPageFiles($array['pager_array']['request_no'], NULL);
			$array['file_count']			= $array['get_page_files']->num_rows();
			$array['file_result']			= $array['get_page_files']->result_array();
			/** 첨부파일 가져오기 끝 **/
			
			
			/** 파일첨부 기능 시작 **/
			$file_count						= count(@$_FILES['file_name']['name']);
			$total_file_count				= $array['pager_array']['request_count'];
			
			
			$config['upload_path']			= $this->common->SetDirectory("pager", $array['pager_array']['request_no']);
			$config['allowed_types']		= "gif|jpg|jpeg|png";	// 게시판 본문 삽입은 공통적으로 이미지 파일만 허용시킴
			
			
			if($file_count >= 1 && $array['file_count'] < 5) {
				
				if (in_array($_FILES['file_name']['type'], array("image/jpeg", "image/jpg", "image/pjpeg", "image/png", "image/gif")) && $_FILES['file_name']['size'] < 5242880) {
					
					/** 파일 이동 메서드 시작 **/
					$file_move              = $this->common->SetFileMove($config, $_FILES['file_name']['name'], $_FILES['file_name']['tmp_name'], $array['session_array']['sess_member_id']);    // 파일 이동 메서드
					/** 파일 이동 메서드 끝
					
					
					1. 파일을 서버에 복사 한다
					2. 복사 된 파일을 리사이징 메서드로 불러온다
					3. 파일을 리사이징 한다
					4. 리사이징 한 파일을 덮어 쓴다
					5. 리사이징 전 파일을 따로 삭제 할 필요가 없어진다
					
					
					파일 리사이징 메서드 시작 **/
					
					
					$path						= DOCUMENT_ROOT . "/uploaded/pager/" .  $array['pager_array']['request_no'] . "/";
					
					$recreate					= $this->common->SetImageRecreate($path, $file_move['encrypt_name'], "800", "500");
					
					
					$this->PagerModel->SetPageFilesInsert(
					    $array['pager_array']['request_no']
					    , $array['session_array']['sess_member_id']
					    , $recreate['encrypt_name']
					    , $recreate['file_name']
					    , $recreate['width']
					    , $recreate['height']
					    , $recreate['type']
					);
					/** 파일 리사이징 메서드 끝 **/
					
					
					$call_back['return_code']	= "0000";
					$call_back['encrypt_name']	= $file_move['encrypt_name'];
					
				} else {
					
					@$call_back['return_code']	= "9997";
					@$this->common->file_delete("member", $array['sess_member_id'], $file_move['encrypt_name']);
					
				}
				
			} else if($array['file_count'] >= 6){
				
				$call_back['return_code']	= "9998";
				@$this->common->file_delete("member", $array['session_array']['sess_member_id'], $file_move['encrypt_name']);
				
			} else {
				
				@$call_back['return_code']	= "9999";
				@$this->common->file_delete("member", $array['session_array']['sess_member_id'], $file_move['encrypt_name']);
				
			}
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);
			/** 파일첨부 기능 끝 **/
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	public function page_file_remove(){
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		/** 첨부파일 가져오기 시작 **/
		$array['get_page_files']		= $this->PagerModel->GetPageFiles($array['pager_array']['post_page_no'], $array['pager_array']['post_file_no']);
		$array['file_count']			= $array['get_page_files']->num_rows();
		$array['file_print']			= $array['get_page_files']->row_array();
		/** 첨부파일 가져오기 끝 **/
		
		
		$this->common->SetFileDelete("pager", $array['file_print']['page_no'], $array['file_print']['file_encrypt_name']);
		$this->PagerModel->SetPageFileDelete($array['file_print']['file_no'], $array['file_print']['page_no']);
		
		
		if($this->db->affected_rows() >= 1){
			
			$call_back['return_code']       = "0000";
			
		} else {
			
			$call_back['return_code']       = "9999";
			
		}
		
		
		echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
		
		
	}
	
	
	public function popup_list(){	// 팝업 목록 2019-09-24
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
			$rows['config_count']			= $rows['get_config']->num_rows();
			$rows['config'] 				= $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$rows['start']                  = 0;
			$rows['end']                    = 15;
			$rows['page']                   = $array['search_array']['page'];
			
			$rows['popup_list']             = $this->PagerModel->GetPopupList(@$array['search_array']['search_type'], @$array['search_array']['search_name'], $rows['page'], $rows['start'], $rows['end']);
			$rows['count']                  = $rows['popup_list']['result']->num_rows();
			$rows['result']                 = $rows['popup_list']['result']->result_array();
			
			$rows['num']                    = $rows['popup_list']['num'];
			
			$rows['id']                     = $rows['popup_list']['id'];
			$rows['popup_count']            = $this->PagerModel->GetPopupCount(@$array['search_array']['search_type'], @$array['search_array']['search_name']);
			
			
			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/PopupListView', $rows);
			$this->load->view('dashboard/FooterView');
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
		
	}
	
	
	public function popup_add(){     // 팝업 추가 2019-09-24
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
			$rows['config_count']			= $rows['get_config']->num_rows();
			$rows['config'] 				= $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			$rows['layout']		            = $this->common->GetDesign("layouts", "10");
			
			
			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/PopupAddView', $rows);
			$this->load->view('dashboard/FooterView');
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	public function popup_add_ok(){      // 페이지 추가 완료 2019-09-18
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$parameters         =   array(
										  "member_id"=>$array['session_array']['sess_member_id']
										, "member_name"=>$this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
										, "popup_title"=>$array['pager_array']['post_popup_title']
										, "popup_name"=>$array['pager_array']['post_popup_name']
										, "popup_content"=>preg_replace($this->common->CheckFilter(), "", $array['pager_array']['post_popup_content'])
									);
			
			//$rows['page_content']       = $this->PagerModel->GetPageContent($parameters);
			
			$this->PagerModel->SetPopupInsert($parameters);
			
			$rows                       = $this->db->affected_rows();
			
			if($rows >= 1){
				
				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/popup_mod/" . $array['pager_array']['post_popup_name']);
				
			} else {
				
				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/popup_add/");
				
			}
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	public function tmp_popup_add_ok(){
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			if(empty($array['pager_array']['post_popup_title'])){
				
				$tmp_popup_title             = $this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv) . "님의 임시 게시물";
				
			} else {
				
				$tmp_popup_title             = $array['pager_array']['post_popup_title'];
				
			}
			
			
			if(empty($array['pager_array']['post_popup_content'])){
				
				$tmp_popup_content           = $this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv) . "님의 임시 게시물";
				
			} else {
				
				$tmp_popup_content           = preg_replace($this->common->CheckFilter(), "", $array['pager_array']['post_popup_content']);
			}
			
			
			$tmp_popup_name                  = $this->common->SetStringToken();
			
			
			$parameters                     =   array(
													"member_id"=>$array['session_array']['sess_member_id']
													, "member_name"=>$this->common->GetDecrypt($array['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
													, "popup_title"=>$tmp_popup_title
													, "popup_name"=>$tmp_popup_name
													, "popup_content"=>$tmp_popup_content
												);
			
			
			$this->PagerModel->SetPopupInsert($parameters);
			
			$rows                           = $this->db->affected_rows();
			
			if($rows >= 1){
				
				$call_back['return_code']   = "0000";
				$call_back['popup_name']     = $tmp_popup_name;
				
			} else {
				
				$call_back['return_code']   = "9999";
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
		
	}
	
	
	public function popup_mod(){     // 팝업 수정 2019-09-24
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1) {    // 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
			$rows['config_count']			= $rows['get_config']->num_rows();
			$rows['config'] 				= $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$parameters                     =   array(
													"popup_name"=>$array['pager_array']['request_popup_name']
												);
			
			
			$rows['popup_content']          = $this->PagerModel->GetPopupContent($parameters);
			$rows['count']                  = $rows['popup_content']->num_rows();
			$rows['print']                  = $rows['popup_content']->row_array();
			
			
			if($rows['count'] >= 1) {
				
				
				/** 첨부파일 가져오기 시작 **/
				$array['get_popup_files']   = $this->PagerModel->GetPopupFiles($rows['print']['popup_no'], NULL);
				$array['file_count']        = $array['get_popup_files']->num_rows();
				$array['file_result']       = $array['get_popup_files']->result_array();
				/** 첨부파일 가져오기 끝 **/
				
				
				$this->load->view('dashboard/HeaderView', $array);
				$this->load->view('dashboard/PopupModView', $rows);
				$this->load->view('dashboard/FooterView');
				
			} else {
				
				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/pager/popup_list");
				
			}
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	
	public function popup_mod_ok(){     // 팝업수정 완료 2019-09-24
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1) {    // 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
			$rows['config_count']			= $rows['get_config']->num_rows();
			$rows['config'] 				= $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$parameters                     =   array(
													"popup_title"=>$array['pager_array']['post_popup_title']
													, "popup_name"=>$array['pager_array']['request_popup_name']
													, "popup_content"=>$array['pager_array']['post_popup_content']
												);
													
			
			$this->PagerModel->SetPopupUpdate($parameters);
			
			
			$rows                           = $this->db->affected_rows();
			
			if($rows >= 1){
				
				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/popup_mod/" . $array['pager_array']['request_popup_name']);
				
			} else {
				
				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/pager/popup_mod/" . $array['pager_array']['request_popup_name']);
				
			}
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
		
	}
	
	
	public function popup_remove(){
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1) {    // 로그인 여부 및 권한 체크 2019-02-08
			
			
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']             = $this->HomeModel->GetConfig();
			$rows['config_count']           = $rows['get_config']->num_rows();
			$rows['config']                 = $rows['get_config']->row_array();
			
			$array['config']                = $rows['config'];
			/**  기본환경설정 메서드 추가 끝 2019-09-09 **/
			
			
			$parameters                     =   array(
												  "popup_name"=>$array['pager_array']['request_popup_name']
												);
			
			$rows['popup_content']          = $this->PagerModel->GetPopupContent($parameters);
			$rows['count']                  = $rows['popup_content']->num_rows();
			$rows['print']                  = $rows['popup_content']->row_array();
			
			
			/** 첨부파일 가져오기 시작 **/
			$rows['get_popup_files']	    = $this->PagerModel->GetPopupFiles($rows['print']['popup_no'], NULL);
			$rows['file_count']			    = $rows['get_popup_files']->num_rows();
			$rows['file_result']			= $rows['get_popup_files']->result_array();
			
			
			if($rows['count'] >= 1){
				
				if($rows['file_count'] >= 1) {
					
					foreach ($rows['file_result'] as $files) {
						
						$this->common->SetBoardFileDelete("popup", $files['popup_no'], $files['file_encrypt_name']);
						$this->PagerModel->SetPopupFileDelete($files['file_no'], $files['popup_no']);
						
					}
					
				}
				
				
				$this->PagerModel->SetPopupDelete($rows['print']['popup_name']);
				
				$rows                       = $this->db->affected_rows();
				
				if($rows >= 1) {
					
					$call_back['return_code']   = "0000";
					
				} else {
					
					$call_back['return_code']   = "9999";
					
				}
				
			} else {
				
				$call_back['return_code']	= "9998";
				
			}
			/** 첨부파일 가져오기 끝 **/
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
		
	}
	
	
	public function popup_insert_files(){
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			/** 첨부파일 가져오기 시작 **/
			$array['get_popup_files']		= $this->PagerModel->GetPopupFiles($array['pager_array']['request_no'], NULL);
			$array['file_count']			= $array['get_popup_files']->num_rows();
			$array['file_result']			= $array['get_popup_files']->result_array();
			/** 첨부파일 가져오기 끝 **/
			
			
			/** 파일첨부 기능 시작 **/
			$file_count						= count(@$_FILES['file_name']['name']);
			$total_file_count				= $array['pager_array']['request_count'];
			
			
			$config['upload_path']			= $this->common->SetDirectory("popup", $array['pager_array']['request_no']);
			$config['allowed_types']		= "gif|jpg|jpeg|png";	// 게시판 본문 삽입은 공통적으로 이미지 파일만 허용시킴
			
			
			if($file_count >= 1 && $array['file_count'] < 5) {
				
				if (in_array($_FILES['file_name']['type'], array("image/jpeg", "image/jpg", "image/pjpeg", "image/png", "image/gif")) && $_FILES['file_name']['size'] < 5242880) {
					
					/** 파일 이동 메서드 시작 **/
					$file_move              = $this->common->SetFileMove($config, $_FILES['file_name']['name'], $_FILES['file_name']['tmp_name'], $array['session_array']['sess_member_id']);    // 파일 이동 메서드
					/** 파일 이동 메서드 끝
					
					
					1. 파일을 서버에 복사 한다
					2. 복사 된 파일을 리사이징 메서드로 불러온다
					3. 파일을 리사이징 한다
					4. 리사이징 한 파일을 덮어 쓴다
					5. 리사이징 전 파일을 따로 삭제 할 필요가 없어진다
					
					
					파일 리사이징 메서드 시작 **/
					$path						= DOCUMENT_ROOT . "/uploaded/popup/" .  $array['pager_array']['request_no'] . "/";
					
					$recreate					= $this->common->SetImageRecreate($path, $file_move['encrypt_name'], "800", "500");
					
					
					$this->PagerModel->SetPopupFilesInsert(
						$array['pager_array']['request_no']
						, $array['session_array']['sess_member_id']
					    , $recreate['encrypt_name']
					    , $recreate['file_name']
					    , $recreate['width']
					    , $recreate['height']
					    , $recreate['type']
					);
					/** 파일 리사이징 메서드 끝 **/
					
					$call_back['return_code']	= "0000";
					$call_back['encrypt_name']	= $file_move['encrypt_name'];
					
				} else {
					
					@$call_back['return_code']	= "9997";
					@$this->common->file_delete("member", $array['sess_member_id'], $file_move['encrypt_name']);
					
				}
				
			} else if($array['file_count'] >= 6){
				
				$call_back['return_code']	= "9998";
				@$this->common->file_delete("member", $array['session_array']['sess_member_id'], $file_move['encrypt_name']);
				
			} else {
				
				@$call_back['return_code']	= "9999";
				@$this->common->file_delete("member", $array['session_array']['sess_member_id'], $file_move['encrypt_name']);
				
			}
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);
			/** 파일첨부 기능 끝 **/
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}
	
	
	public function popup_file_remove(){
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		/** 첨부파일 가져오기 시작 **/
		$array['get_popup_files']		= $this->PagerModel->GetPopupFiles($array['pager_array']['post_popup_no'], $array['pager_array']['post_file_no']);
		$array['file_count']			= $array['get_popup_files']->num_rows();
		$array['file_print']			= $array['get_popup_files']->row_array();
		/** 첨부파일 가져오기 끝 **/
		
		
		$this->common->SetFileDelete("popup", $array['file_print']['popup_no'], $array['file_print']['file_encrypt_name']);
		$this->PagerModel->SetPopupFileDelete($array['file_print']['file_no'], $array['file_print']['popup_no']);
		
		
		if($this->db->affected_rows() >= 1){
			
			$call_back['return_code']       = "0000";
			
		} else {
			
			$call_back['return_code']       = "9999";
			
		}
		
		
		echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
		
		
	}


}