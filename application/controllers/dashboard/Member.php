<?php
if(!defined('BASEPATH')) exit;


class Member extends CI_Controller {



	public function __construct(){


		parent::__construct();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');


		$this->load->helper(array('form', 'url'));


		$this->load->model('dashboard/MemberModel');
        $this->load->model('dashboard/HomeModel');


	}


	public function signin(){	// signin method 2019-01-11


		$array					= $this->admin->GetParameter();

		if(isset($array['session_array']['sess_member_id']) && isset($array['session_array']['sess_member_email'])){	// 로그인 여부 및 권한 체크 2019-02-08

			echo $this->common->move_error(KO_LOGGED, PROTOCOLS . HTTP_HOST . "/dashboard/home");

		} else {

			$this->load->view('dashboard/SigninView', $array);

		}


	}

	
	public function signin_ok(){	// success signin method 2019-01-14


		$array					= $this->admin->GetParameter();


		$post_member_id			= $array['member_array']['request_member_id'];
		$post_member_password	= $this->common->GetEncrypt($array['member_array']['request_member_password']);	// Fatal error: Cannot use isset() on the result of a function call (you can use "null !== func()" instead) in

		
		if(isset($post_member_id) && isset($post_member_password)){

			$parameter			=	array(
										"member_id"=>$post_member_id
										, "member_password"=>$post_member_password
									);


			$value	= $this->MemberModel->SetSigninSuccess($parameter);

			$count	= $value->num_rows();

			
			if($count >= 1){


				$member					= $value->row_array();
				
				$sess_member_id			= $this->session->set_userdata('member_id', $member['member_id']);
				$sess_member_name		= $this->session->set_userdata('member_name', $member['member_name']);
				$sess_member_email		= $this->session->set_userdata('member_email', $member['member_email']);
				$sess_member_level		= $this->session->set_userdata('member_level', $member['member_level']);
				$sess_member_profile	= $this->session->set_userdata('member_profile', $member['member_profile']);
				$sess_member_formal		= $this->session->set_userdata('member_formal', $member['member_formal']);

				echo $this->common->move_error(KO_LOGGED, PROTOCOLS . HTTP_HOST . "/dashboard/home");


			} else {


				echo $this->common->move_error(KO_CANTLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


			}


		} else {


			echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}

	
	public function signout(){		// signout method 2019-01-28


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if(isset($array['session_array']['sess_member_id'])){	// 로그인 여부 및 권한 체크 2019-02-08


			$this->session->sess_destroy();

			echo $this->common->move_error(KO_LOGOUT, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	public function members(){	// member list 2019-02-27


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-03-18


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			$rows['start']					= 0;
			$rows['end']					= 15;
			$rows['page']					= $array['search_array']['page'];

			$rows['get_member_list']		= $this->MemberModel->GetMemberList(@$array['search_array']['search_type'], @$array['search_array']['search_name'], $rows['page'], $rows['start'], $rows['end']);
			$rows['count']					= $rows['get_member_list']['result']->num_rows();
			$rows['result']					= $rows['get_member_list']['result']->result_array();

			$rows['num']					= $rows['get_member_list']['num'];

			$rows['id']						= $rows['get_member_list']['id'];
			$rows['get_member_count']		= $this->MemberModel->GetMemberCount(@$array['search_array']['search_type'], @$array['search_array']['search_name']);


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MembersView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function member_info(){	// create member 2019-03-19

		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-03-19
			
			
			/**
				1. 수퍼어드민 권한 수정 영역은 보이지 않는다.
				2. 권한 수정이 있는 회원만 권한 수정 영역이 보인다.
				3. 권한 수정이 없는 회원은 권한 수정 영역 내 로직이 실행 되지 않는다.
				4. 회원 수정 권한이 있는 회원은 2,3 을 포함 시킨다.
				5. 수퍼어드민 프로필은 다른 유저가 볼 수 없다.
			**/


			$rows['get_member']					= $this->MemberModel->GetMember(@$array['member_array']['request_member_id']);
			$rows['member_get_count']			= $rows['get_member']->num_rows();
			$rows['member_get_print']			= $rows['get_member']->row_array();
			
			$rows['check_member_perm']			= $this->MemberModel->CheckMemberPermission(@$array['member_array']['request_member_id'], "");
			$rows['member_perm_get_count']		= $rows['check_member_perm']->num_rows();
			$rows['member_perm_get_result']		= $rows['check_member_perm']->result_array();
			
			$rows['get_member_perm_count']		= $this->MemberModel->GetMemberPermissionCount(@$array['member_array']['request_member_id']);
			$rows['member_perm_count']			= $rows['get_member_perm_count']->num_rows();
			

			if($rows['member_get_count'] >= 1){
				
				
				if($array['member_array']['request_member_id'] == "admin" && $array['session_array']['sess_member_id'] != "admin"){	// 어드민 페이지이며 어드민이 아닐때
					
					echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
					
				} else {


				    /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
                    $rows['get_config']			    = $this->HomeModel->GetConfig();
                    $rows['config_count']			= $rows['get_config']->num_rows();
                    $rows['config'] 				= $rows['get_config']->row_array();

                    $array['config']                 = $rows['config'];
                    /**  기본환경설정 메서드 추가 끝 2019-09-09 **/

					
					$this->load->view('dashboard/HeaderView', $array);
					$this->load->view('dashboard/MemberInfoView', $rows);
					$this->load->view('dashboard/FooterView', $array);
					
				}
				

			} else {

				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/member/members");

			}


		} else if(isset($array['session_array']['sess_member_id'])){
			

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			

		} else {
			

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}

	}


	public function member_info_ok(){	// modify member 2019-03-20

		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-03-20

			
			$rows['get_member']			= $this->MemberModel->GetMember(@$array['member_array']['request_member_id']);
			$count						= $rows['get_member']->num_rows();
			$print						= $rows['get_member']->row_array();
			

			if($count >= 1){

				$parameters				=	array(
												"member_id"=>$array['member_array']['request_member_id']
												, "member_password"=>$this->common->GetEncrypt(@$array['member_array']['request_member_password'], @$secret_key, @$secret_iv)
												, "member_name"=>$this->common->GetEncrypt(@$array['member_array']['request_member_name'], @$secret_key, @$secret_iv)
												, "member_email_1"=>$this->common->GetEncrypt(@$array['member_array']['request_member_email_1'], @$secret_key, @$secret_iv)
												, "member_email_2"=>$this->common->GetEncrypt(@$array['member_array']['request_member_email_2'], @$secret_key, @$secret_iv)
												, "member_level"=>$array['member_array']['request_member_level']
												, "member_profile"=>$array['member_array']['request_member_profile']
												, "member_formal"=>$array['member_array']['request_member_formal']
											);

				$this->MemberModel->SetMemberUpdate($parameters);


				$rows					= $this->db->affected_rows();

				
				if($rows >= 1){


					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/member/member_info/" . $array['member_array']['request_member_id']);


				} else {


					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/member/member_info/" . $array['member_array']['request_member_id']);

				}


			} else {

				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/member/member_info/" . $array['member_array']['request_member_id']);

			}


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}
	
	
	public function member_add_perm_ok(){	// 회원 권한 수정 2019-04-04
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-05
		
			
			$get_count					= $array['member_array']['post_count'] == 1 ? 1 : +1;	// post_count 의 값이 1이상일 때 +1 을 더한다
			
			
			for($i = 0; $i < $get_count; $i++){
				
				
				$exp_get_post			= explode("|", $array['member_array']['post_from']);
				
				$member_perm_get		= $this->MemberModel->CheckMemberPermission($exp_get_post['2'], $exp_get_post['0']);
				$count					= $member_perm_get->num_rows();
				$print					= $member_perm_get->row_array();
				
				
				if($count <= 0){
					
					
					$parameters			=	array(
												"member_id"=>$array['session_array']['sess_member_id']
												, "perm_member_id"=>$exp_get_post['2']
												, "perm_page"=>$exp_get_post['0']
												, "perm_name"=>$exp_get_post['1']
											);
					
					
					$this->MemberModel->SetMemberPermissionInsert($parameters);	// member_id, per_member_id, perm_page, perm_name
					
					$rows					= $this->db->affected_rows();
					
					if($rows >= 1){	// 1개 이상일 때
						
						$call_back['return_code']	= "0000";
						
					} else {
						
						$call_back['return_code']	= "9999";
						
					}
					
				} else if($count >= 1){
					
					$call_back['return_code']		= "9998";
					
				}
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else {
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		}
		
	}
	
	
	public function member_remove_perm_ok(){	// 회원 권한 삭제 2019-04-04
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-05
			
			
			$get_count					= $array['member_array']['post_count'] == 1 ? 1 : +1;	// post_count 의 값이 1개 일때는 1이고 1이상일 때 +1 을 더한다
			
			
			for($i = 0; $i < $get_count; $i++){
				
				
				$exp_get_post			= explode("|", $array['member_array']['post_to']);
				
				$member_perm_get		= $this->MemberModel->CheckMemberPermission($exp_get_post['2'], $exp_get_post['0']);
				$count					= $member_perm_get->num_rows();
				$print					= $member_perm_get->row_array();
				
				
				if($count >= 1){
					
					
					$parameters			=	array(
												"member_id"=>$array['session_array']['sess_member_id']
												, "perm_member_id"=>$exp_get_post['2']
												, "perm_page"=>$exp_get_post['0']
												, "perm_name"=>$exp_get_post['1']
											);
					
					
					$this->MemberModel->SetMemberPermissionDelete($parameters);	// member_id, per_member_id, perm_page, perm_name
					
					$rows					= $this->db->affected_rows();
					
					if($rows >= 1){	// 1개 이상일 때
						
						$call_back['return_code']	= "0000";
						
					} else {
						
						$call_back['return_code']	= "9999";
						
					}
					
					
				} else if($count <= 0){
					
					$call_back['return_code']		= "9998";
					
				}
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
			
			
		} else {
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		}
		
	}


	public function member_remove(){	// 회원 삭제 2019-03-21


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


			$array['get_member']		= $this->MemberModel->GetMember(@$array['member_array']['request_member_id']);
			$count						= $array['get_member']->num_rows();
			$print						= $array['get_member']->row_array();


			if($count >= 1){


				$parameters				=	array(
												"member_no"=>$print['member_no']
												, "member_id"=>$print['member_id']
												, "member_email"=>$print['member_email']
											);

				$this->MemberModel->SetMemberDelete($parameters);


				$rows					= $this->db->affected_rows();


				if($rows >= 1){

					$call_back['return_code']	= "0000";

				} else {

					$call_back['return_code']	= "9999";

				}


			} else {

				$call_back['return_code']		= "9998";

			}


			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	public function member_add(){	// 회원 추가 2019-03-21


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MemberAddView');
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	public function member_add_ok(){	// 회원 추가 완료 2019-03-22


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


			$array['get_member']		= $this->MemberModel->GetMember(@$array['member_array']['request_member_id']);
			$count						= $array['get_member']->num_rows();
			$print						= $array['get_member']->row_array();


			$parameters					=	array(
												"member_id"=>@$array['member_array']['request_member_id']
												, "member_password"=>@$this->common->GetEncrypt(@$array['member_array']['request_member_password'], @$secret_key, @$secret_iv)
												, "member_name"=>@$this->common->GetEncrypt(@$array['member_array']['request_member_name'], @$secret_key, @$secret_iv)
												, "member_email_1"=>@$this->common->GetEncrypt(@$array['member_array']['request_member_email_1'], @$secret_key, @$secret_iv)
												, "member_email_2"=>@$this->common->GetEncrypt(@$array['member_array']['request_member_email_2'], @$secret_key, @$secret_iv)
												, "member_level"=>@$array['member_array']['request_member_level']
												, "member_profile"=>@$array['member_array']['request_member_profile']
												, "member_formal"=>@$array['member_array']['request_member_formal']
											);


			if($count <= 0){


				$this->MemberModel->SetMemberInsert($parameters);
				$rows					= $this->db->affected_rows();


				if($rows >= 1){

					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/member/member_info/" . $array['member_array']['request_member_id']);

				} else {

					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/member/member_add");

				}


			} else if($count >= 1){


				echo $this->common->move_error(KO_DUPLICATED, PROTOCOLS . HTTP_HOST . "/dashboard/member/member_add");


			} else {


				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/member/member_add");


			}


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	public function member_gen_pw(){	// 비밀번호 발급 현황 2019-07-04


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));

		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/

			
			$rows['start']					= 0;
			$rows['end']					= 15;
			$rows['page']					= $array['search_array']['page'];

			$rows['get_member_gen_pw']		= $this->MemberModel->GetMemberGenPw(@$array['search_array']['search_type'], @$array['search_array']['search_name'], $rows['page'], $rows['start'], $rows['end']);

			$rows['count']					= $rows['get_member_gen_pw']['result']->num_rows();
			$rows['result']					= $rows['get_member_gen_pw']['result']->result_array();

			$rows['num']					= $rows['get_member_gen_pw']['num'];

			$rows['id']						= $rows['get_member_gen_pw']['id'];
			$rows['get_member_gen_pw_count']= $this->MemberModel->GetMemberGenPwCount(@$array['search_array']['search_type'], @$array['search_array']['search_name']);


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MemberGenPwView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}
		
	}


	public function member_gen_tk(){	// 토큰 발급 현황 2019-07-04


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));

		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/

			
			$rows['start']					= 0;
			$rows['end']					= 15;
			$rows['page']					= $array['search_array']['page'];

			$rows['get_member_gen_tk']		= $this->MemberModel->GetMemberGenTk($rows['page'], $rows['start'], $rows['end']);

			$rows['count']					= $rows['get_member_gen_tk']['result']->num_rows();
			$rows['result']					= $rows['get_member_gen_tk']['result']->result_array();

			$rows['num']					= $rows['get_member_gen_tk']['num'];

			$rows['id']						= $rows['get_member_gen_tk']['id'];
			$rows['get_member_gen_tk_count']= $this->MemberModel->GetMemberGenTkCount();

			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MemberGenTkView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}
		
	}


	public function member_board_list(){	// 회원의 게시물 목록 2019-07-09


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));

		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			$rows['get_admin_board_list']		= $this->MemberModel->GetAdminBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();

			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['search_array']['page'];

			$rows['get_member_board_list']		= $this->MemberModel->GetMembersBoardList($rows['page'], $rows['start'], $rows['end'], $array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_board_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_board_list']['result']->result_array();

			$rows['num']						= $rows['get_member_board_list']['num'];
			$rows['id']							= $rows['get_member_board_list']['id'];

			$rows['get_members_board_count']	= $this->MemberModel->GetMembersBoardCount($array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MemberBoardListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}
		
	}


	public function member_file_list(){	// 회원의 첨부파일 목록 2019-07-09


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));

		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			/** 추가된 게시판 목록 시작 **/
			$rows['get_admin_board_list']		= $this->MemberModel->GetAdminBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();
			/** 추가된 게시판 목록 끝 **/


			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['search_array']['page'];

			$rows['get_member_file_list']		= $this->MemberModel->GetMembersFileList($rows['page'], $rows['start'], $rows['end'], $array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_file_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_file_list']['result']->result_array();

			$rows['num']						= $rows['get_member_file_list']['num'];
			$rows['id']							= $rows['get_member_file_list']['id'];
			

			$rows['get_members_file_count']	= $this->MemberModel->GetMembersFileCount($array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MemberFileListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}
		
	}


	public function member_reply_list(){	// 회원의 첨부파일 목록 2019-07-09


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			/** 추가된 게시판 목록 시작 **/
			$rows['get_admin_board_list']		= $this->MemberModel->GetAdminBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();
			/** 추가된 게시판 목록 끝 **/


			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['search_array']['page'];

			$rows['get_member_reply_list']		= $this->MemberModel->GetMembersReplyList($rows['page'], $rows['start'], $rows['end'], $array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_reply_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_reply_list']['result']->result_array();

			$rows['num']						= $rows['get_member_reply_list']['num'];
			$rows['id']							= $rows['get_member_reply_list']['id'];

			$rows['get_members_reply_count']	= $this->MemberModel->GetMembersReplyCount($array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MemberReplyListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}
		
	}


	public function member_voted_list(){	// 회원의 추천/비추천 목록 2019-07-09


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));

		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			/** 추가된 게시판 목록 시작 **/
			$rows['get_admin_board_list']		= $this->MemberModel->GetAdminBoardList();
			$rows['admin_board_count']			= $rows['get_admin_board_list']->num_rows();
			$rows['admin_board_result']			= $rows['get_admin_board_list']->result_array();
			/** 추가된 게시판 목록 끝 **/


			$rows['start']						= 0;
			$rows['end']						= 15;
			$rows['page']						= $array['search_array']['page'];

			$rows['get_member_voted_list']		= $this->MemberModel->GetMembersVotedList($rows['page'], $rows['start'], $rows['end'], $array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);

			$rows['count']						= $rows['get_member_voted_list']['result']->num_rows();
			$rows['result']						= $rows['get_member_voted_list']['result']->result_array();

			$rows['num']						= $rows['get_member_voted_list']['num'];
			$rows['id']							= $rows['get_member_voted_list']['id'];

			$rows['get_members_voted_count']	= $this->MemberModel->GetMembersVotedCount($array['member_array']['request_member_id'], $rows['admin_board_count'], $rows['admin_board_result']);


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/MemberVotedListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}
		
	}

}