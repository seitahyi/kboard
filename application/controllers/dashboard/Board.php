<?php
if(!defined('BASEPATH')) exit;



class Board extends CI_Controller {


	
	public function __construct(){


		parent::__construct();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');


		$this->load->helper(array('form', 'url', 'download'));


		$this->load->model('dashboard/BoardModel');
        $this->load->model('dashboard/HomeModel');


	}


	public function board_add(){	// 게시판 설정 추가 2019-05-03

		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-05-03


		    /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			$rows['skin']		= $this->common->GetDesign("boards", "10");
			$rows['layout']		= $this->common->GetDesign("layouts", "10");

			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/BoardAddView', $rows);
			$this->load->view('dashboard/FooterView');

		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function board_add_ok(){	// 게시판 설정 추가 완료 2019-05-03


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-05-03


			$parameters			=	array(
										"member_id"=>$array['session_array']['sess_member_id']
										, "con_title"=>$array['board_array']['post_con_title']
										, "con_name"=>"board_" . $array['board_array']['post_con_name']
										, "con_category"=>$array['board_array']['post_con_category']
										, "con_skin"=>$array['board_array']['post_con_skin']
										, "con_layout"=>$array['board_array']['post_con_layout']
										, "con_thumb_filter"=>$array['board_array']['post_con_thumb_filter']
										, "con_diff"=>$array['board_array']['post_con_diff']
			                            , "con_list_level"=>$array['board_array']['post_con_list_level']
			                            , "con_content_level"=>$array['board_array']['post_con_content_level']
			                            , "con_write_level"=>$array['board_array']['post_con_write_level']
			                            , "con_reply_level"=>$array['board_array']['post_con_reply_level']
										, "ext_title_1"=>$array['board_array']['post_ext_title_1']
										, "ext_title_2"=>$array['board_array']['post_ext_title_2']
										, "ext_title_3"=>$array['board_array']['post_ext_title_3']
										, "ext_title_4"=>$array['board_array']['post_ext_title_4']
										, "ext_title_5"=>$array['board_array']['post_ext_title_5']
									);
			

			$rows['get_board']	= $this->BoardModel->GetBoard($parameters);
			$count				= $rows['get_board']->num_rows();
			

			if($count <= 0){


				$this->BoardModel->SetBoardInsert($parameters);

				$rows				= $this->db->affected_rows();


				if($rows >= 1){

					$this->BoardModel->SetCreateBoard($parameters);	// 게시판 생성 스키마 실행
					$this->BoardModel->SetCreateVote($parameters);	// 게시판의 추천/비추천 생성 스키마 실행
					$this->BoardModel->SetCreateReply($parameters);	// 게시판의 댓글 생성 스키마 실행
					$this->BoardModel->SetCreateFile($parameters);	// 게시판의 파일 생성 스키마 실행

					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/board/board_list/");

				} else {

					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/board/board_add/");

				}


			} else if($count >= 1){	// 같은 테이블명을 가진 게시판이 있을때

				echo $this->common->move_error(KO_DUPLICATED, PROTOCOLS . HTTP_HOST . "/dashboard/board/board_add/");

			}


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function board_list(){	// 게시판 목록 2019-05-03
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-05-03


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			$rows['start']				= 0;
			$rows['end']				= 15;
			$rows['page']				= $array['search_array']['page'];

			$rows['get_board_list']		= $this->BoardModel->GetBoardList(@$array['search_array']['search_type'], @$array['search_array']['search_name'], $rows['page'], $rows['start'], $rows['end']);
			$rows['count']				= $rows['get_board_list']['result']->num_rows();
			$rows['result']				= $rows['get_board_list']['result']->result_array();

			$rows['num']				= $rows['get_board_list']['num'];

			$rows['id']					= $rows['get_board_list']['id'];
			$rows['board_count']		= $this->BoardModel->GetBoardCount(@$array['search_array']['search_type'], @$array['search_array']['search_name']);
			
			
			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/BoardListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function board_mod(){	// 게시판 설정 2019-05-02


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-26


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			$parameters				=	array(
											"con_name"=>$array['board_array']['request_con_name']
										);


			$rows['get_board']	= $this->BoardModel->GetBoard($parameters);
			$rows['count']		= $rows['get_board']->num_rows();
			$rows['print']		= $rows['get_board']->row_array();
			
			
			if($rows['count'] >= 1) {
				
				$rows['skin']   = $this->common->GetDesign("boards", "10");
				$rows['layout'] = $this->common->GetDesign("layouts", "10");
				
				$this->load->view('dashboard/HeaderView', $array);
				$this->load->view('dashboard/BoardModView', $rows);
				$this->load->view('dashboard/FooterView');
				
				
			} else {
				
				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/board/board_list");
				
			}


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function board_mod_ok(){	// 게시판 설정 수정 완료 2019-05-02


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-30


			$parameters			=	array(
										"member_id"=>$array['session_array']['sess_member_id']
										, "con_title"=>$array['board_array']['post_con_title']
										, "con_name"=>$array['board_array']['post_con_name']
										, "con_category"=>$array['board_array']['post_con_category']
										, "con_skin"=>$array['board_array']['post_con_skin']
										, "con_layout"=>$array['board_array']['post_con_layout']
										, "con_thumb_filter"=>$array['board_array']['post_con_thumb_filter']
										, "con_diff"=>$array['board_array']['post_con_diff']
			                            , "con_list_level"=>$array['board_array']['post_con_list_level']
			                            , "con_content_level"=>$array['board_array']['post_con_content_level']
			                            , "con_write_level"=>$array['board_array']['post_con_write_level']
			                            , "con_reply_level"=>$array['board_array']['post_con_reply_level']
			                            , "ext_title_1"=>$array['board_array']['post_ext_title_1']
			                            , "ext_title_2"=>$array['board_array']['post_ext_title_2']
			                            , "ext_title_3"=>$array['board_array']['post_ext_title_3']
			                            , "ext_title_4"=>$array['board_array']['post_ext_title_4']
			                            , "ext_title_5"=>$array['board_array']['post_ext_title_5']
									);


			$this->BoardModel->SetBoardUpdate($parameters);

			$rows				= $this->db->affected_rows();
			

			if($rows >= 1){

				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/board/board_mod/" . $array['board_array']['request_con_name']);

			} else {

				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/board/board_mod/" . $array['board_array']['request_con_name']);

			}


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function board_remove(){	// 게시판 삭제 완료 2019-05-29

		echo "게시판 삭제 메서드는 생성 된 게시판의 테이블이 더 이상 증량 및 설계 할 필요가 없어질 때 추가한다.";

	}


	public function custom_board_list(){	// 사용자 게시판 목록 2019-06-04


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-26


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			/** 사용자단 메서드 시작 **/
			$rows['start']					= 0;
			$rows['end']					= 10;
			$rows['page']					= $array['search_array']['page'];


			$rows['get_board_list']			= $this->BoardModel->GetCustomBoardList($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name'], @$rows['page'], @$rows['start'], @$rows['end']);
			//$rows['board_count']			= $rows['get_board_list']['result']->num_rows();
			$rows['board_result']			= $rows['get_board_list']['result']->result_array();

			$rows['num']					= $rows['get_board_list']['num'];
			$rows['id']						= $rows['get_board_list']['id'];

			$rows['board_count']			= $this->BoardModel->GetCustomBoardCount($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name']);
			/** 사용자단 메서드 끝 **/


			$parameters				=	array(
											"con_name"=>$array['board_array']['request_con_name']
										);


			$rows['get_board']	= $this->BoardModel->GetBoard($parameters);
			$rows['count']		= $rows['get_board']->num_rows();
			$rows['print']		= $rows['get_board']->row_array();


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/CustomerBoardListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function content_remove(){	// 게시물 본문 삭제 2019-05-31


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-30


			/** 게시판 불러오기 시작 **/
			$rows['get_board_content']		= $this->BoardModel->SetCommonCount($array['board_array']['request_con_name'], "bbs_no", $array['board_array']['request_no']);
			$rows['board_count']			= $rows['get_board_content']->num_rows();
			$rows['board_print']			= $rows['get_board_content']->row_array();
			/** 게시판 불러오기 끝 **/


			/**
				관련 DB 삭제 시작
				$this->db->affected_rows() 문제로
				$this->BoardModel->SetCustomContentDelete 보다 상위에 위치
			 
				1. 사용자단이 따로 존재하는 로직만 선택 삭제 기능을 제공 한다.
			**/


			/** 첨부파일 삭제 시작 **/
			$rows['get_board_files']		= $this->BoardModel->SetCommonCount("file_" . $array['board_array']['request_con_name'], "bbs_no", $array['board_array']['request_no']);
			$rows['file_count']				= $rows['get_board_files']->num_rows();
			$rows['file_result']			= $rows['get_board_files']->result_array();


			if($rows['file_count'] >= 1){

				foreach($rows['file_result'] as $files){

					$this->common->SetBoardFileDelete($array['board_array']['request_con_name'], $files['bbs_no'], $files['file_encrypt_name']);
					$this->BoardModel->SetCommonDelete("file_" . $array['board_array']['request_con_name'], "bbs_no", $files['bbs_no']);

				}

			}
			/** 첨부파일 삭제 끝 **/


			/** 추천수 삭제 시작 **/
			$rows['get_voted_rows']		= $this->BoardModel->SetCommonCount("vote_" . $array['board_array']['request_con_name'], "bbs_no", $array['board_array']['request_no']);
			$rows['voted_count']		= $rows['get_voted_rows']->num_rows();
			$rows['voted_result']		= $rows['get_voted_rows']->result_array();

			if($rows['voted_count'] >= 1){

				foreach($rows['voted_result'] as $voted){

					$this->BoardModel->SetCommonDelete("vote_" . $array['board_array']['request_con_name'], "bbs_no", $voted['bbs_no']);

				}

			}
			/** 추천수 삭제 끝 **/


			/** 댓글 삭제 시작 **/
			$rows['get_board_comments']	= $this->BoardModel->SetCommonCount("reply_" . $array['board_array']['request_con_name'], "bbs_no", $array['board_array']['request_no']);
			$rows['reply_count']		= $rows['get_board_comments']->num_rows();
			$rows['reply_result']		= $rows['get_board_comments']->result_array();

			if($rows['reply_count'] >= 1){

				foreach($rows['reply_result'] as $reply){

					$this->BoardModel->SetCommonDelete("reply_" . $array['board_array']['request_con_name'], "bbs_no" ,$reply['bbs_no']);

				}

			}
			/** 댓글 삭제 끝 **/


			if($rows['board_count'] >= 1){
			
				$this->BoardModel->SetCommonDelete($array['board_array']['request_con_name'], "bbs_no", $array['board_array']['request_no']);

				$rows				= $this->db->affected_rows();


				if($rows >= 1){

					$call_back['return_code'] = "0000";

				} else {

					$call_back['return_code'] = "9999";

				}

			} else {

				$call_back['return_code']	= "9998";

			}


			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}

	}


	public function custom_comment_list(){	// 댓글 목록 2019-05-02


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-26


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			/** 사용자단 메서드 시작 **/
			$rows['start']					= 0;
			$rows['end']					= 10;
			$rows['page']					= $array['search_array']['page'];


			$rows['get_comment_list']		= $this->BoardModel->GetCustomCommentList($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name'], @$rows['page'], @$rows['start'], @$rows['end']);
			//$rows['board_count']			= $rows['get_board_list']['result']->num_rows();
			$rows['comment_result']			= $rows['get_comment_list']['result']->result_array();

			$rows['num']					= $rows['get_comment_list']['num'];
			$rows['id']						= $rows['get_comment_list']['id'];

			$rows['comment_count']			= $this->BoardModel->GetCustomCommentCount($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name']);
			/** 사용자단 메서드 끝 **/


			$parameters				=	array(
											"con_name"=>$array['board_array']['request_con_name']
										);


			$rows['get_board']	= $this->BoardModel->GetBoard($parameters);
			$rows['count']		= $rows['get_board']->num_rows();
			$rows['print']		= $rows['get_board']->row_array();

			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/CustomerCommentListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function comment_remove(){	// 댓글 본문 삭제 2019-05-31


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-30


			/** 댓글 불러오기 시작 **/
			$rows['get_comment_content']	= $this->BoardModel->SetCommonCount("reply_" . $array['board_array']['request_con_name'], "rep_no", $array['board_array']['request_no']);
			$rows['comment_count']			= $rows['get_comment_content']->num_rows();
			$rows['comment_print']			= $rows['get_comment_content']->row_array();
			/** 댓글 불러오기 끝 **/


			if($rows['comment_count'] >= 1){
			
				$this->BoardModel->SetCommonDelete("reply_" . $array['board_array']['request_con_name'], "rep_no", $array['board_array']['request_no']);

				$rows				= $this->db->affected_rows();


				if($rows >= 1){

					$call_back['return_code'] = "0000";

				} else {

					$call_back['return_code'] = "9999";

				}

			} else {

				$call_back['return_code']	= "9998";

			}


			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}

	}


	public function custom_voted_list(){	// 추천/비추천 목록 2019-05-02


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-26


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                 = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			/** 사용자단 메서드 시작 **/
			$rows['start']					= 0;
			$rows['end']					= 10;
			$rows['page']					= $array['search_array']['page'];


			$rows['get_voted_list']			= $this->BoardModel->GetCustomVotedList($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name'], @$rows['page'], @$rows['start'], @$rows['end']);
			//$rows['board_count']			= $rows['get_board_list']['result']->num_rows();
			$rows['voted_result']			= $rows['get_voted_list']['result']->result_array();

			$rows['num']					= $rows['get_voted_list']['num'];
			$rows['id']						= $rows['get_voted_list']['id'];

			$rows['voted_count']			= $this->BoardModel->GetCustomVotedCount($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name']);
			/** 사용자단 메서드 끝 **/


			$parameters				=	array(
											"con_name"=>$array['board_array']['request_con_name']
										);


			$rows['get_board']	= $this->BoardModel->GetBoard($parameters);
			$rows['count']		= $rows['get_board']->num_rows();
			$rows['print']		= $rows['get_board']->row_array();


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/CustomerVotedListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}
	
	
	public function voted_remove(){	// 추천/비추천 삭제 2019-05-31
		
		
		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));
		
		
		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-30
			
			
			/** 댓글 불러오기 시작 **/
			$rows['get_voted']          	= $this->BoardModel->SetCommonCount("vote_" . $array['board_array']['request_con_name'], "vo_no", $array['board_array']['request_no']);
			$rows['voted_count']			= $rows['get_voted']->num_rows();
			$rows['voted_print']			= $rows['get_voted']->row_array();
			/** 댓글 불러오기 끝 **/
			
			
			if($rows['voted_count'] >= 1){
				
				$this->BoardModel->SetCommonDelete("vote_" . $array['board_array']['request_con_name'], "vo_no", $array['board_array']['request_no']);
				
				$rows				= $this->db->affected_rows();
				
				
				if($rows >= 1){
					
					$call_back['return_code'] = "0000";
					
				} else {
					
					$call_back['return_code'] = "9999";
					
				}
				
			} else {
				
				$call_back['return_code']	= "9998";
				
			}
			
			
			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);
			
			
		} else if(isset($array['session_array']['sess_member_id'])){
			
			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");
			
		} else {
			
			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");
			
		}
		
	}


	public function custom_files_list(){	// 파일 목록 2019-05-02


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-26


            /**  기본환경설정 메서드 추가 시작 2019-09-09 **/
            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/


			/** 사용자단 메서드 시작 **/
			$rows['start']					= 0;
			$rows['end']					= 10;
			$rows['page']					= $array['search_array']['page'];


			$rows['get_files_list']			= $this->BoardModel->GetCustomFilesList($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name'], @$rows['page'], @$rows['start'], @$rows['end']);
			//$rows['board_count']			= $rows['get_board_list']['result']->num_rows();
			$rows['files_result']			= $rows['get_files_list']['result']->result_array();

			$rows['num']					= $rows['get_files_list']['num'];
			$rows['id']						= $rows['get_files_list']['id'];

			$rows['files_count']			= $this->BoardModel->GetCustomFilesCount($array['board_array']['request_con_name'], @$array['search_array']['search_type'], @$array['search_array']['search_name']);
			/** 사용자단 메서드 끝 **/


			$parameters				=	array(
											"con_name"=>$array['board_array']['request_con_name']
										);


			$rows['get_board']	= $this->BoardModel->GetBoard($parameters);
			$rows['count']		= $rows['get_board']->num_rows();
			$rows['print']		= $rows['get_board']->row_array();


			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/CustomerFilesListView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}

	}


	public function file_remove(){	// 파일 삭제 2019-06-07


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-30


			/** 파일 불러오기 시작 **/
			$rows['get_files']				= $this->BoardModel->SetCommonCount("file_" . $array['board_array']['request_con_name'], "file_no", $array['board_array']['request_no']);
			$rows['files_count']			= $rows['get_files']->num_rows();
			$rows['files_print']			= $rows['get_files']->row_array();
			/** 파일 불러오기 끝 **/


			if($rows['files_count'] >= 1){


				$this->common->SetBoardFileDelete($array['board_array']['request_con_name'], $rows['files_print']['bbs_no'], $rows['files_print']['file_encrypt_name']);
				$this->BoardModel->SetCommonDelete("file_" . $array['board_array']['request_con_name'], "file_no", $array['board_array']['request_no']);


				$rows				= $this->db->affected_rows();


				if($rows >= 1){

					$call_back['return_code'] = "0000";

				} else {

					$call_back['return_code'] = "9999";

				}

			} else {

				$call_back['return_code']	= "9998";

			}


			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}

	}


	public function download($con_name){


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-04-30


			/** 파일 불러오기 시작 **/
			$rows['get_files']				= $this->BoardModel->SetCommonCount("file_" . $array['board_array']['request_con_name'], "file_no", $array['board_array']['request_no']);
			$rows['files_count']			= $rows['get_files']->num_rows();
			$rows['files_print']			= $rows['get_files']->row_array();
			/** 파일 불러오기 끝 **/

			$path							= file_get_contents(DOCUMENT_ROOT . "/uploaded/" . $con_name . "/" . $rows['files_print']['bbs_no'] . "/" . $rows['files_print']['file_encrypt_name']);

			force_download($rows['files_print']['file_encrypt_name'], $path);

		} else {

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		}

	}

}