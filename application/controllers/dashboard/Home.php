<?php
if(!defined('BASEPATH')) exit;


class Home extends CI_Controller {



	public function __construct(){


		parent::__construct();


		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('Admin');


		$this->load->helper(array('form', 'url'));


		$this->load->model('dashboard/HomeModel');


	}

	
	public function index(){	// index method 2019-01-11


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("2"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config']					= $rows['get_config']->row_array();
			
			
			/** 사업장 정보 가져오기 시작 **/
            $rows['get_company']    	    = $this->HomeModel->GetCompany();
			$rows['company_count']			= $rows['get_company']->num_rows();
			$rows['company']				= $rows['get_company']->row_array();
			/** 사업장 정보 가져오기 끝 **/
			
			
			/** 테이블 사용량 가져오기 시작 **/
			$rows['get_tables']             = $this->admin->GetTables($this->common->GetPath());
			$rows['tables_count']           = $rows['get_tables']->num_rows();
			$rows['tables']                 = $rows['get_tables']->row_array();
			/** 테이블 사용량 가져오기 끝 **/
			
			
			/** 추가 된 게시판 목록 시작 **/
			$rows['get_installed_board']	= $this->HomeModel->GetInstalledBoard();
			$rows['inst_board_count']		= $rows['get_installed_board']->num_rows();
			$rows['inst_board_result']		= $rows['get_installed_board']->result_array();
			/** 추가 된 게시판 목록 끝 **/
			
			
			/** 최신 게시물 불러오기 시작 **/
			$rows['get_boards']		        = $this->HomeModel->GetLatestArticles($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['boards_result']			= $rows['get_boards']->result_array();
			
			$rows['get_boards_count']       = $this->HomeModel->GetLatestArticlesCount($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['boards_count']           = $rows['get_boards']->num_rows();
			/** 최신 게시물 불러오기 끝 **/
			
			
			/** 최신 댓글 불러오기 시작 **/
			$rows['get_replies']	        = $this->HomeModel->GetLatestReplies($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['replies_result']			= $rows['get_replies']->result_array();
			
			$rows['get_replies_count']      = $this->HomeModel->GetLatestRepliesCount($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['replies_count']			= $rows['get_replies_count']->num_rows();
			/** 최신 댓글 불러오기 끝 **/
			
			
			/** 최근 가입 회원 목록 불러오기 시작 **/
			$rows['get_members']	        = $this->HomeModel->GetJoinMembers($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['members_result']			= $rows['get_members']->result_array();
			
			$rows['get_members_count']      = $this->HomeModel->GetJoinMembersCount($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['members_count']			= $rows['get_members_count']->num_rows();
			/** 최근 가입 회원 목록 불러오기 끝 **/
			
			
			/** 최신 파일 불러오기 시작 **/
			$rows['get_files']	            = $this->HomeModel->GetLatestFiles($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['files_result']			= $rows['get_files']->result_array();
			
			$rows['get_files_count']        = $this->HomeModel->GetLatestFiles($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['files_count']			= $rows['get_files_count']->num_rows();
			/** 최신 파일 불러오기 끝 **/
			
			
			/** 최근 추천/비추천 시작 **/
			$rows['get_voted']	            = $this->HomeModel->GetLatestVoted($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['voted_result']			= $rows['get_voted']->result_array();
			
			$rows['get_voted_count']        = $this->HomeModel->GetLatestVoted($rows['inst_board_count'], $rows['inst_board_result']);
			$rows['voted_count']			= $rows['get_voted_count']->num_rows();
			/** 최근 추천/비추천 끝 **/
			

            $array['config']                = $rows['config'];  // 타이틀용

			
			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/IndexView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){

			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function configs(){	// admin configuration 2019-01-29


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


            $rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config']					= $rows['get_config']->row_array();

			$rows['get_layout']				= $this->common->GetDesign("layouts", "10");
			$rows['get_skin']               = $this->common->GetDesign("members", "10");

            $array['config']                = $rows['config'];
            

			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/ConfigsView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	public function configs_ok(){	// admin configuration update 2019-01-31


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


			if(isset($array['config_array']['post_home_title'])){	// 파라미터 체크 2019-02-08


				$array['get_config']			= $this->HomeModel->GetConfig();
				$count							= $array['get_config']->num_rows();
				$print							= $array['get_config']->row_array();


				if($count >= 1){

					$parameters				=	array(
													"home_no"=>$print['home_no']
													, "home_title"=>$array['config_array']['post_home_title']
													, "home_layout"=>$array['config_array']['post_home_layout']
					                                , "member_skin"=>$array['config_array']['post_member_skin']
                                                    , "home_id_blocks"=>$array['config_array']['post_home_id_blocks']
                                                    , "home_name_blocks"=>$array['config_array']['post_home_name_blocks']
												);


					$this->HomeModel->SetConfigUpdate($parameters);

				} else {

					$parameters				=	array(
													"home_no"=>$this->common->SetIntToken()
													, "home_title"=>$array['config_array']['post_home_title']
													, "home_layout"=>$array['config_array']['post_home_layout']
					                                , "member_skin"=>$array['config_array']['post_member_skin']
					                                , "home_id_blocks"=>$array['config_array']['post_home_id_blocks']
                                                    , "home_name_blocks"=>$array['config_array']['post_home_name_blocks']
												);


					$this->HomeModel->SetConfigInsert($parameters);

				}


				$rows					= $this->db->affected_rows();


				if($rows >= 1){

					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");

				} else {

					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");

				}


			} else {

				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");

			}


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function configs_logo_ok(){	// 홈페이지 로고 업로드


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


			$array['get_config']			= $this->HomeModel->GetConfig();
			$count							= $array['get_config']->num_rows();
			$print							= $array['get_config']->row_array();


			/** 파일 업로드 시작 **/
			$config['upload_path']		= DOCUMENT_ROOT . "/uploaded/configs/";
			$config['allowed_types']	= "jpg|jpeg|png|bmp";
			$config['overwirte']		= TRUE;
			$config['encrypt_name']		= TRUE;

			$this->load->library('upload', $config);


			if(!empty($_FILES['home_logo']['tmp_name'])){


				if($this->upload->do_upload('home_logo') >= 1){	// 파일 업로드 갯수가 1개 이상일 때 실행


					if(is_file(DOCUMENT_ROOT . "/uploaded/configs/" . $print['home_logo'])){


						@$this->common->SetFileDelete("configs", "", $print['home_logo']);

						$home_logo_data		= array('home_logo'=>$this->upload->data());	// 상단에 있는 메서드 호출이 선행 되어야 해서 각 조건문에 $this->upload->data() 메서드를 넣는다


					} else {


						$home_logo_data		= array('home_logo'=>$this->upload->data());	// 상단에 있는 메서드 호출이 선행 되어야 해서 각 조건문에 $this->upload->data() 메서드를 넣는다


					}


				} else {


					$error					= strip_tags($this->upload->display_errors());

					echo $this->common->move_error($error, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");


				}


			}
			/** 파일 업로드 끝 **/


			$parameters				=	array(
											"home_no"=>$print['home_no']
											, "home_logo"=>$home_logo_data['home_logo']['file_name']	// application/uploaded/configs/ 디렉토리에 암호화 되어 업로드 된 파일명을 얻는다.
										);


			$this->HomeModel->SetImageUpdate($parameters);


			$rows					= $this->db->affected_rows();


			if($rows >= 1){

				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");

			} else {

				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");

			}


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	public function configs_icon_ok(){	// 홈페이지 아이콘 업로드


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


			$array['get_config']			= $this->HomeModel->GetConfig();
			$count							= $array['get_config']->num_rows();
			$print							= $array['get_config']->row_array();


			/** 파일 업로드 시작 **/
			$config['upload_path']		= DOCUMENT_ROOT . "/uploaded/configs/";
			$config['allowed_types']	= "ico";
			$config['overwirte']		= TRUE;
			$config['encrypt_name']		= TRUE;
			$config['max_size']			= 100;
			$config['max_width']		= 16;
			$config['max_height']		= 16;

			$this->load->library('upload', $config);


			if(!empty($_FILES['home_icon']['tmp_name'])){


				if($this->upload->do_upload('home_icon') >= 1){	// 파일 업로드 갯수가 1개 이상일 때 실행


					if(is_file(DOCUMENT_ROOT . "/uploaded/configs/" . $print['home_icon'])){


						@$this->common->SetFileDelete("configs", "", $print['home_icon']);

						$home_icon_data		= array('home_icon'=>$this->upload->data());	// 상단에 있는 메서드 호출이 선행 되어야 해서 각 조건문에 $this->upload->data() 메서드를 넣는다


					} else {


						$home_icon_data		= array('home_icon'=>$this->upload->data());	// 상단에 있는 메서드 호출이 선행 되어야 해서 각 조건문에 $this->upload->data() 메서드를 넣는다


					}


				} else {


					$error					= strip_tags($this->upload->display_errors());

					echo $this->common->move_error($error, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");


				}


			}
			/** 파일 업로드 끝 **/


			$parameters				=	array(
											"home_no"=>$print['home_no']
											, "home_icon"=>$home_icon_data['home_icon']['file_name']	// application/uploaded/configs/ 디렉토리에 암호화 되어 업로드 된 파일명을 얻는다.
										);

			$this->HomeModel->SetImageUpdate($parameters);


			$rows					= $this->db->affected_rows();


			if($rows >= 1){

				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");

			} else {

				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");

			}


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	/**
	public function configs_file_remove(){	// 홈페이지 로고 파일 삭제 해당 메서드 삭제 2020-03-04


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


			$array['get_config']			= $this->HomeModel->GetConfig();
			$count							= $array['get_config']->num_rows();
			$print							= $array['get_config']->row_array();


			$home_logo						= (!empty($array['config_array']['post_home_logo'])?"":@$array['config_array']['post_home_logo']);
			$home_icon						= (!empty($array['config_array']['post_home_icon'])?"":@$array['config_array']['post_home_icon']);


			$parameters						=	array(
													"home_no"=>$print['home_no']
													, "home_logo"=>@$home_logo
													, "home_icon"=>@$home_icon
												);


			$this->HomeModel->SetImageUpdate($parameters);
			exit;

			$rows							= $this->db->affected_rows();


			if($rows >= 1){

				@$this->common->SetFileDelete($array['admin_menu_array']['configs'], "", $array['config_array']['post_home_logo'] . $array['config_array']['post_home_icon'] );	// 로고파일 아니면 아이콘 파일의 받은 파라미터를 받아 해당 파일을 삭제 한다.

				$call_back['return_code']	= "0000";

			} else {

				$call_back['return_code']	= "9999";

			}


			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}
	**/
	
	
	public function configs_smtp_ok(){	// admin smtp 설정 2020-03-04


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$array['get_config']			= $this->HomeModel->GetConfig();
			$count							= $array['get_config']->num_rows();
			$print							= $array['get_config']->row_array();
			
				
			$parameters				        =	array(
													  "home_no"=>$print['home_no']
													, "home_email_1"=>$this->common->GetEncrypt(@$array['config_array']['post_home_email_1'], @$secret_key, @$secret_iv)
													, "home_email_2"=>$this->common->GetEncrypt(@$array['config_array']['post_home_email_2'], @$secret_key, @$secret_iv)
													, "home_email_password"=>$this->common->GetEncrypt(@$array['config_array']['post_home_email_password'], @$secret_key, @$secret_iv)
													, "home_email_host"=>$this->common->GetEncrypt(@$array['config_array']['post_home_email_host'], @$secret_key, @$secret_iv)
												);
				
				
			$this->HomeModel->SetSmtpUpdate($parameters);
			
			
			$rows					= $this->db->affected_rows();
			
			
			if($rows >= 1){
				
				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");
				
			} else {
				
				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");
				
			}

		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}
	
	
	public function configs_meta_ok(){	// admin smtp 설정 2020-03-04


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			
			
			$array['get_config']			= $this->HomeModel->GetConfig();
			$count							= $array['get_config']->num_rows();
			$print							= $array['get_config']->row_array();
			
			$parameters				        =	array(
			                                        "type"=>$this->input->post('type')
													, "home_no"=>$print['home_no']
													, "home_naver"=>@$array['config_array']['post_home_naver']
													, "home_google"=>@$array['config_array']['post_home_google']
												);
				
			$this->HomeModel->SetMetaUpdate($parameters);
			
			$rows					= $this->db->affected_rows();
			
			if($rows >= 1){
				
				echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");
				
			} else {
				
				echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/configs");
				
			}

		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");

		}


	}


	public function companies(){	// admin configuration 2019-01-29


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08
			

		    $rows['get_company']    	    = $this->HomeModel->GetCompany();
			$rows['company_count']			= $rows['get_company']->num_rows();
			$rows['company']				= $rows['get_company']->row_array();

   
			/**  기본환경설정 메서드 추가 시작 2019-09-09 **/
			$rows['get_config']			    = $this->HomeModel->GetConfig();
            $rows['config_count']			= $rows['get_config']->num_rows();
            $rows['config'] 				= $rows['get_config']->row_array();

            $array['config']                = $rows['config'];
            /**  기본환경설정 메서드 추가 끝 2019-09-09 **/
            

			$this->load->view('dashboard/HeaderView', $array);
			$this->load->view('dashboard/CompaniesView', $rows);
			$this->load->view('dashboard/FooterView');


		} else if(isset($array['session_array']['sess_member_id'])){


			echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/dashboard/home/");


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");


		}


	}


	public function companies_ok(){	// admin configuration update 2019-02-13


		$array					= $this->admin->GetParameter();
		$permission_check		= $this->admin->CheckPermission($array['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("3"));


		if($permission_check->num_rows() >= 1){	// 로그인 여부 및 권한 체크 2019-02-08


			if(isset($array['config_array']['post_com_name'])){


				$array['company_print_value']	= $this->HomeModel->GetCompany();
				$count							= $array['company_print_value']->num_rows();
				$print							= $array['company_print_value']->row_array();


				if($count >= 1){

				
					$parameters					=	array(
														'com_no'=>$print['com_no']
														,'com_name'=>$this->common->GetEncrypt(@$array['config_array']['post_com_name'], @$secret_key, @$secret_iv)
														, 'com_owner'=>$this->common->GetEncrypt(@$array['config_array']['post_com_owner'], @$secret_key, @$secret_iv)
														, 'com_rotary_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_rotary_1'], @$secret_key, @$secret_iv)
														, 'com_rotary_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_rotary_2'], @$secret_key, @$secret_iv)
														, 'com_rotary_3'=>$this->common->GetEncrypt(@$array['config_array']['post_com_rotary_3'], @$secret_key, @$secret_iv)
														, 'com_fax_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_fax_1'], @$secret_key, @$secret_iv)
														, 'com_fax_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_fax_2'], @$secret_key, @$secret_iv)
														, 'com_fax_3'=>$this->common->GetEncrypt(@$array['config_array']['post_com_fax_3'], @$secret_key, @$secret_iv)
														, 'com_zip_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_zip_1'], @$secret_key, @$secret_iv)
														, 'com_zip_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_zip_2'], @$secret_key, @$secret_iv)
														, 'com_address_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_address_1'], @$secret_key, @$secret_iv)
														, 'com_address_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_address_2'], @$secret_key, @$secret_iv)
														, 'com_manager'=>$this->common->GetEncrypt(@$array['config_array']['post_com_manager'], @$secret_key, @$secret_iv)
														, 'com_manager_email_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_manager_email_1'], @$secret_key, @$secret_iv)
														, 'com_manager_email_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_manager_email_2'], @$secret_key, @$secret_iv)
													);

					$this->HomeModel->SetCompanyUpdate($parameters);


				} else {


					$parameters					=	array(
														'com_no'=>$this->common->SetIntToken()
														,'com_name'=>$this->common->GetEncrypt(@$array['config_array']['post_com_name'], @$secret_key, @$secret_iv)
														, 'com_owner'=>$this->common->GetEncrypt(@$array['config_array']['post_com_owner'], @$secret_key, @$secret_iv)
														, 'com_rotary_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_rotary_1'], @$secret_key, @$secret_iv)
														, 'com_rotary_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_rotary_2'], @$secret_key, @$secret_iv)
														, 'com_rotary_3'=>$this->common->GetEncrypt(@$array['config_array']['post_com_rotary_3'], @$secret_key, @$secret_iv)
														, 'com_fax_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_fax_1'], @$secret_key, @$secret_iv)
														, 'com_fax_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_fax_2'], @$secret_key, @$secret_iv)
														, 'com_fax_3'=>$this->common->GetEncrypt(@$array['config_array']['post_com_fax_3'], @$secret_key, @$secret_iv)
														, 'com_zip_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_zip_1'], @$secret_key, @$secret_iv)
														, 'com_zip_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_zip_2'], @$secret_key, @$secret_iv)
														, 'com_address_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_address_1'], @$secret_key, @$secret_iv)
														, 'com_address_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_address_2'], @$secret_key, @$secret_iv)
														, 'com_manager'=>$this->common->GetEncrypt(@$array['config_array']['post_com_manager'], @$secret_key, @$secret_iv)
														, 'com_manager_email_1'=>$this->common->GetEncrypt(@$array['config_array']['post_com_manager_email_1'], @$secret_key, @$secret_iv)
														, 'com_manager_email_2'=>$this->common->GetEncrypt(@$array['config_array']['post_com_manager_email_2'], @$secret_key, @$secret_iv)
													);

					$this->HomeModel->SetCompanyInsert($parameters);


				}


				$rows					= $this->db->affected_rows();


				if($rows >= 1){


					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/companies");


				} else {


					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/dashboard/home/companies");


				}


			} else {


				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/dashboard/home/companies");


			}


		} else {


			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST . "/dashboard/member/signin");	// KO_NEEDLOGIN


		}


	}


}