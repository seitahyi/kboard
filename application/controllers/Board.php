<?php
if(!defined('BASEPATH')) exit;


class Board extends CI_Controller {


	public function __construct(){

		parent::__construct();

		$this->load->library('Common');
		$this->load->library('session');
		$this->load->library('User');

		$this->load->helper(array('form', 'url', 'cookie', 'download'));

		$this->load->model('BoardModel');
        $this->load->model('MemberModel');

	}


	public function lists(){	// 게시판목록 2019-05-14
		
		
        $array['get_parameter']			= $this->user->GetParameter();


		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']			= $this->user->GetConfig();
		$array['config_count']			= $array['get_config']['config']->num_rows();
		$array['config']			    = $array['get_config']['config']->row_array();
		$array['company_count']			= $array['get_config']['company']->num_rows();
		$array['company']			    = $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']) {
			
			
			if ($array['board_config_print']['con_list_level'] == "0" || $array['board_config_print']['con_list_level'] <= $array['get_parameter']['session_array']['sess_member_level']) {
				
				
				/** 게시판 불러오기 시작 **/
				$rows['start']          = 0;
				$rows['end']            = 15;
				$rows['page']           = $array['get_parameter']['search_array']['page'];
				
				
				$rows['get_board_list'] = $this->BoardModel->GetBoardList($array['board_config_print']['con_name'], @$array['get_parameter']['search_array']['search_type'], @$array['get_parameter']['search_array']['search_name'], @$rows['page'], @$rows['start'], @$rows['end']);
				$rows['board_result']   = $rows['get_board_list']['result']->result_array();
				
				$rows['num']            = $rows['get_board_list']['num'];
				$rows['id']             = $rows['get_board_list']['id'];
				
				$rows['board_count']    = $this->BoardModel->GetBoardCount($array['board_config_print']['con_name'], @$array['get_parameter']['search_array']['search_type'], @$array['get_parameter']['search_array']['search_name']);
				/** 게시판 불러오기 끝 **/
				
				
				$rows['bbs_category']   = explode("|", $array['board_config_print']['con_category']);
				
				
				$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
				$this->load->view("skin/boards/" . $array['board_config_print']['con_skin'] . "/ListView", $rows);
				$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
				
				
			} else {
				
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST);
				
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function contents(){	// 게시판 본문 2019-05-15


		$array['get_parameter']			= $this->user->GetParameter();
		$array['permission_check']      = $this->user->CheckPermission($array['get_parameter']['session_array']['sess_member_id'], $this->common->SetDirectoryExplode("2"));


		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']			= $this->user->GetConfig();
		$array['config_count']			= $array['get_config']['config']->num_rows();
		$array['config']			    = $array['get_config']['config']->row_array();
		$array['company_count']			= $array['get_config']['company']->num_rows();
		$array['company']		    	= $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){
			

			if($array['board_config_print']['con_content_level'] == "0" || $array['board_config_print']['con_content_level'] <= $array['get_parameter']['session_array']['sess_member_level']) {
				
				
				/** 게시판 불러오기 시작 **/
				$rows['get_board_content']		= $this->BoardModel->GetBoardContent($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
				$rows['board_count']			= $rows['get_board_content']->num_rows();
				$rows['board_print']			= $rows['get_board_content']->row_array();
				/** 게시판 불러오기 끝 **/
				
				
				$array['board_print']           = $rows['board_print']; // 게시판 타이틀 출력
				
				
				if($rows['board_count'] >= 1) {
					
					
					/** 첨부파일 가져오기 시작 **/
					$rows['get_board_files']    = $this->BoardModel->GetBoardFiles($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['board_array']['request_file']);
					$rows['file_count']         = $rows['get_board_files']->num_rows();
					$rows['file_result']        = $rows['get_board_files']->result_array();
					/** 첨부파일 가져오기 끝 **/
					
					
					/** 쿠키 시작 **/
					$array['readed']            = array("name"=>"readed_" . $array['board_config_print']['con_name'] . "_".$array['get_parameter']['board_array']['request_no'], "value"=>"Y", "expire"=>"86400");
					
					$this->input->set_cookie($array['readed']);
					
					$rows['readed']             = get_cookie($array['readed']['name']);
					
					
					if(is_null($rows['readed'])){
						
						$this->BoardModel->SetReadUpdate($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $rows['board_print']['bbs_readed']);
						
					}
					/** 쿠키 끝 **/
					
					
					/** 추천/비추천 메서드 끝 2019-05-18 **/
					$rows['check_like']         = $this->BoardModel->CheckVoted($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id']);
					$rows['like_count']         = $rows['check_like']->num_rows();
					
					$rows['get_voted']          = $this->BoardModel->GetVoted($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
					$rows['voted_count']        = $rows['get_voted']->num_rows();
					$rows['voted_print']        = $rows['get_voted']->row_array();
					/** 추천/비추천 메서드 끝 2019-05-18 **/
					
					
					/** 댓글 전용 페이징 시작 **/
					$rows['start']              = 0;
					$rows['end']                = 15;
					$rows['page']               = $array['get_parameter']['search_array']['page'];
					
					$rows['get_reply']          = $this->BoardModel->GetCommentList($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $rows['page'], $rows['start'], $rows['end']);
					$rows['reply_count']        = $rows['get_reply']['reply_result']->num_rows();
					$rows['reply_result']       = $rows['get_reply']['reply_result']->result_array();
					
					$rows['reply_num']          = $rows['get_reply']['reply_num'];
					
					$rows['reply_id']           = $rows['get_reply']['reply_id'];
					$rows['reply_all_count']    = $this->BoardModel->GetCommentCount($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
					/** 댓글 전용 페이징 끝 **/
					
					
					if ($rows['board_print']['bbs_secret'] == "Y") {
						
						
						if ($array['permission_check']->num_rows() >= 1 || $rows['board_print']['member_id'] == $array['get_parameter']['session_array']['sess_member_id']) {
							
							
							$rows['bbs_category'] = explode("|", $array['board_config_print']['con_category']);
							
							
							$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
							$this->load->view("skin/boards/" . $array['board_config_print']['con_skin'] . "/ContentView", $rows);
							$this->load->view("skin/boards/" . $array['board_config_print']['con_skin'] . "/CommentView", $rows);
							$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
							
						} else {
							
							echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
							
						}
						
					} else if ($rows['board_print']['bbs_secret'] == "N") {
						
						$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
						$this->load->view("skin/boards/" . $array['board_config_print']['con_skin'] . "/ContentView", $rows);
						$this->load->view("skin/boards/" . $array['board_config_print']['con_skin'] . "/CommentView", $rows);
						$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
						
					}
					
					
				} else {
					
					echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
					
				}
			
			
			} else {
				
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
				
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}


	}


	public function remove(){	// 게시판 본문 삭제 2019-05-17

		
		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		

		/** 게시판 불러오기 시작 **/
		$rows['get_board_content']		= $this->BoardModel->GetBoardContent($array['get_parameter']['board_array']['request_con_name'], $array['get_parameter']['board_array']['request_no']);
		$rows['board_count']			= $rows['get_board_content']->num_rows();
		$rows['board_print']			= $rows['get_board_content']->row_array();
		/** 게시판 불러오기 끝 **/


		if($rows['board_count'] >= 1){

	
			if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){

				
				//$this->BoardModel->GetVoted($array['get_parameter']['board_array']['request_con_name'], $array['get_parameter']['board_array']['request_no']); ?? 이건 뭔지 모르겠음 2019-10-21

				/**
					관련 DB 삭제 시작
					$this->db->affected_rows() 문제로 
					$this->BoardModel->SetContentDelete 보다 상위에 위치
				**/

				/** 첨부파일 삭제 시작 **/
				$rows['get_files']		    = $this->BoardModel->GetBoardFiles($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], NULL);
				$rows['file_count']			= $rows['get_files']->num_rows();
				$rows['file_result']		= $rows['get_files']->result_array();

				if($rows['file_count'] >= 1){

					foreach($rows['file_result'] as $files){

						$this->common->SetBoardFileDelete($array['board_config_print']['con_name'], $files['bbs_no'], $files['file_encrypt_name']);
						$this->BoardModel->SetFileDelete($array['board_config_print']['con_name'], $files['file_no'], $files['bbs_no'], $array['get_parameter']['session_array']['sess_member_id']);

					}

				}
				/** 첨부파일 삭제 끝 **/


				/** 추천수 삭제 시작 **/
				$rows['get_voted']		= $this->BoardModel->GetVotedRow($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
				$rows['voted_count']	= $rows['get_voted']->num_rows();
				$rows['voted_result']	= $rows['get_voted']->result_array();

				if($rows['voted_count'] >= 1){

					foreach($rows['voted_result'] as $voted){

						$this->BoardModel->SetVotedDelete($array['board_config_print']['con_name'], $voted['bbs_no']);

					}

				}
				/** 추천수 삭제 끝 **/
				
				
				/** 댓글 삭제 시작 **/
				$rows['get_comments']       = $this->BoardModel->GetCommentCount($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
				$rows['comments_count']     = $rows['get_comments']->num_rows();
				$rows['comments_result']    = $rows['get_comments']->result_array();
				
				if($rows['comments_count'] >= 1){
					
					foreach($rows['comments_result'] as $comments){
						
						$this->BoardModel->SetCommentRemove($array['board_config_print']['con_name'], $comments['rep_no'], $comments['bbs_no'], $comments['member_id']);
					
					}
					
				}
				
				/** 댓글 삭제 끝 **/
				
				/** 관련 DB 삭제 끝 **/


				$this->BoardModel->SetContentDelete($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id']);

				$rows					= $this->db->affected_rows();

				if($rows >= 1){

					$call_back['return_code']	= "0000";

				} else {

					$call_back['return_code']	= "9999";

				}


			} else {	// 권한이 없음

				$call_back['return_code']	= "9998";

			}


			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴


		} else {

			echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST);

		}

	}


	public function write(){	// 글쓰기 2019-05-20


		$array['get_parameter']			= $this->user->GetParameter();


		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']			= $this->user->GetConfig();
		$array['config_count']			= $array['get_config']['config']->num_rows();
		$array['config']    			= $array['get_config']['config']->row_array();
		$array['company_count']			= $array['get_config']['company']->num_rows();
		$array['company']   			= $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		$rows['bbs_category']           = explode("|", $array['board_config_print']['con_category']);
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']) {
			
			
			if (isset($array['get_parameter']['session_array']['sess_member_id'])) {
				
				
				if ($array['board_config_print']['con_write_level'] == "0" || $array['board_config_print']['con_write_level'] <= $array['get_parameter']['session_array']['sess_member_level']) {
					
					
					/**
					게시글 작성 간격 메서드 시작
					1. $array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name'] 조건문 실행이 되지 않으면 CheckContDiff 메서드에 $array['get_parameter']['board_array']['request_con_name'] 원소를 받지 못해 에러가 나기 때문에 여기에 위체 시킴
					**/
					$check_diff = $this->BoardModel->CheckContDiff($array['get_parameter']['board_array']['request_con_name'], $array['get_parameter']['session_array']['sess_member_id']);
					$diff_count = $check_diff->num_rows();
					$diff_rows = $check_diff->row_array();
					/** 게시글 작성 간격 메서드 끝 **/
					
					
					if (strtotime(date("Y-m-d H:i:s")) - strtotime($diff_rows['bbs_date']) >= $array['board_config_print']['con_diff']) {
						
						$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
						$this->load->view("skin/boards/" . $array['board_config_print']['con_skin'] . "/WriteView", $rows);
						$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
						
					} else {
						
						echo $this->common->move_error(KO_NOTNOW, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
						
					}
					
				} else {
					
					echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
					
				}
				
				
			} else {
				
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/member/signin");
				
			}
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function write_ok(){	// 글쓰기 확인 2019-05-20


		$array['get_parameter']			= $this->user->GetParameter();


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']) {


			if(isset($array['get_parameter']['session_array']['sess_member_id'])){
				
				
				/**
				게시글 작성 간격 메서드 시작
				1. $array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name'] 조건문 실행이 되지 않으면 CheckContDiff 메서드에 $array['get_parameter']['board_array']['request_con_name'] 원소를 받지 못해 에러가 나기 때문에 여기에 위체 시킴
				**/
				$check_diff				= $this->BoardModel->CheckContDiff($array['board_config_print']['con_name'], $array['get_parameter']['session_array']['sess_member_id']);
				$diff_count				= $check_diff->num_rows();
				$diff_rows				= $check_diff->row_array();
				/** 게시글 작성 간격 메서드 끝 **/
	
	
				if(strtotime(date("Y-m-d H:i:s")) - strtotime($diff_rows['bbs_date']) >= $array['board_config_print']['con_diff']){
					
					
					$extra_desc_1       = (!empty($array['get_parameter']['board_array']['request_ext_desc_1'])) ? $array['get_parameter']['board_array']['request_ext_desc_1'] : "";
					$extra_desc_2       = (!empty($array['get_parameter']['board_array']['request_ext_desc_2'])) ? $array['get_parameter']['board_array']['request_ext_desc_2'] : "";
					$extra_desc_3       = (!empty($array['get_parameter']['board_array']['request_ext_desc_3'])) ? $array['get_parameter']['board_array']['request_ext_desc_3'] : "";
					$extra_desc_4       = (!empty($array['get_parameter']['board_array']['request_ext_desc_3'])) ? $array['get_parameter']['board_array']['request_ext_desc_4'] : "";
					$extra_desc_5       = (!empty($array['get_parameter']['board_array']['request_ext_desc_3'])) ? $array['get_parameter']['board_array']['request_ext_desc_5'] : "";
	
	
					$parameters			=	array(
												"member_id"=>$array['get_parameter']['session_array']['sess_member_id']
												, "member_name"=>$this->common->GetDecrypt($array['get_parameter']['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
												, "bbs_title"=>$array['get_parameter']['board_array']['request_bbs_title']
												, "bbs_category"=>($array['get_parameter']['board_array']['request_bbs_category']!="")?$array['get_parameter']['board_array']['request_bbs_category']:""
												, "bbs_content"=>preg_replace($this->common->CheckFilter(), "", $array['get_parameter']['board_array']['request_bbs_content'])
					                            , "bbs_secret"=>($array['get_parameter']['board_array']['request_bbs_secret']=="on")?"Y":"N"
												, "ext_desc_1"=>$extra_desc_1
												, "ext_desc_2"=>$extra_desc_2
												, "ext_desc_3"=>$extra_desc_3
												, "ext_desc_4"=>$extra_desc_4
												, "ext_desc_5"=>$extra_desc_5
											);
	
	
					$this->BoardModel->SetBoardInsert($array['board_config_print']['con_name'], $parameters);
					
	
					$last_id		    = $this->db->insert_id();	// last_insert 값은 추후 트랜잭션을 위해 변수에 담아 놓는다.
					$rows				= $this->db->affected_rows();
	
					if($rows >= 1){
	
	
						/** 파일첨부 기능 시작 **/
						$file_count						= count(@$_FILES['file_name']['name']);
	
						$config['upload_path']			= $this->common->SetDirectory($array['board_config_print']['con_name'], $last_id);
						$config['allowed_types']		= $array['board_config_print']['con_thumb_filter'];
	
	
						if($file_count >= 1){
	
							for($i = 0; $i < $file_count; $i++){
	
								$file_move					= @$this->common->SetFileMove($config, $_FILES['file_name']['name'][$i], $_FILES['file_name']['tmp_name'][$i], $array['get_parameter']['session_array']['sess_member_id']);	// 파일 이동 메서드
	
								$file_name					= $file_move['file_name'];
								$file_tmp_name				= $file_move['file_tmp_name'];
								$encrypt_name				= $file_move['encrypt_name'];
	
								@$this->BoardModel->SetFilesInsert($array['board_config_print']['con_name'], $last_id, $array['get_parameter']['session_array']['sess_member_id'], $encrypt_name,  $file_name, 0, 0, $_FILES['file_name']['type'][$i]);	// 파일 인서트 메서드
	
							}
	
						}
						/** 파일첨부 기능 끝 **/
	
	
						echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $last_id);
	
					} else {
	
						echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
	
					}
	
				
				} else {
	
					echo $this->common->move_error(KO_NOTNOW, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
				}
	
	
			} else {
	
				echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function tmp_write_ok(){	// 글쓰기 임시 저장 확인 2019-06-26


		$array['get_parameter']			= $this->user->GetParameter();


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/


		if(isset($array['get_parameter']['session_array']['sess_member_id'])){
			
			
			/** 게시글 작성 간격 메서드 시작 **/
			$check_diff						= $this->BoardModel->CheckContDiff($array['board_config_print']['con_name'], $array['get_parameter']['session_array']['sess_member_id']);
			$diff_count						= $check_diff->num_rows();
			$diff_rows						= $check_diff->row_array();
			/** 게시글 작성 간격 메서드 끝 **/


			if(strtotime(date("Y-m-d H:i:s")) - strtotime($diff_rows['bbs_date']) >= $array['board_config_print']['con_diff']){


				if(empty($array['get_parameter']['board_array']['request_bbs_title'])){

					$tmp_title				= $this->common->GetDecrypt($array['get_parameter']['session_array']['sess_member_name'], @$secret_key, @$secret_iv) . "님의 임시 게시물";

				} else {

					$tmp_title				= $array['get_parameter']['board_array']['request_bbs_title'];

				}


				if(empty($array['get_parameter']['board_array']['request_bbs_content'])){

					$tmp_content			= $this->common->GetDecrypt($array['get_parameter']['session_array']['sess_member_name'], @$secret_key, @$secret_iv) . "님이 임시 게시물";

				} else {

					$tmp_content			= preg_replace($this->common->CheckFilter(), "", $array['get_parameter']['board_array']['request_bbs_content']);

				}


				$parameters					=	array(
													"member_id"=>$array['get_parameter']['session_array']['sess_member_id']
													, "member_name"=>$this->common->GetDecrypt($array['get_parameter']['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
													, "bbs_title"=>$tmp_title
													, "bbs_category"=>""
													, "bbs_content"=>$tmp_content
				                                    , "bbs_secret"=>($array['get_parameter']['board_array']['request_bbs_secret']=="on")?"Y":"N"
													, "ext_desc_1"=>""
													, "ext_desc_2"=>""
													, "ext_desc_3"=>""
													, "ext_desc_4"=>""
													, "ext_desc_5"=>""
												);


				$this->BoardModel->SetBoardInsert($array['board_config_print']['con_name'], $parameters);

				$last_id					= $this->db->insert_id();	// last_insert 값은 추후 트랜잭션을 위해 변수에 담아 놓는다.
				$rows						= $this->db->affected_rows();

				if($rows >= 1){

					$call_back['return_code']	= "0000";
					$call_back['last_id']		= $last_id;

				} else {

					$call_back['return_code']	= "9999";

				}

			} else {

				$call_back['return_code']	= "9998";	// 글을 작성 할 시간이 지나지 않음.

			}

			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);

		} else {

			echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);

		}

	}


	public function modify(){	// 글수정 2019-05-20


		$array['get_parameter']			= $this->user->GetParameter();


		/** 기본환경설정 및 회사정보 시작 **/
		$array['get_config']			= $this->user->GetConfig();
		$array['config_count']			= $array['get_config']['config']->num_rows();
		$array['config']			    = $array['get_config']['config']->row_array();
		$array['company_count']			= $array['get_config']['company']->num_rows();
		$array['company']			    = $array['get_config']['company']->row_array();
		/** 기본환경설정 및 회사정보 끝 **/


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){


			/** 게시판 불러오기 시작 **/
			$rows['get_board_content']		= $this->BoardModel->GetBoardContent($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
			$rows['board_count']			= $rows['get_board_content']->num_rows();
			$rows['board_print']			= $rows['get_board_content']->row_array();
			/** 게시판 불러오기 끝 **/
			
			
			$rows['bbs_category']           = explode("|", $array['board_config_print']['con_category']);
	
	
			if($array['get_parameter']['session_array']['sess_member_id'] == $rows['board_print']['member_id']){
	
	
				if(isset($rows['board_count'])){
	
					
					/** 첨부파일 가져오기 시작 **/
					$rows['get_board_files']		= $this->BoardModel->GetBoardFiles($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['board_array']['request_file']);
					$rows['file_count']				= $rows['get_board_files']->num_rows();
					$rows['file_result']			= $rows['get_board_files']->result_array();
					/** 첨부파일 가져오기 끝 **/
	
	
					$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/HeaderView", $array);
					$this->load->view("skin/boards/" . $array['board_config_print']['con_skin']. "/ModifyView", $rows);
					$this->load->view("skin/layouts/" . $array['config']['home_layout'] . "/FooterView", $array);
	
				} else {
	
					echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
				}
	
	
			} else {
	
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function modify_ok(){	// 글수정 확인 2019-05-20


		$array['get_parameter']			= $this->user->GetParameter();


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){
			

			/** 게시판 불러오기 시작 **/
			$rows['get_board_content']		= $this->BoardModel->GetBoardContent($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
			$rows['board_count']			= $rows['get_board_content']->num_rows();
			$rows['board_print']			= $rows['get_board_content']->row_array();
			/** 게시판 불러오기 끝 **/
	
	
			if($array['get_parameter']['session_array']['sess_member_id'] == $rows['board_print']['member_id']){
				
				
				$extra_desc_1       = (!empty($array['get_parameter']['board_array']['request_ext_desc_1'])) ? $array['get_parameter']['board_array']['request_ext_desc_1'] : "";
				$extra_desc_2       = (!empty($array['get_parameter']['board_array']['request_ext_desc_2'])) ? $array['get_parameter']['board_array']['request_ext_desc_2'] : "";
				$extra_desc_3       = (!empty($array['get_parameter']['board_array']['request_ext_desc_3'])) ? $array['get_parameter']['board_array']['request_ext_desc_3'] : "";
				$extra_desc_4       = (!empty($array['get_parameter']['board_array']['request_ext_desc_3'])) ? $array['get_parameter']['board_array']['request_ext_desc_4'] : "";
				$extra_desc_5       = (!empty($array['get_parameter']['board_array']['request_ext_desc_3'])) ? $array['get_parameter']['board_array']['request_ext_desc_5'] : "";
				
	
				$parameters         =	array(
				                            "bbs_no"=>$array['get_parameter']['board_array']['request_no']
											, "member_id"=>$array['get_parameter']['session_array']['sess_member_id']
											, "member_name"=>$this->common->GetDecrypt($array['get_parameter']['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
											, "bbs_title"=>$array['get_parameter']['board_array']['request_bbs_title']
											, "bbs_category"=>$array['get_parameter']['board_array']['request_bbs_category']
											, "bbs_content"=>$array['get_parameter']['board_array']['request_bbs_content']
											, "bbs_secret"=>($array['get_parameter']['board_array']['request_bbs_secret']=="on")?"Y":"N"
											, "ext_desc_1"=>$extra_desc_1
											, "ext_desc_2"=>$extra_desc_2
											, "ext_desc_3"=>$extra_desc_3
											, "ext_desc_4"=>$extra_desc_4
											, "ext_desc_5"=>$extra_desc_5
										);
	
	
				$this->BoardModel->SetBoardModify($array['board_config_print']['con_name'], $parameters);
	
				$rows               = $this->db->affected_rows();
	
				if($rows >= 1){
	
	
					/** 파일첨부 기능 시작 **/
					$file_count						= count(@$_FILES['file_name']['name']);
	
					$config['upload_path']			= $this->common->SetDirectory($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
					$config['allowed_types']		= $array['board_config_print']['con_thumb_filter'];
	
	
					if($file_count >= 1){
	
						for($i = 0; $i < $file_count; $i++){
	
							$file_move					= $this->common->SetFileMove($config, $_FILES['file_name']['name'][$i], $_FILES['file_name']['tmp_name'][$i], $array['get_parameter']['session_array']['sess_member_id']);	// 파일 이동 메서드
	
							$file_name					= $file_move['file_name'];
							$file_tmp_name				= $file_move['file_tmp_name'];
							$encrypt_name				= $file_move['encrypt_name'];
	
							$this->BoardModel->SetFilesInsert($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id'], $encrypt_name,  $file_name, 0, 0, $_FILES['file_name']['type'][$i]);	// 파일 인서트 메서드
	
						}
	
					}
					/** 파일첨부 기능 끝 **/
	
	
					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
	
				} else {
	
					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
	
				}
	
			} else {
	
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
			}
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function comment_write(){


		$array['get_parameter']			= $this->user->GetParameter();


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){
			

			if(isset($array['get_parameter']['session_array']['sess_member_id'])){
				
				
				if($array['board_config_print']['con_reply_level'] == "0" || $array['board_config_print']['con_reply_level'] <= $array['get_parameter']['session_array']['sess_member_level']) {
				
				
					/** 게시글 작성 간격 메서드 시작 **/
					$check_diff					= $this->BoardModel->CheckComDiff($array['board_config_print']['con_name'], $array['get_parameter']['session_array']['sess_member_id']);
					$diff_count					= $check_diff->num_rows();
					$diff_rows					= $check_diff->row_array();
					/** 게시글 작성 간격 메서드 끝 **/
					
					
					if(strtotime(date("Y-m-d H:i:s")) - strtotime($diff_rows['rep_date']) >= $array['board_config_print']['con_diff']){
						
						
						$get_parent_comment_count	= $this->BoardModel->GetParentCommentMaxCount($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
						$parent_print				= $get_parent_comment_count->row_array();
						$parent_count				= $parent_print['max_parent'];
						
						
						$this->BoardModel->SetCommentInsert(
							$array['board_config_print']['con_name']
							, ($parent_count+1)
							, "0"
							, $array['get_parameter']['board_array']['request_no']
							, $array['get_parameter']['session_array']['sess_member_id']
							, $this->common->GetDecrypt($array['get_parameter']['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
							, $array['get_parameter']['board_array']['request_rep_content']
						);
						
						
						$rows						= $this->db->affected_rows();
						
						if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
							
							echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
							
						} else {	// 문제가 있을때
							
							echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
							
						}
						
						
					} else {
						
						echo $this->common->move_error(KO_NOTNOW, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
						
					}
					
					
				} else {
					
					echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
					
				}
	
	
			} else {
	
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function comment_add_write(){


		$array['get_parameter']			= $this->user->GetParameter();


		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){
			

			if(isset($array['get_parameter']['session_array']['sess_member_id'])){
				
				
				if($array['board_config_print']['con_reply_level'] == "0" || $array['board_config_print']['con_reply_level'] <= $array['get_parameter']['session_array']['sess_member_level']) {
				
				
					/** 게시글 작성 간격 메서드 시작 **/
					$check_diff					= $this->BoardModel->CheckComDiff($array['board_config_print']['con_name'], $array['get_parameter']['session_array']['sess_member_id']);
					$diff_count					= $check_diff->num_rows();
					$diff_rows					= $check_diff->row_array();
					/** 게시글 작성 간격 메서드 끝 **/
		
		
					if(strtotime(date("Y-m-d H:i:s")) - strtotime($diff_rows['rep_date']) >= $array['board_config_print']['con_diff']){
		
		
						$get_parent_comment_count	= $this->BoardModel->GetParentCommentMaxCount($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
						$parent_print				= $get_parent_comment_count->row_array();
						$parent_count				= $array['get_parameter']['board_array']['request_rep_parent_no'];
		
						$get_child_comment_count	= $this->BoardModel->GetChildCommentMaxCount($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $parent_count);
						$child_print				= $get_child_comment_count->row_array();
						$child_count				= $child_print['max_child'];
		
		
						$this->BoardModel->SetCommentInsert(
							$array['board_config_print']['con_name']
							, $parent_count
							, ($child_count+1)
							, $array['get_parameter']['board_array']['request_no']
							, $array['get_parameter']['session_array']['sess_member_id']
							, $this->common->GetDecrypt($array['get_parameter']['session_array']['sess_member_name'], @$secret_key, @$secret_iv)
							, $array['get_parameter']['board_array']['request_rep_content']
						);
		
		
						$rows						= $this->db->affected_rows();
						
						if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
							
							echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
							
						} else {	// 문제가 있을때
							
							echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
							
						}
						
						
					} else {
						
						echo $this->common->move_error(KO_NOTNOW, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
						
					}
	
	
				} else {
	
					echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
	
				}
	
	
			} else {
	
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function comment_modify(){


		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){
			

			if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){
	
	
				$this->BoardModel->SetCommentModify(
					$array['board_config_print']['con_name']
					, $array['get_parameter']['board_array']['request_rep_no']
					, $array['get_parameter']['board_array']['request_no']
					, $array['get_parameter']['session_array']['sess_member_id']
					, $array['get_parameter']['board_array']['request_rep_content']
				);
	
	
				$rows						= $this->db->affected_rows();
				
				if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
					
					echo $this->common->move_error(KO_SUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
					
				} else {	// 문제가 있을때
					
					echo $this->common->move_error(KO_UNSUCCESS, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
					
				}
	
	
			} else {
	
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function comment_remove(){


		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){
			
		
			if($array['get_parameter']['session_array']['sess_member_id'] == $array['get_parameter']['member_array']['request_member_id']){
				
	
				$rows['reply_parent_all_count']	= $this->BoardModel->GetParentCommentCount($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['board_array']['request_rep_parent_no']);
				$rows['reply_parent_count']		= $rows['reply_parent_all_count']->num_rows();
	
	
				if($rows['reply_parent_count'] <= 1){	// 1개 이하일 때만 삭제 가능
	
	
					$this->BoardModel->SetCommentRemove(
						$array['board_config_print']['con_name']
						, $array['get_parameter']['board_array']['request_rep_no']
						, $array['get_parameter']['board_array']['request_no']
						, $array['get_parameter']['session_array']['sess_member_id']
					);
	
	
					$rows						= $this->db->affected_rows();
					
					if($rows >= 1){	// last_id 값이 1개 이상일 때 insert
						
						$call_back['return_code']	= "0000";
						
					} else {	// 문제가 있을때
						
						$call_back['return_code']	= "9999";
						
					}
	
	
				} else {	// 2개 이상일 때 삭제하지 못한다
	
					$call_back['return_code']	= "9998";
	
				}
	
	
				echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
	
	
			} else {
	
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function set_likes(){	// 추천 메서드 2019-05-16


		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){

		
			/** 게시판 불러오기 시작 **/
			$rows['get_board_content']		= $this->BoardModel->GetBoardContent($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
			$rows['board_count']			= $rows['get_board_content']->num_rows();
			$rows['board_print']			= $rows['get_board_content']->row_array();
			/** 게시판 불러오기 끝 **/
	
	
			if($rows['board_count'] >= 1){
	
				
				if(isset($array['get_parameter']['session_array']['sess_member_id'])){
	
	
					$rows['check_like']			= $this->BoardModel->CheckVoted($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id']);
					$rows['like_count']			= $rows['check_like']->num_rows();
	
	
					if($rows['like_count'] <= 0){
	
				
						$this->BoardModel->SetLikeInsert($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id']);
	
						$rows					= $this->db->affected_rows();
	
						if($rows >= 1){
	
							$call_back['return_code']	= "0000";
	
						} else {	// 인서트 되지 않았을때 에러
	
							$call_back['return_code']	= "9999";
	
						}
	
					} else {	// 1개 이상일때 에러
	
						$call_back['return_code']	= "9997";
	
					}
	
	
				} else {	// 로그인 하지 않았을때 에러
	
					$call_back['return_code']	= "9998";
	
				}
	
	
				echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
	
	
			} else {
	
				echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function set_dislikes(){	// 추천 메서드 2019-05-16

		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){
			

			/** 게시판 불러오기 시작 **/
			$rows['get_board_content']		= $this->BoardModel->GetBoardContent($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
			$rows['board_count']			= $rows['get_board_content']->num_rows();
			$rows['board_print']			= $rows['get_board_content']->row_array();
			/** 게시판 불러오기 끝 **/
			
	
			if($rows['board_count'] >= 1){
	
				
				if(isset($array['get_parameter']['session_array']['sess_member_id'])){
	
	
					$rows['check_like']			= $this->BoardModel->CheckVoted($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id']);
					$rows['like_count']			= $rows['check_like']->num_rows();
	
	
					if($rows['like_count']	 <= 0){
	
	
						$this->BoardModel->SetDislikeInsert($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id']);
	
						$rows					= $this->db->affected_rows();
	
						if($rows >= 1){
	
							$call_back['return_code']	= "0000";
	
						} else {	// 인서트 되지 않았을때 에러
	
							$call_back['return_code']	= "9999";
	
						}
	
					} else {	// 1개 이상일때 에러
	
						$call_back['return_code']	= "9997";
	
					}
	
	
				} else {	// 로그인 하지 않았을때 에러
	
					$call_back['return_code']	= "9998";
	
				}
	
				echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴
	
	
			} else {
	
				echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}


	}


	public function download(){


		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		
		if($array['get_parameter']['board_array']['request_con_name'] == $array['board_config_print']['con_name']){


			if(isset($array['get_parameter']['session_array']['sess_member_id'])){
	
	
				$rows['get_board_files']		= $this->BoardModel->GetBoardFiles($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['board_array']['request_file']);
				$rows['file_count']				= $rows['get_board_files']->num_rows();
				$rows['file_print']				= $rows['get_board_files']->row_array();
	
	
				$path							= file_get_contents(DOCUMENT_ROOT . "/uploaded/" . $array['board_config_print']['con_name'] . "/" . $array['get_parameter']['board_array']['request_no'] . "/" . $rows['file_print']['file_encrypt_name']);
	
				force_download($rows['file_print']['file_encrypt_name'], $path);
	
	
			} else {
	
				echo $this->common->move_error(KO_NOPERMI, PROTOCOLS . HTTP_HOST . "/board/contents/" . $array['board_config_print']['con_name'] . "/no/" . $array['get_parameter']['board_array']['request_no']);
	
			}
			
			
		} else {
			
			echo $this->common->move_error(KO_EMPTY, PROTOCOLS . HTTP_HOST);
			
		}

	}


	public function file_remove(){


		$array['get_parameter']			= $this->user->GetParameter();
		
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/


		$rows['get_board_files']		= $this->BoardModel->GetBoardFiles($array['get_parameter']['board_array']['request_con_name'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['board_array']['request_file']);
		$rows['file_count']				= $rows['get_board_files']->num_rows();
		$rows['file_print']				= $rows['get_board_files']->row_array();

		
		if($array['get_parameter']['session_array']['sess_member_id'] == $rows['file_print']['member_id']){


			$this->common->SetBoardFileDelete($array['get_parameter']['board_array']['request_con_name'], $array['get_parameter']['board_array']['request_no'], $rows['file_print']['file_encrypt_name']);

			$this->BoardModel->SetFileDelete($array['get_parameter']['board_array']['request_con_name'], $array['get_parameter']['board_array']['request_file'], $array['get_parameter']['board_array']['request_no'], $array['get_parameter']['session_array']['sess_member_id']);

			$rows							= $this->db->affected_rows();

			if($rows >= 1){

				$call_back['return_code']	= "0000";

			} else {

				$call_back['return_code']	= "9999";

			}

		} else {

			$call_back['return_code']	= "9998";

		}

		echo json_encode($call_back, JSON_UNESCAPED_UNICODE);	// json 리턴

	}


	public function insert_files(){


		$array['get_parameter']			= $this->user->GetParameter();


		if(isset($array['get_parameter']['session_array']['sess_member_id'])){


			/** 게시판설정 시작 **/
			$array['get_board_config']		= $this->user->GetBoardConfig($array['get_parameter']['board_array']['request_con_name']);
			$array['board_config_count']	= $array['get_board_config']->num_rows();
			$array['board_config_print']	= $array['get_board_config']->row_array();
			/** 게시판설정 끝 **/


			/** 파일첨부 기능 시작 **/
			$file_count						= count(@$_FILES['file_name']['name']);
			$total_file_count				= $array['get_parameter']['board_array']['request_count'];


			$config['upload_path']			= $this->common->SetDirectory($array['board_config_print']['con_name'], $array['get_parameter']['board_array']['request_no']);
			$config['allowed_types']		= "gif|jpg|jpeg|png";	// 게시판 본문 삽입은 공통적으로 이미지 파일만 허용시킴 SetFileMove 메서드용 파라미터


			if($file_count >= 1 && $total_file_count <= 5){

				if( in_array($_FILES['file_name']['type'], array("image/jpeg","image/jpg","image/pjpeg","image/png")) && $_FILES['file_name']['size'] < 5242880){


					/** 파일 이동 메서드 시작 **/
					$file_move					= $this->common->SetFileMove($config, $_FILES['file_name']['name'], $_FILES['file_name']['tmp_name'], $array['get_parameter']['session_array']['sess_member_id']);
					/** 파일 이동 메서드 끝

					
					1. 파일을 서버에 복사 한다
					2. 복사 된 파일을 리사이징 메서드로 불러온다
					3. 파일을 리사이징 한다
					4. 리사이징 한 파일을 덮어 쓴다
					5. 리사이징 전 파일을 따로 삭제 할 필요가 없어진다

					
					파일 리사이징 메서드 시작 **/
					$path						= DOCUMENT_ROOT . "/uploaded/" . $array['get_parameter']['board_array']['request_con_name'] . "/" .  $array['get_parameter']['board_array']['request_no'] . "/";
					
					$recreate					= $this->common->SetImageRecreate($path, $file_move['encrypt_name'], "800", "500");


					$this->BoardModel->SetFilesInsert(
						$array['board_config_print']['con_name']
						, $array['get_parameter']['board_array']['request_no']
						, $array['get_parameter']['session_array']['sess_member_id']
						, $recreate['encrypt_name']
						, $recreate['file_name']
						, $recreate['width']
						, $recreate['height']
						, $recreate['type']
					);	// 파일 인서트 메서드
					/** 파일 리사이징 메서드 끝 **/


					$call_back['return_code']	= "0000";
					$call_back['encrypt_name']	= $recreate['encrypt_name'];


				} else if ( in_array($_FILES['file_name']['type'], array("image/gif")) && $_FILES['file_name']['size'] < 5242880){	// gif 는 당분간 그냥 올린다; 2019-06-24


					/** 파일 이동 메서드 시작 **/
					$file_move					= $this->common->SetFileMove($config, $_FILES['file_name']['name'], $_FILES['file_name']['tmp_name'], $array['get_parameter']['session_array']['sess_member_id']);

					$this->BoardModel->SetFilesInsert(
						$array['board_config_print']['con_name']
						, $array['get_parameter']['board_array']['request_no']
						, $array['get_parameter']['session_array']['sess_member_id']
						, $file_move['encrypt_name']
						, $file_move['file_name']
						, 0     // 0 으로 들어가는게 맞음. 파일 첨부 선택 게시물 작성 완료시 디비 구분
					    , 0     // 0 으로 들어가는게 맞음. 파일 첨부 선택 게시물 작성 완료시 디비 구분
					    , "image/gif"
					);	// 파일 인서트 메서드
					/** 파일 이동 메서드 끝 **/


					$call_back['return_code']	= "0000";
					$call_back['encrypt_name']	= $file_move['encrypt_name'];


				} else {

					$call_back['return_code']	= "9997";

					@$this->common->SetFileDelete("member", $array['get_parameter']['session_array']['sess_member_id'], $file_move['encrypt_name']);

				}

			} else if($total_file_count >= 6){

				$call_back['return_code']	= "9998";

				@$this->common->SetFileDelete("member", $array['get_parameter']['session_array']['sess_member_id'], $file_move['encrypt_name']);

			} else {

				$call_back['return_code']	= "9999";

				@$this->common->SetFileDelete("member", $array['get_parameter']['session_array']['sess_member_id'], $file_move['encrypt_name']);

			}

			echo json_encode($call_back, JSON_UNESCAPED_UNICODE);
			/** 파일첨부 기능 끝 **/

		} else {

			echo $this->common->move_error(KO_NEEDLOGIN, PROTOCOLS . HTTP_HOST);

		}

	}
	
	
	public function rss(){
		
		$array['get_parameter']			= $this->user->GetParameter();
		
		/** 게시판설정 시작 **/
		$array['get_board_config']		= $this->user->GetBoardConfig($this->uri->segment('3'));
		$array['board_config_count']	= $array['get_board_config']->num_rows();
		$array['board_config_print']	= $array['get_board_config']->row_array();
		/** 게시판설정 끝 **/
		
		if($array['board_config_count'] >= 1){
			
			$array['start']          = 0;
			$array['end']            = 100;
			$array['page']           = $array['get_parameter']['search_array']['page'];
			
			$array['get_board_list'] = $this->BoardModel->GetBoardList($array['board_config_print']['con_name'], @$array['get_parameter']['search_array']['search_type'], @$array['get_parameter']['search_array']['search_name'], @$array['page'], @$array['start'], @$array['end']);
			$array['board_result']   = $array['get_board_list']['result']->result_array();
			
			$this->load->view("skin/boards/" . $array['board_config_print']['con_skin']. "/RssContentView", $array);
			
		} else {
			
			echo $this->common->move_error(KO_WRONG, PROTOCOLS . HTTP_HOST . "/board/lists/" . $array['board_config_print']['con_name']);
			
		}
		
	}

}