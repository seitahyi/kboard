<?php
if(!defined('BASEPATH')) exit;


/** PHPMailer Library Namespace Start **/
require "../vendor/autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
/** PHPMailer Library Namespace End **/


class Common {	// basic custom library instance 2019-01-11


	
	protected $CI;



	public function __construct(){

		$this->CI	=& get_instance();

	}


	public function SetUnicode(){		// utf-8 encoding 2019-01-11

		echo "<meta http-equiv='content-type' content='text/html;charset=utf-8'>";

	}


	public function CheckFilter(){ // check filtering 2019-06-20

		//$filter_array = array("(\<(/?[^\>]+)\>)","/\s|&nbsp;/","/\s|p/","/\s|<style/","/\s|text-align/","/\s|align/","/\s|cente/","/\s|&nbs/","/\s|<sanstyle/","/\s|font-size/","/\s|line/","/[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i");
		
		//$filter_array = array("/\s|align/","/\s|float/","/\s|margin/","/\s|padding/","/\s|<script>/","/\s|<div>/","/\s|<iframe>/","/\s|load/","/\s|ajax/","/\s|type/","/\s|jquery/","/\s|function/","/\s|ready/","/\s|document/","/[ #\&\+\%@=,\.'\"\^`~\_|\!\?\*$#\[\]\{\}]/i");
		
		//$filter_array = array("/align/","/float/","/margin/","/padding/","/script/","/div/","/iframe/","/load/","/ajax/","/type/","/jquery/","/function/","/ready/","/document/","/&lt;/","/&gt;/");

		$filter_array	= array("/javascript/", "/script/", "/jquery/", "/function/", "/ready/", "/document/", "/&lt;/", "/&gt;/");
		
		return $filter_array;
		
	}


	public function CheckDevice(){		// check device 2019-01-11

		$mobile_device	= array("iphone","lgtelecom","skt","kt","mobile","samsung","nokia","blackberry","android","sony","phone");

		$count		= 0;

		for($i = 0; $i < sizeof($mobile_device); $i++){

			if(preg_match("/$mobile_device[$i]/", strtolower($_SERVER['HTTP_USER_AGENT']))){

				$count++;
				break;

			}

		}

		return ($count >= 1) ? "MO" : "PC";

	}


	public function GetDesign($list, $index){	// 레이아웃이랑 스킨 목록 가져옴 2019-05-07, Admin 메서드에서 Common 메서드로 옮기고 메서드명도 변경 2019-05-07

		$locate		= glob(APPPATH . "views/skin/" . $list . "/"."*", GLOB_BRACE);
		
		foreach($locate as $i => $value){

			$exp_locate			= explode("/", $locate[$i]);
			$exp_directory[]	= $exp_locate[$index];

		}

		return $exp_directory;

	}


	public function SetString($length, $title){	// string crop 2019-01-11

		$strimwidth	= mb_strimwidth($title, "0", $length, "...", "utf-8");

		//return htmlspecialchars($strimwidth); if you want to HTML entity ouput remove annotation
		return strip_tags($strimwidth);

	}


	public function move_error($message, $url){		// string output then move 2019-01-13

		$string	= "";
		$string	.=	"<script type='text/javascript'>";
		$string	.=		"alert('" . $message . "');";
		$string	.=		"location.href='" . $url . "';";
		$string	.=	"</script>";

		return $string;

	}


	public function SetStringToken(){		// make tokens random string 2019-01-11

		return md5(uniqid(rand(), true));

	}


	public function SetIntToken(){		// make random 4 numbers 2019-01-11

		$random = str_pad(mt_rand(0, 999999), 4, "0");

		for($i = 0, $random = ""; $i < 4; $i++){

			$random .= mt_rand(0, 9);

		}

		return $random;

	}


	/**
	코드이그나이터 내부적으로 단방향 암호화를 기본으로 사용해야 하나
	비밀번호 해쉬에 대한 컨트로를 자체적으로 해야 할 필요가 있으므로
	양방향 알고리즘을 이용한다.
	**/

	public function GetEncrypt($str, $secret_key="", $secret_iv=""){ // password full-duplex encryption 2019-01-29

		$key	= hash('sha256', $secret_key);
		$iv		= substr(hash('sha256', $secret_iv), 0, 32);

		return str_replace("=", "", base64_encode(@openssl_encrypt($str, "AES-256-CBC", $key, 0, $iv)));

	}


	public function GetDecrypt($str, $secret_key="", $secret_iv=""){ // password full-duplex decryption 2019-01-29

		$key	= hash('sha256', $secret_key);
		$iv		= substr(hash('sha256', $secret_iv), 0, 32);

		return @openssl_decrypt(base64_decode($str), "AES-256-CBC", $key, 0, $iv);

	}

	/**
	기본적으로 코드이그나이터 안에서는 단방향 암호화를 기본으로 사용해야 하나
	비밀번호 해쉬에 대한 컨트로를 자체적으로 해야 할 필요가 있으므로
	양방향 알고리즘을 이용한다.
	**/


	
	public function SetDirectory($directory, $member_id){	// 디렉토리 체크 및 생성 2019-02-18

		if(is_dir(DOCUMENT_ROOT . "/uploaded/" . $directory . "/" . $member_id . "/")){

			$created	= DOCUMENT_ROOT . "/uploaded/" . $directory . "/" . $member_id . "/";

		} else {

			umask(0);
			@mkdir(DOCUMENT_ROOT . "/uploaded/" . $directory . "/" . $member_id . "/", 0707, true);
			chmod(DOCUMENT_ROOT . "/uploaded/" . $directory . "/" . $member_id . "/", 0707);

			$created	= DOCUMENT_ROOT . "/uploaded/" . $directory . "/" . $member_id . "/";

		}

		return $created;

	}


	public function SetFileDelete($directory, $member_id, $file_name){	// 파일 삭제 메서드 2019-02-19

		$directory		= DOCUMENT_ROOT . "/uploaded/" . (!empty($directory)? $directory . "/":"") . (!empty($member_id)? $member_id . "/":"") . $file_name;

		return unlink($directory);

	}


	public function SetBoardFileDelete($con_name, $no, $file_encrypt_name){	// 게시판 파일 삭제 메서드 2019-04-25

		$directory		= DOCUMENT_ROOT . "/uploaded/" . $con_name . "/" . $no . "/" . $file_encrypt_name;

		return @unlink($directory);

	}


	public function SetFileMove($array, $file_name, $file_tmp_name, $member_id){	// 파일 이동 메서드 2019-05-22


		@$allowed_files		= explode("|", $array['allowed_types']);
		$allowed_mimes		= array("image/gif", "image/jpeg","image/jpg","image/pjpeg","image/png","image/bmp","application/pdf","application/excel");

		
		if(is_uploaded_file($file_tmp_name)){

			@$base_name			= basename($file_name);
			@$exten_name		= array_pop(explode(".", strtolower($base_name)));

			$mime_type			= mime_content_type($file_tmp_name);

			$encrypt_name		= $this->GetEncrypt($file_tmp_name, @$secret_key, @$secret_iv) . "." . $exten_name;
			$move_to_path		= $array['upload_path'] . $encrypt_name;


			if(in_array($exten_name, $allowed_files) && in_array($mime_type, $allowed_mimes)){

				move_uploaded_file($file_tmp_name, $move_to_path);

				return array("file_name"=>$file_name, "file_tmp_name"=>$file_tmp_name, "encrypt_name"=>$encrypt_name);

			}

		}


	}


	public function SetImageRecreate($path, $encrypt_name, $set_width, $set_height){

		$set_img_info		= getimagesize($path . $encrypt_name);
		$get_width			= $set_img_info['0'];
		$get_height			= $set_img_info['1'];
		$get_type			= $set_img_info['mime'];

		if($get_width > $set_width){

			$set_img_height	= (int)( ( $get_height * $set_width ) / $get_width );
			$set_img_create	= imagecreatetruecolor($set_width, $set_img_height);


			switch($get_type){

				case "imgage/jpg" :
					$set_img	= imagecreatefromjpeg($path . $encrypt_name);
				break;


				case "image/jpeg" :
					$set_img	= imagecreatefromjpeg($path . $encrypt_name);
				break;


				case "image/pjpeg" :
					$set_img	= imagecreatefromjpeg($path . $encrypt_name);
				break;

				
				case "image/png" :
					$set_img	= imagecreatefrompng($path . $encrypt_name);
				break;

			}


			imagecopyresized($set_img_create, $set_img, 0, 0, 0, 0, $set_width, $set_img_height, $get_width, $get_height);


			switch($get_type){

				case "imgage/jpg" :
					imagejpeg($set_img_create, $path . $encrypt_name);
				break;


				case "image/jpeg" :
					imagejpeg($set_img_create, $path . $encrypt_name);
				break;


				case "image/pjpeg" :
					imagejpeg($set_img_create, $path . $encrypt_name);
				break;

				
				case "image/png" :
					imagepng($set_img_create, $path . $encrypt_name);
				break;

			}

		}

		return array("file_name"=>$_FILES['file_name']['name'], "encrypt_name"=>$encrypt_name, "width"=>$get_width, "height"=>$get_height, "type"=>$get_type);

	}


	public function SetDirectoryExplode($num){

		$request_uri		= explode("/", REQUEST_URI);

		return @$request_uri[$num];

	}


	public function GetPaging($basename, $list_count, $id, $page, $search_type, $search_name, $start, $end){	// 2019-03-15


		if(!$page){

			$page			= 1;

		}

		$total				= ceil($list_count / $end);
		$first_page			= ((ceil($page / 10) - 1) * 10) + 1;
		$last_page			= min($total, ceil($page / 10) * 10);

		echo	"<nav class=\"well\" style=\"text-align:center;\">";
		echo		"<ul class=\"pagination\">";
		
		if($id > 1){

			echo		"<li class=\"page-item\">";
			
			if(isset($search_type)){

				echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . ($id - 1) . "/search_type/" . $search_type . "/search_name/" . $search_name . "\" class=\"page-link\">";
				echo			"이전";
				echo		"</a>";

			} else {

				echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . ($id - 1) . "\" class=\"page-link\">";
				echo			"이전";
				echo		"</a>";

			}

			echo		"</li>";

		}

		for($i = $first_page; $i <= $last_page; $i++){

			if($i == $id){

				echo	"<li class=\"page-item active\">";
				echo		"<a href=\"#\" class=\"page-link\">" . $i . "</a>";
				echo	"</li>";

			} else {

				if(isset($search_type)){

					echo	"<li class=\"page-item\">";
					echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . $i . "/search_type/" . $search_type . "/search_name/" . $search_name . "\" class=\"page-link\">";
					echo			$i;
					echo		"</a>";
					echo	"</li>";

				} else {

					echo	"<li class=\"page-item\">";
					echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . $i . "\" class=\"page-link\">";
					echo			$i;
					echo		"</a>";
					echo	"</li>";

				}

			}

		}

		if($id != $total && $total > 0){

			echo	"<li class=\"page-item\">";

			if(isset($search_type)){

				echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . ($id + 1) . "/search_type/" . $search_type . "/search_name/" . $search_name . "\" class=\"page-link\">";
				echo			"다음";
				echo		"</a>";

			} else {

				echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . ($id + 1) . "\" class=\"page-link\">";
				echo			"다음";
				echo		"</a>";

			}

			echo	"</li>";

		}

		echo		"</ul>";
		echo	"</nav>";

	}


	public function GetCommentPaging($basename, $list_count, $id, $page, $start, $end){	// 2019-05-28


		if(!$page){

			$page			= 1;

		}

		$total				= ceil($list_count / $end);
		$first_page			= ((ceil($page / 10) - 1) * 10) + 1;
		$last_page			= min($total, ceil($page / 10) * 10);

		echo	"<nav class=\"well\" style=\"text-align:center;\">";
		echo		"<ul class=\"pagination\">";
		
		if($id > 1){

			echo		"<li class=\"page-item\">";
			echo			"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . ($id - 1) . "\" class=\"page-link\">";
			echo				"이전";
			echo			"</a>";
			echo		"</li>";

		}

		for($i = $first_page; $i <= $last_page; $i++){

			if($i == $id){

				echo	"<li class=\"page-item active\">";
				echo		"<a href=\"#\" class=\"page-link\">" . $i . "</a>";
				echo	"</li>";

			} else {

				echo	"<li class=\"page-item\">";
				echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . $i . "\" class=\"page-link\">";
				echo			$i;
				echo		"</a>";
				echo	"</li>";

			}

		}

		if($id != $total && $total > 0){

			echo	"<li class=\"page-item\">";
			echo		"<a href=\"http://" . HTTP_HOST . "/" . $basename . "/page/" . ($id + 1) . "\" class=\"page-link\">";
			echo			"다음";
			echo		"</a>";
			echo	"</li>";

		}

		echo		"</ul>";
		echo	"</nav>";

	}


	public function CheckFile($path, $file_name){	// 파일 체크 2019-05-10

		return file_exists($path . $file_name);

	}


	public function SendEmail($user, $password, $host, $to, $subject, $body){	// 메일 보내기 2019-06-10


		$mail				= new PHPMailer;
		
		
		/**
		PHPMailer Troubleshooting Guideline
		https://github.com/PHPMailer/PHPMailer/wiki/Troubleshooting
		
		how to use google configutarions
		https://myaccount.google.com/lesssecureapps?utm_source=google-account&utm_medium=web
		보안 수준이 낮은 앱의 액세스 허용
		**/


		$mail->IsSMTP();        // set mailer to use SMTP
 
		$mail->Mailer			= "smtp";
		$mail->CharSet			= "UTF-8";
		//$mail->Host			= "smtp.gmail.com";			    // smtp 주소
		$mail->Host				= $host;
		$mail->SMTPAuth			= true;							// Enable SMTP authentication
		$mail->Username			= $user;						// SMTP username메일 주소
		$mail->Password			= $password;					// SMTP password메일 접속 시 비밀번호
		$mail->SetFrom("no-reply@ " . HTTP_HOST, "admin");
		$mail->Port				= "465";						// 465,587 포트가 열려있어야함.
		$mail->SMTPSecure		= "ssl";
		//$mail->SMTPDebug		= 2;							// debugging: 1 = errors and messages, 2 = messages only
		 
		$mail->From				= "no-reply@" . HTTP_HOST;		//보내는 사람 메일 주소
		$mail->FromName			= "no-reply";					//보내는 사람 이름
		 
		$mail->AddAddress($to);									//받는 사람 주소
		 
		$mail->WordWrap         = 50;							// set word wrap to 50 characters
		$mail->IsHTML(true);									// set email format to HTML 메일을 보낼 때 html형식으로 메일을 보낼 경우 true
		 
		$mail->Subject			= $subject;						//메일 제목
		$mail->Body				= $body;						//메일 본문
		//$mail->AltBody			= "This is the body in plain text for non-HTML mail clients";    //메일 본문을 오로지 텍스트 형식으로만 보낼 때


		if( !$mail->Send() ){

			$print	= "Message could not be sent";
			//$print	= "Mailer Error: " . $mail->ErrorInfo;	//메일 전송에 실패 했을 경우 에러
			exit;

		} else {
		 
			$print	= "Message has been sent";

		}

		return $print;

	}
	
	
	public function GetPath(){ //  절대경로 위치 파악 메서드 2019-11-05
		
		$explode                        = explode("/", FCPATH);
		$count                          = count($explode);
		$rtn_idx                        = $count-2;
		
		return $explode[$rtn_idx];
		
	}

}