<?php
if(!defined('BASEPATH')) exit;


class Admin{	// basic admin library instance 2019-01-30



	protected $CI;


	public function __construct(){

		$this->CI	=& get_instance();

	}


	public function GetParameter(){	// 자주 사용 할 파라미터들 v4.0 2019-02-22


		$session_array		=	array(
									"sess_regen"=>$this->CI->session->userdata('__ci_last_regenerate')
									, "sess_member_id"=>$this->CI->session->userdata('member_id')
									, "sess_member_name"=>$this->CI->session->userdata('member_name')
									, "sess_member_email"=>$this->CI->session->userdata('member_email')
									, "sess_member_level"=>$this->CI->session->userdata('member_level')
									, "sess_member_profile"=>$this->CI->session->userdata('member_profile')
									, "sess_member_formal"=>$this->CI->session->userdata('member_formal')
								);


		$admin_menu_array	=	array(
									"home"=>"home"
									, "configs"=>"configs"
									, "companies"=>"companies"
									, "menus"=>"menus"
									, "page_add"=>"page_add"
		                            , "page_mod"=>"page_mod"
		                            , "page_list"=>"page_list"
									, "popup_add"=>"popup_add"
		                            , "popup_mod"=>"popup_mod"
		                            , "popup_list"=>"popup_list"
									, "members"=>"members"
									, "member_info"=>"member_info"
									, "member_board_list"=>"member_board_list"
									, "member_file_list"=>"member_file_list"
									, "member_reply_list"=>"member_reply_list"
									, "member_voted_list"=>"member_voted_list"
									, "member_add"=>"member_add"
									, "member_gen_pw"=>"member_gen_pw"
									, "member_gen_tk"=>"member_gen_tk"
									, "board"=>"board"
									, "board_add"=>"board_add"
									, "board_mod"=>"board_mod"
									, "board_list"=>"board_list"
									, "comment_mod"=>"comment_mod"
		                            , "custom_board_list"=>"custom_board_list"
		                            , "custom_comment_list"=>"custom_comment_list"
		                            , "custom_voted_list"=>"custom_voted_list"
		                            , "custom_files_list"=>"custom_files_list"
		                            , "lib_list"=>"lib_list"
								);


		/** URI 에 포함 될 request 요청 배열 **/
		$search_array		=	array(
									"search_type"=>$this->CI->input->request('search_type')
									, "search_name"=>$this->CI->input->request('search_name')
									, "page"=>$this->CI->input->request('page')
								);


		$member_array		=	array(
									"request_member_no"=>$this->CI->input->request('member_no')
									, "request_member_id"=>$this->CI->input->request('member_id')
									, "request_member_password"=>$this->CI->input->request('member_password')
									, "request_member_name"=>$this->CI->input->request('member_name')
									, "request_member_email"=>$this->CI->input->request('member_email')
									, "request_member_email_1"=>$this->CI->input->request('member_email_1')
									, "request_member_email_2"=>$this->CI->input->request('member_email_2')
									, "request_member_level"=>$this->CI->input->request('member_level')
									, "request_member_profile"=>$this->CI->input->request('member_profile')
									, "request_member_formal"=>$this->CI->input->request('member_formal')
									, "post_count"=>$this->CI->input->post("count")	// request 를 post 로 보내야함
									, "post_from"=>$this->CI->input->post("from")	// request 를 post 로 보내야함
									, "post_to"=>$this->CI->input->post("to")	// request 를 post 로 보내야함
								);

		
		$board_array		=	array(
									"request_no"=>$this->CI->input->request("no")
									, "request_con_no"=>$this->CI->input->request('con_no')
									, "request_con_title"=>$this->CI->input->request('con_title')
									, "request_con_name"=>$this->CI->input->request('con_name')
									, "request_con_skin"=>$this->CI->input->request('con_skin')
									, "request_con_layout"=>$this->CI->input->request('con_layout')
									, "request_con_thumb_filter"=>$this->CI->input->request('con_thumb_filter')
									, "request_con_date"=>$this->CI->input->request('con_date')
									, "post_con_no"=>$this->CI->input->post('con_no')
									, "post_con_title"=>$this->CI->input->post('con_title')
									, "post_con_name"=>$this->CI->input->post('con_name')
									, "post_con_skin"=>$this->CI->input->post('con_skin')
									, "post_con_category"=>$this->CI->input->post('con_category')
									, "post_con_layout"=>$this->CI->input->post('con_layout')
									, "post_con_thumb_filter"=>$this->CI->input->post('con_thumb_filter')
									, "post_con_diff"=>$this->CI->input->post('con_diff')
		                            , "post_con_list_level"=>$this->CI->input->post('con_list_level')
		                            , "post_con_content_level"=>$this->CI->input->post('con_content_level')
		                            , "post_con_write_level"=>$this->CI->input->post('con_write_level')
		                            , "post_con_reply_level"=>$this->CI->input->post('con_reply_level')
									, "post_con_date"=>$this->CI->input->post('con_date')
									, "post_ext_title_1"=>$this->CI->input->post('ext_title_1')
									, "post_ext_title_2"=>$this->CI->input->post('ext_title_2')
									, "post_ext_title_3"=>$this->CI->input->post('ext_title_3')
									, "post_ext_title_4"=>$this->CI->input->post('ext_title_4')
									, "post_ext_title_5"=>$this->CI->input->post('ext_title_5')
								);
		/** URI 에 포함 될 request 요청 배열 **/
		
		
		$perm_array			=	array(
									"home"=>array("0"=>"home","1"=>"메인")
				
									, "configs"=>array("0"=>"configs", "1"=>"기본환경설정")
									, "configs_ok"=>array("0"=>"configs_ok", "1"=>"기본환경설정 완료")
									, "configs_logo_ok"=>array("0"=>"configs_logo_ok", "1"=>"기본환경설정로고 추가")
									, "configs_icon_ok"=>array("0"=>"configs_icon_ok", "1"=>"기본환경설정 아이콘 추가")
									, "configs_file_remove"=>array("0"=>"configs_file_remove", "1"=>"기본환경설정 파일삭제")
				
									, "companies"=>array("0"=>"companies", "1"=>"회사정보")
									, "companies_ok"=>array("0"=>"companies_ok", "1"=>"회사정보 완료")

									, "bbs_content"=>array("0"=>"contents", "1"=>"게시판 본문")
									, "board_add"=>array("0"=>"board_add", "1"=>"게시판 추가")
									, "board_add_ok"=>array("0"=>"board_add_ok", "1"=>"게시판추가 완료")
									, "board_mod"=>array("0"=>"board_mod", "1"=>"게시판설정")
									, "board_mod_ok"=>array("0"=>"board_mod_ok", "1"=>"게시판설정 완료")
									, "board_list"=>array("0"=>"board_list", "1"=>"게시판 목록")
									, "custom_board_list"=>array("0"=>"custom_board_list", "1"=>"사용자게시판 목록")
									, "content_remove"=>array("0"=>"content_remove", "1"=>"사용자게시물 삭제")
									, "custom_comment_list"=>array("0"=>"custom_comment_list", "1"=>"사용자댓글 목록")
									, "comment_remove"=>array("0"=>"comment_remove", "1"=>"사용자댓글 삭제")
		                            , "custom_voted_list"=>array("0"=>"custom_voted_list", "1"=>"사용자추천 목록")
				
									, "menus"=>array("0"=>"menus", "1"=>"메뉴설정")
									, "menus_ok"=>array("0"=>"menus_ok", "1"=>"메뉴설정 완료")
									, "menus_gd_add"=>array("0"=>"menus_gd_add", "1"=>"1차메뉴 추가")
									, "menus_pa_add"=>array("0"=>"menus_pa_add", "1"=>"2차메뉴 추가")
									, "menus_mod_add"=>array("0"=>"menu_mod_add", "1"=>"메뉴 수정")
									, "menus_remove"=>array("0"=>"menus_remove", "1"=>"메뉴 삭제")
				
									, "page_add"=>array("0"=>"page_add", "1"=>"페이지 추가")
									, "page_add_ok"=>array("0"=>"page_add_ok", "1"=>"페이지추가 완료")
		                            , "page_mod"=>array("0"=>"page_mod", "1"=>"페이지설정 수정")
		                            , "page_mod_ok"=>array("0"=>"page_mod_ok", "1"=>"페이지설정 완료")
		                            , "page_list"=>array("0"=>"page_list", "1"=>"페이지 목록")
				
									, "popup_add"=>array("0"=>"popup_add", "1"=>"팝업 추가")
									, "popup_add_ok"=>array("0"=>"popup_add_ok", "1"=>"팝업추가 완료")
		                            , "popup_mod"=>array("0"=>"popup_mod", "1"=>"팝업설정 수정")
		                            , "popup_mod_ok"=>array("0"=>"popup_mod_ok", "1"=>"팝업설정 완료")
		                            , "popup_list"=>array("0"=>"popup_list", "1"=>"팝업 목록")
				
									, "members"=>array("0"=>"members", "1"=>"회원목록")
									, "member_add"=>array("0"=>"member_add", "1"=>"회원 추가")
									, "member_add_ok"=>array("0"=>"member_add_ok", "1"=>"회원추가 수정")
									, "member_info"=>array("0"=>"member_info", "1"=>"회원정보")
									, "member_info_ok"=>array("0"=>"member_info_ok", "1"=>"회원정보 수정")
									, "member_add_perm_ok"=>array("0"=>"member_add_perm_ok", "1"=>"회원권한 수정")
									, "member_remove_perm_ok"=>array("0"=>"member_remove_perm_ok", "1"=>"회원권한 삭제")
									, "member_gen_pw"=>array("0"=>"member_gen_pw", "1"=>"회원 비밀번호발급 목록")
									, "member_gen_tk"=>array("0"=>"member_gen_tk", "1"=>"회원 토큰발급 목록")
									, "member_board_list"=>array("0"=>"member_board_list", "1"=>"회원 작성글")
									, "member_file_list"=>array("0"=>"member_file_list", "1"=>"회원 첨부파일")
									, "member_reply_list"=>array("0"=>"member_reply_list", "1"=>"회원 댓글")
									, "member_voted_list"=>array("0"=>"member_voted_list", "1"=>"회원 추천/비추천")
		  
		                            , "library"=>array("0"=>"library", "1"=>"라이브러리")
								);
		
		
		$menu_array			=	array(
									"post_menu_no"=>$this->CI->input->post('menu_no')
									, "post_menu_grandparent"=>$this->CI->input->post('menu_grandparent')
									, "post_menu_parent"=>$this->CI->input->post('menu_parent')
									, "post_menu_child"=>$this->CI->input->post('menu_child')
									, "post_menu_name"=>$this->CI->input->post('menu_name')
									, "post_menu_url"=>$this->CI->input->post('menu_url')
									, "post_menu_target"=>$this->CI->input->post('menu_target')
								);
		
		$pager_array        =   array(
		                            "request_no"=>$this->CI->input->request("no")
			                        , "request_count"=>$this->CI->input->request("count")
									, "request_dir"=>$this->CI->input->request('dir')
									, "request_page_name"=>$this->CI->input->request('page_name')
			                        , "post_page_no"=>$this->CI->input->post('page_no')
			                        , "post_file_no"=>$this->CI->input->post('file_no')
			                        , "post_page_title"=>$this->CI->input->post('page_title')
			                        , "post_page_name"=>$this->CI->input->post('page_name')
			                        , "post_page_content"=>$this->CI->input->post('page_content')
			                        , "post_page_layout"=>$this->CI->input->post('page_layout')
			                        , "post_page_include"=>$this->CI->input->post('page_include')
			                        , "post_page_ip"=>$this->CI->input->post('page_ip')
			                        , "post_page_date"=>$this->CI->input->post('page_date')
			
									, "request_popup_name"=>$this->CI->input->request('popup_name')
									, "post_popup_no"=>$this->CI->input->post('popup_no')
									, "post_popup_title"=>$this->CI->input->post('popup_title')
									, "post_popup_name"=>$this->CI->input->post('popup_name')
									, "post_popup_content"=>$this->CI->input->post('popup_content')
									, "post_popup_layout"=>$this->CI->input->post('popup_layout')
									, "post_popup_ip"=>$this->CI->input->post('popup_ip')
									, "post_popup_date"=>$this->CI->input->post('popup_date')
								);
		
		
		$popup_array        =   array(
									  "request_no"=>$this->CI->input->request("no")
									, "request_count"=>$this->CI->input->request("count")
									, "request_dir"=>$this->CI->input->request('dir')
									, "request_pop_name"=>$this->CI->input->request('pop_name')
								);


		$config_array		=	array(
									"post_home_no"=>$this->CI->input->post('home_no')
									, "post_home_title"=>$this->CI->input->post('home_title')
									, "post_home_logo"=>$this->CI->input->post('home_logo')
									, "post_home_icon"=>$this->CI->input->post('home_icon')
									, "post_home_naver"=>$this->CI->input->post('home_naver')
									, "post_home_google"=>$this->CI->input->post('home_google')
									, "post_home_layout"=>$this->CI->input->post('home_layout')
		                            , "post_member_skin"=>$this->CI->input->post("member_skin")
                                    , "post_home_id_blocks"=>$this->CI->input->post('home_id_blocks')
                                    , "post_home_name_blocks"=>$this->CI->input->post('home_name_blocks')
									, "post_home_email_1"=>$this->CI->input->post('home_email_1')
									, "post_home_email_2"=>$this->CI->input->post('home_email_2')
									, "post_home_email_password"=>$this->CI->input->post('home_email_password')
									, "post_home_email_host"=>$this->CI->input->post('home_email_host')
									, "post_home_ip"=>$this->CI->input->post('home_ip')
									, "post_home_date"=>$this->CI->input->post('home_date')
									, "post_com_no"=>$this->CI->input->post('com_no')
									, "post_com_name"=>$this->CI->input->post('com_name')
									, "post_com_owner"=>$this->CI->input->post('com_owner')
									, "post_com_rotary_1"=>$this->CI->input->post('com_rotary_1')
									, "post_com_rotary_2"=>$this->CI->input->post('com_rotary_2')
									, "post_com_rotary_3"=>$this->CI->input->post('com_rotary_3')
									, "post_com_fax_1"=>$this->CI->input->post('com_fax_1')
									, "post_com_fax_2"=>$this->CI->input->post('com_fax_2')
									, "post_com_fax_3"=>$this->CI->input->post('com_fax_3')
									, "post_com_zip_1"=>$this->CI->input->post('com_zip_1')
									, "post_com_zip_2"=>$this->CI->input->post('com_zip_2')
									, "post_com_address_1"=>$this->CI->input->post('com_address_1')
									, "post_com_address_2"=>$this->CI->input->post('com_address_2')
									, "post_com_manager"=>$this->CI->input->post('com_manager')
									, "post_com_manager_email_1"=>$this->CI->input->post('com_manager_email_1')
									, "post_com_manager_email_2"=>$this->CI->input->post('com_manager_email_2')
								);
		
		
		$library_array      =   array(
									"post_lib"=>$this->CI->input->request("no")
								);


		return array("session_array"=>$session_array, "admin_menu_array"=>$admin_menu_array, "search_array"=>$search_array, "member_array"=>$member_array, "perm_array"=>$perm_array, "board_array"=>$board_array, "menu_array"=>$menu_array, "pager_array"=>$pager_array, "popup_array"=>$popup_array, "config_array"=>$config_array, "library"=>$library_array);


	}


	public function CheckPermission($perm_member_id, $perm_page){	// permission check 2019-02-26

		$query	= "";
		$query	.= " select ";
		$query	.= " member_id ";
		$query	.= " , perm_member_id ";
		$query	.= " , perm_page ";
		$query	.= " from ";
		$query	.= " admin_permission ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";

		if($perm_member_id == "admin"){

			$query	.= " and ";
			$query	.= " member_id = 'admin' ";

		} else {

			$query	.= " and ";
			$query	.= " perm_member_id = " . $this->CI->db->escape($perm_member_id) . " ";
			$query	.= " and ";
			$query	.= " perm_page = " . $this->CI->db->escape($perm_page) . " ";

		}
		//echo $query;

		$result	= $this->CI->db->query($query);

		return $result;

	}
	
	
	public function SetVolume($size){  // 받은 인자값을 1024byte 로 나눠 리턴 2019-11-04
		
		$base       = log($size) / log(1024);
		$suffix     = array("Byte", "KB", "MB", "GB", "TB");
		
		$floor_base = floor($base);
		
		return round(pow(1024, $base - floor($base)), 1) . $suffix[$floor_base];
		
	}
	
	
	public function GetDirectory($directory){   // 디렉토리 값의 크기를 구해서 리턴 2019-11-04
		
		$size       = 0;
		
		foreach(glob(rtrim($directory, "/") . "/*", GLOB_NOSORT) as $each){
			
			$size    += is_file($each) ? filesize($each) : $this->GetDirectory($each);
			
		}
		
		return $size;
		
	}
	
	
	public function GetTables($table){  // 테이블 값의 크기를 구해서 리턴 2019-11-04
		
		$query  = "";
		$query  .= " select ";
		$query  .= " table_schema ";
		//$query  .= " , sum((data_length + index_length) / 1024 /1024) MB ";
		$query  .= " , sum(data_length + index_length) Byte ";
		$query  .= " from ";
		$query  .= " information_schema.tables ";
		$query  .= " where ";
		$query  .= " table_schema = " . $this->CI->db->escape($table) . " ";
		$query  .= " group by 1 ";
		//echo $query;
		
		$result	= $this->CI->db->query($query);
		
		return $result;
		
	}
	
	
	public function GetCURL($url){  // Curl 전용 메서드 2020-03-09
		
		//$url                            = $url;             // 주소셋팅
		$ch                             = curl_init();      // curl 로딩
		
		curl_setopt($ch, CURLOPT_URL,$url);                 // curl에 url 셋팅
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        // 이 셋팅은 1로 고정하는 것이 정신건강에 좋음
		
		$result                         = curl_exec ($ch);  // curl 실행 및 결과값 저장
		
		return $result;
		
		curl_close ($ch);                                   // curl 종료
		
	}


}