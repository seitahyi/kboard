<?php
if(!defined('BASEPATH')) exit;


class User {


	protected $CI;


	public function __construct(){

		$this->CI	=& get_instance();

	}


	public function GetParameter(){	// 자주 사용 할 파라미터들 2019-05-07


		$session_array		=	array(
									"sess_regen"=>$this->CI->session->userdata('__ci_last_regenerate')
									, "sess_member_id"=>$this->CI->session->userdata('member_id')
									, "sess_member_name"=>$this->CI->session->userdata('member_name')
									, "sess_member_email"=>$this->CI->session->userdata('member_email')
									, "sess_member_level"=>$this->CI->session->userdata('member_level')
									, "sess_member_profile"=>$this->CI->session->userdata('member_profile')
									, "sess_member_formal"=>$this->CI->session->userdata('member_formal')
									, "sess_search_type"=>$this->CI->session->userdata('search_type')
									, "sess_search_name"=>$this->CI->session->userdata('search_name')
								);


		$menu_array			=	array(
									"post_menu_no"=>$this->CI->input->post('menu_no')
									, "post_menu_grandparent"=>$this->CI->input->post('menu_grandparent')
									, "post_menu_parent"=>$this->CI->input->post('menu_parent')
									, "post_menu_child"=>$this->CI->input->post('menu_child')
									, "post_menu_name"=>$this->CI->input->post('menu_name')
									, "post_menu_url"=>$this->CI->input->post('menu_url')
									, "post_menu_target"=>$this->CI->input->post('menu_target')
								);


		$member_array		=	array(
									"request_member_no"=>$this->CI->input->request('member_no')
									, "request_member_id"=>$this->CI->input->request('member_id')
									, "request_member_password"=>$this->CI->input->request('member_password')
									, "request_member_name"=>$this->CI->input->request('member_name')
									, "request_member_email"=>$this->CI->input->request('member_email')
									, "request_member_email_1"=>$this->CI->input->request('member_email_1')
									, "request_member_email_2"=>$this->CI->input->request('member_email_2')
									, "request_member_level"=>$this->CI->input->request('member_level')
									, "request_member_profile"=>$this->CI->input->request('member_profile')
									, "request_member_formal"=>$this->CI->input->request('member_formal')
									, "request_check_agree"=>$this->CI->input->request('check_agree')
									, "post_regen"=>$this->CI->input->request("__ci_last_renerate")
									, "post_count"=>$this->CI->input->post("count")	// request 를 post 로 보내야함
									, "post_from"=>$this->CI->input->post("from")	// request 를 post 로 보내야함
									, "post_to"=>$this->CI->input->post("to")	// request 를 post 로 보내야함
								);


		$board_array		=	array(
									"request_no"=>$this->CI->input->request("no")
		                            , "request_count"=>$this->CI->input->request("count")
									, "request_file"=>$this->CI->input->request("file")
									, "request_con_no"=>$this->CI->input->request("con_no")
									, "request_con_name"=>$this->CI->input->request("con_name")
									, "request_con_latest_skin"=>$this->CI->input->request("con_latest_skin")
									, "request_con_skin"=>$this->CI->input->request("con_skin")
									, "request_con_layout"=>$this->CI->input->request("con_layout")
									, "request_con_filter"=>$this->CI->input->request("con_filter")
									, "request_bbs_no"=>$this->CI->input->request("bbs_no")
									, "request_bbs_title"=>$this->CI->input->request("bbs_title", true)
									, "request_bbs_category"=>$this->CI->input->request("bbs_category", true)
									, "request_bbs_content"=>$this->CI->input->request("bbs_content")
                                    , "request_bbs_secret"=>$this->CI->input->request("bbs_secret")
									, "request_bbs_like"=>$this->CI->input->request("bbs_like")
									, "request_bbs_dislike"=>$this->CI->input->request("bbs_dislike")
									, "request_bbs_readed"=>$this->CI->input->request("bbs_readed")
									, "request_bbs_ip"=>$this->CI->input->request("bbs_ip")
									, "request_bbs_date"=>$this->CI->input->request("bbs_date")
									, "request_file_no"=>$this->CI->input->request("file_no")
									, "request_file_encrypt_name"=>$this->CI->input->request("file_encrypt_name")
									, "request_file_real_name"=>$this->CI->input->request("file_real_name")
									, "request_file_ip"=>$this->CI->input->request("file_ip")
									, "request_file_date"=>$this->CI->input->request("file_date")
									, "request_rep_no"=>$this->CI->input->request("rep_no")
									, "request_rep_parent_no"=>$this->CI->input->request("rep_parent_no")
									, "request_rep_child_no"=>$this->CI->input->request("rep_child_no")
									, "request_rep_content"=>$this->CI->input->request("rep_content", true)
									, "request_rep_ip"=>$this->CI->input->request("rep_ip")
									, "request_rep_date"=>$this->CI->input->request("rep_date")
                                    , "request_ext_desc_1"=>$this->CI->input->post('ext_desc_1')
                                    , "request_ext_desc_2"=>$this->CI->input->post('ext_desc_2')
                                    , "request_ext_desc_3"=>$this->CI->input->post('ext_desc_3')
                                    , "request_ext_desc_4"=>$this->CI->input->post('ext_desc_4')
                                    , "request_ext_desc_5"=>$this->CI->input->post('ext_desc_5')
								);
		
		$pager_array        =   array(
                                      "request_no"=>$this->CI->input->request("no")
                                    , "request_count"=>$this->CI->input->request("count")
                                    , "request_dir"=>$this->CI->input->request('dir')
                                    , "request_page_name"=>$this->CI->input->request('page_name')
                                    , "post_page_no"=>$this->CI->input->post('page_no')
                                    , "post_file_no"=>$this->CI->input->post('file_no')
                                    , "post_page_title"=>$this->CI->input->post('page_title')
                                    , "post_page_name"=>$this->CI->input->post('page_name')
                                    , "post_page_content"=>$this->CI->input->post('page_content')
                                    , "post_page_layout"=>$this->CI->input->post('page_layout')
                                    , "post_page_ip"=>$this->CI->input->post('page_ip')
                                    , "post_page_date"=>$this->CI->input->post('page_date')
                                    
                                    , "request_popup_name"=>$this->CI->input->request('popup_name')
                                    , "post_popup_no"=>$this->CI->input->post('popup_no')
                                    , "post_popup_title"=>$this->CI->input->post('popup_title')
                                    , "post_popup_name"=>$this->CI->input->post('popup_name')
                                    , "post_popup_content"=>$this->CI->input->post('popup_content')
                                    , "post_popup_layout"=>$this->CI->input->post('popup_layout')
                                    , "post_popup_ip"=>$this->CI->input->post('popup_ip')
                                    , "post_popup_date"=>$this->CI->input->post('popup_date')
								);


		$search_array		=	array(
									"search_type"=>$this->CI->input->request('search_type')
									, "search_name"=>$this->CI->input->request('search_name')
									, "page"=>$this->CI->input->request('page')
								);


		return array("session_array"=>$session_array, "menu_array"=>$menu_array, "member_array"=>$member_array, "board_array"=>$board_array, "pager_array"=>$pager_array ,"search_array"=>$search_array);

	}


	public function GetConfig(){	// 기본설정 가져오기 2019-05-07

		$config_query	= "";
		$config_query	.= " select ";
		$config_query	.= " * ";
		$config_query	.= " from ";
		$config_query	.= " admin_config ";
		$config_query	.= " where ";
		$config_query	.= " 1 = 1 ";
		//echo $config_query;

		$config_result	= $this->CI->db->query($config_query);


		$company_query	= "";
		$company_query	.= " select ";
		$company_query	.= " * ";
		$company_query	.= " from ";
		$company_query	.= " admin_company ";
		$company_query	.= " where ";
		$company_query	.= " 1 = 1 ";
		//echo $company_query;

		$company_result	= $this->CI->db->query($company_query);

		return array("config"=>$config_result, "company"=>$company_result);

	}


	public function GetMenu(){	// 메뉴 가져오기 2019-05-09

		$query	= "";
		$query	.= " select ";
		$query	.= " menu_no ";
		$query	.= " , menu_grandparent ";
		$query  .= " , menu_parent ";
		$query	.= " , menu_name ";
		$query	.= " , menu_url ";
		$query	.= " , menu_target ";
		$query	.= " , menu_date ";
		$query	.= " from ";
		$query	.= " admin_menu ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " order by menu_grandparent , menu_parent, menu_child ";
		//echo $query;
		
		$result	= $this->CI->db->query($query);
		
		return $result;
		
	}


	public function GetBoardConfig($array){	// 게시판 기본설정 가져오기 2019-05-14


		$query	= "";
		$query	.= " select ";
		$query	.= " * ";
		$query	.= " from ";
		$query	.= " admin_board";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		$query	.= " and ";
		$query	.= " con_name = " . $this->CI->db->escape($array) . " ";
		//echo $query;

		$result	= $this->CI->db->query($query);

		return $result;

	}


	public function GetLatest($table, $range, $pk, $limit){	// 최신 게시물 가져오기 2019-05-28 (테이블명, desc/asc, 정렬순 컬럼, 갯수)


		$bbs_query	= "";
		$bbs_query	.= " select ";
		$bbs_query	.= " * ";
		$bbs_query	.= " from ";
		$bbs_query	.= " " . $table . " ";
		$bbs_query	.= " where ";
		$bbs_query	.= " 1 = 1 ";
		$bbs_query	.= " order by " . $range . " " . $pk . " limit " . $limit . " ";
		//echo $bbs_query;

		$result	= $this->CI->db->query($bbs_query);

		return $result;

	}


	public function GetLatestContent($skin, $table, $range, $pk, $limit){	// 최신 게시물 스킨 가져오기 2019-05-29


		$rows['get_config']		= $this->GetBoardConfig($table);
		$rows['config_count']	= $rows['get_config']->num_rows();
		$rows['config_print']	= $rows['get_config']->row_array();

		$rows['title']			= $rows['config_print']['con_title'];
		$rows['table']			= $rows['config_print']['con_name'];
		$rows['skin']			= $rows['config_print']['con_skin'];


		if($rows['config_count'] >= 1){
			
			$rows['get_crud']		= $this->GetLatest($table, $range, $pk, $limit);
			$rows['crud_count']		= $rows['get_crud']->num_rows();
			$rows['crud_result']	= $rows['get_crud']->result_array();
			
			$this->CI->load->view("skin/latests/" . $skin . "/LatestView", $rows);
			
		} else {
			
			$this->CI->load->view("skin/latests/" . $skin . "/LatestView", $rows);
			
		}

	}
	
	
	public function GetPopupContent($parameters, $top, $zindex, $set_num){      // 팝업 컨텐츠 가져오기 2019-09-25
		
		
		$this->CI->load->model('dashboard/PagerModel');
		
		
		$rows['popup_content']          = $this->CI->PagerModel->GetPopupContent($parameters);
		$rows['count']                  = $rows['popup_content']->num_rows();
		$rows['print']                  = $rows['popup_content']->row_array();
		
		
		if($rows['count'] >= 1){
	?>
		
            <style>
            /**
                포지션은 가변적인 변화를 줄 수 있도록 파라미터를 이용하고
                나머지는 디자이너가 디자인 할 수 있도록 자율성을 보장
            **/
            .pop_<?=$set_num?> {
                position:absolute;
                top:<?=$top?>px;
                z-index:<?=$zindex?>;
            }
            </style>
            
            
            <script>
            function getCookie_<?=$set_num?>(cname) {
                
                var name    = cname + "=";
                var ca      = document.cookie.split(';');
                
                for(var i = 0; i < ca.length; i++) {
                    
                    var c   = ca[i];
                    
                    while (c.charAt(0)==' ')
                        c   = c.substring(1);
                    
                    if (c.indexOf(name) != -1)
                        return c.substring(name.length,c.length);
                    
                }
                
                return "";
                
            }
            
            
            function setCookie_<?=$set_num?>(cname, cvalue, exdays) {
                
                var date    = new Date();
                date.setTime(date.getTime() + (exdays*24*60*60*1000));
                
                var expires = "expires="+date.toUTCString();
                document.cookie = cname + "=" + cvalue + "; " + expires;
                
            }
            
            
            function couponClose_<?=$set_num?>(){
                
                if($("input[name='chkbox_<?=$set_num?>']").is(":checked") == true){
                    
                    setCookie_<?=$set_num?>("close_<?=$set_num?>", "Y", 1);
                    
                }
                
                $(".pop_<?=$set_num?>").hide();
                
            }
            
            
            $(document).ready(function(){
                
                cookiedata      = document.cookie;
                
                if(cookiedata.indexOf("close_<?=$set_num?>=Y") < 0){
                    
                    $(".pop_<?=$set_num?>").show();
                    
                } else {
                    
                    $(".pop_<?=$set_num?>").hide();
                    
                }
                
                $(".close_<?=$set_num?>").click(function(){
                    
                    couponClose_<?=$set_num?>();
                    
                });
                
            });
            </script>
		
	<?php
		
            $html   = "";
            $html   .=  "<div class=\"pop_".$set_num."\">";
            $html   .=      "<div class=\"cont_".$set_num."\">";
            $html   .=          $rows['print']['popup_content'];
            $html   .=      "</div>";
            $html   .=      "<div class=\"close\">";
            $html   .=          "<form method=\"post\" action=\"\" name=\"pop_form\">";
            $html   .=              "<span class=\"check\"><input type=\"checkbox\" value=\"checkbox\" name=\"chkbox_".$set_num."\" class=\"chkday\" /><label for=\"chkday\">오늘 하루동안 보지 않기</label></span>";
            $html   .=              "<span class=\"close_".$set_num."\">Close</span>";
            $html   .=          "</form>";
            $html   .=      "</div>";
            $html   .=  "</div>";
            
        } else {
		    
		    $html   = "";
		    
        }

		return $html;
		
	}
	
	
	public function CheckPermission($perm_member_id, $perm_page){	// permission check 2019-10-08
		
		$query	= "";
		$query	.= " select ";
		$query	.= " member_id ";
		$query	.= " , perm_member_id ";
		$query	.= " , perm_page ";
		$query	.= " from ";
		$query	.= " admin_permission ";
		$query	.= " where ";
		$query	.= " 1 = 1 ";
		
		if($perm_member_id == "admin"){
			
			$query	.= " and ";
			$query	.= " member_id = 'admin' ";
			
		} else {
			
			$query	.= " and ";
			$query	.= " perm_member_id = " . $this->CI->db->escape($perm_member_id) . " ";
			$query	.= " and ";
			$query	.= " perm_page = " . $this->CI->db->escape($perm_page) . " ";
			
		}
		//echo $query;
		
		$result	= $this->CI->db->query($query);
		
		return $result;
		
	}


}