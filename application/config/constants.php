<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0664);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0664);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/** Custom define funtionals start 2019-01-11 **/
define("DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT']);														// 루트 디렉토리 상대경로
define("SERVER_NAME", $_SERVER['SERVER_NAME']);															// 루트 디렉토리 절대경로
define("REQUEST_URI", $_SERVER['REQUEST_URI']);															// 현재 페이지 주소
define("GET_IP", $_SERVER['REMOTE_ADDR']);																// 현재 IP 주소
define("HTTP_HOST", $_SERVER['HTTP_HOST']);																// 호스트 정보
define("PROTOCOLS", (@$_SERVER['HTTPS'] == "on") ? "https://" : "http://");								// 프로토콜 타입
define("GET_DIRECTORY", getcwd());																		// 현재 디렉토리를 포함 한 상대 경로를 출력
define("PHP_SELF_FILE", basename($_SERVER['PHP_SELF']));												// 현재 파일명을 가리킨다
define("PHP_SELF", $_SERVER['PHP_SELF']);


define("KO_LOGGED", "로그인 되었습니다.");
define("KO_LOGOUT", "로그아웃 되었습니다.");
define("KO_NEEDLOGIN", "로그인이 필요합니다.");
define("KO_CANTLOGIN", "로그인 하지 못 했습니다.");
define("KO_CANTMODIFY", "수정 할 수 없습니다.");
define("KO_CANTDELETE", "삭제 할 수 없습니다.");
define("KO_WRONG", "뭔가 잘 못 됐습니다.");
define("KO_SUCCESS", "완료 되었습니다.");
define("KO_UNSUCCESS", "완료 하지 못 했습니다.");
define("KO_NOPERMI", "권한이 없습니다.");
define("KO_NOPERMI_NEEDLOGIN", "권한이 없거나 로그인이 필요합니다.");
define("KO_DUPLICATED", "중복 된 값이 있습니다.");
define("KO_INVALID", "누락 된 정보가 있습니다.");
define("KO_CANCELED", "취소 되었습니다.");
define("KO_EMPTY", "내용이 없습니다.");
define("KO_NOTNOW", "지금은 작성할 수 업습니다.");
/** Custom define funtionals end 2019-01-11 **/