<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>


<script>
    $(document).ready(function(){

        var member_id, member_password, member_name, member_email_1, member_email_2, member_profile, check_agree, message, add_duplicated_id, add_duplicated_email;
        var regular		= /[0-9a-z_-]/;

        $("#SetForms").submit(function(){

            member_id				= $(".member_id").val();
            member_password			= $(".member_password").val();
            member_name				= $(".member_name").val();
            member_email_1			= $(".member_email_1").val();
            member_email_2			= $(".member_email_2").val();
            member_profile			= $("select[name=\"member_profile\"]").val();
            check_agree				= $("input:checkbox[name=\"check_agree\"]:checked").val();
            add_duplicated_id       = $(".add_duplicated_id").val();
            add_duplicated_email    = $(".add_duplicated_email").val();

            message					= confirm("회원가입을 할까요?");

            if(message){

                if(member_id == "") {

                    alert("아이디를 입력하세요.");
                    $(".member_id").focus();
                    return false;

                } else if(member_password == ""){

                    alert("비밀번호를 입력하세요.");
                    $(".member_password").focus();
                    return false;

                } else if(member_name == ""){

                    alert("이름을 입력하세요.");
                    $(".member_name").focus();
                    return false;

                } else if(member_email_1 == ""){

                    alert("이메일 아이디를 입력하세요.");
                    $(".member_email_1").focus();
                    return false;

                } else if(member_email_2 == ""){

                    alert("이메일 호스트를 입력하세요.");
                    $(".member_email_2").focus();
                    return false;

                } else if(member_profile == ""){

                    alert("회원정보 공개 유무를 선택하세요.");
                    $(".member_profile").focus();
                    return false;

                } else if(check_agree != "on"){

                    alert("약관 동의 없이 회원가입은 불가능 합니다.");
                    $(".check_agree").focus();
                    return false;

                } else if(add_duplicated_id != "1"){

                    alert("아이디 중복 체크를 하세요.");
                    $(".add_duplicated_id").focus();
                    return false;

                } else if(add_duplicated_email != "1"){

                    alert("이메일 중복 체크를 하세요.");
                    $(".add_duplicated_email").focus();
                    return false;

                }


            } else {

                alert("취소 되었습니다.");
                return false;

            }

        });


        $(".CheckDuplicatedID").click(function(){

            member_id				= $(".member_id").val();

            if(member_id == "") {

                alert("아이디를 입력하세요.");
                $(".member_id").focus();
                return false;

            } else if(member_id.length < "4"){

                alert("4자 이상 입력하세요.");
                $(".member_id").focus();
                return false;

            } else {

                CheckDuplicatedID(member_id);

            }

        });


        $(".CheckDuplicatedName").click(function(){

            member_name				= $(".member_name").val();

            if(member_name == "") {

                alert("이름을 입력하세요.");
                $(".member_name").focus();
                return false;

            } else if(member_name.length < "3"){

                alert("이름을 3자 이상 입력하세요.");
                $(".member_name").focus();
                return false;

            } else {

                CheckDuplicatedName(member_name);

            }

        });


        $(".CheckDuplicatedEmail").click(function() {

            member_email_1 = $(".member_email_1").val();
            member_email_2 = $(".member_email_2").val();

            if (member_email_1 == "") {

                alert("이메일 아이디를 입력하세요..");
                $(".member_email_1").focus();
                return false;

            } else if(member_email_1.length < "4"){

                alert("이메일 아이디를 4자 이상 입력하세요.");
                $(".member_email_1").focus();
                return false;

            } else if (member_email_2 == "") {

                alert("이메일 호스트를 입력하세요.");
                $(".member_email_2").focus();
                return false;

            } else if(member_email_2.length < "4"){

                alert("이메일 호스트를 4자 이상 입력하세요.");
                $(".member_email_2").focus();
                return false;

            } else {

                CheckDuplicatedEmail(member_email_1, member_email_2);

            }

        });


        $(".member_id").keyup(function(event){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();
                $(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
            }
        });

        $(".member_email_1").keyup(function(event){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();
                $(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
            }
        });

        $(".member_email_2").keyup(function(event){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();
                $(this).val(inputVal.replace(/[^a-z0-9.]/gi,''));
            }
        });

    });

    $(document).keypress(function(e){

        if(e.keyCode == 13)
            e.preventDefault();

    });


    function CheckDuplicatedID(member_id){

        var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/member/check_id/";
        var ajax_type			= "post";
        var ajax_return_type	= "json";
        var ajax_param			= [];
        var ajax_return_data;

        ajax_param.push("member_id=" + member_id);

        call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

        if(call_back.return_code == "0000"){

            html    = "";
            html    += "<input type=\"hidden\" name=\"add_duplicated_id\" class=\"add_duplicated_id\" value=\"1\">";

            alert("사용 가능 한 아이디 입니다.");
            $(".member_id").attr('readonly', 'readonly');
            $(".AddDuplicatedID").html(html);

        } else if(call_back.return_code == "9998" || call_back.return_code == "9999"){

            alert("<?=KO_DUPLICATED?>");

        }

    }


    function CheckDuplicatedName(member_name){

        var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/member/check_name/";
        var ajax_type			= "post";
        var ajax_return_type	= "json";
        var ajax_param			= [];
        var ajax_return_data;

        ajax_param.push("member_name=" + member_name);

        call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

        if(call_back.return_code == "0000"){

            html    = "";
            html    += "<input type=\"hidden\" name=\"add_duplicated_name\" class=\"add_duplicated_name\" value=\"1\">";

            alert("사용 가능 한 이름 입니다.");
            $(".member_name").attr('readonly', 'readonly');
            $(".AddDuplicatedName").html(html);

        } else if(call_back.return_code == "9998" || call_back.return_code == "9999"){

            alert("<?=KO_DUPLICATED?>");

        }

    }


    function CheckDuplicatedEmail(member_email_1, member_email_2){

        var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/member/check_email/";
        var ajax_type			= "post";
        var ajax_return_type	= "json";
        var ajax_param			= [];
        var ajax_return_data;

        ajax_param.push("member_email_1=" + member_email_1);
        ajax_param.push("member_email_2=" + member_email_2);

        call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

        if(call_back.return_code == "0000"){

            html    = "";
            html    += "<input type=\"hidden\" name=\"add_duplicated_email\" class=\"add_duplicated_email\" value=\"1\">";

            alert("사용 가능 한 이메일 입니다.");
            $(".member_email_1").attr('readonly', 'readonly');
            $(".member_email_2").attr('readonly', 'readonly');

            $(".AddDuplicatedEmail").html(html);

        } else if(call_back.return_code == "9999"){

            alert("<?=KO_DUPLICATED?>");

        }

    }
</script>



<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">
	<form id="SetForms" method="post" action="signup_ok" role="form">
		<h2 class="text-center">회원가입</h2>
		
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<input type="text" name="member_id" class="form-control input-lg member_id" placeholder="아이디(영문)">
					<span class="AddDuplicatedID" style="display:none;"></span>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<button type="button" class="btn btn-outline-primar CheckDuplicatedID">아이디 중복</button>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<input type="password" name="member_password" class="form-control input-lg member_password" placeholder="비밀번호">
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<input type="text" name="member_name" class="form-control input-lg member_name" placeholder="이름">
					<span class="AddDuplicatedName" style="display:none;"></span>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<button type="button" class="btn btn-outline-primar CheckDuplicatedName">이름 중복</button>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-10">
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="member_email_1" class="form-control input-lg member_email_1" placeholder="이메일 아이디">
						
						<span class="input-group-prepend" id="basic-addon1">
							<span class="input-group-text">@</span>
						</span>
						
						<input type="text" name="member_email_2" class="form-control input-lg member_email_2" placeholder="이메일 호스트">
						<span class="AddDuplicatedEmail" style="display:none;"></span>
					</div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<button type="button" class="btn btn-outline-primar CheckDuplicatedEmail">이메일 중복</button>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<select name="member_profile" id="inlineFormCustomSelect" class="custom-select mr-sm-2 member_profile">
						<option value="0" selected>비공개</option>
						<option value="1">공개</option>
					</select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-4 col-sm-3 col-md-3">
				<span class="button-checkbox">
					<label class="checkbox"><input type="checkbox" name="check_agree" class="btn btn-primary check_agree" data-dismiss="modal">동의</label>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type="submit" class="btn btn-primary btn-block btn-lg" value="회원가입">
			</div>
		</div>
	</form>
</div>