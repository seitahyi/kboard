<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script>
    $(document).ready(function(){

        var member_id, member_id2, member_password, member_name, member_email_1, member_email_2, member_profile, message;
        var regular		= /[0-9a-z_-]/;

        $("#SetForms").submit(function(){

            member_id				= $(".member_id").val();
            member_password			= $(".member_password").val();
            member_name				= $(".member_name").val();
            member_email_1			= $(".member_email_1").val();
            member_email_2			= $(".member_email_2").val();
            member_profile			= $("select[name=\"member_profile\"]").val();

            message					= confirm("정보를 수정 할까요?");


            if(message){

                if(member_id == ""){

                    alert("아이디를 입력하세요.");
                    $(".member_id").focus();
                    return false;

                } else if(member_password == ""){

                    alert("비밀번호를 입력하세요.");
                    $(".member_password").focus();
                    return false;

                } else if(member_name == ""){

                    alert("이름을 입력하세요.");
                    $(".member_name").focus();
                    return false;

                } else if(member_email_1 == ""){

                    alert("이메일 아이디를 입력하세요.");
                    $(".member_email_1").focus();
                    return false;

                } else if(member_email_2 == ""){

                    alert("이메일 호스트를 입력하세요.");
                    $(".member_email_2").focus();
                    return false;

                } else if(member_profile == ""){

                    alert("회원정보 공개 유무를 선택하세요.");
                    $(".member_profile").focus();
                    return false;

                }


            } else {

                alert("취소 되었습니다.");
                return false;

            }

        });


        /////////////////////////////////////////////


        $("#RemoveOk").click(function(){


            member_id				= "<?=$member_print['member_id']?>";
            member_id2				= "<?=$get_parameter['session_array']['sess_member_id']?>";
            message					= confirm("모든 내용이 소실됩니다. 그래도 탈퇴 할까요?");


            if(message){

                if(member_id == member_id2){

                    RemoveMember(member_id, member_id2);
                    location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>";

                } else {

                    alert("<?=KO_UNSUCCESS?>");
                    return false;

                }

            } else {

                alert("취소 되었습니다.");
                return false;

            }

        });

    });


    /////////////////////////////////////////////


    $(document).keypress(function(e){

        if(e.keyCode == 13)
            e.preventDefault();

    });


    /////////////////////////////////////////////


    function RemoveMember(member_id, member_id2){

        var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/member/drop";
        var ajax_type			= "post";
        var ajax_return_type	= "json";
        var ajax_param			= [];
        var ajax_return_data;

        ajax_param.push("member_id=" + member_id);
        ajax_param.push("member_id2=" + member_id2);

        call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

        if(call_back.return_code == "0000"){

            alert("<?=KO_SUCCESS?>");
            //location.reload();

        } else if(call_back.return_code == "9999"){

            alert("<?=KO_UNSUCCESS?>");

        } else {

            alert(call_back.return_code);

        }

    }
</script>


<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">
	
	<nav>
		<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
			<a class="nav-item nav-link active" id="nav-modify-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/modify/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-modify" aria-selected="true">회원수정</a>
			<a class="nav-item nav-link" id="nav-board_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/board_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-board_list" aria-selected="false">작성글</a>
			<a class="nav-item nav-link" id="nav-reply_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/reply_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-reply_list" aria-selected="false">작성 댓글</a>
			<a class="nav-item nav-link" id="nav-voted_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/voted_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-voted_list" aria-selected="false">추천/비추천</a>
			<a class="nav-item nav-link" id="nav-file_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/file_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-file_list" aria-selected="false">첨부파일</a>
		</div>
	</nav>
	<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
		<div class="tab-pane fade show active" id="nav-modify" role="tabpanel" aria-labelledby="nav-modify-tab">
			
			
			<form id="SetForms" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/member/modify_ok" role="form">
				
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="member_id" class="form-control input-lg member_id" value="<?=$member_print['member_id']?>" readonly>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<input type="password" name="member_password" class="form-control input-lg member_password" value="<?=$this->common->GetDecrypt($member_print['member_password'], @$secret_key, @$secret_iv)?>" placeholder="비밀번호">
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="member_name" class="form-control input-lg member_name" value="<?=$this->common->GetDecrypt($member_print['member_name'], @$secret_key, @$secret_iv)?>" placeholder="이름">
						</div>
					</div>
				</div>
				
				<?php
				$email_explode		= explode("@", $member_print['member_email']);
				?>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control input-lg member_email_1" value="<?=$this->common->GetDecrypt($email_explode['0'], @$secret_key, @$secret_iv)?>" placeholder="이메일 아이디" readonly>
								<span class="input-group-prepend" id="basic-addon1">
									<span class="input-group-text">@</span>
								</span>
								<input type="text" class="form-control input-lg member_email_2" value="<?=$this->common->GetDecrypt($email_explode['1'], @$secret_key, @$secret_iv)?>" placeholder="이메일 호스트" readonly>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<select name="member_profile" id="inlineFormCustomSelect" class="custom-select mr-sm-2 member_profile">
								<option value="0" <?=($member_print['member_profile']==0)?"selected":""?>>비공개</option>
								<option value="1" <?=($member_print['member_profile']==1)?"selected":""?>>공개</option>
							</select>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<input type="submit" class="btn btn-primary btn-block btn-lg" value="회원수정">
					</div>
					<div class="col-md-6">
						<input type="button" id="RemoveOk" class="btn btn-danger btn-block btn-lg" value="회원탈퇴">
					</div>
				</div>
			</form>
		
		
		</div>
		
		<div class="tab-pane fade" id="nav-board_list" role="tabpanel" aria-labelledby="nav-board_list-tab">
		</div>
		
		<div class="tab-pane fade" id="nav-reply_list" role="tabpanel" aria-labelledby="nav-reply_list-tab">
		</div>
		
		<div class="tab-pane fade" id="nav-voted_list" role="tabpanel" aria-labelledby="nav-voted_list-tab">
		</div>
		
		<div class="tab-pane fade" id="nav-file_list" role="tabpanel" aria-labelledby="nav-file_list-tab">
		</div>
	</div>

</div>