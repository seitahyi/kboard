<?php
if(!defined('BASEPATH')) exit;
?>


	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/mobile.css">
	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/style.css">


	<script>
	$(document).ready(function(){
		
		var member_id, member_password;
		var regular		= /[0-9a-z_-]/;
		
		$("#flogin").submit(function(){
			
            member_id				= $(".member_id").val();
            member_password			= $(".member_password").val();

			if(member_id == ""){

                alert("아이디를 입력하세요.");
                $(".member_id").focus();
                return false;

            } else if(member_password == ""){

                alert("비밀번호를 입력하세요.");
                $(".member_password").focus();
                return false;

            }
			         
		});
		
		$(".member_id").keyup(function(event){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();
                $(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
            }
        });
		
	});
	
	$(document).keypress(function(e){

        if(e.keyCode == 13)
            e.preventDefault();

    });
	</script>
	
	
	<div id="mb_login" class="mbskin">
		<h1>로그인</h1>
		
		<form name="flogin" action="signin_ok" method="post" id="flogin">
			<div id="login_frm">
				<input type="text" name="member_id" id="login_id" placeholder="아이디" required class="frm_input member_id required" maxlength="20">
				<input type="password" name="member_password" id="login_pw" placeholder="비밀번호" required class="frm_input member_password required" maxlength="20">
				<input type="submit" class="btn_submit btn" value="로그인">
			</div>
			
			<section class="mb_login_join">
				<div>
					<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signup" class="login_join">회원 가입</a>
					<a href="#" id="login_password_lost">&nbsp;</a>
				</div>
				<div>
					<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/lost_id" class="login_join">아이디 찾기</a>
					<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/lost_password" id="login_password_lost">비밀번호 찾기</a>
				</div>
			</section>
		</form>
	</div>