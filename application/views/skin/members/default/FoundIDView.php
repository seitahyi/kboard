<?php
if(!defined('BASEPATH')) exit;
?>


	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/mobile.css">
	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/style.css">


	<div id="wrapper">
		
		<div id="container">
			<h2 id="container_title" class="top sub_tit">
				<span>아이디 찾기</span>
			</h2>
			
			<div class="register">
				<div id="fregisterform" class="register_inner">
					<div class="form_01">
						<h2>찾은 아이디</h2>
						<ul>
							<li>
								<label for="reg_mb_id">
									가입된 아이디 : <?=$set_id?>****
								</label>
							</li>
							<li>
								<label for="reg_mb_id">
									최근 로그인 : <?=$member_print['member_signup_date']?>
								</label>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>