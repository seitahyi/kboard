<?php
if(!defined('BASEPATH')) exit;
?>


	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/mobile.css">
	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/style.css">

	<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>


	<script>
	$(document).ready(function(){
		
		var member_id, member_id2, member_password, member_name, member_email_1, member_email_2, member_profile, message;
		var regular		= /[0-9a-z_-]/;
		
		$("#fregisterform").submit(function(){
			
			member_id				= $(".member_id").val();
			member_password			= $(".member_password").val();
			member_name				= $(".member_name").val();
			member_profile			= $("select[name=\"member_profile\"]").val();
			
			message					= confirm("정보를 수정 할까요?");
			
			if(message){
				
				if(member_id == ""){
					
					alert("아이디를 입력하세요.");
					$(".member_id").focus();
					return false;
					
				} else if(member_password == ""){
					
					alert("비밀번호를 입력하세요.");
					$(".member_password").focus();
					return false;
					
				} else if(member_name == ""){
					
					alert("이름을 입력하세요.");
					$(".member_name").focus();
					return false;
					
				} else if(member_profile == ""){
					
					alert("회원정보 공개 유무를 선택하세요.");
					$(".member_profile").focus();
					return false;
					
				}
				
				
			} else {
				
				alert("취소 되었습니다.");
				return false;
				
			}
			
		});
		
		
		/////////////////////////////////////////////
		
		
		$("#RemoveOk").click(function(){
			
			member_id				= "<?=$member_print['member_id']?>";
			member_id2				= "<?=$get_parameter['session_array']['sess_member_id']?>";
			message					= confirm("모든 내용이 소실됩니다. 그래도 탈퇴 할까요?");
			
			
			if(message){
				
				if(member_id == member_id2){
					
					RemoveMember(member_id, member_id2);
					location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>";
					
				} else {
					
					alert("<?=KO_UNSUCCESS?>");
					return false;
					
				}
				
			} else {
				
				alert("취소 되었습니다.");
				return false;
				
			}
			
		});
	
	});
	
	
	/////////////////////////////////////////////
	
	
	$(document).keypress(function(e){

        if(e.keyCode == 13)
            e.preventDefault();

    });
	
	
	/////////////////////////////////////////////


    function RemoveMember(member_id, member_id2){

        var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/member/drop";
        var ajax_type			= "post";
        var ajax_return_type	= "json";
        var ajax_param			= [];
        var ajax_return_data;

        ajax_param.push("member_id=" + member_id);
        ajax_param.push("member_id2=" + member_id2);

        call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

        if(call_back.return_code == "0000"){

            alert("<?=KO_SUCCESS?>");
            //location.reload();

        } else if(call_back.return_code == "9999"){

            alert("<?=KO_UNSUCCESS?>");

        } else {

            alert(call_back.return_code);

        }

    }
	</script>

	<div id="wrapper">
		
		<div id="container">
			<h2 id="container_title" class="top sub_tit">
				<span>회원 정보 수정</span>
			</h2>
			
			<div class="register">
				
				<form id="fregisterform" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/member/modify_ok">
					
					<div class="register_inner">
						<div class="form_01">
							<h2>이용정보 입력</h2>
							<ul>
								<li>
									<label for="reg_mb_id">아이디</label>
									<input type="text" name="member_id" class="frm_input full_input readonly member_id" minlength="3" placeholder="아이디" value="<?=$member_print['member_id']?>" readonly>
									<!--
									<span class="frm_info"><i class="fa fa-exclamation-circle"></i> 영문자, 숫자, _ 만 입력 가능. 최소 3자이상 입력하세요.</span>
									-->
								</li>
								<li>
									<label for="reg_mb_password">비밀번호</label>
									<input type="password" name="member_password" class="frm_input full_input member_password" minlength="3" placeholder="비밀번호" value="<?=$this->common->GetDecrypt($member_print['member_password'], @$secret_key, @$secret_iv)?>">
								</li>
								<li>
									<label for="reg_mb_password">이름</label>
									<input type="text" name="member_name" class="frm_input full_input member_name" minlength="3" placeholder="이름(닉네임)" value="<?=$this->common->GetDecrypt($member_print['member_name'], @$secret_key, @$secret_iv)?>">
								</li>
								<li>
									<?php
									$email_explode		= explode("@", $member_print['member_email']);
									?>
									<label for="reg_mb_password">이메일</label>
									<input type="text" class="frm_input full_input" minlength="3" placeholder="이메일 아이디" value="<?=$this->common->GetDecrypt($email_explode['0'], @$secret_key, @$secret_iv)?>@<?=$this->common->GetDecrypt($email_explode['1'], @$secret_key, @$secret_iv)?>" readonly>
								</li>
								<li>
									<label for="reg_mb_password">공개여부</label>
									<select name="member_profile" class="frm_input full_input member_profile">
										<option value="0" <?=($member_print['member_profile']==0)?"selected":""?>>비공개</option>
										<option value="1" <?=($member_print['member_profile']==1)?"selected":""?>>공개</option>
									</select>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="btn_confirm">
						<button type="submit" id="btn_submit" class="btn btn_submit">정보수정</button>
						<button type="button" id="RemoveOk" class="btn btn_cancel">회원탈퇴</button>
					</div>
				
				</form>
			</div>
		</div>
	</div>