<?php
if(!defined('BASEPATH')) exit;
?>


	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/mobile.css">
	<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/members/<?=$config['member_skin']?>/style.css">


	<script>
	$(document).ready(function(){
		
		var member_email_1, member_email_2;
        var regular		= /[0-9a-z_-]/;

        $("#fregisterform").submit(function(){

            member_email_1			= $(".member_email_1").val();
            member_email_2			= $(".member_email_2").val();

            if(member_email_1 == ""){

                alert("이메일 아이디를 입력하세요.");
                $(".member_email_1").focus();
                return false;

            } else if(member_email_2 == ""){

                alert("이메일 호스트를 입력하세요.");
                $(".member_email_2").focus();
                return false;

            }

        });
        
		$(".member_email_1").keyup(function(event){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();
                $(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
            }
        });

        $(".member_email_2").keyup(function(event){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();
                $(this).val(inputVal.replace(/[^a-z0-9.]/gi,''));
            }
        });
	});
	
	
	$(document).keypress(function(e){

        if(e.keyCode == 13)
            e.preventDefault();

    });
	</script>


	<div id="wrapper">
		
		<div id="container">
			<h2 id="container_title" class="top sub_tit">
				<span>아이디 찾기</span>
			</h2>
			
			<div class="register">
				
				<form id="fregisterform" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/member/lost_id_ok">
					
					<div class="register_inner">
						<div class="form_01">
							<h2>아이디 찾기</h2>
							<ul>
								<li>
									<label for="reg_mb_id">이메일</label>
									<input type="text" name="member_email_1" class="frm_input full_input readonly member_email_1" minlength="3" placeholder="이메일 아이디">@
									<input type="text" name="member_email_2" class="frm_input full_input readonly member_email_2" minlength="3" placeholder="이메일 호스트">
								</li>
							</ul>
						</div>
					</div>
					
					<div class="btn_confirm">
						<button type="submit" id="btn_submit" class="btn btn_submit">찾기</button>
					</div>
				
				</form>
			</div>
		</div>
	</div>