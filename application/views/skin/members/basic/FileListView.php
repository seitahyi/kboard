<?php
if(!defined('BASEPATH')) exit;
?>


<style>
	img{
		width:359px;
	}
</style>


<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">
	
	<nav>
		<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
			<a class="nav-item nav-link" id="nav-modify-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/modify/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-modify" aria-selected="false">회원수정</a>
			<a class="nav-item nav-link" id="nav-board_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/board_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-board_list" aria-selected="false">작성글</a>
			<a class="nav-item nav-link" id="nav-reply_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/reply_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-reply_list" aria-selected="false">작성 댓글</a>
			<a class="nav-item nav-link" id="nav-voted_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/voted_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-voted_list" aria-selected="false">추천/비추천</a>
			<a class="nav-item nav-link active" id="nav-file_list-tab" data-toggle="link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/file_list/<?=$get_parameter['session_array']['sess_member_id']?>" role="link" aria-controls="nav-file_list" aria-selected="true">첨부파일</a>
		</div>
	</nav>
	
	
	<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
		
		<div class="tab-pane fade" id="nav-modify" role="tabpanel" aria-labelledby="nav-modify-tab">
		</div>
		
		<div class="tab-pane fade" id="nav-board_list" role="tabpanel" aria-labelledby="nav-board_list-tab">
		</div>
		
		<div class="tab-pane fade" id="nav-reply_list" role="tabpanel" aria-labelledby="nav-reply_list-tab">
		</div>
		
		<div class="tab-pane fade" id="nav-voted_list" role="tabpanel" aria-labelledby="nav-voted_list-tab">
		</div>
		
		<div class="tab-pane fade show active" id="nav-file_list" role="tabpanel" aria-labelledby="nav-file_list-tab">
			
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">첨부파일(총:<?=$get_members_file_count->num_rows()?>)</h3>
				</div>
				
				<div class="card-body">
					<div class="table-responsive">
						<table id="table" class="table table-striped text-nowrap">
							<thead>
							<tr>
								<th>
									번호
								</th>
								<th>
									파일
								</th>
								<th>
									파일형식
								</th>
								<th>
									아이피
								</th>
								<th>
									작성일
								</th>
							</tr>
							</thead>
							<tbody>
							
							<?php
							if($count >= 1){
								
								foreach($result as $print){
									?>
									
									<tr>
										<td>
											<?=$num++?>
										</td>
										<td>
											<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$print['con_name']?>/no/<?=$print['bbs_no']?>" target="_blank">
												<?php
												$file_type		= array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/png","image/bmp");
												
												if(in_array($print['file_type'], $file_type)){
													?>
													<img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/<?=$print['con_name']?>/<?=$print['bbs_no']?>/<?=$print['file_encrypt_name']?>">
													<?php
												} else {
													echo $print['file_real_name'];
												}
												?>
											</a>
										</td>
										<td>
											<?=$print['file_type']?>
										</td>
										<td>
											<?=$print['file_ip']?>
										</td>
										<td>
											<?=$print['file_date']?>
										</td>
									</tr>
									
									<?php
								}
								
								
							} else {
								?>
								
								<tr>
									<td>
										내용이 없습니다.
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								
								<?php
							}
							?>
							
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="card-footer">
					
					<div class="row">
						<div class="col-md-8 text-left">
							<div class="form-group">
								<?=$this->common->GetPaging("member/file_list/" . $get_parameter['member_array']['request_member_id'], $get_members_file_count->num_rows(), $id, $page, @$get_parameter['search_array']['search_type'], @$get_parameter['search_array']['search_name'], $start, $end)?>
							</div>
						</div>
					</div>
				
				</div>
			
			</div>
		
		
		</div>
	</div>

</div>