<?php
if(!defined('BASEPATH')) exit;
?>


<!DOCTYPE html>
<html lang="ko" class="js no-overflowscrolling">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=0,maximum-scale=10,user-scalable=yes">
		<meta name="HandheldFriendly" content="true">
		<meta name="format-detection" content="telephone=no">
		<meta http-equiv="X-UA-Compatible" content="IE=10,chrome=1">
		<?=@$config['home_naver']?>
		<?=@$config['home_google']?>
		
		<title><?=@$config['home_title']?>::<?=@$board_print['bbs_title']?></title>
		
		<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/mobile.css">
		<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/style.css">
		<!-- <link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/latest.css"> -->
		<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/font-awesome.min.css">
		
		<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/jquery.js"></script>
		<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/jquery.bxslider.js"></script>
		
		<?php
        if(empty($config['home_icon'])) {
        ?>
            <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/favicon.ico" type="image/x-icon"/>
        <?php
        } else {
        ?>
            <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_icon']?>" type="image/x-icon"/>
        <?php
        }
        ?>
		
		<script>
		$(document).ready(function(){
			
			$(".btn_gnb_op").click(function(){
				
				 $(this).toggleClass("btn_gnb_cl").next(".gnb2_2dul").slideToggle(300);
				 
			});
			
			$(".hd_closer").on("click", function() {
				
				var idx = $(".hd_closer").index($(this));
				
				$(".hd_div:visible").hide();
				$(".hd_opener:eq(" + idx + ")").find("span").text("열기");
				
			});
			
			$(".hd_sch_btn").on("click", function() {
				
				 $("#hd_sch").show();
				 
			});
			
			$("#hd_sch .btn_close").on("click", function() {
				
				$("#hd_sch").hide();
				
			});
			
			$("#gnb_open").on("click", function() {
				
				$("#gnb2").show();
				
			});
			
			$("#gnb2 .btn_close").on("click", function() {
				
				$("#gnb2").hide();
				
			});
			
			
			/** 위아래 메뉴 슬라이더 시작 **/
			var nav         = $('.depth > li');
			var childnav    = nav.children('ul');
			var _childnav   = childnav.children('li');
			var durian      = 400;
			var bg          = $('.bgGnb');
			
			nav.on({
				mouseenter : function(){
					bg.stop().slideDown(durian);
					childnav.stop().slideDown(durian);
					$(this).addClass('on');
					$(this).siblings('li').removeClass('on')
				}
			});
			
			_childnav.mouseenter(function(){
				$(this).parent().addClass('on')
			});
			
			$("#hd").mouseleave(function(){
				nav.removeClass('on');
				bg.stop().slideUp(durian);
				childnav.stop().slideUp(durian);
			});
			/** 위아래 메뉴 슬라이더 끝 **/
			
			
			/** 좌우 이미지 슬라이더 시작 **/
			$('.lt_bn ul').show().bxSlider({
				pagerCustom: '#bx_pager',
			    controls:false,
			    auto:true,
			    pause: 8000,
			    speed: 800,
			
			    onSliderLoad: function () {
					$('.lt_bn .bn_txt').eq(1).addClass('active-slide');
			        $(".bn_txt.active-slide").addClass("test");
			    },
				onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
					console.log(currentSlideHtmlObject);
			        $('.active-slide').removeClass('active-slide');
			        $('.lt_bn .bn_txt').eq(currentSlideHtmlObject + 1).addClass('active-slide');
			        $(".bn_txt.active-slide").addClass("test");
			    },
			    onSlideBefore: function () {
					$(".bn_txt.active-slide").removeClass("test");
			        $(".one.bn_txt.active-slide").removeAttr('style');
			    }
			});
			/** 좌우 이미지 슬라이더 끝 **/
			
		});
		</script>
	</head>
	
	<body>

		<header id="hd">
			
		    <h1 id="hd_h1"><?=@$config['home_title']?></h1>
			
		    <div class="to_content">
			    <a href="#container">본문 바로가기</a>
		    </div>
		
		    <div id="hd_wrapper">
		
		        <div id="logo">
			        <!--
		            <a href="#"><img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_logo']?>" alt="<?=@$config['home_title']?>"></a>
		            -->
			        <a href="#">1111</a>
		        </div>
		
		        <button type="button" id="gnb_open"><i class="fa fa-bars"></i><span class="sound_only"> 메뉴열기</span></button>
				<ul>
					<li>
						<a class="left_util" href="#"><span class="top_txt">약도다</span></a>
					</li>
					<li>
						<a class="left_util" href="#"><span class="top_txt">HOME</span></a>
					</li>
				</ul>
		
		        <div id="gnb" class="pc_view">
		            <ul class="depth">
			            <li class="depth_li">
		                    <a class="mm" href="#">1차 메뉴 예시</a>
		                </li>
		                <li class="depth_li">
		                    <a class="mm" href="#">2차 메뉴 예시</a>
		                    <ul class="subnav" style="overflow:hidden;">
		                        <li>
			                        <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1">테스트 게시판 1</a>
		                        </li>
								<li>
									<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_2">테스트 게시판 2</a>
								</li>
			                    <li>
									<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_3">테스트 게시판 3</a>
								</li>
		                    </ul>
		                </li>
		            </ul>
		        </div>
				
				<div class="bgGnb" style="overflow: hidden;"></div>
		
		        <div id="gnb2">
		            <button type="button" class="btn_close"><i class="fa fa-times"></i></button>
		            <ul class="gnb_tnb">
			            <?php
	                    if(empty($get_parameter['session_array']['sess_member_id'])){
	                    ?>
	                        <li>
	                            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signup">회원가입</a>
	                        </li>
	                        <li>
	                            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signin">로그인</a>
	                        </li>
	                    <?php
	                    }
	                    
	                    if(isset($get_parameter['session_array']['sess_member_id'])){
	                    ?>
		                    <li>
			                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/modify/<?=$get_parameter['session_array']['sess_member_id']?>">회원수정</a>
		                    </li>
		                    <li>
			                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signout">로그아웃</a>
		                    </li>
	                    <?php
	                    }
	                    ?>
		            </ul>
		            <ul id="gnb2_1dul">
			            <li class="gnb2_1dli">
		                    <a href="#" class="gnb2_1da">1차 메뉴 예시</a>
		                </li>
			            <li class="gnb2_1dli">
		                    <a href="#" class="gnb2_1da">2차 메뉴 예시</a>
		                    <button type="button" class="btn_gnb_op">하위분류</button>
				            <ul class="gnb2_2dul">
					            <li class="gnb2_2dli">
						            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb2_2da"><span></span>테스트 게시판 1</a>
					            </li>
					            <li class="gnb2_2dli">
						            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb2_2da"><span></span>테스트 게시판 2</a>
					            </li>
					            <li class="gnb2_2dli">
						            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb2_2da"><span></span>테스트 게시판 3</a>
					            </li>
		                    </ul>
		                </li>
		            </ul>
		        </div>
		    </div>
		</header>