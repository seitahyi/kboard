<?php
if(!defined('BASEPATH')) exit;
?>


		<div id="ft">
		    <div class="ft_wr">
		        <div id="ft_copy">
		            <div class="ft_copy_left">
			            <?php
		                $exp_email      = explode("@", $company['com_manager_email']);
		                $exp_rotary     = explode("-", $company['com_rotary']);
		                ?>
						<p>
							상호명 : <?=@$this->common->GetDecrypt($company['com_name'], @$secret_key, @$secret_iv)?>　
							|　대표자 : <?=@$this->common->GetDecrypt($company['com_owner'], @$secret_key, @$secret_iv)?><br>
							주소 : <?=$this->common->GetDecrypt($company['com_zip_1'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($company['com_zip_2'], @$secret_key, @$secret_iv)?> <?=$this->common->GetDecrypt($company['com_address_1'], @$secret_key, @$secret_iv)?><?=$this->common->GetDecrypt($company['com_address_2'], @$secret_key, @$secret_iv)?><br>
							TEL : <?=$this->common->GetDecrypt($exp_rotary['0'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($exp_rotary['1'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($exp_rotary['2'], @$secret_key, @$secret_iv)?>　
							|　E-MAIL : <?=$this->common->GetDecrypt($exp_email['0'], @$secret_key, @$secret_iv)?>@<?=$this->common->GetDecrypt($exp_email['1'], @$secret_key, @$secret_iv)?>
						</p>
						<p>
							COPYRIGHT © <strong style="color:#afafaf; margin:0 5px;">목포방문요양.kr</strong> ALL RIGHT RESERVED
						</p>
					</div>
		        </div>
		    </div>
		</div>


	</body>
</html>