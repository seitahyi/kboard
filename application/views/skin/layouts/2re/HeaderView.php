<?php
if(!defined('BASEPATH')) exit;
?>


<!DOCTYPE html>
<html class="js no-overflowscrolling" lang="ko">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=0,maximum-scale=10,user-scalable=yes">
        <meta name="HandheldFriendly" content="true">
        <meta name="format-detection" content="telephone=no">
        <meta http-equiv="X-UA-Compatible" content="IE=10,chrome=1">
	    
        <title>
	        <?=@$config['home_title']?>::<?=@$board_print['bbs_title']?>
        </title>
	    
        <link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/mobile.css">
        <link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/font-awesome.css">
        <link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/font.css">
	    
        <script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/jquery.js"></script>
        <script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/common.js"></script>
        
	    <?php
        if(empty($config['home_icon'])) {
        ?>
            <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/favicon.ico" type="image/x-icon"/>
        <?php
        } else {
        ?>
            <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_icon']?>" type="image/x-icon"/>
        <?php
        }
        ?>
	    
	    
        <script>
        $(document).ready(function(){
        	
            $(".btn_gnb_op").click(function(){
                $(this).toggleClass("btn_gnb_cl").next(".gnb2_2dul").slideToggle(300);
            });
        
            $(".hd_closer").on("click", function() {
                var idx = $(".hd_closer").index($(this));
                $(".hd_div:visible").hide();
                $(".hd_opener:eq("+idx+")").find("span").text("열기");
            });
        
            $(".hd_sch_btn").on("click", function() {
                $("#hd_sch").show();
            });
        
            $("#hd_sch .btn_close").on("click", function() {
                $("#hd_sch").hide();
            });
        
            $("#gnb_open").on("click", function() {
                $("#gnb2").show();
            });
        
            $("#gnb2 .btn_close").on("click", function() {
                $("#gnb2").hide();
            });
        
        });
        </script>
    </head>
    
    <body class="fixed">

        <header id="hd">
            <h1 id="hd_h1"></h1>

            <div id="hd_wrapper">
        
                <div id="logo">
                    <a href="#">
                        <img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_logo']?>" width="288px" height="28" alt="이레 방문요양 센터">
                    </a>
                </div>
        
                <button type="button" id="gnb_open"><i class="fa fa-bars"></i><span class="sound_only"> 메뉴열기</span></button>
        
                <div id="gnb" class="pc_view">
                    <ul id="gnb_1dul">
	                    <li class="gnb_1dli">
                            <a href="#" class="gnb_1da">1차 메뉴 예시</a>
                        </li>
                        <li class="gnb_1dli">
                            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb_1da">2차 메뉴 예시</a>
                            <ul class="gnb_2dul">
                                <li class="gnb_2dli">
                                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb_2da"><span></span>테스트 게시판 1</a>
                                </li>
                                <li class="gnb_2dli">
                                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_2" class="gnb_2da"><span></span>테스트 게시판 2</a>
                                </li>
	                            <li class="gnb_2dli">
                                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_3" class="gnb_2da"><span></span>테스트 게시판 3</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
        
                <div id="gnb2">
                    <button type="button" class="btn_close"><i class="fa fa-times"></i></button>
                    <ul class="gnb_tnb">
	                    <?php
	                    if(empty($get_parameter['session_array']['sess_member_id'])){
	                    ?>
	                        <li>
	                            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signup">회원가입</a>
	                        </li>
	                        <li>
	                            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signin">로그인</a>
	                        </li>
	                    <?php
	                    }
	                    
	                    if(isset($get_parameter['session_array']['sess_member_id'])){
	                    ?>
		                    <li>
			                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/modify/<?=$get_parameter['session_array']['sess_member_id']?>">회원수정</a>
		                    </li>
		                    <li>
			                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signout">로그아웃</a>
		                    </li>
	                    <?php
	                    }
	                    ?>
                    </ul>
                    <ul id="gnb2_1dul">
	                    <li class="gnb2_1dli">
		                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb2_1da">1차 메뉴 예시</a>
	                    </li>
                        <li class="gnb2_1dli">
                            <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb2_1da">2차 메뉴 예시</a>
                            <button type="button" class="btn_gnb_op">하위분류</button>
                            <ul class="gnb2_2dul">
                                <li class="gnb2_2dli">
                                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_1" class="gnb2_2da"><span></span>테스트 게시판 1</a>
                                </li>
                                <li class="gnb2_2dli">
                                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_2" class="gnb2_2da"><span></span>테스트 게시판 2</a>
                                </li>
	                            <li class="gnb2_2dli">
                                    <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/board_test_3" class="gnb2_2da"><span></span>테스트 게시판 3</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </header>