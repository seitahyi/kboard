<?php
if(!defined('BASEPATH')) exit;
?>

		<div id="ft">
            <div class="ft_info">
	            
	            <?=$this->user->GetLatestContent("2re", "board_test_3", "bbs_no", "desc", "3");?>
                
                <div id="ft_contact">
	                <?php
	                $exp_email      = explode("@", $company['com_manager_email']);
	                $exp_rotary     = explode("-", $company['com_rotary']);
	                ?>
                    <h2><?=@$this->common->GetDecrypt($company['com_name'], @$secret_key, @$secret_iv)?></h2>
                    <ul>
                        <li>
	                        <i class="fa fa-phone"></i>TEL : <?=$this->common->GetDecrypt($exp_rotary['0'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($exp_rotary['1'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($exp_rotary['2'], @$secret_key, @$secret_iv)?>
                        </li>
                        <li>
	                        <i class="fa fa-phone"></i>E-mail : <?=$this->common->GetDecrypt($exp_email['0'], @$secret_key, @$secret_iv)?>@<?=$this->common->GetDecrypt($exp_email['1'], @$secret_key, @$secret_iv)?>
                        </li>
                        <li>
	                        <i class="fa fa-map-marker"></i><?=$this->common->GetDecrypt($company['com_zip_1'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($company['com_zip_2'], @$secret_key, @$secret_iv)?> <?=$this->common->GetDecrypt($company['com_address_1'], @$secret_key, @$secret_iv)?><?=$this->common->GetDecrypt($company['com_address_2'], @$secret_key, @$secret_iv)?>
                        </li>
                        <li class="res">
	                        <a href="#">고객문의</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="ft_wr">
                <div id="ft_copy">
	                <?=@$this->common->GetDecrypt($company['com_name'], @$secret_key, @$secret_iv)?>
	                ｜ 대표자 : <?=@$this->common->GetDecrypt($company['com_owner'], @$secret_key, @$secret_iv)?>
	                ㅣ E-mail : <?=$this->common->GetDecrypt($exp_email['0'], @$secret_key, @$secret_iv)?>@<?=$this->common->GetDecrypt($exp_email['1'], @$secret_key, @$secret_iv)?>
	                | 사업자번호 : 466-52-00403
	                ｜ 주소 : <?=$this->common->GetDecrypt($company['com_zip_1'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($company['com_zip_2'], @$secret_key, @$secret_iv)?> <?=$this->common->GetDecrypt($company['com_address_1'], @$secret_key, @$secret_iv)?><?=$this->common->GetDecrypt($company['com_address_2'], @$secret_key, @$secret_iv)?>
	                | TEL : <?=$this->common->GetDecrypt($exp_rotary['0'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($exp_rotary['1'], @$secret_key, @$secret_iv)?>-<?=$this->common->GetDecrypt($exp_rotary['2'], @$secret_key, @$secret_iv)?>
	                <br>All rights reserved.
                </div>
            </div>
        </div>
    
    </body>
</html>