<?php
if(!defined('BASEPATH')) exit;
?>


<div class="col-lg-8">
	
	<h1 class="mt-4">
		<?=$page_print['page_title']?>
	</h1>
	<hr>
	
	<p class="lead">
		<?=$page_print['page_content']?>
	</p>
	
	<p class="lead">
		<?php
		if(!empty($page_print['page_include'])){
			include APPPATH . "/views/skin/layouts/" . $page_print['page_layout'] . "/" . $page_print['page_include'];
		}
		?>
	</p>
	<hr>

	
</div>