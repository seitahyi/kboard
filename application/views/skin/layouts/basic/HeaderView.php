<?php
if(!defined('BASEPATH')) exit;
?>


<!DOCTYPE html>

	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<meta http-equiv="Content-Language" content="ko" />
		<meta name="msapplication-TileColor" content="#2d89ef">
		<meta name="theme-color" content="#4188c9">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">

        <title><?=@$config['home_title']?>::<?=@$board_print['bbs_title']?></title>

		<link href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/layouts/<?=$config['home_layout']?>/feathericon.css" rel="stylesheet">
		<link href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/bootstrap/bootstrap.min.css" rel="stylesheet">

        <?php
        if(empty($config['home_icon'])) {
        ?>
            <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/favicon.ico" type="image/x-icon"/>
        <?php
        } else {
        ?>
            <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_icon']?>" type="image/x-icon"/>
        <?php
        }
        ?>

		<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/vendors/bootstrap.bundle.min.js"></script>


		<style>
		body {
			padding-top: 56px;
		}
		</style>

	</head>


	<body>

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
		<div class="container">
			<a class="navbar-brand" href="<?=PROTOCOLS?><?=HTTP_HOST?>" title="<?=$config['home_title']?>">
                <?php
                if(empty($config['home_logo'])){
                ?>
                    <img src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/demo/brand/tabler.svg" class="header-brand-img">
                <?php
                } else {
                ?>
                    <img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_logo']?>" style="width:250px;height:40px;">
                <?php
                }
                ?>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">

                    
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                            2
                        </a>
                        <ul class="dropdown-menu">
                            <a href="#" class="dropdown-item">
                                2-1
                            </a>
                            <a href="#" class="dropdown-item">
                                2-2
                            </a>
                        </ul>
                    </li>

                    <li class="nav-item active">
						<a class="nav-link" href="<?=PROTOCOLS?><?=HTTP_HOST?>">메인</a>
					</li>

					<?php
					if(empty($get_parameter['session_array']['sess_member_id'])){
					?>
						<li class="nav-item">
							<a class="nav-link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signup">회원가입</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signin">로그인</a>
						</li>
					<?php
					}

					if(isset($get_parameter['session_array']['sess_member_id'])){
					?>
						<li class="nav-item">
							<a class="nav-link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/modify/<?=$get_parameter['session_array']['sess_member_id']?>">회원수정</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/signout">로그아웃</a>
						</li>
					<?php
					}
					?>
				</ul>
			</div>
		</div>
	</nav>


	<!-- Page Content -->
	<div class="container">

		<div class="row">