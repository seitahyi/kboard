<?php
if(!defined('BASEPATH')) exit;
?>


<div class="col-lg-8">
	
	<h1 class="mt-4">
		<?=$page_print['page_title']?>
	</h1>
	<hr>
	
	<p class="lead">
		<?=$page_print['page_content']?>
	</p>
	<hr>

	
</div>