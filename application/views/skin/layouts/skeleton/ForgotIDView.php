<?php
if(!defined('BASEPATH')) exit;
?>


<script>
$(document).ready(function(){

	var member_email_1, member_email_2;
	var regular		= /[0-9a-z_-]/;

	$("#SetForms").submit(function(){

		member_email_1			= $(".member_email_1").val();
		member_email_2			= $(".member_email_2").val();


		if(member_email_1 == ""){

			alert("이메일 아이디를 입력하세요.");
			$(".member_email_1").focus();
			return false;

		} else if(member_email_2 == ""){

			alert("이메일 호스트를 입력하세요.");
			$(".member_email_2").focus();
			return false;

		}

	});


	$(".member_email_1").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
		}
	});

	$(".member_email_2").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z0-9.]/gi,''));
		}
	});

});


///////////////////////////////////////


$(document).keypress(function(e){

	if(e.keyCode == 13)
		e.preventDefault();

});
</script>


<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">

	<form id="SetForms" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/member/lost_id_ok">
		<h2 class="text-center">아이디 찾기</h2>
		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="member_email_1" class="form-control input-lg member_email_1" placeholder="이메일 아이디">
						<span class="input-group-prepend" id="basic-addon1">
							<span class="input-group-text">@</span>
						</span>
						<input type="text" name="member_email_2" class="form-control input-lg member_email_2" placeholder="이메일 호스트">
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<input type="submit" class="btn btn-success btn-block btn-lg" value="찾기">
			</div>
		</div>
	</form>
</div>