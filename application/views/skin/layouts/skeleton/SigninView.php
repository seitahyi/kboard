<?php
if(!defined('BASEPATH')) exit;
?>


<script>
$(document).ready(function(){

	var member_id, member_password;
	var regular		= /[0-9a-z_-]/;

	$("#SetForms").submit(function(){

		member_id				= $(".member_id").val();
		member_password			= $(".member_password").val();

		if(member_id == ""){

			alert("아이디를 입력하세요.");
			$(".member_id").focus();
			return false;

		} else if(member_password == ""){

			alert("비밀번호를 입력하세요.");
			$(".member_password").focus();
			return false;

		}

	});


	$(".member_id").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
		}
	});

});

$(document).keypress(function(e){

	if(e.keyCode == 13)
		e.preventDefault();

});
</script>


<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">
	<form id="SetForms" method="post" action="signin_ok" role="form">
		<h2 class="text-center">로그인</h2>
		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" name="member_id" class="form-control input-lg member_id" placeholder="아이디(영문)">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<input type="password" name="member_password" class="form-control input-lg member_password" placeholder="비밀번호">
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<input type="submit" class="btn btn-success btn-block btn-lg" value="로그인">
			</div>
		</div>
	</form>

	<div class="row">
		<div class="col-md-12">
			<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/lost_id">아이디 찾기</a> | <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/member/lost_password">비밀번호 찾기</a>
		</div>
	</div>
</div>