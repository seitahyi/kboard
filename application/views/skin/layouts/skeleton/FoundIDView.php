<?php
if(!defined('BASEPATH')) exit;
?>

<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">
	<h2 class="text-center">찾은 아이디</h2>
	
	<br />
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<input type="text" class="form-control input-lg" value="가인 된 아이디 : <?=$set_id?>****" readonly>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<input type="text" class="form-control input-lg" value="최근 로그인 : <?=$member_print['member_signup_date']?>" readonly>
			</div>
		</div>
	</div>

</div>