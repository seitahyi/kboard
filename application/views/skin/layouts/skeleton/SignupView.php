<?php
if(!defined('BASEPATH')) exit;
?>


<script>
$(document).ready(function(){

	var member_id, member_password, member_name, member_email_1, member_email_2, member_profile, check_agree, message;
	var regular		= /[0-9a-z_-]/;

	$("#SetForms").submit(function(){

		member_id				= $(".member_id").val();
		member_password			= $(".member_password").val();
		member_name				= $(".member_name").val();
		member_email_1			= $(".member_email_1").val();
		member_email_2			= $(".member_email_2").val();
		member_profile			= $("select[name=\"member_profile\"]").val();
		check_agree				= $("input:checkbox[name=\"check_agree\"]:checked").val();

		message					= confirm("회원가입을 할까요?");

		if(message){

			if(member_id == ""){

				alert("아이디를 입력하세요.");
				$(".member_id").focus();
				return false;

			} else if(member_password == ""){

				alert("비밀번호를 입력하세요.");
				$(".member_password").focus();
				return false;

			} else if(member_name == ""){

				alert("이름을 입력하세요.");
				$(".member_name").focus();
				return false;

			} else if(member_email_1 == ""){

				alert("이메일 아이디를 입력하세요.");
				$(".member_email_1").focus();
				return false;

			} else if(member_email_2 == ""){

				alert("이메일 호스트를 입력하세요.");
				$(".member_email_2").focus();
				return false;

			} else if(member_profile == ""){

				alert("회원정보 공개 유무를 선택하세요.");
				$(".member_profile").focus();
				return false;

			} else if(check_agree != "on"){

				alert("약관 동의 없이 회원가입은 불가능 합니다.");
				$(".check_agree").focus();
				return false;

			}


		} else {

			alert("취소 되었습니다.");
			return false;

		}

	});


	$(".member_id").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
		}
	});

	$(".member_email_1").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
		}
	});

	$(".member_email_2").keyup(function(event){
		if (!(event.keyCode >=37 && event.keyCode<=40)) {
			var inputVal = $(this).val();
			$(this).val(inputVal.replace(/[^a-z0-9.]/gi,''));
		}
	});

});

$(document).keypress(function(e){

	if(e.keyCode == 13)
		e.preventDefault();

});
</script>



<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">
	<form id="SetForms" method="post" action="signup_ok" role="form">
		<h2 class="text-center">회원가입</h2>
		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" name="member_id" class="form-control input-lg member_id" placeholder="아이디(영문)">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<input type="password" name="member_password" class="form-control input-lg member_password" placeholder="비밀번호">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<input type="text" name="member_name" class="form-control input-lg member_name" placeholder="이름">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="member_email_1" class="form-control input-lg member_email_1" placeholder="이메일 아이디">

						<span class="input-group-prepend" id="basic-addon1">
							<span class="input-group-text">@</span>
						</span>
						<input type="text" name="member_email_2" class="form-control input-lg member_email_2" placeholder="이메일 호스트">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<select name="member_profile" id="inlineFormCustomSelect" class="custom-select mr-sm-2 member_profile">
						<option value="0" selected>비공개</option>
						<option value="1">공개</option>
					</select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-4 col-sm-3 col-md-3">
				<span class="button-checkbox">
					<label class="checkbox"><input type="checkbox" name="check_agree" class="btn btn-primary check_agree" data-dismiss="modal">동의</label>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<input type="submit" class="btn btn-primary btn-block btn-lg" value="회원가입">
			</div>
		</div>
	</form>
</div>