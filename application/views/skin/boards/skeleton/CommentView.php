<?php
if(!defined('BASEPATH')) exit;
?>

    <?php
    if(isset($get_parameter['session_array']['sess_member_id'])){
    ?>
        
        <div class="card my-4">
            <h5 class="card-header">댓글 쓰기</h5>
            <div class="card-body">
                
                <form id="ReplySubmitOk" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_write/<?=$board_config_print['con_name']?>/no/<?=$board_print['bbs_no']?>">
                    <div class="form-group">
                        <textarea name="rep_content" class="form-control rep_content" rows="5" <?=($get_parameter['session_array']['sess_member_id']==""?"readonly":"")?> ></textarea>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">저장</button>
                    </div>
                </form>
            
            </div>
        </div>
        
    <?php
    } else {
    ?>
        
        <hr>
        
    <?php
    }
    ?>
    
    
    <?php
    if($reply_count >= 1){
        
        foreach($reply_result as $reply){
            
            if($reply['rep_child_no'] == 0){
    ?>
             
                <div class="media mb-4">
                    <img class="d-flex mr-3 rounded-circle" src="<?=PROTOCOLS?>placehold.it/50x50" alt="">
                    <div class="media-body">
                        <h5 class="mt-0"><?=$reply['member_name']?></h5>
                        
                        
                        <?php
                        if(isset($get_parameter['session_array']['sess_member_id'])){
                        ?>
                            
                            <a href="javascript:void(0);" class="ReplyAdded" data="<?=$reply['rep_no']?>" data-toggle="modal" data-target=".myModal_Added_<?=$reply['rep_no']?>">
                                <span class="Reply_<?=$reply['rep_no']?>">댓글</span>
                            </a>
                            
                            <?php
                            if($reply['member_id'] == $get_parameter['session_array']['sess_member_id']){
                            ?>
                                <!-- 자신의 댓글이었을 때만 출력 -->
                                <a href="javascript:void(0);" data-toggle="modal" data-target=".myModal_Modify_<?=$reply['rep_no']?>">
                                    <span class="Modify_<?=$reply['rep_no']?>">수정</span>
                                </a>
                                <a href="javascript:void(0);" class="ReplyRemoveOk" data="<?=$reply['rep_no']?>" name="<?=$reply['rep_parent_no']?>">
                                    <span class="rem_rep_no_<?=$reply['rep_no']?>"> 삭제 </span>
                                </a>
                                <!-- 자신의 댓글이었을 때만 출력 -->
                            <?php
                            }
                            ?>
                            
                            <br>
                            
                        <?php
                        }
                        ?>
                        
                        
                        <span style="word-break:break-all;">
                            <?=$reply['rep_content']?>
                        </span>
                    </div>
                </div>
                
                
            <?php
            }
            
            
            if($reply['rep_child_no'] != 0){
            ?>
             
                <div class="media mx-auto" style="width: 500px;">
                    <img class="d-flex mr-3 rounded-circle" src="<?=PROTOCOLS?>placehold.it/50x50" alt="">
                    <div class="media-body">
                        <h5 class="mt-0"><?=$reply['member_name']?></h5>
                        
                        
                        <?php
                        if($reply['member_id'] == $get_parameter['session_array']['sess_member_id']){
                        ?>
                         
                            <a href="javascript:void(0);" data-toggle="modal" data-target=".myModal_Modify_<?=$reply['rep_no']?>">
                                <span class="Modify_<?=$reply['rep_no']?>">수정</span>
                            </a>
                            
                            <a href="javascript:void(0);" class="ReplyRemoveOk" data="<?=$reply['rep_no']?>" name="<?=$reply['rep_parent_no']?>">
                                <span class="rem_rep_no_<?=$reply['rep_no']?>">삭제</span>
                            </a>
                            <br>
                            
                        <?php
                        }
                        ?>
                        
                        
                        <span style="word-break:break-all;">
                            <?=$reply['rep_content']?>
                        </span>
                    </div>
                </div><br />
                
            <?php
            }
            
            
            if(isset($get_parameter['session_array']['sess_member_id'])){
            ?>
                
                
                <!-- reply add row modal start -->
                <div class="modal fade myModal_Added_<?=$reply['rep_no']?>" role="dialog">
                    <div class="modal-dialog modal-lg">
                        
                        <form class="ReplyAddedOk" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_add_write/<?=$board_config_print['con_name']?>/no/<?=$board_print['bbs_no']?>">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4>댓글 추가</h4>
                                    <button type="button" class="close" data-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="my-3 my-md-5">
                                        <div class="container">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        
                                                        <input type="hidden" name="rep_no" class="add_bbs_no_<?=$reply['rep_no']?>" value="<?=$reply['rep_no']?>" readonly>
                                                        <input type="hidden" name="rep_parent_no" class="add_rep_parent_no_<?=$reply['rep_no']?>" value="<?=$reply['rep_parent_no']?>" readonly>
                                                        <input type="hidden" name="bbs_no" class="add_rep_no_<?=$reply['rep_no']?>" value="<?=$reply['bbs_no']?>" readonly>
                                                        
                                                        <div class="col-sm-12 col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-label">댓글 내용</label>
                                                                <textarea name="rep_content" class="form-control add_rep_content_<?=$reply['rep_no']?>" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" data="<?=$reply['rep_no']?>">저장</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- reply add row modal end -->
                
                
                <!-- reply modify row modal start -->
                <div class="modal fade myModal_Modify_<?=$reply['rep_no']?>" role="dialog">
                    <div class="modal-dialog modal-lg">
                        
                        <form class="ReplyModifyOk" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_modify/<?=$board_config_print['con_name']?>/no/<?=$board_print['bbs_no']?>">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4>댓글 수정</h4>
                                    <button type="button" class="close" data-dismiss="modal"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="my-3 my-md-5">
                                        <div class="container">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        
                                                        <input type="hidden" name="rep_no" class="mod_bbs_no_<?=$reply['rep_no']?>" value="<?=$reply['rep_no']?>" readonly>
                                                        <input type="hidden" name="bbs_no" class="mod_rep_no_<?=$reply['rep_no']?>" value="<?=$reply['bbs_no']?>" readonly>
                                                        <input type="hidden" name="member_id" class="mod_member_id_<?=$reply['rep_no']?>" value="<?=$reply['member_id']?>" readonly>
                                                        
                                                        <div class="col-sm-12 col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-label">댓글 내용</label>
                                                                <textarea name="rep_content" class="form-control mod_rep_content_<?=$reply['rep_no']?>" rows="5"><?=$reply['rep_content']?></textarea>
                                                            </div>
                                                        </div>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-warning" data="<?=$reply['rep_no']?>">수정</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- reply modify row modal end -->
                
    <?php
            }
            
        }
        
    }
    
    
    $this->common->GetCommentPaging("board/contents/" . $board_config_print['con_name'] . "/no/" . $board_print['bbs_no'], $reply_all_count->num_rows(), $reply_id, $page, $start, $end);	// 댓글 전용 페이징
    ?>


</div>