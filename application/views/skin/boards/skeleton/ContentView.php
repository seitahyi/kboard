<?php
if(!defined('BASEPATH')) exit;
?>


<link href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/skeleton.css" rel="stylesheet">

<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script>
$(document).ready(function(){

	var con_name			= "<?=$board_config_print['con_name']?>";
	var no					= "<?=$board_print['bbs_no']?>";
	var member_id			= "<?=$board_print['member_id']?>";
	var member_id2			= "<?=$get_parameter['session_array']['sess_member_id']?>";
	var likes, dislikes, message, file_no;


	$(".GetWrite").click(function(){

		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/write/" + con_name;

	});


	///////////////////////////////////


	$(".GetModify").click(function(){

		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/modify/" + con_name + "/no/" + no;

	});


	///////////////////////////////////


	$(".MoveList").click(function(){

		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/" + con_name;

	});


	///////////////////////////////////


	$(".GetRemove").click(function(){

		message				= confirm("삭제 할까요?");

		if(message){


			if(member_id == member_id2){

				
				/**
				member_id 와 member_id2 의 파라미터는 컨트롤러에서 비교연산
				**/


				RemoveContent(con_name, no, member_id, member_id2);

			} else {

				alert("<?=KO_NOPERMI?>");

			}


		} else {

			alert("<?=KO_CANCELED?>");
			return false;

		}

	});


	///////////////////////////////////


	$(".Likes").click(function(){

		message		= confirm("추천할까요?");

		if(message){

			if(member_id != member_id2){

				InsertLikes(con_name, no, member_id, member_id2);

			} else {	// 자신의 글은 추천 할 수 없음

				alert("<?=KO_NOPERMI?>");
				return false;

			}

		} else {

			alert("<?=KO_CANCELED?>");
			return false;

		}

	});


	///////////////////////////////////


	$(".Dislikes").click(function(){

		message		= confirm("비추천할까요?");

		if(message){

			if(member_id != member_id2){

				InsertDislikes(con_name, no, member_id, member_id2);

			} else {	// 자신의 글은 비추천 할 수 없음

				alert("<?=KO_NOPERMI?>");
				return false;

			}

		} else {

			alert("<?=KO_CANCELED?>");
			return false;

		}

	});


	///////////////////////////////////


	$(".download").click(function(){

		file_no			= $(this).attr("data");

		$(".rows_" + file_no).each(function(){

			GetDownload(con_name, no, file_no);

		});

	});


	///////////////////////////////////


	$("#ReplySubmitOk").submit(function(){

		rep_content		= $(".rep_content").val();
		rep_parent_no	= "";
		message			= confirm("댓글을 작성할까?");


		if(message){

			if(rep_content == ""){

				alert("내용을 입력하세요.");
				return false;

			}

		} else {

			alert("취소되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".ReplyAddedOk").submit(function(){

		rep_no			= $(this).attr("data");
		rep_content		= $(".add_rep_content_" + rep_no).val();
		rep_parent_no	= $(".add_rep_parent_no_" + rep_no).val();
		rep_child_no	= $(".add_rep_child_no_" + rep_no).val();
		message			= confirm("댓글을 작성할까요?");


		if(message){

			if(rep_content == ""){

				alert("내용을 입력하세요.");
				return false;

			}

		} else {

			alert("취소되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".ReplyModifyOk").submit(function(){

		rep_no			= $(this).attr("data");
		rep_content		= $(".mod_rep_content_" + rep_no).val();
		message			= confirm("댓글을 수정할까요?");


		if(message){

			if(rep_content == ""){

				alert("내용을 입력하세요.");
				return false;

			}

		} else {

			alert("취소되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".ReplyRemoveOk").click(function(){

		rep_no			= $(this).attr("data");
		parent_no		= $(this).attr("name");

		$(".rem_rep_no_" + rep_no).each(function(){

			message				= confirm("댓글을 삭제 할까요?");

			if(message){

				RemoveReply(con_name, rep_no, parent_no, no, member_id2);

			} else {

				alert("취소 되었습니다.");
				return false;

			}

		});

	});

});


//////////////////////////////////////


function RemoveContent(con_name, no, member_id, member_id2){
	
	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/remove/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("member_id=" + member_id);
	ajax_param.push("member_id2=" + member_id2);
	ajax_param.push("con_name=" + con_name);
	ajax_param.push("no=" + no);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/" + con_name;

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else {

		alert(call_back.return_code);

	}
	
}


//////////////////////////////////////


function InsertLikes(con_name, no, member_id, member_id2){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/set_likes/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("member_id=" + member_id);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9997"){

		alert("<?=KO_DUPLICATED?>");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function InsertDislikes(con_name, no, member_id, member_id2){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/set_dislikes/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("member_id=" + member_id);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9997"){

		alert("<?=KO_DUPLICATED?>");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function RemoveReply(con_name, rep_no, parent_no, no, member_id2){
	
	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_remove/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("rep_no=" + rep_no);
	ajax_param.push("rep_parent_no=" + parent_no);
	ajax_param.push("no=" + no);
	ajax_param.push("con_name=" + con_name);
	ajax_param.push("member_id=" + member_id2);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9998"){

		alert("<?=KO_CANTDELETE?>");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function GetDownload(con_name, no, file){

	window.open("<?=PROTOCOLS?><?=HTTP_HOST?>/board/download/" + con_name + "/no/" + no + "/file/" + file);

}
</script>


<!-- Post Content Column -->
<div class="col-lg-8">

	<!-- Title -->
	<h1 class="mt-4">
		<?=$board_print['bbs_title']?>
	</h1>

	<hr>

	<p>
        <?=($board_print['bbs_secret'] == "Y")?"비밀글 입니다. / ":""?><?=(@$board_print['bbs_category'] != "")?"분류 : " . $board_print['bbs_category'] . "&nbsp;/":""?> 작성자 : <?=$board_print['member_name']?> / 조회수 : <?=$board_print['bbs_readed']?> / 아이피 : <?=$board_print['bbs_ip']?> / 작성일 : <?=$board_print['bbs_date']?>
	</p>

	<hr>
 
	
    <?php
    $i  = "";
    for($i = 1; $i <= 5; $i++) {

        if(!empty($board_config_print['ext_title_'.$i])) {
	?>
	        <p class="lead">
		        <?=$board_config_print['ext_title_'.$i]?> : <?= $board_print['ext_desc_'.$i] ?>
            </p>

            <hr>
	<?php
        }
        
    }
    unset($i);
    ?>
 

	<!-- Post Content -->
	<p class="lead">
		<?=$board_print['bbs_content']?>
	</p>
	
	<hr>

	<div class="card-body">
		<div class="row">

			<div class="col-sm"></div>

			<div class="col-sm" style="text-align:center;">
				<div class="form-group">
					<button type="button" name="bbs_like" class="btn btn-outline-primary Likes"><i class="fe fe-thumbs-up"></i>&nbsp;
					<?php
					if($voted_print['sum_like'] >= 1){

						echo $voted_print['sum_like'];
					
					} else {

						echo "0";

					}
					?>
					</button>

					<button type="button" name="bbs_dislike" class="btn btn-outline-primary Dislikes"><i class="fe fe-thumbs-down"></i>&nbsp;
					<?php
					if($voted_print['sum_dislike'] >= 1){

						echo $voted_print['sum_dislike'];
					
					} else {

						echo "0";

					}
					?>
					</button>
				</div>
			</div>

			<div class="col-sm"></div>
		</div>
	</div>


	<div class="card-footer">

		<div class="col-sm-12 col-md-12">
			<div class="form-group" style="margin-top:14px;">
				<?php
				if($file_count >= 1){

					unset($no);
					$no		= 1;
					foreach($file_result as $files){
				?>
						<a href="#" class="download" data="<?=$files['file_no']?>">
							<span class="rows_<?=$files['file_no']?>">
								첨부파일 <?=$no++?>
							</span>
						</a>
				<?php
					}

				} else {
				 
					echo "첨부파일이 없습니다.";
					
				}
				?>
			</div>
		</div>

	</div>


	<div class="card-footer text-right">
		<button type="button" class="btn btn-success GetWrite">글쓰기</button>

		<?php
		if($get_parameter['session_array']['sess_member_id'] == $board_print['member_id']){
		?>
			<button type="button" class="btn btn-warning GetModify">수정</button>
			<button type="button" class="btn btn-danger GetRemove">삭제</button>
		<?php
		}
		?>

		<button type="button" class="btn btn-secondary MoveList">목록</button>
	</div>