<?php
if(!defined('BASEPATH')) exit;
?>


<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/mobile.css">
<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/style.css">
<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.css">

<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.min.js"></script>
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote-ko-KR.js"></script>
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>


<script>
$(document).ready(function(){

	var con_name			= "<?=$board_config_print['con_name']?>";
	var no					= "<?=$board_print['bbs_no']?>";
	var member_id			= "<?=$board_print['member_id']?>";
	var member_id2			= "<?=$get_parameter['session_array']['sess_member_id']?>";
	var likes, dislikes, message, file_no, edit;


	$(".GetWrite").click(function(){

		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/write/" + con_name;

	});


	///////////////////////////////////


	$(".GetModify").click(function(){

		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/modify/" + con_name + "/no/" + no;

	});


	///////////////////////////////////


	$(".MoveList").click(function(){

		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/" + con_name;

	});


	///////////////////////////////////


	$(".GetRemove").click(function(){

		message				= confirm("삭제 할까요?");

		if(message){


			if(member_id == member_id2){

				
				/**
				member_id 와 member_id2 의 파라미터는 컨트롤러에서 비교연산
				**/


				RemoveContent(con_name, no, member_id, member_id2);

			} else {

				alert("<?=KO_NOPERMI?>");

			}


		} else {

			alert("<?=KO_CANCELED?>");
			return false;

		}

	});


	///////////////////////////////////


	$(".Likes").click(function(){

		message		= confirm("추천할까요?");

		if(message){

			if(member_id != member_id2){

				InsertLikes(con_name, no, member_id, member_id2);

			} else {	// 자신의 글은 추천 할 수 없음

				alert("<?=KO_NOPERMI?>");
				return false;

			}

		} else {

			alert("<?=KO_CANCELED?>");
			return false;

		}

	});


	///////////////////////////////////


	$(".Dislikes").click(function(){

		message		= confirm("비추천할까요?");

		if(message){

			if(member_id != member_id2){

				InsertDislikes(con_name, no, member_id, member_id2);

			} else {	// 자신의 글은 비추천 할 수 없음

				alert("<?=KO_NOPERMI?>");
				return false;

			}

		} else {

			alert("<?=KO_CANCELED?>");
			return false;

		}

	});


	///////////////////////////////////


	$(".download").click(function(){

		file_no			= $(this).attr("data");

		$(".rows_" + file_no).each(function(){

			GetDownload(con_name, no, file_no);

		});

	});


	///////////////////////////////////


	$("#fviewcomment").submit(function(){

		rep_content		= $(".rep_content").val();
		rep_parent_no	= "";
		message			= confirm("댓글을 작성할까요?");

		if(message){

			if(rep_content == ""){

				alert("내용을 입력하세요.");
				return false;

			}

		} else {

			alert("취소되었습니다.");
			return false;

		}

	});
	
	
	///////////////////////////////////
	
	
	$(".add").click(function(){    // append for loop
		
		rep_no			= $(this).attr("data");
		rep_parent_no   = $(".SetRepParNo_" + rep_no).text();
		rep_content     = $(".add_rep_content_" + rep_no).text();
		
		$(".add_rep_no_" + rep_no).each(function(){
			
			html        = "";
			html        +=  "<form name=\"fviewcomment\" action=\"<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_add_write/<?=$board_config_print['con_name']?>/no/<?=$board_print['bbs_no']?>\" class=\"ReplyModifyOk\" method=\"post\">";
			
			html        +=  "<input type=\"hidden\" name=\"rep_no\" class=\"add_bbs_no_" + rep_no + "\" value=\""+ rep_no +"\" readonly>";
			html        +=  "<input type=\"hidden\" name=\"rep_parent_no\" class=\"add_rep_parent_no_" + rep_no + "\" value=\""+ $.trim(rep_parent_no) +"\" readonly>";
            html        +=  "<input type=\"hidden\" name=\"bbs_no\" class=\"add_rep_no_" + rep_no + "\" value=\"" + no + "\" readonly>";
			  
			html        +=      "<textarea id=\"wr_content\" name=\"rep_content\" maxlength=\"10000\" required class=\"required\" title=\"내용\" placeholder=\"댓글 내용을 입력해주세요.\"></textarea>";
			html        +=      "<div class=\"bo_vc_w_wr\">";
			html        +=          "<div class=\"btn_confirm\">";
			html        +=              "<input type=\"button\" class=\"btn_b01 btn\" onclick=\"location.reload();\" value=\"취소\">&nbsp;&nbsp;";
			html        +=              "<input type=\"submit\" id=\"btn_submit\" class=\"btn_submit btn\" value=\"댓글등록\">";
			html        +=          "</div>";
			html        +=      "</div>";
			html        +=  "</form>";
			
			$(".GetRepContent_" + rep_no).html(html);
			$(".removeAll_" + rep_no).remove();
		
		});
		
	});
	
	
	///////////////////////////////////
	
	
	$(".edit").click(function(){    // append for loop
		
		rep_no			= $(this).attr("data");
		rep_content     = $(".SetRepContent_" + rep_no).text();
		
		$(".rep_contents_" + rep_no).each(function(){
			
			html        = "";
			html        +=  "<form name=\"fviewcomment\" action=\"<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_modify/<?=$board_config_print['con_name']?>/no/<?=$board_print['bbs_no']?>\" class=\"ReplyModifyOk\" method=\"post\">";
			
			html        +=  "<input type=\"hidden\" name=\"rep_no\" value=\""+ rep_no +"\" readonly>";
            html        +=  "<input type=\"hidden\" name=\"bbs_no\" value=\"" + no + "\" readonly>";
            html        +=  "<input type=\"hidden\" name=\"member_id\" value=\"" + member_id2 + "\" readonly>";
			  
			html        +=      "<textarea id=\"wr_content\" name=\"rep_content\" maxlength=\"10000\" required class=\"required mod_rep_content_" + rep_no + "\" title=\"내용\" placeholder=\"댓글 내용을 입력해주세요.\">" + $.trim(rep_content) + "</textarea>";
			html        +=      "<div class=\"bo_vc_w_wr\">";
			html        +=          "<div class=\"btn_confirm\">";
			html        +=              "<input type=\"button\" class=\"btn_b01 btn\" onclick=\"location.reload();\" value=\"취소\">&nbsp;&nbsp;";
			html        +=              "<input type=\"submit\" id=\"btn_submit\" class=\"btn_submit btn\" value=\"댓글등록\">";
			html        +=          "</div>";
			html        +=      "</div>";
			html        +=  "</form>";
			
			$(".GetRepContent_" + rep_no).html(html);
			$(".removeAll_" + rep_no).remove();
		
		});
		
	});


	///////////////////////////////////


	$(".ReplyAddedOk").submit(function(){

		rep_no			= $(this).attr("data");
		rep_content		= $(".add_rep_content_" + rep_no).val();
		rep_parent_no	= $(".add_rep_parent_no_" + rep_no).val();
		rep_child_no	= $(".add_rep_child_no_" + rep_no).val();
		message			= confirm("댓글을 작성할까요?");


		if(message){

			if(rep_content == ""){

				alert("내용을 입력하세요.");
				return false;

			}

		} else {

			alert("취소되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".ReplyModifyOk").submit(function(){

		rep_no			= $(this).attr("data");
		rep_content		= $(".mod_rep_content_" + rep_no).val();
		message			= confirm("댓글을 수정할까요?");


		if(message){

			if(rep_content == ""){

				alert("내용을 입력하세요.");
				return false;

			}

		} else {

			alert("취소되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".ReplyRemoveOk").click(function(){

		rep_no			= $(this).attr("data");
		parent_no		= $(this).attr("name");

		$(".rem_rep_no_" + rep_no).each(function(){

			message				= confirm("댓글을 삭제 할까요?");

			if(message){

				RemoveReply(con_name, rep_no, parent_no, no, member_id2);

			} else {

				alert("취소 되었습니다.");
				return false;

			}

		});

	});

});


//////////////////////////////////////


function RemoveContent(con_name, no, member_id, member_id2){
	
	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/remove/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("member_id=" + member_id);
	ajax_param.push("member_id2=" + member_id2);
	ajax_param.push("con_name=" + con_name);
	ajax_param.push("no=" + no);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/" + con_name;

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else {

		alert(call_back.return_code);

	}
	
}


//////////////////////////////////////


function InsertLikes(con_name, no, member_id, member_id2){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/set_likes/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("member_id=" + member_id);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9997"){

		alert("<?=KO_DUPLICATED?>");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function InsertDislikes(con_name, no, member_id, member_id2){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/set_dislikes/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("member_id=" + member_id);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9997"){

		alert("<?=KO_DUPLICATED?>");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function RemoveReply(con_name, rep_no, parent_no, no, member_id2){
	
	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_remove/" + con_name + "/no/" + no;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("rep_no=" + rep_no);
	ajax_param.push("rep_parent_no=" + parent_no);
	ajax_param.push("no=" + no);
	ajax_param.push("con_name=" + con_name);
	ajax_param.push("member_id=" + member_id2);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9998"){

		alert("<?=KO_CANTDELETE?>");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function GetDownload(con_name, no, file){

	window.open("<?=PROTOCOLS?><?=HTTP_HOST?>/board/download/" + con_name + "/no/" + no + "/file/" + file);

}
</script>


<div id="wrapper">

    <div id="container">

		<h2 id="container_title" class="top sub_tit">
			<span>
				<?=$board_config_print['con_title']?>
			</span>
		</h2>
		
		<article id="bo_v">
			<header>
				<h2 id="bo_v_title">
					<span class="bo_v_tit">
						<?=(@$board_print['bbs_category'] != "")?"[" . $board_print['bbs_category'] . "]&nbsp;":""?><?=$board_print['bbs_title']?>
					</span>
				</h2>
			</header>
			
			<section id="bo_v_info">
				<?=($board_print['bbs_secret'] == "Y")?"비밀글 입니다.":""?>
				<span class="sv_member">
					<?=$board_print['member_name']?>
				</span>
				
				<span class="ip">
					&nbsp;<?=$board_print['bbs_ip']?>
				</span>
				
				<i class="fa fa-clock-o" aria-hidden="true"></i><?=$board_print['bbs_date']?>
				
				<strong>
					<i class="fa fa-eye" aria-hidden="true"></i><?=$board_print['bbs_readed']?>회
				</strong>
			</section>
			
			<section id="bo_v_atc">
				
				<div id="bo_v_con">
					
					<?php
					$i  = "";
					for($i = 1; $i <= 5; $i++) {
						
						if(!empty($board_config_print['ext_title_'.$i])) {
					?>
							<p>
								<?=$board_config_print['ext_title_'.$i]?> : <?= $board_print['ext_desc_'.$i] ?>
							</p>
					<?php
						}
						
					}
					unset($i);
					?>
					
					<p>
						<span style="font-family:나눔고딕, NanumGothic;">
							<?=$board_print['bbs_content']?>
						</span>
					</p>
				</div>
				
				<div id="bo_v_share">
					
					<button type="button" name="bbs_like" class="Likes btn btn_b01">
					<?php
					if($voted_print['sum_like'] >= 1){
							
						echo "추천 : " . $voted_print['sum_like'];
							
					} else {
							
						echo "추천 : 0";
							
					}
					?>
					</button>
					
					<button type="button" name="bbs_dislike" class="Dislikes btn btn_b01">
					<?php
					if($voted_print['sum_dislike'] >= 1){
						
						echo "비추천 : " . $voted_print['sum_dislike'];
						
					} else {
							
						echo "비추천 : 0";
							
					}
					?>
					</button>
				</div>
			</section>
			
			<div id="bo_v_top">
				
				<?php
				if($file_count >= 1){
				?>
					<ul class="bo_v_left">
						<?php
						unset($no);
						$no     = 1;
						foreach($file_result as $files){
						?>
							<li>
								<a href="#" class="btn_b01 btn download" data="<?=$files['file_no']?>">
									<span class="rows_<?=$files['file_no']?>">
										첨부파일 <?=$no++?>
									</span>
								</a>
							</li>
						<?php
						}
						?>
					</ul>
				<?php
				}
				?>
				
			</div>
			
			<div id="bo_v_top">
				
				<?php
				if($get_parameter['session_array']['sess_member_id'] == $board_print['member_id']){
				?>
					<ul class="bo_v_left">
						<li>
							<a href="#" class="btn_b01 btn GetModify">수정</a>
						</li>
						<li>
							<a href="#" class="btn_b01 btn GetRemove"> 삭제</a>
						</li>
					</ul>
				<?php
				}
				?>
				
				<div class="bo_v_right">
					<a href="#" class="btn_b01 btn MoveList">목록</a>
					<a href="#" class="btn_b02 btn GetWrite">글쓰기</a>
				</div>
			</div>