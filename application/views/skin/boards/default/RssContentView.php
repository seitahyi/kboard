<?php
if(!defined('BASEPATH')) exit;


header("Content-type: text/xml;charset=utf-8");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");


$html       =   "";
$html       .=  "<?xml version=\"1.0\" encoding=\"euc-kr\"?>";
$html       .=      "<rss version=\"2.0\">";
$html       .=          "<channel>";
$html       .=              "<title>" . $board_config_print['con_title'] . "</title>";
$html       .=              "<link>" . PROTOCOLS . HTTP_HOST . "/board/lists/" . $board_config_print['con_name'] . "</link>";
//$html       .=              "<description>간단한 설명글을 넣자</description>";


foreach($board_result as $board){
	
	$html   .=              "<item>";
	$html   .=                  "<author>" . $board['member_name'] . "</author>";
	$html   .=                  "<category>" . $board['bbs_category'] . "</category>";
	$html   .=                  "<title>" . $board['bbs_title'] . "</title>";
	$html   .=                  "<link>" . PROTOCOLS . HTTP_HOST . "/board/contents/" . $board_config_print['con_name'] . "/no/" . $board['bbs_no'] . "</link>";
	$html   .=                  "<description>" . htmlspecialchars($board['bbs_content']) . "</description>";
	$html   .=              "</item>";
	
}


$html       .=          "</channel>";
$html       .=      "</rss>";

echo $html;
?>
