<?php
if(!defined('BASEPATH')) exit;
?>


<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/mobile.css">
<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/style.css">


<script>
$(document).ready(function(){

	var con_name			= "<?=$board_config_print['con_name']?>";
	var search_type, search_name;

	$("#SubmitOk").submit(function(){

		search_type		= $("select[name=\"search_type\"]").val();
		search_name		= $(".search_name").val();

		if(search_type == ""){

			alert("검색 찾기를 선택하세요.");
			$(".search_type").focus();
			return false;

		} else if(search_name == ""){

			alert("검색어를 입력하세요.");
			$(".search_name").focus();
			return false;

		}

	});

});
</script>


<div id="wrapper">
    <div id="container">

		<h2 id="container_title" class="top sub_tit">
			<span>
				<?=$board_config_print['con_title']?>
			</span>
		</h2>

		<div id="bo_list">
   
			<fieldset id="bo_sch">
				<form id="SubmitOk" action="/board/lists/<?=$board_config_print['con_name']?>" method="post">

					<select name="search_type" id="sfl" class="search_type">
						<option <?=(@$get_parameter['search_array']['search_type']=="")?"selected":""?> value="">선택 찾기</option>
						<option <?=(@$get_parameter['search_array']['search_type']=="member_name")?"selected":""?> value="member_name">작성자</option>
						<option <?=(@$get_parameter['search_array']['search_type']=="bbs_title")?"selected":""?> value="bbs_title">제목</option>
						<option <?=(@$get_parameter['search_array']['search_type']=="bbs_content")?"selected":""?> value="bbs_content">본문</option>
					</select>

					<input name="search_name" value="<?=@$get_parameter['search_array']['search_name']?>" placeholder="검색어(필수)" required id="stx" class="sch_input search_name" size="15" maxlength="20">
					<button type="submit" value="검색" class="sch_btn"><i class="fa fa-search" aria-hidden="true"></i> <span class="sound_only">검색</span></button>
				</form>
			</fieldset>
			
			<form name="fboardlist" id="fboardlist" action="#" method="post">
			
				<div class="board_list">
					
					<?php
					if($board_count->num_rows() >= 1){  // 게시물이 1개 이상일 때
						
						foreach($board_result as $board){   // 게시물을 뽑아온다
							
							/**
							 * 추천수 뽑아오기 시작
							 */
							$get_voted			= $this->BoardModel->GetVoted($board_config_print['con_name'], $board['bbs_no']);
							$voted_count		= $get_voted->num_rows();
							$voted_print		= $get_voted->row_array();
							/**
							 * 추천수 뽑아오기 끝
							 */
							
							/**
							 * 댓글 뽑아오기 시작
							 */
							$get_comments		= $this->BoardModel->GetCommentCount($board_config_print['con_name'], $board['bbs_no']);
							$comments_count		= $get_comments->num_rows();
							/**
							 * 댓글 뽑아오기 끝
							 */
					?>

							<ul>
								<li>
									<div class="bo_subject">
										<?php
	                                    if(count($bbs_category) > 1) {  // 분류가 있을 때
		                                ?>
	                                        <td>
			                                    <?=$board['bbs_category']?> |
	                                        </td>
		                                <?php
	                                    }
	                                    
										if($board['bbs_secret'] == "Y") {   // 비밀글일 때
										?>
											<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$board_config_print['con_name']?>/no/<?=$board['bbs_no']?>">비밀글 입니다.</a>
										<?php
										} else {
										?>
											<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$board_config_print['con_name']?>/no/<?=$board['bbs_no']?>"><?=$this->common->SetString("30", $board['bbs_title'])?></a>
											<?php
											if ($comments_count >= 1) { // 댓글이 있을 때
											?>
												&nbsp;<span class="badge badge-dark">
													<?=$comments_count?>
												</span>
										<?php
		                                    }
											
										}
										?>
									</div>
									<div class="bo_info">
										
										<!-- 글쓴이 -->
										<span class="sv_member">
											<?=$board['member_name']?>
										</span>
										
										<!-- 날짜 -->
										<span class="bo_date">
											<i class="fa fa-clock-o"></i>
											<?=$board['bbs_date']?>
										</span>
										
										<!-- 조회수 -->
										<span class="bo_date">
											<i class="fa fa-clock-o"></i>
											<?=$board['bbs_readed']?>
										</span>
										
									</div>
								</li>
							</ul>
					
					<?php
						}
						
					} else {
					?>
						<ul>
							<li>
								내용이 없습니다.
							</li>
						</ul>
					<?php
					}
					?>
					
				</div>
				
				<div class="bo_fx">
				
					
					<!--
					쓸일 있을지 모르니 놔둡니다.
					<ul class="btn_bo_adm">
						<li>
							<button type="submit" name="btn_submit" value="선택삭제" onclick="document.pressed=this.value" class="btn btn_b01">선택삭제</button>
						</li>
						<li>
							<button type="submit" name="btn_submit" value="선택복사" onclick="document.pressed=this.value" class="btn btn_b01">선택복사</button>
						</li>
						<li>
							<button type="submit" name="btn_submit" value="선택이동" onclick="document.pressed=this.value" class="btn btn_b01">선택이동</button>
						</li>
					</ul>
					-->
					
					
					<ul class="btn_wr">
						<li>
							<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/write/<?=$board_config_print['con_name']?>" class="btn btn_b02">글쓰기</a>
						</li>
					</ul>
				</div>
			</form>
		</div>
	    
	    
	    <!--
	    디자인 참고용으로 놔둡니다.
	    <nav class="pg_wrap">
		    <span class="pg">
			    <a href="#" class="pg_page">처음</a>
			    <strong class="pg_current">1</strong>
			    <a href="#" class="pg_page">2</a>
			    <a href="#" class="pg_page">맨끝</a>
		    </span>
	    </nav>
	    -->
	    
	    
	    <?=$this->common->GetPaging("board/lists/" . $board_config_print['con_name'], $board_count->num_rows(), $id, $page, @$get_parameter['search_array']['search_type'], @$get_parameter['search_array']['search_name'], $start, $end)?>
	    
    </div>
</div>