<?php
if(!defined('BASEPATH')) exit;
?>

			<?php
		    if(isset($get_parameter['session_array']['sess_member_id'])){
		    ?>

				<button type="button" class="cmt_btn">댓글목록 <span><?=@$reply_count?></span></button>
			    
			    <aside id="bo_vc_w" class="bo_vc_w">
					<form name="fviewcomment" id="fviewcomment" action="<?=PROTOCOLS?><?=HTTP_HOST?>/board/comment_write/<?=$board_config_print['con_name']?>/no/<?=$board_print['bbs_no']?>" method="post">
						<textarea id="wr_content" name="rep_content" maxlength="10000" required class="required rep_content" title="내용" placeholder="댓글 내용을 입력해주세요."></textarea>
						<div class="bo_vc_w_wr">
							<div class="btn_confirm">
								<input type="submit" id="btn_submit" class="btn_submit btn" value="댓글등록">
							</div>
						</div>
					</form>
				</aside>
			 
			<?php
		    }
		    ?>


			<?php
			if($reply_count >= 1){
				
				foreach($reply_result as $reply){
			?>
					<section id="bo_vc">
					
						<?php
						if($reply['rep_child_no'] == 0){
						?>
						    
						    <article id="c_21">
						        <header style="z-index:7;">
						            <h2><?=$reply['member_name']?> 님의 댓글</h2>
							        
							        <span class="comment_profile_img">
										<img src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/no_profile.gif" width="40" height="40" alt="profile_image">
									</span>
									<span class="member">
										<?=$reply['member_name']?>
									</span>
							        <?php
							        if(isset($get_parameter['session_array']['sess_member_id'])){
							        ?>
										<span>
											<?=$reply['rep_ip']?>
										</span>
									<?php
							        }
									?>
									<span class="bo_vc_hdinfo">
										<i class="fa fa-clock-o"></i> <?=$reply['rep_date']?>
									</span>
								</header>
						
						        <!-- 댓글 출력 -->
						        <div class="cmt_contents">
						            <p class="GetRepContent_<?=$reply['rep_no']?>">
										<?=$reply['rep_content']?>
									</p>
							        
							        <p class="SetRepContent_<?=$reply['rep_no']?>" style="display:none;">
										<?=$reply['rep_content']?>
									</p>
							        
							        <p class="SetRepParNo_<?=$reply['rep_no']?>" style="display:none;">
								        <?=$reply['rep_parent_no']?>
							        </p>
							
							        <div class="bo_vl_opt removeAll_<?=$reply['rep_no']?>">
								        <ul class="btn_edit">
									        <li>
										        <a href="javascript:void(0);" class="add" data="<?=$reply['rep_no']?>">
													<span class="add_rep_no_<?=$reply['rep_no']?>">
														답변
													</span>
										        </a>
									        </li>
									        
									        <?php
									        if($reply['member_id'] == $get_parameter['session_array']['sess_member_id']){
										    ?>
										        <li>
											        <a href="javascript:void(0);" class="edit" data="<?=$reply['rep_no']?>">
														<span class="rep_contents_<?=$reply['rep_no']?>">
															수정
														</span>
											        </a>
										        </li>
										        <li>
											        <a href="javascript:void(0);" class="ReplyRemoveOk" data="<?=$reply['rep_no']?>" name="<?=$reply['rep_parent_no']?>">
														<span class="rem_rep_no_<?=$reply['rep_no']?>">
															삭제
														</span>
											        </a>
										        </li>
										    <?php
									        }
									        ?>
									        
								        </ul>
							        </div>
							        
								</div>
						    </article>
						<?php
						}
						
						if($reply['rep_child_no'] != 0){
						?>
							<article id="c_24" style="margin-left:50px;border-top-color:#e0e0e0">
						        <header style="z-index:7;">
						            <h2><?=$reply['member_name']?> 님의 댓글</h2>
							        
							        <span class="comment_profile_img">
										<img src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/no_profile.gif" width="40" height="40" alt="profile_image">
									</span>
									<span class="member">
										<?=$reply['member_name']?>
									</span>
							        <?php
							        if(isset($get_parameter['session_array']['sess_member_id'])){
							        ?>
										<span>
											<?=$reply['rep_ip']?>
										</span>
									<?php
							        }
									?>
									<span class="bo_vc_hdinfo">
										<i class="fa fa-clock-o"></i> <?=$reply['rep_date']?>
									</span>
								</header>
						
						        <!-- 댓글 출력 -->
						        <div class="cmt_contents">
						            <p class="GetRepContent_<?=$reply['rep_no']?>">
										<?=$reply['rep_content']?>
									</p>
							        
							        <p class="SetRepContent_<?=$reply['rep_no']?>" style="display:none;">
										<?=$reply['rep_content']?>
									</p>
							        
							        <?php
							        if($reply['member_id'] == $get_parameter['session_array']['sess_member_id']){
							        ?>
										<div class="bo_vl_opt removeAll_<?=$reply['rep_no']?>">
							                <ul class="btn_edit">
												<li>
													<a href="javascript:void(0);" class="edit" data="<?=$reply['rep_no']?>">
														<span class="rep_contents_<?=$reply['rep_no']?>">
															수정
														</span>
													</a>
												</li>
												<li>
													<a href="javascript:void(0);" class="ReplyRemoveOk" data="<?=$reply['rep_no']?>" name="<?=$reply['rep_parent_no']?>">
														<span class="rem_rep_no_<?=$reply['rep_no']?>">
															삭제
														</span>
													</a>
												</li>
											</ul>
							            </div>
						            <?php
							        }
									?>
								</div>
						    </article>
						<?php
						}
						?>
					</section>
				<?php
				}
				
			} else {
			?>

				<section id="bo_vc">
					<p id="bo_vc_empty">
						등록된 댓글이 없습니다.
					</p>
				</section>

			<?php
			}
			?>

		</article>
	</div>
</div>