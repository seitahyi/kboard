<?php
if(!defined('BASEPATH')) exit;
?>


<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/mobile.css">
<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/style.css">
<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.css">

<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.min.js"></script>
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote-ko-KR.js"></script>
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>


<script>
$(document).ready(function(){


	var con_name			= "<?=$board_config_print['con_name']?>";
	var member_id			= "<?=$get_parameter['session_array']['sess_member_id']?>";
	var title, category, content, html, count, message;


	$(".MoveList").click(function(){

		location.href = '<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/' + con_name;

	});


	///////////////////////////////////


	$("#SubmitOk").submit(function(){

		title				= $(".bbs_title").val();
		category            = $("select[name=\"bbs_category\"]").val();
		content				= $(".bbs_content").val();
		message				= confirm("글을 작성 할까요?");


		if(message){

			if(title == "") {

                alert("제목을 입력하세요.");
                $(".bbs_title").focus();
                return false;

            } else if(category == ""){
			    
			    alert("카테고리를 선택하세요.");
			    $(".bbs_category").focus();
			    return false;

			} else if(content == ""){

				alert("본문을 입력하세요.");
				$(".bbs_content").focus();
				return false;

			}

		} else {

			alert("취소 되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".summernote").summernote({

		height : 500,

		lang : 'ko-KR',

		popover : {
			image : [],
			link : [],
			air : []
		},
		
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['insert', ['link', 'picture2', 'video']],
			['view', ['codeview']]
		]

	});


	///////////////////////////////////


	$(".note-icon-picture").click(function(){

		title				= $(".bbs_title").val();
		content				= $(".bbs_content").val();
		message				= confirm("임시 저장 후 사용 가능 합니다. 계속 진행 할까요?");

		if(message){

			SetTMPContent(con_name, title, content, member_id);

		} else {

			alert("취소 되었습니다.");

		}

	});

});


function SetTMPContent(con_name, title, content, member_id){


	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/tmp_write_ok/" + con_name;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("bbs_title=" + title);
	ajax_param.push("bbs_category=");
	ajax_param.push("bbs_content=" + content);
	ajax_param.push("con_name=" + con_name);
	ajax_param.push("member_id=" + member_id);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		console.log("<?=KO_SUCCESS?>");
		location.href = '<?=PROTOCOLS?><?=HTTP_HOST?>/board/modify/' + con_name + '/no/' + call_back.last_id;

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9997"){

		alert("<?=KO_DUPLICATED?>");

	} else {

		alert(call_back.return_code);

	}

}
</script>


<div id="wrapper">
	<div id="container">
		<h2 id="container_title" class="top sub_tit" title="<?=$board_config_print['con_title']?>">
			<span>
				<?=$board_config_print['con_title']?>
			</span>
		</h2>

		<section id="bo_w">

			<form id="SubmitOk" action="<?=PROTOCOLS?><?=HTTP_HOST?>/board/write_ok/<?=$board_config_print['con_name']?>" method="post" enctype="multipart/form-data">

				<div class="form_01 write_div">
				
					<div class="bo_w_tit write_div">
						
						<?php
						if(count($bbs_category) > 1) {
						?>
							<select name="bbs_category" class="form-control bbs_category custom-select w-auto">
								<option value="">선택</option>
								<?php
								for ($i = 0; $i < count($bbs_category); $i++) {
								?>
									<option value="<?=$bbs_category[$i]?>"><?=$bbs_category[$i]?></option>
								<?php
								}
								unset($i);
								?>
							</select>
						<?php
						}
						?>
						
						<input type="text" name="bbs_title" required class="frm_input full_input bbs_title" style="width:80%;" placeholder="제목">
					</div>
					
					<div class="bo_w_tit write_div">
						<?php
	                    $i  = "";
	                    for($i = 1; $i <= 5; $i++) {
	                        
	                        if(!empty($board_config_print['ext_title_'.$i])) {
		                ?>
	                            <div class="col-sm-12 col-md-12">
	                                <div class="form-group">
	                                    <label class="form-label"><?= $board_config_print['ext_title_'.$i] ?></label>
	                                    <input type="text" name="ext_desc_<?=$i?>" class="frm_input ext_desc_<?=$i?>" style="width:100%;" value="<?= @$board_print['ext_desc_'.$i] ?>" placeholder="여분필드 본문 <?=$i?>을 입력하세요.">
	                                </div>
	                            </div>
		                <?php
	                        }
	                        
	                    }
	                    unset($i);
	                    ?>
					</div>
					
					<div class="bo_w_tit write_div">
						<input type="checkbox" name="bbs_secret" class="bbs_secret"> 비밀글
					</div>
					
					<div class="write_div">
						<textarea name="bbs_content" class="summernote bbs_content" placeholder="본문을 입력하세요."></textarea>
					</div>

					<div class="bo_w_flie write_div">
						
						<?php
						for($i = 0; $i <= 4; $i++){
						?>
							<div class="file_wr write_div">
								<label for="bf_file_1" class="lb_icon"><i class="fa fa-download" aria-hidden="true"></i></label>
								<input type="file" name="file_name[<?=$i?>]" class="frm_file">
							</div>
						<?php
						}
						?>
					
					</div>
				</div>
				
				<div class="write_div">
					<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/<?=$board_config_print['con_name']?>" class="btn_b01 btn">취소</a>
					<input type="submit" value="작성완료" class="btn_submit btn">
				</div>
			</form>

		</section>
    </div>
</div>