<?php
if(!defined('BASEPATH')) exit;
?>


<script>
$(document).ready(function(){

	var con_name			= "<?=$board_config_print['con_name']?>";
	var search_type, search_name;

	$("#SubmitOk").submit(function(){

		search_type		= $("select[name=\"search_type\"]").val();
		search_name		= $(".search_name").val();

		if(search_type == ""){

			alert("검색 찾기를 선택하세요.");
			$(".search_type").focus();
			return false;

		} else if(search_name == ""){

			alert("검색어를 입력하세요.");
			$(".search_name").focus();
			return false;

		}

	});


	///////////////////////////////////

	
	$(".GetWrite").click(function(){

		location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/board/write/" + con_name;

	});

});
</script>


<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">
		
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">게시판목록(총:<?=$board_count->num_rows()?>)</h3>
		</div>

		<div class="card-body">
			<div class="table-responsive">
				<table id="table" class="table table-striped text-nowrap">
					<thead>
						<tr>
							<th>
								번호
							</th>
							<th>
								작성자
							</th>
							<th>
								제목
							</th>
                            <?php
                            if(count($bbs_category) > 1){
                            ?>
                                <th>
                                    분류
                                </th>
                            <?php
                            }
                            ?>
							<th>
								추천수
							</th>
							<th>
								비추천수
							</th>
							<th>
								조회수
							</th>
							<th>
								작성일
							</th>
						</tr>
					</thead>
					<tbody>

						<?php
						if($board_count->num_rows() >= 1){

							foreach($board_result as $board){


								$get_voted			= $this->BoardModel->GetVoted($board_config_print['con_name'], $board['bbs_no']);
								$voted_count		= $get_voted->num_rows();
								$voted_print		= $get_voted->row_array();


								$get_comments		= $this->BoardModel->GetCommentCount($board_config_print['con_name'], $board['bbs_no']);
								$comments_count		= $get_comments->num_rows();
						?>
								
								<tr>
									<td>
										<span class="text-muted">
											<?=$num++?>
										</span>
									</td>
									<td>
										<?=$board['member_name']?>
									</td>
									<td>
                                        <?php
                                        if($board['bbs_secret'] == "Y") {
	                                    ?>
                                            <a href="<?= PROTOCOLS ?><?= HTTP_HOST ?>/board/contents/<?= $board_config_print['con_name'] ?>/no/<?= $board['bbs_no'] ?>">비밀글 입니다.</a>
	                                    <?php
                                        } else {
	                                    ?>
                                            <a href="<?= PROTOCOLS ?><?= HTTP_HOST ?>/board/contents/<?= $board_config_print['con_name'] ?>/no/<?= $board['bbs_no'] ?>"><?= $this->common->SetString("30", $board['bbs_title']) ?></a>
                                            
                                            <?php
	                                        if ($comments_count >= 1) {
		                                    ?>
                                                &nbsp;<span class="badge badge-dark"><?= $comments_count ?></span>
		                                <?php
	                                        }
	                                        
                                        }
                                        ?>
									</td>
                                    <?php
                                    if(count($bbs_category) > 1) {
	                                ?>
                                        <td>
		                                    <?= $board['bbs_category'] ?>
                                        </td>
	                                <?php
                                    }
                                    ?>
									<td>
										<?php
										if($voted_print['sum_like'] >= 1){

											echo $voted_print['sum_like'];

										} else {

											echo "0";

										}
										?>
									</td>
									<td>
										<?php
										if($voted_print['sum_dislike'] >= 1){

											echo $voted_print['sum_dislike'];

										} else {

											echo "0";

										}
										?>
									</td>
									<td>
										<?=$board['bbs_readed']?>
									</td>
									<td>
										<?=$board['bbs_date']?>
									</td>
								</tr>

						<?php
							}


						} else {
						?>

							<tr>
								<td>
									내용이 없습니다.
								</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>

						<?php
						}
						?>
						
					</tbody>
				</table>
			</div>
		</div>

		<div class="card-footer">

			<div class="row">
				<div class="col-md-8 text-left">
					<div class="form-group">
						<?=$this->common->GetPaging("board/lists/" . $board_config_print['con_name'], $board_count->num_rows(), $id, $page, @$get_parameter['search_array']['search_type'], @$get_parameter['search_array']['search_name'], $start, $end)?>
					</div>
				</div>

				<div class="col-md-4 text-right">
					<div class="form-group">
						<button type="button" class="btn btn-success GetWrite">글쓰기</button>
					</div>
				</div>
			</div>

		</div>

		<div class="card-footer text-right">

			<div class="row">
				<div class="col-md-8">
					<div class="form-group">

						<form id="SubmitOk" action="/board/lists/<?=$board_config_print['con_name']?>" method="post">
							<div class="page-options d-flex">
								<select name="search_type" class="form-control search_type custom-select w-auto">
									<option <?=(@$get_parameter['search_array']['search_type']=="")?"selected":""?> value="">선택 찾기</option>
									<option <?=(@$get_parameter['search_array']['search_type']=="member_name")?"selected":""?> value="member_name">작성자</option>
									<option <?=(@$get_parameter['search_array']['search_type']=="bbs_title")?"selected":""?> value="bbs_title">제목</option>
									<option <?=(@$get_parameter['search_array']['search_type']=="bbs_content")?"selected":""?> value="bbs_content">본문</option>
								</select>
								<div class="input-icon ml-2">
									<input type="text" name="search_name" class="form-control search_name w-10" value="<?=@$get_parameter['search_array']['search_name']?>">
								</div>
								<div class="input-icon ml-2">
									<input type="submit" class="btn btn-primary" value="검색">
								</div>
							</div>
						</form>
					
					</div>
				</div>
			</div>
		</div>

	</div>
		
</div>