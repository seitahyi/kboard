<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.min.js"></script>
<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote-ko-KR.js"></script>
<link href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.css" rel="stylesheet">

<script>
$(document).ready(function(){

	var no					= "<?=$board_print['bbs_no']?>";
	var con_name			= "<?=$board_config_print['con_name']?>";
	var member_id			= "<?=$get_parameter['session_array']['sess_member_id']?>";
	var file_count			= "<?=$file_count?>";
	var title, category, content, file, html, count, message;


	$(".MoveList").click(function(){

		location.href = '<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/' + con_name;

	});


	///////////////////////////////////


	$("#SubmitOk").submit(function(){

		title				= $(".bbs_title").val();
        category            = $("select[name=\"bbs_category\"]").val();
		content				= $(".bbs_content").val();
		message				= confirm("글을 작성 할까요?");


		if(message){

			if(title == ""){

				alert("제목을 입력하세요.");
				$(".bbs_title").focus();
				return false;

            } else if(category == ""){

                alert("카테고리를 선택하세요.");
                $(".bbs_category").focus();
                return false;

			} else if(content == ""){

				alert("본문을 입력하세요.");
				$(".bbs_content").focus();
				return false;

			}

		} else {

			alert("취소 되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".AddFile").click(function(){

		count	= $("input[name=\"file_name[]\"]").length;

		html	=	"";
		html	+=	"<input type=\"file\" name=\"file_name[]\" class=\"note-image-input form-control\" required /><br>";

		if(count < (5-file_count)){

			$(".Files").append(html);

		} else {

			alert("추가 할 수 없습니다.");

		}

	});


	///////////////////////////////////


	$(".RemoveFile").click(function(){

		file			= $(this).attr("data");
		message			= confirm("파일을 삭제하면 브라우저를 새로고침 합니다. 삭제를 계속 진행 할까요?");

		$(".RemoveFileRows_" + file).each(function(){

			if(message){

				RemoveFile(con_name, no, file);

			} else {

				alert("취소 되었습니다.");
				return false;

			}

		});

	});


	///////////////////////////////////


	$(".summernote").summernote({

		callbacks : {
			onImageUpload : function(files){
				SendFile(no, con_name, member_id, files, file_count);
			}
		},

		height : 500,

		lang : 'ko-KR',

		popover : {
			image : [],
			link : [],
			air : []
		},

		maximumImageFileSize : 5242880,
		
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['insert', ['link', 'picture', 'video']],
			['view', ['codeview']]
		]

	});


	///////////////////////////////////


	$(".note-icon-picture").click(function(){

		var loader	= $(".loader").show();

	});

});


///////////////////////////////////////


function RemoveFile(con_name, no, file){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/file_remove/" + con_name;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("no=" + no);
	ajax_param.push("file=" + file);
	ajax_param.push("con_name=" + con_name);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);


	if(call_back.return_code == "0000"){
		
		alert("삭제 되었습니다.");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("삭제 할 수 없습니다." + call_back.return_code);

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function SendFile(no, con_name, member_id, files, file_count){


	if(files.length < (6-file_count)){	// 썸머노트가 배열을 던질 때 기본적으로 1을 더해서 던져주는듯; 2019-07-01


		for(var i = 0; i < files.length; i++){

			(function(i){

				var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/insert_files/";
				var ajax_type			= "post";
				var ajax_return_type	= "json";
				var ajax_param			= new FormData();
				var ajax_return_data;


				ajax_param.append("no", no);
				ajax_param.append("file_name", files[i]);
				ajax_param.append("con_name", con_name);
				ajax_param.append("count", files.length);


				call_back				= ajaxSendFile(ajax_url, ajax_param, ajax_type, ajax_return_type);


				if(call_back.return_code == "0000"){

					var image	= $("<img>").attr("src", "<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/" + con_name + "/" + no + "/" + call_back.encrypt_name);
					$(".summernote").summernote("insertNode", image[0]);

				} else if(call_back.return_code == "9997"){

					alert(call_back.return_code + ": 파일이 크거나 파일 형식이 올바르지 않습니다.");

				} else if(call_back.return_code == "9998"){

					alert(call_back.return_code + ": 5개를 초과 할 수 없습니다.");

				} else {

					alert(call_back.return_code + ": <?=KO_UNSUCCESS?>");

				}


			})(i);

		}

	} else {

		alert("추가 할 수 없습니다.");

	}

}
</script>


<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">


	<form id="SubmitOk" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/board/modify_ok/<?=$board_config_print['con_name']?>/no/<?=$board_print['bbs_no']?>" enctype="multipart/form-data">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">

                    <div class="row">
	
	                    <?php
	                    if(count($bbs_category) > 1) {
	                    ?>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <select name="bbs_category" class="form-control bbs_category custom-select w-auto">
                                        <option value="">선택</option>
                                        <?php
                                        for($i = 0; $i < count($bbs_category); $i++){
                                        ?>
                                            <option value="<?=$bbs_category[$i]?>" <?=($bbs_category[$i]==$board_print['bbs_category'])?"selected":""?> ><?=$bbs_category[$i]?></option>
                                        <?php
                                        }
                                        unset($i);
                                        ?>
                                    </select>
                                </div>
                            </div>
		                <?php
	                    }
	                    ?>

                        <div class="col-sm-9 col-md-9">
                            <div class="form-group">
                                <input type="text" name="bbs_title" class="form-control bbs_title" value="<?=$board_print['bbs_title']?>" placeholder="제목을 입력하세요.">
                            </div>
                        </div>
                    </div>
                    
				</h3>
			</div>

			<div class="card-body">
				<div class="row">
					
                    <?php
                    $i  = "";
                    for($i = 1; $i <= 5; $i++) {
                        
                        if(!empty($board_config_print['ext_title_'.$i])) {
	                ?>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label class="form-label"><?= $board_config_print['ext_title_'.$i] ?></label>
                                    <input type="text" name="ext_desc_<?=$i?>" class="form-control ext_desc_<?=$i?>" value="<?= @$board_print['ext_desc_'.$i] ?>" placeholder="여분필드 본문 <?=$i?>을 입력하세요.">
                                </div>
                            </div>
	                <?php
                        }
                        
                    }
                    unset($i);
                    ?>

					<div class="col-sm-12 col-md-12">
						<div class="form-group">
							<textarea name="bbs_content" class="form-control summernote bbs_content" placeholder="본문을 입력하세요."><?=$board_print['bbs_content']?></textarea>
						</div>
					</div>

				</div>
			</div>

			<div class="card-body">
				<div class="row">

					<div class="col-sm-12 col-md-12">
						<div class="form-group">
							<div>
								<a class="AddFile btn btn-info" role="button" style="cursor:pointer;">첨부파일</a>
								<br /><br />
							</div>
							<div class="Files">
								<div class="alert alert-info">
									허용파일 : (<?=$board_config_print['con_thumb_filter']?>) / 총 <?=(5-$file_count)?>개
								</div>
							</div>

							<div>
								<?php
								if($file_count >= 1){

									$i		= 1;
									foreach($file_result as $files){
								?>
										<button type="button" class="btn btn-secondary RemoveFile" data="<?=$files['file_no']?>">
											<i class="fe fe-paperclip RemoveFileRows_<?=$files['file_no']?>"></i>&nbsp;파일삭제&nbsp;<?=($i++)?>
										</button>
								<?php
									}

								}
								?>
							</div>

						</div>
					</div>
				</div>
			</div>

            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="bbs_secret" class="bbs_secret" <?=($board_print['bbs_secret']=="Y")?"checked":""?> > 비밀글
                        </div>
                    </div>

                </div>
            </div>

			<div class="card-footer text-right">
				<button type="submit" class="btn btn-primary">저장</button>
				<button type="button" class="btn btn-secondary MoveList">목록</button>
			</div>
		</div>
	</form>

</div>