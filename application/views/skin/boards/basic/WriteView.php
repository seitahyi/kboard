<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.min.js"></script>
<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote-ko-KR.js"></script>
<link href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/boards/<?=$board_config_print['con_skin']?>/summernote/summernote.css" rel="stylesheet">

<script>
$(document).ready(function(){


	var con_name			= "<?=$board_config_print['con_name']?>";
	var member_id			= "<?=$get_parameter['session_array']['sess_member_id']?>";
	var title, category, content, html, count, message;


	$(".MoveList").click(function(){

		location.href = '<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/' + con_name;

	});


	///////////////////////////////////


	$("#SubmitOk").submit(function(){

		title				= $(".bbs_title").val();
		category            = $("select[name=\"bbs_category\"]").val();
		content				= $(".bbs_content").val();
		message				= confirm("글을 작성 할까요?");


		if(message){

			if(title == "") {

                alert("제목을 입력하세요.");
                $(".bbs_title").focus();
                return false;

            } else if(category == ""){
			    
			    alert("카테고리를 선택하세요.");
			    $(".bbs_category").focus();
			    return false;

			} else if(content == ""){

				alert("본문을 입력하세요.");
				$(".bbs_content").focus();
				return false;

			}

		} else {

			alert("취소 되었습니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".AddFile").click(function(){

		count	= $("input[name=\"file_name[]\"]").length;

		html	=	"";
		html	+=	"<input type=\"file\" name=\"file_name[]\" class=\"note-image-input form-control\" required />";

		if(count < 5){

			$(".Files").append(html);

		} else {

			alert("추가 할 수 없습니다.");

		}

	});


	///////////////////////////////////


	$(".summernote").summernote({

		height : 500,

		lang : 'ko-KR',

		popover : {
			image : [],
			link : [],
			air : []
		},
		
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['insert', ['link', 'picture2', 'video']],
			['view', ['codeview']]
		]

	});


	///////////////////////////////////


	$(".note-icon-picture").click(function(){

		title				= $(".bbs_title").val();
		content				= $(".bbs_content").val();
		message				= confirm("임시 저장 후 사용 가능 합니다. 계속 진행 할까요?");

		if(message){

			SetTMPContent(con_name, title, content, member_id);

		} else {

			alert("취소 되었습니다.");

		}

	});

});


function SetTMPContent(con_name, title, content, member_id){


	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/board/tmp_write_ok/" + con_name;
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("bbs_title=" + title);
	ajax_param.push("bbs_category=");
	ajax_param.push("bbs_content=" + content);
	ajax_param.push("con_name=" + con_name);
	ajax_param.push("member_id=" + member_id);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		console.log("<?=KO_SUCCESS?>");
		location.href = '<?=PROTOCOLS?><?=HTTP_HOST?>/board/modify/' + con_name + '/no/' + call_back.last_id;

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else if(call_back.return_code == "9997"){

		alert("<?=KO_DUPLICATED?>");

	} else {

		alert(call_back.return_code);

	}

}
</script>


<div class="col-lg-8" style="margin-top:50px; margin-bottom:80px;">

	<form id="SubmitOk" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/board/write_ok/<?=$board_config_print['con_name']?>" enctype="multipart/form-data">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">

                    <div class="row">
                        
                        <?php
                        if(count($bbs_category) > 1) {
	                    ?>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <select name="bbs_category" class="form-control bbs_category custom-select w-auto">
                                        <option value="">선택</option>
				                        <?php
				                        for ($i = 0; $i < count($bbs_category); $i++) {
					                    ?>
                                            <option value="<?= $bbs_category[$i] ?>"><?= $bbs_category[$i] ?></option>
					                    <?php
				                        }
				                        unset($i);
				                        ?>
                                    </select>
                                </div>
                            </div>
	                    <?php
                        }
                        ?>
                        
                        <div class="col-sm-9 col-md-9">
                            <div class="form-group">
                                <input type="text" name="bbs_title" class="form-control bbs_title" placeholder="제목을 입력하세요.">
                            </div>
                        </div>
                    </div>
                    
				</h3>
			</div>

			<div class="card-body">
				<div class="row">
					
                    <?php
                    $i  = "";
                    for($i = 1; $i <= 5; $i++){
                        
                        if(!empty($board_config_print['ext_title_'.$i])){
                    ?>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label class="form-label"><?= $board_config_print['ext_title_'.$i] ?></label>
                                    <input type="text" name="ext_desc_<?=$i?>" class="form-control ext_desc_<?=$i?>" value="<?= @$board_print['ext_desc_'.$i] ?>" placeholder="여분필드 본문 <?=$i?>을 입력하세요.">
                                </div>
                            </div>
					<?php
					    }
                    
                    }
                    unset($i);
                    ?>

					<div class="col-sm-12 col-md-12">
						<div class="form-group">
							<textarea name="bbs_content" class="form-control summernote bbs_content" placeholder="본문을 입력하세요."></textarea>
						</div>
					</div>

				</div>
			</div>

			<div class="card-body">
				<div class="row">

					<div class="col-sm-12 col-md-12">
						<div class="form-group">
							<div>
								<a class="AddFile btn btn-info" role="button" style="cursor:pointer;">첨부파일</a>
								<br /><br />
							</div>
							<div class="Files">
								<div class="alert alert-info">
									허용파일 : (<?=$board_config_print['con_thumb_filter']?>) / 총 5개
								</div>
							</div>
						</div>
					</div>
     
				</div>
			</div>

            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="bbs_secret" class="bbs_secret"> 비밀글
                        </div>
                    </div>

                </div>
            </div>

			<div class="card-footer text-right">
				<button type="submit" class="btn btn-primary">저장</button>
				<button type="button" class="btn btn-secondary MoveList">목록</button>
			</div>
		</div>
	</form>

</div>