<?php
if(!defined('BASEPATH')) exit;
?>


<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/latests/hyoju/default.css">

<!-- 최신글 -->
<div class="lt">
	<h2 class="lt_h2"><a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/<?=$table?>" class="lt_title"><?=$title?></a></h2>
	<ul>
		<?php
		if($crud_count >= 1){
			
			foreach($crud_result as $print){
				
		?>
				<li>
					<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$table?>/no/<?=$print['bbs_no']?>" class="lt_tit"><?=$this->common->SetString("60", $print['bbs_title'])?></a>
					<span class="lt_date"><?=date("Y-m-d", strtotime($print['bbs_date']))?></span>
				</li>
		<?php
			}
			
		} else {
		?>
			
			<li>
				게시물이 없습니다.
			</li>
			
		<?php
		}
		?>
	</ul>
	<div class="more">
		<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/<?=$table?>" class="lt_more">더보기 +</a>
	</div>
</div>
<!-- 최신글 -->