<?php
if(!defined('BASEPATH')) exit;
?>


<div class="card border-light p-3 m-4 col-lg-5">
	<div class="form-group">
		<div class="card-header">
			<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/<?=$table?>"><?=$title?></a>
		</div>
		<div class="card-body">

			<br>

			<?php
			if($crud_count >= 1){

				foreach($crud_result as $print){
			?>

					<p class="card-text">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$table?>/no/<?=$print['bbs_no']?>"><?=$this->common->SetString("20", $print['bbs_title'])?></a> | <?=$this->common->SetString("10", $print['member_name'])?> | <?=date("Y-m-d", strtotime($print['bbs_date']))?>
					</p>

			<?php
				}

			} else {
			?>
				<p class="card-text">
					내용이 없습니다.
				</p>
			<?php
			}
			?>

		</div>
	</div>
</div>