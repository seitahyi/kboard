<?php
if(!defined('BASEPATH')) exit;
?>


<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/latests/2re/default.css">

<div class="lt">
	<h2><?=$title?></h2>
	<ul>
		
		<?php
		if($crud_count >= 1){
			
			foreach($crud_result as $print){
			
		?>
				<li>
					<a href="#" class="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$table?>/no/<?=$print['bbs_no']?>"><?=$this->common->SetString("60", $print['bbs_title'])?></a>
					<span class="lt_date">
						<?=date("Y-m-d", strtotime($print['bbs_date']))?>
				    </span>
				</li>
				
		<?php
			}
			
		} else {
		?>
		 
			<li class="empty_li">
				게시물이 없습니다.
			</li>
			
		<?php
		}
		?>
		
	</ul>
</div>