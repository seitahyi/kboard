<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script>
$(document).ready(function(){

	var con_no, con_title, con_name, con_board_skin, con_skin, con_layout, con_thumb_filter, con_diff, con_list_level, con_content_level, con_write_level, con_reply_level, con_member_id, message, rows, counts;


	$("#SubmitOk").submit(function(){

		con_title			    = $(".con_title").val();
		con_name			    = $(".con_name").val();
		con_skin			    = $("select[name=\"con_skin\"]").val();
		con_layout			    = $("select[name=\"con_layout\"]").val();
		con_thumb_filter	    = $(".con_thumb_filter").val();
		con_diff			    = $(".con_diff").val();
        con_list_level          = $("select[name=\"con_list_level\"]").val();
        con_content_level       = $("select[name=\"con_content_level\"]").val();
        con_write_level         = $("select[name=\"con_write_level\"]").val();
        con_reply_level         = $("select[name=\"con_reply_level\"]").val();

		message				    = confirm("게시판을 수정 할까요?");

		if(con_title == ""){

			alert("게시판 이름을 입력하세요.");
			$(".con_title").focus();
			return false;

		} else if(con_name == ""){

			alert("게시판 테이블명을 입력하세요.");
			$(".con_name").focus();
			return false;

		} else if(con_skin == ""){

			alert("최신게시물 스킨을 선택하세요.");
			$(".con_skin").focus();
			return false;

		} else if(con_layout == ""){

			alert("최신게시물 스킨을 선택하세요.");
			$(".con_layout").focus();
			return false;

		} else if(con_thumb_filter == ""){

			alert("허용 가능 한 첨부파일을 입력하세요.");
			$(".con_thumb_filter").focus();
			return false;

		} else if(con_diff == ""){

			alert("작성 간격을 입력하세요.");
			$(".con_diff").focus();
			return false;

		} else if(con_list_level == ""){
		    
		    alert("목록 읽기 레벨을 설정하세요.");
		    $(".con_list_level").focus();
		    return false;
		    
        } else if(con_content_level == ""){
		    
		    alert("본문 읽기 레벨을 설정하세요.");
		    $(".con_content_level").focus();
		    return false;
		    
        } else if(con_write_level == ""){
		    
		    alert("본문 쓰기 레벨을 입력하세요.");
		    $(".con_write_level").focus();
		    return false;
		    
        } else if(con_reply_level == ""){
		    
		    alert("댓글 쓰기 레벨을 입력하세요.");
		    $(".con_reply_level").focus();
		    return false;
		    
        }

	});


});
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">게시판관리</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_list" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>게시판목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_add" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>게시판추가
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">게시판관리 / <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/lists/<?=$print['con_name']?>" target="_blank">바로가기</a> / <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/rss/<?=$print['con_name']?>">RSS</a></h3>
					</div>

					<form id="SubmitOk" method="post" action="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_mod_ok">
						<div class="card-body">

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">게시판 이름</label>
										<input type="text" name="con_title" class="form-control con_title" value="<?=$print['con_title']?>" placeholder="게시판 이름을 입력하세요.">
									</div>
								</div>
								
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">게시판 테이블명 (영어만 가능)</label>
										<input type="text" name="con_name" class="form-control con_name" value="<?=$print['con_name']?>" readonly>
									</div>
								</div>
							</div>

                            <div class="alert alert-primary"></div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">게시판 카테고리 (예:분식|중식)</label>
                                        <input type="text" name="con_category" class="form-control con_category" value="<?=@$print['con_category']?>" placeholder="카테고리명을 입력하세요.">
                                    </div>
                                </div>
                            </div>

							<div class="alert alert-primary"></div>

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">게시판 스킨</label>
										<select name="con_skin" class="form-control custom-select" required>
											<option value="" selected>유무 선택</option>
											<?php
											foreach($skin as $skins){
											?>
												<option value="<?=$skins?>" <?=($skins==$print['con_skin'])?"selected":""?> ><?=$skins?></option>
											<?php
											}
											unset($skins);
											?>
										</select>
									</div>
								</div>
								
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">레이아웃</label>
										<select name="con_layout" class="form-control custom-select" required>
											<option value="" selected>유무 선택</option>
											<?php
											foreach($layout as $skins){
											?>
												<option value="<?=$skins?>" <?=($skins==$print['con_layout'])?"selected":""?> ><?=$skins?></option>
											<?php
											}
											unset($skins);
											?>
										</select>
									</div>
								</div>
							</div>

							<div class="alert alert-primary"></div>

							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">첨부파일 허용 확장자 (예:jpg|mp3|png)</label>
										<textarea name="con_thumb_filter" class="form-control con_thumb_filter" placeholder="허용 가능한 첨부파일 확장자를 입력하세요."><?=$print['con_thumb_filter']?></textarea>
									</div>
								</div>
							</div>

							<div class="alert alert-primary"></div>

							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">(게시글 및 댓글) 작성 간격 (예:86400초 = 24시간 / 0 = 제한 없음)</label>
										<input type="text" name="con_diff" class="form-control con_diff" value="<?=@$print['con_diff']?>" placeholder="초단위">
									</div>
								</div>
							</div>

                            <div class="alert alert-primary"></div>

                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">목록 읽기 권한 (0 = 제한 없음)</label>
                                        <select name="con_list_level" class="form-control custom-select" required>
                                            <option value="" selected>레벨 선택</option>
		                                    <?php
		                                    for($i = 0; $i < 11; $i++){
			                                ?>
                                                <option value="<?=$i?>" <?=($i==$print['con_list_level'])?"selected":""?> ><?=$i?></option>
			                                <?php
		                                    }
		                                    unset($i);
		                                    ?>
                                        </select>
                                    </div>
                                </div>
	                            
	                            <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">본문 읽기 권한 (0 = 제한 없음)</label>
                                        <select name="con_content_level" class="form-control custom-select" required>
                                            <option value="" selected>레벨 선택</option>
											<?php
											for($i = 0; $i < 11; $i++){
											?>
                                                <option value="<?=$i?>" <?=($i==$print['con_content_level'])?"selected":""?> ><?=$i?></option>
											<?php
											}
											unset($i);
											?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-primary"></div>

                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">본문 쓰기 권한 (글쓰기는 로그인만 허용)</label>
                                        <select name="con_write_level" class="form-control custom-select" required>
                                            <option value="" selected>레벨 선택</option>
											<?php
											for($i = 1; $i < 10; $i++){
											?>
                                                <option value="<?=$i?>" <?=($i==$print['con_write_level'])?"selected":""?> ><?=$i?></option>
											<?php
											}
											unset($i);
											?>
                                        </select>
                                    </div>
                                </div>
	                            
	                            <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">댓글 쓰기 권한 (0 = 제한 없음)</label>
                                        <select name="con_reply_level" class="form-control custom-select" required>
                                            <option value="" selected>레벨 선택</option>
											<?php
											for($i = 0; $i < 11; $i++){
											?>
                                                <option value="<?=$i?>" <?=($i==$print['con_reply_level'])?"selected":""?> ><?=$i?></option>
											<?php
											}
											unset($i);
											?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-primary"></div>

                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">여분필드 제목 1</label>
                                        <input type="text" name="ext_title_1" class="form-control ext_title_1" value="<?=@$print['ext_title_1']?>" placeholder="여분필드 제목 1">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">여분필드 제목 2</label>
                                        <input type="text" name="ext_title_2" class="form-control ext_title_2" value="<?=@$print['ext_title_2']?>" placeholder="여분필드 제목 2">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">여분필드 제목 3</label>
                                        <input type="text" name="ext_title_3" class="form-control ext_title_3" value="<?=@$print['ext_title_3']?>" placeholder="여분필드 제목 3">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">여분필드 제목 4</label>
                                        <input type="text" name="ext_title_4" class="form-control ext_title_4" value="<?=@$print['ext_title_4']?>" placeholder="여분필드 제목 4">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">여분필드 제목 5</label>
                                        <input type="text" name="ext_title_5" class="form-control ext_title_5" value="<?=@$print['ext_title_5']?>" placeholder="여분필드 제목 5">
                                    </div>
                                </div>
                            </div>

						</div>

						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="submit" class="btn btn-primary btn-space">저장</button>
								<!--<button type="button" id="RemoveOk" class="btn btn-danger btn-space">삭제</button>-->
							</div>
						</div>
					</form>
				</div>

			</div>

		</div>
	</div>
</div>