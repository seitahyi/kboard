<?php
if(!defined('BASEPATH')) exit;
?>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">회원댓글</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/members" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_add" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원추가
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_pw" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 비밀번호 발급
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_tk" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 토큰 발급
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">댓글 (총:<?=$get_members_reply_count->num_rows()?>)</h3>
					</div>

					<div class="card-body">
						<div class="table-responsive">
							<table class="table card-table table-vcenter text-nowrap">
								<thead>
									<tr>
										<th>
											번호
										</th>
										<th>
											내용
										</th>
										<th>
											아이피
										</th>
										<th>
											작성일
										</th>
									</tr>
								</thead>
								<tbody>

									<?php
									if($count >= 1){

										foreach($result as $print){
									?>
											<tr>
												<td>
													<?=$num++?>
												</td>
												<td>
													<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$print['con_name']?>/no/<?=$print['bbs_no']?>" target="_blank">
														<?=$print['rep_content']?>
													</a>
												</td>
												<td>
													<?=$print['rep_ip']?>
												</td>
												<td>
													<?=$print['rep_date']?>
												</td>
											</tr>
									<?php
										}

									} else {
									?>
										<tr>
											<td>
												내용이 없습니다.
											</td>
											<td>
											</td>
											<td>
											</td>
											<td>
											</td>
											<td>
											</td>
										</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="card-footer text-left">

						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<?=$this->common->GetPaging("dashboard/member/member_reply_list/" . $member_array['request_member_id'], $get_members_reply_count->num_rows(), $id, @$search_array['page'], @$search_array['search_type'], @$search_array['search_name'], $start, $end)?>
								</div>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>
</div>