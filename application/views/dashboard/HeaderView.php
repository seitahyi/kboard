<?php
if(!defined('BASEPATH')) exit;
?>


<!doctype html>
	<html>
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<meta name="msapplication-TileColor" content="#2d89ef">
			<meta name="theme-color" content="#4188c9">
			<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
			<meta name="apple-mobile-web-app-capable" content="yes">
			<meta name="mobile-web-app-capable" content="yes">
			<meta name="HandheldFriendly" content="True">
			<meta name="MobileOptimized" content="320">

            <title><?=@$config['home_title']?>::관리자</title>

            <?php
            if(empty($config['home_icon'])) {
            ?>
                <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/favicon.ico" type="image/x-icon"/>
            <?php
            } else {
            ?>
                <link rel="icon" href="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_icon']?>" type="image/x-icon"/>
            <?php
            }
            ?>

			<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/css/font-awesome.min.css">
			<!--link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext"-->
			<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/css/dashboard.css">

			<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/vendors/jquery-3.2.1.min.js"></script>
			<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/bootstrap/bootstrap.bundle.min.js"></script>
		</head>

		<body>

			<div class="page">
				<div class="page-main">
					<div class="header py-4">
						<div class="container">
							<div class="d-flex">
								<a class="header-brand" href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home">
                                    <?php
                                    if(empty($config['home_logo'])) {
                                    ?>
                                        <img src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/demo/brand/tabler.svg" class="header-brand-img" alt="tabler logo">
                                    <?php
                                    } else {
                                    ?>
                                        <img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_logo']?>" class="header-brand-img" alt="tabler logo">
                                    <?php
                                    }
                                    ?>
								</a>
								<div class="d-flex order-lg-2 ml-auto">
									<div class="dropdown">
										<a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
											<span class="avatar" style="background-image: url('<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/demo/brand/default-profile-image.jpg')">
											</span>
											<span class="ml-2 d-none d-lg-block">
												<span class="text-default"><?=$this->common->GetDecrypt(@$session_array['sess_member_name'], @$secret_key, @$secret_iv)?></span>
												<small class="text-muted d-block mt-1"><?=$session_array['sess_member_id']?></small>
											</span>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
											<a class="dropdown-item" href="/dashboard/member/member_info/<?=$session_array['sess_member_id']?>">
												<i class="dropdown-icon fe fe-file"></i> 회원정보 수정
											</a>
											<a class="dropdown-item" href="/dashboard/member/signout/">
												<i class="dropdown-icon fe fe-log-out"></i> 로그아웃
											</a>
										</div>
									</div>
								</div>

								<a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
									<span class="header-toggler-icon"></span>
								</a>
							</div>
						</div>
					</div>
					<div id="headerMenuCollapse" class="header collapse d-lg-flex p-0">
						<div class="container">
							<div class="row align-items-center">
								<div class="col-lg order-lg-first">
									<ul class="nav nav-tabs border-0 flex-column flex-lg-row">
										<li class="nav-item">
											<?php
											$active	= "";
											if(@$admin_menu_array['home'] == @basename(REQUEST_URI)){

												$active	= "active";

											}
											?>
											<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home" class="nav-link <?=$active?>">
												<i class="fe fe-home"></i> 메인
											</a>
										</li>

										<li class="nav-item">
											<?php
											$active	= "";
											if(@$admin_menu_array['configs'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['companies'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											}
											?>
											<a href="javascript:void(0)" class="nav-link <?=$active?>" data-toggle="dropdown">
												<i class="fe fe-airplay"></i> 환경설정
											</a>
											<div class="dropdown-menu dropdown-menu-arrow">
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home/configs" class="dropdown-item"> 기본환경설정 </a>
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home/companies" class="dropdown-item"> 회사정보 </a>
											</div>
										</li>

										<li class="nav-item">
											<?php
											$active	= "";
											if(@$admin_menu_array['menus'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";
												
											} else if(@$admin_menu_array['page_add'] == @$this->common->SetDirectoryExplode("3")){
											    
											    $active = "active";
												
											} else if(@$admin_menu_array['page_mod'] == @$this->common->SetDirectoryExplode("3")){
												
												$active = "active";

											} else if(@$admin_menu_array['page_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";
												
											} else if(@$admin_menu_array['popup_add'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";
												
											} else if(@$admin_menu_array['popup_mod'] == @$this->common->SetDirectoryExplode("3")){
												
												$active	= "active";

											} else if(@$admin_menu_array['popup_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											}
											?>
											<a href="javascript:void(0)" class="nav-link <?=$active?>" data-toggle="dropdown">
												<i class="fe fe-book-open"></i> 페이지설정
											</a>
											<div class="dropdown-menu dropdown-menu-arrow">
												<!--<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus" class="dropdown-item"> 메뉴설정 </a>-->
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/page_list" class="dropdown-item"> 페이지설정 </a>
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_list" class="dropdown-item"> 팝업설정 </a>
											</div>
										</li>

										<li class="nav-item">
											<?php
											$active	= "";
											if(@$admin_menu_array['members'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['member_info'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['member_add'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['member_gen_pw'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['member_gen_tk'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['member_board_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['member_file_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['member_reply_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											}  else if(@$admin_menu_array['member_voted_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											}
											?>
											<a href="javascript:void(0)" class="nav-link <?=$active?>" data-toggle="dropdown">
												<i class="fe fe-user"></i> 회원관리
											</a>

											<div class="dropdown-menu dropdown-menu-arrow">
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/members" class="dropdown-item"> 회원목록 </a>
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_add" class="dropdown-item"> 회원추가 </a>
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_pw" class="dropdown-item"> 회원 비밀번호 발급 </a>
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_tk" class="dropdown-item"> 회원 토큰 발급 </a>
											</div>
										</li>
										
										<li class="nav-item">
											<?php
											$active	= "";
											if(@$admin_menu_array['board'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['board_add'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['board_mod'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['board_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['comment_mod'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											} else if(@$admin_menu_array['custom_board_list'] == @$this->common->SetDirectoryExplode("3")){
											    
											    $active = "active";
											    
											} else if(@$admin_menu_array['custom_comment_list'] == @$this->common->SetDirectoryExplode("3")){
											    
											    $active = "active";
											    
											} else if(@$admin_menu_array['custom_voted_list'] == @$this->common->SetDirectoryExplode("3")){
											    
											    $active = "active";
											    
											} else if(@$admin_menu_array['custom_files_list'] == @$this->common->SetDirectoryExplode("3")){
											    
											    $active = "active";
											    
											}
											?>
											<a href="javascript:void(0)" class="nav-link <?=$active?>" data-toggle="dropdown">
												<i class="fe fe-server"></i> 게시판설정
											</a>

											<div class="dropdown-menu dropdown-menu-arrow">
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_list" class="dropdown-item"> 게시판목록 </a>
												<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_add" class="dropdown-item"> 게시판추가 </a>
											</div>
										</li>
										
										<li class="nav-item">
											<?php
											$active	= "";
											if(@$admin_menu_array['lib_list'] == @$this->common->SetDirectoryExplode("3")){

												$active	= "active";

											}
											?>
											<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/library/lib_list" class="nav-link <?=$active?>">
												<i class="fe fe-package"></i> 라이브러리
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>