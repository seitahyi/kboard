<?php
if(!defined('BASEPATH')) exit;
?>


<script>
$(document).ready(function(){

	var member_id, member_password, member_name, member_email_1, member_email_2, member_level, member_profile, member_formal;

	$("#SubmitOk").submit(function(){

		member_id			= $(".member_id").val();
		member_password		= $(".member_password").val();
		member_name			= $(".member_name").val();
		member_email_1		= $(".member_email_1").val();
		member_email_2		= $(".member_email_2").val();
		member_level		= $("select[name=\"member_level\"]").val();
		member_profile		= $("select[name=\"member_profile\"]").val();
		member_formal		= $("select[name=\"member_formal\"]").val();

		if(member_password == ""){

			alert("비밀번호가 비었습니다.");
			$(".member_password").focus();
			return false;

		} else if(member_name == ""){

			alert("이름이 비었습니다.");
			$(".member_name").focus();
			return false;

		} else if(member_email_1 == ""){

			alert("이메일 아이디가 비었습니다.");
			$(".member_email_1").focus();
			return false;

		} else if(member_email_2 == ""){

			alert("이메일 호스트가 비었습니다.");
			$(".member_email_2").focus();
			return false;

		} else if(member_level == ""){

			alert("레벨을 선택하세요.");
			$(".member_level").focus();
			return false;

		} else if(member_profile == ""){

			alert("프로필 공개 유무를 선택하세요.");
			$(".member_profile").focus();
			return false;

		} else if(member_formal == ""){

			alert("회원의 정지 유무를 선택하세요.");
			$(".member_formal").focus();
			return false;

		}

	});
});
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">회원추가</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/members" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_add" class="list-group-item list-group-item-action d-flex align-items-center active">
							<span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>회원 추가
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_pw" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 비밀번호 발급
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_tk" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 토큰 발급
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">회원 추가</h3>
					</div>

					<form id="SubmitOk" action="/dashboard/member/member_add_ok/" method="post">
						<div class="card-body">

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">아이디</label>
										<input type="text" name="member_id" class="form-control member_id" placeholder="아이디를 입력하세요." />
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">비밀번호</label>
										<input type="password" name="member_password" class="form-control member_password" placeholder="비밀번호를 입력하세요" required />
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">이름</label>
										<input type="text" name="member_name" class="form-control member_name" placeholder="이름을 입력하세요." required />
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">이메일</label>
										<div class="input-group">
											<input type="text" name="member_email_1" class="form-control member_email_1" placeholder="이메일 아이디를 입력하세요." required />

											<span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">@</span>
											</span>

											<input type="text" name="member_email_2" class="form-control member_email_2" placeholder="이메일 아이디를 입력하세요." required />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">레벨</label>
										<select name="member_level" class="form-control custom-select member_level" required />
											<option value="" selected>레벨 선택</option>
											<?php
											for($i = 1; $i <= 10; $i++){
											?>
												<option value="<?=$i?>"><?=$i?></option>
											<?php
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">프로필 공개 유무</label>
										<select name="member_profile" class="form-control custom-select member_profile" required />
											<option value="" selected>유무 선택</option>
											<option value="0">비공개</option>
											<option value="1">공개</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">회원 정지 유무</label>
										<select name="member_formal" class="form-control custom-select member_formal" required />
											<option value="" selected>유무 선택</option>
											<option value="0">정지</option>
											<option value="1">승인</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">가입 일자</label>
										<input type="text" name="member_signup_date" class="form-control member_signup_date" placeholder="가입 일자는 선택 할 수 없습니다." readonly />
									</div>
								</div>
							</div>
						</div>
						
						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="submit" class="btn btn-primary btn-space">저장</button>
							</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
</div>