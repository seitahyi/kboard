<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/multiselect.min.js"></script>


<script>
$(document).ready(function(){

	var member_id, member_password, member_name, member_email_1, member_email_2, member_level, member_profile, member_formal, count, where, i, message;

	$("#SubmitOk").submit(function(){

		member_id			= $(".member_id").val();
		member_password		= $(".member_password").val();
		member_name			= $(".member_name").val();
		member_email_1		= $(".member_email_1").val();
		member_email_2		= $(".member_email_2").val();
		member_level		= $("select[name=\"member_level\"]").val();
		member_profile		= $("select[name=\"member_profile\"]").val();
		member_formal		= $("select[name=\"member_formal\"]").val();

		if(member_password == ""){

			alert("비밀번호가 비었습니다.");
			$(".member_password").focus();
			return false;

		} else if(member_name == ""){

			alert("이름이 비었습니다.");
			$(".member_name").focus();
			return false;

		} else if(member_email_1 == ""){

			alert("이메일 아이디가 비었습니다.");
			$(".member_email_1").focus();
			return false;

		} else if(member_email_2 == ""){

			alert("이메일 호스트가 비었습니다.");
			$(".member_email_2").focus();
			return false;

		} else if(member_level == ""){

			alert("레벨을 선택하세요.");
			$(".member_level").focus();
			return false;

		} else if(member_profile == ""){

			alert("프로필 공개 유무를 선택하세요.");
			$(".member_profile").focus();
			return false;

		} else if(member_formal == ""){

			alert("회원의 정지 유무를 선택하세요.");
			$(".member_formal").focus();
			return false;

		}

	});


	/////////////////////////////////////////////
	

	$("#search_rightSelected").click(function(){

		count			= $("#search :selected").length;
		where			= new Array(count);

		message			= confirm("권한을 추가 할까요?");

		if(message){

			if(count >= 1){

				for(i = 0; i < count; i++){
		
					where[i]		= $("#search :selected")[i].value;
		
					AddPerm(count, where[i]);
					
				}

			} else {

				alert("1개 이상 선택하세요.");
				location.reload();
				return false;

			}

		} else {

			alert("취소 되었습니다.");
			location.reload();
			return false;
			
		}
		
	});


	/////////////////////////////////////////////
	
	
	$("#search_leftSelected").click(function(){

		count			= $("#search_to :selected").length;
		where			= new Array(count);

		message			= confirm("권한을 삭제 할까요?");
		

		if(message){

			if(count >= 1){

				for(i = 0; i < count; i++){
					
					where[i]		= $("#search_to :selected")[i].value;
		
					RemovePerm(count, where[i]);
					
				}

			} else {

				alert("1개 이상 선택하세요.");
				location.reload();
				return false;

			}

		} else {

			alert("취소 되었습니다.");
			location.reload();
			return false;
			
		}
		
	});


	/////////////////////////////////////////////


	$("#RemoveMember").click(function(){

		member_no		= "<?=$member_get_print['member_no']?>";
		member_id		= "<?=$member_get_print['member_id']?>";
		member_email	= "<?=$member_get_print['member_email']?>";

		message			= confirm("회원을 삭제 할까요?");

		if(message){

			RemoveMember(member_no, member_id, member_email);

		} else {

			alert("취소 되었습니다.");
			location.reload();
			return false;

		}

	});


	/////////////////////////////////////////////


	$("#search").multiselect();


	/////////////////////////////////////////////


	$("#search").dblclick(function(){

		alert("더블 클릭은 사용 할 수 없습니다.");
		location.reload();
		return false;
		
	});


	/////////////////////////////////////////////


	$("#search_to").dblclick(function(){

		alert("더블 클릭은 사용 할 수 없습니다.");
		location.reload();
		return false;
		
	});


});


/////////////////////////////////////////////////


function AddPerm(count, where){
	
	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_add_perm_ok";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("count=" + count);
	ajax_param.push("from=" + where);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		console.log("<?=KO_SUCCESS?>");

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else {

		alert(call_back.return_code);

	}
	
}


/////////////////////////////////////////////////


function RemovePerm(count, where){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_remove_perm_ok";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("count=" + count);
	ajax_param.push("to=" + where);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		console.log("<?=KO_SUCCESS?>");

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else {

		alert(call_back.return_code);

	}
	
}


/////////////////////////////////////////////////


function RemoveMember(member_no, member_id, member_email){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_remove";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("member_no=" + member_no);
	ajax_param.push("member_id=" + member_id);
	ajax_param.push("member_email=" + member_email);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.href='<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/members/';

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else {

		alert(call_back.return_code);

	}

}
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">회원수정</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/members" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_add" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 추가
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_pw" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 비밀번호 발급
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_tk" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 토큰 발급
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">회원수정</h3>
					</div>

					<form id="SubmitOk" action="/dashboard/member/member_info_ok/" method="post">
						<div class="card-body">

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">아이디</label>
										<input type="text" name="member_id" class="form-control member_id" placeholder="아이디를 입력하세요." value="<?=$member_get_print['member_id']?>" readonly />
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">비밀번호</label>
										<input type="password" name="member_password" class="form-control member_password" placeholder="비밀번호를 입력하세요" value="<?=$this->common->GetDecrypt(@$member_get_print['member_password'], @$secret_key, @$secret_iv)?>" required />
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">이름</label>
										<input type="text" name="member_name" class="form-control member_name" placeholder="이름을 입력하세요." value="<?=$this->common->GetDecrypt(@$member_get_print['member_name'], @$secret_key, @$secret_iv)?>" required />
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">이메일</label>
										<div class="input-group">
											<?php
											$get_member_email = explode("@", $member_get_print['member_email']);
											?>
											<input type="text" name="member_email_1" class="form-control member_email_1" placeholder="이메일 아이디를 입력하세요." value="<?=$this->common->GetDecrypt(@$get_member_email['0'], @$secret_key, @$secret_iv)?>" readonly />
											<span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">@</span>
											</span>
											<input type="text" name="member_email_2" class="form-control member_email_2" placeholder="이메일 아이디를 입력하세요." value="<?=$this->common->GetDecrypt(@$get_member_email['1'], @$secret_key, @$secret_iv)?>" readonly />
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">레벨</label> 
										<select name="member_level" class="form-control custom-select member_level" required />
											<option value="">레벨 선택</option>
												<?php
												for($i = 1; $i <= 10; $i ++){
												?>
													<option value="<?=$i?>" <?=($member_get_print['member_level']==$i)?"selected":""?>><?=$i?></option>
												<?php
												}
												?>
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">프로필 공개 유무</label>
										<select name="member_profile" class="form-control custom-select member_profile" required />
											<option value="">유무 선택</option>
											<option value="0" <?=($member_get_print['member_profile']==0)?"selected":""?>>비공개</option>
											<option value="1" <?=($member_get_print['member_profile']==1)?"selected":""?>>공개</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">회원 정지 유무</label>
										<select name="member_formal" class="form-control custom-select member_formal" required />
											<option value="">유무 선택</option>
											<option value="0" <?=($member_get_print['member_formal']==0)?"selected":""?>>정지</option>
											<option value="1" <?=($member_get_print['member_formal']==1)?"selected":""?>>승인</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">가입 일자</label>
										<input type="text" name="member_signup_date" class="form-control member_signup_date" placeholder="레벨을 입력하세요." value="<?=$member_get_print['member_signup_date']?>" readonly />
									</div>
								</div>
							</div>
						</div>

						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="submit" class="btn btn-primary btn-space">저장</button>
								<button type="button" id="RemoveMember" class="btn btn-danger btn-space">삭제</button>
							</div>
						</div>
					</form>
				</div>


				<?php
				$member_perm_check	= $this->admin->CheckPermission($session_array['sess_member_id'], "member_add_perm_ok");
				
				
				if($member_perm_check->num_rows() >= 1 || $session_array['sess_member_id'] == "admin"){	// 권한이 있으며 어드민은 권한 유무를 따지지 않음
					
					if($session_array['sess_member_id'] != $member_array['request_member_id']){	// member_id 파라미터로 받은 값이 다를때 자신의 프로필은 해당 영역에 출력 시키지 않는다
				?>
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">어드민 접근 권한</h3>
							</div>
				
							<form method="post">
								<div class="card-body">
									<div class="row">
										<div class="col-xs-5">
											<select name="from[]" id="search" class="form-control" size="20" multiple="multiple" style="width:300px;">
												<?php
												foreach($perm_array as $print){
												?>
													<option value="<?=$print['0']?>|<?=$print['1']?>|<?=$member_get_print['member_id']?>"><?=$print['1']?></option>
												<?php
												}
												
												unset($print);
												?>
											</select>
										</div>
												
										<div class="col-xs-2">
											<button type="button" id="search_rightSelected" class="btn btn-block"> > </button>
											<button type="button" id="search_leftSelected" class="btn btn-block"> < </button>
										</div>
											
										<div class="col-xs-5">
											<select name="to[]" id="search_to" class="form-control" size="20" multiple="multiple" style="width:300px;">
												<?php
												foreach($member_perm_get_result as $right){
												?>
													<option value="<?=$right['perm_page']?>|<?=$right['perm_name']?>|<?=$right['perm_member_id']?>"><?=$right['perm_name']?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
							</form>
						</div>
				<?php
					}
					
				}
				?>

			</div>
		</div>
	</div>
</div>