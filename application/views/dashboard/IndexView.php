<?php
if(!defined('BASEPATH')) exit;
?>


<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/css/chart.min.css" />


<div class="my-3 my-md-5">
	<div class="container">
        <div class="row row-cards row-deck">

            
            <!--
                사업장 정보 시작
            -->
            <div class="col-md-8 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">사업장 정보</h3>
                    </div>
                    <div class="card-body">

                        <ul class="list-unstyled list-separated">
                            
                            
                            <?php
                            if($company_count >= 1) {
	                            ?>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <strong>회사명</strong>
                                        </div>
                                        <div class="col">
                                            <div>
					                            <?= $this->common->GetDecrypt($company['com_name'], @$secret_key, @$secret_iv) ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <strong>대표자명</strong>
                                        </div>
                                        <div class="col">
                                            <div>
					                            <?= $this->common->GetDecrypt($company['com_owner'], @$secret_key, @$secret_iv) ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <strong>사업장 주소</strong>
                                        </div>
                                        <div class="col">
                                            <div>
					                            <?= $this->common->GetDecrypt($company['com_address_1'], @$secret_key, @$secret_iv) ?>
					                            <?= $this->common->GetDecrypt($company['com_address_2'], @$secret_key, @$secret_iv) ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div class="col-auto">
                                            <strong>정보관리 책임자 이메일</strong>
                                        </div>
                                        <div class="col">
                                            <div>
					                            <?php
					                            $email = explode("@", $company['com_manager_email']);
					                            echo $this->common->GetDecrypt($email['0'], @$secret_key, @$secret_iv) . "@" . $this->common->GetDecrypt($email['1'], @$secret_key, @$secret_iv);
					                            ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
	
	                        <?php
                            } else {
	                        ?>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <div>
					                            내용이 없음.
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                
	                        <?php
                            }
                            ?>

                        </ul>

                    </div>
                </div>
            </div>
            <!--
                사업장 정보 끝
            -->


            <!--
                홈페이지 정보 시작
            -->
            <div class="col-md-8 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">홈페이지 사용량</h3>
                    </div>
                    <div class="card-body">

                        <ul class="list-unstyled list-separated">

                            <li class="list-separated-item">
                                <div class="charts">
                                    <span>HDD 사용량</span>
                                    <div class="charts__chart" data-percent="<?=$this->admin->SetVolume($this->admin->GetDirectory(FCPATH))?> / 1024GB"></div>
                                </div>
                            </li>

                            <li class="list-separated-item">
                                <div class="charts">
                                    <span>DB 사용량</span>
                                    <div class="charts__chart" data-percent="<?=$this->admin->SetVolume($tables['Byte'])?> / 100MB"></div>
                                </div>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <!--
                홈페이지 정보 끝
            -->


            <!--
                총 게시물 갯수 시작
            -->
            <div class="col-md-4 col-lg-2">
                <div class="card p-3">
                    <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-blue mr-3">
                      <i class="fe fe-align-right"></i>
                    </span>
                        <div>
                            <h4 class="m-0"><?=($boards_count != 0)?$boards_count:"0"?> <small>개</small></h4>
                            <small class="text-muted">게시물 갯수</small>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                총 게시물 갯수 끝
            -->


            <!--
                총 댓글 갯수 시작
            -->
            <div class="col-md-4 col-lg-2">
                <div class="card p-3">
                    <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-purple mr-3">
                      <i class="fe fe-message-square"></i>
                    </span>
                        <div>
                            <h4 class="m-0"><?=($replies_count != 0)?$replies_count:"0"?> <small>개</small></h4>
                            <small class="text-muted">댓글 갯수</small>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                총 댓글 갯수 끝
            -->


            <!--
                총 회원 수 시작
            -->
            <div class="col-md-4 col-lg-2">
                <div class="card p-3">
                    <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-yellow mr-3">
                      <i class="fe fe-user"></i>
                    </span>
                        <div>
                            <h4 class="m-0"><?=($members_count != 0)?$members_count:"0"?> <small>명</small></h4>
                            <small class="text-muted">총 회원</small>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                총 회원 수 끝
            -->


            <!--
                총 추천/비추천 갯수 시작
            -->
            <div class="col-md-4 col-lg-2">
                <div class="card p-3">
                    <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-orange mr-3">
                      <i class="fe fe-thumbs-up"></i>
                    </span>
                        <div>
                            <h4 class="m-0"><?=($voted_count != 0)?$voted_count:"0"?> <small>개</small></h4>
                            <small class="text-muted">추천/비추천 수</small>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                총 추천/비추천 갯수 끝
            -->


            <!--
                총 첨부파일 갯수 시작
            -->
            <div class="col-md-4 col-lg-2">
                <div class="card p-3">
                    <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-green mr-3">
                      <i class="fe fe-paperclip"></i>
                    </span>
                        <div>
                            <h4 class="m-0"><?=($files_count != 0)?$files_count:"0"?> <small>개</small></h4>
                            <small class="text-muted">첨부파일 갯수</small>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                총 첨부파일 갯수 끝
            -->


            <!--
                최신 게시물 시작
            -->
            <div class="col-md-5 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">최신 게시물</h3>
                    </div>
                    <div class="card-body o-auto" style="height: 15rem">

                        <ul class="list-unstyled list-separated">

                            <?php
                            if($boards_count >= 1) {
                                
                                $i  = 1;
                                foreach($boards_result as $boards) {
	                            ?>

                                    <li class="list-separated-item">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
				                                <?= $i++ ?>
                                            </div>
                                            <div class="col">
                                                <div>
                                                    <a href="<?= PROTOCOLS ?><?= HTTP_HOST ?>/board/contents/<?= $boards['con_name'] ?>/no/<?= $boards['bbs_no'] ?>"><?= $this->common->SetString("12", $boards['bbs_title']) ?></a>
                                                </div>
                                            </div>
                                            <div class="col-auto">
				                                <?= date("Y-m-d", strtotime($boards['bbs_date'])) ?>
                                            </div>
                                        </div>
                                    </li>
                                        
                            <?php
                                }
                                unset($i);
                                
                            } else {
	                        ?>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <div>
                                                내용이 없음.
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                
	                        <?php
                            }
                            ?>

                        </ul>
                        
                    </div>
                </div>
            </div>
            <!--
                최신 게시물 끝
            -->


            <!--
                최신 댓글 시작
            -->
            <div class="col-md-5 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">최신 댓글</h3>
                    </div>
                    <div class="card-body o-auto" style="height: 15rem">

                        <ul class="list-unstyled list-separated">
                            
                            <?php
                            if($replies_count >= 1) {
                                
                                $i  = 1;
                                foreach($replies_result as $replies) {
	                        ?>

                                    <li class="list-separated-item">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
				                                <?= $i++ ?>
                                            </div>
                                            <div class="col">
                                                <div>
                                                    <a href="<?= PROTOCOLS ?><?= HTTP_HOST ?>/board/contents/<?= $boards['con_name'] ?>/no/<?= $replies['bbs_no'] ?>"><?= $this->common->SetString("12", $replies['rep_content']) ?></a>
                                                </div>
                                            </div>
                                            <div class="col-auto">
				                                <?= date("Y-m-d", strtotime($replies['rep_date'])) ?>
                                            </div>
                                        </div>
                                    </li>
	
	                        <?php
                                }
                                unset($i);
                                
                            } else {
	                        ?>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div>
                                            <div>
                                                내용이 없음.
                                            </div>
                                        </div>
                                    </div>
                                </li>
	
	                        <?php
                            }
                            ?>

                        </ul>

                    </div>
                </div>
            </div>
            <!--
                최신 댓글 끝
            -->


            <!--
                최신 가입 시작
            -->
            <div class="col-md-5 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">최근 가입 회원</h3>
                    </div>
                    <div class="card-body o-auto" style="height: 15rem">

                        <ul class="list-unstyled list-separated">

                            <?php
                            if($members_count >= 1) {
                                
                                $i  = 1;
                                foreach($members_result as $members) {
	                        ?>

                                    <li class="list-separated-item">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
				                                <?= $i++ ?>
                                            </div>
                                            <div class="col">
                                                <div>
                                                    <a href="<?= PROTOCOLS ?><?= HTTP_HOST ?>/dashboard/member/member_info/<?= $members['member_id'] ?>"><?= $this->common->GetDecrypt($members['member_name'], @$secret_key, @$secret_iv) ?></a>
                                                </div>
                                            </div>
                                            <div class="col-auto">
				                                <?= date("Y-m-d", strtotime($members['member_signup_date'])) ?>
                                            </div>
                                        </div>
                                    </li>
	
	                        <?php
                                }
                                unset($i);
                                
                            } else {
	                        ?>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <div>
                                                내용이 없음.
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                
	                        <?php
                            }
                            ?>

                        </ul>

                    </div>
                </div>
            </div>
            <!--
                최신 가입 끝
            -->


            <!--
                최신 첨부파일 시작
            -->
            <div class="col-md-5 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">최근 첨부파일</h3>
                    </div>
                    <div class="card-body o-auto" style="height: 15rem">

                        <ul class="list-unstyled list-separated">
					
					        <?php
					        if($files_count >= 1) {
						
						        $i  = 1;
						        foreach($files_result as $files) {
							?>

                                    <li class="list-separated-item">
                                        <div class="row align-items-center">
                                            <div class="col-auto">
										        <?= $i++ ?>
                                            </div>
                                            <div class="col">
                                                <div>
                                                    <a href="<?= PROTOCOLS ?><?= HTTP_HOST ?>/board/contents/<?= $boards['con_name'] ?>/no/<?= $files['bbs_no'] ?>"><?= $this->common->SetString("12", $files['file_real_name']) ?></a>
                                                </div>
                                            </div>
                                            <div class="col-auto">
										        <?= date("Y-m-d", strtotime($files['file_date'])) ?>
                                            </div>
                                        </div>
                                    </li>
							
							<?php
						        }
						        unset($i);
						
					        } else {
						    ?>

                                <li class="list-separated-item">
                                    <div class="row align-items-center">
                                        <div>
                                            <div>
                                                내용이 없음.
                                            </div>
                                        </div>
                                    </div>
                                </li>
						
						    <?php
					        }
					        ?>

                        </ul>

                    </div>
                </div>
            </div>
            <!--
                최신 첨부파일 끝
            -->
            
        
        </div>
	</div>
</div>