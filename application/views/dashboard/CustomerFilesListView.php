<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script>
$(document).ready(function(){


	var con_name			= "<?=$board_array['request_con_name']?>";
	var message, rows, counts;


	$("#table #set_check").click(function(){

		if( $("#table #set_check").is(":checked") ){

			$("#table input[type=\"checkbox\"]").each(function(){

				$(this).prop("checked", true);

			});

		} else {

			$("#table input[type=\"checkbox\"]").each(function(){

				$(this).prop("checked", false);

			});

		}

	});


	///////////////////////////////////


	$("#SetFileDelete").click(function(){


		counts				= $("input:checkbox[name=\"get_check\"]:checked").length;


		if(counts >= 1){


			message		= confirm("삭제 할까요?");

			
			if(message){

				$("input:checkbox[name=\"get_check\"]:checked").each(function(){

					rows		= $(this).attr("value");
					SetDelete(con_name, rows);

				});

			} else {

				alert("취소 되었습니다.");
				return false;

			}


		} else {

			alert("1개 이상을 선택해야 합니다.");
			return false;

		}

	});


	///////////////////////////////////


	$(".download").click(function(){

		no		= $(this).attr("data");

		$(".rows_" + no).each(function(){

			GetDownload(con_name, no);

		});

	});


});


///////////////////////////////////////


function SetDelete(con_name, no){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/file_remove";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("con_name=" + con_name);
	ajax_param.push("no=" + no);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		console.log("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////


function GetDownload(con_name, no){

	window.open("<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/download/" + con_name + "/no/" + no);

}
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">파일관리</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_list" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>게시판목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_add" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>게시판추가
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">

				<div class="card">
					<div class="card-header">
						<h3 class="card-title">파일 (총:<?=@$files_count->num_rows()?>개)</h3>
					</div>

					
					<div class="card-body">
						<div class="table-responsive">
							<table id="table" class="table table-striped">
								<thead>
									<tr>
										<th>
											<input type="checkbox" id="set_check">
										</th>
										<th>
											#
										</th>
										<th>
											작성자
										</th>
										<th>
											제목
										</th>
										<th>
											파일
										</th>
                                        <th>
                                            파일형식
                                        </th>
										<th>
											아이피
										</th>
										<th>
											작성일
										</th>
									</tr>
								</thead>
								<tbody>

									<?php
									if($files_count->num_rows() >= 1){

										foreach($files_result as $files){
									?>
											
											<tr>
												<td style="width:1%;">
													<input type="checkbox" name="get_check" class="get_check" value="<?=$files['file_no']?>">
												</td>
												<td style="width:6%;">
													<span class="text-muted">
														<?=$num++?>
													</span>
												</td>
												<td style="width:15%;">
                                                    <?=$files['member_name']?>
												</td>
												<td style="width:25%;">
													<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$board_array['request_con_name']?>/no/<?=$files['bbs_no']?>" target="_blank"><?=$this->common->SetString("30", $files['bbs_title'])?></a>
												</td>
												<td style="width:25%;">
													<a href="#" class="download" data="<?=$files['file_no']?>">
														<span class="rows_<?=$files['file_no']?>">
															<?php
															$file_type		= array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/png","image/bmp");
															
															if(in_array($files['file_type'], $file_type)) {
															?>
                                                                <img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/<?=$board_array['request_con_name']?>/<?=$files['bbs_no']?>/<?=$files['file_encrypt_name']?>">
															<?php
															} else {
																echo $files['file_real_name'];
															}
															?>
														</span>
													</a>
												</td>
                                                <td style="width:13%;">
													<?=$files['file_type']?>
                                                </td>
												<td style="width:13%;">
													<?=$files['file_ip']?>
												</td>
												<td style="width:15%;">
													<?=$files['file_date']?>
												</td>
											</tr>

									<?php
										}


									} else {
									?>

										<tr>
											<td>
												내용이 없습니다.
											</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>

									<?php
									}
									?>
									
								</tbody>
							</table>
						</div>
					</div>

					<div class="card-footer">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<?=$this->common->GetPaging("dashboard/board/custom_files_list/" . $board_array['request_con_name'], $files_count->num_rows(), $id, $page, @$search_array['search_type'], @$search_array['search_name'], $start, $end)?>
								</div>
							</div>
						</div>
					</div>

					<div class="card-footer">
						<div class="row">
							<div class="col-md-12 text-right">
								<div class="form-group">
                                    <button id="SetFileDelete" class="btn btn-danger">삭제</button>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

		</div>
	</div>
</div>