<?php
if(!defined('BASEPATH')) exit;
?>


<!doctype html>
	<html lang="ko">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<meta http-equiv="Content-Language" content="ko" />
			<meta name="msapplication-TileColor" content="#2d89ef">
			<meta name="theme-color" content="#4188c9">
			<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
			<meta name="apple-mobile-web-app-capable" content="yes">
			<meta name="mobile-web-app-capable" content="yes">
			<meta name="HandheldFriendly" content="True">
			<meta name="MobileOptimized" content="320">

			<title>어드민</title>

			<!-- 라이브러리 시작 -->
			<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/css/dashboard.css">
			<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/css/font-awesome.min.css">
			<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/vendors/jquery-3.2.1.min.js"></script>
			<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/vendors/bootstrap.bundle.min.js"></script>

			<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>
			<!-- 라이브러리 끝 -->
		</head>
		<body>

			
			<script>
			$(document).ready(function(){

				var member_id, member_pw;

				$("#Submit").click(function(){

					member_id	= $(".member_id").val();
					member_pw	= $(".member_pw").val();

					if(member_id == ""){

						alert("아이디를 입력 해주세요.");
						$(".member_id").focus();
						return false;

					} else if(member_pw == ""){

						alert("비밀번호를 입력 해주세요.");
						$(".member_pw").focus();
						return false;

					}

				});


				/////////////////////////////////////////////////////


				$(document).keypress(function(e){
					if(e.keyCode == 13)
						e.preventDefault();
				});
			});
			</script>


		
			<div class="container">
				<div class="row">
				<!-- css수정추가 부트스트랩삭제 (ie호환불가) -->
					<div style="width:358px;padding-top:250px;margin: auto;">
						<form class="card" action="/dashboard/member/signin_ok" method="post">
							<div class="card-body p-6">
								<div class="card-title">
									로그인 계정
								</div>

								<div class="form-group">
									<label class="form-label">
										아이디
									</label>
									<input type="text" name="member_id" class="form-control member_id" placeholder="아이디">
								</div>
								<div class="form-group">
									<label class="form-label">
										비밀번호
									</label>
									<input type="password" name="member_password" class="form-control member_pw" placeholder="비밀번호">
								</div>
								<div class="form-footer">
									<button type="submit" id="Submit" class="btn btn-primary btn-block">로그인</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div id="alert" style="position: absolute; right: 0px; bottom: 0px;"></div>
		</body>
	</html>