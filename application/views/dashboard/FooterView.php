<?php
if(!defined('BASEPATH')) exit;
?>

		</div>

			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-auto ml-lg-auto">
							<div class="row align-items-center">
								<div class="col-auto">
									<ul class="list-inline list-inline-dots mb-0">
										<li class="list-inline-item">
                                            Copyright © 2018 한국웹네트워크. Theme by <a href="https://codecalm.net" target="_blank">codecalm.net</a> All rights reserved.
                                        </li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
                            0.4.0.3.0.1
                        </div>
					</div>
				</div>
			</footer>

		</div>

		<div id="alert" style="position: absolute; right: 0px; bottom: 0px;"></div>

	</body>
</html>