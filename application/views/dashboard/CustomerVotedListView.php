<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>


<script>
$(document).ready(function(){
    
    
    var con_name			= "<?=$board_array['request_con_name']?>";
    var con_no, message, rows, counts;


    $("#table #set_check").click(function(){

        if( $("#table #set_check").is(":checked") ){

            $("#table input[type=\"checkbox\"]").each(function(){

                $(this).prop("checked", true);

            });

        } else {

            $("#table input[type=\"checkbox\"]").each(function(){

                $(this).prop("checked", false);

            });

        }

    });


    ///////////////////////////////////
    
    
    $("#SetVotedDelete").click(function(){

        counts				= $("input:checkbox[name=\"get_check\"]:checked").length;
        
        if(counts >= 1){
            
            message         = confirm("삭제 할까요?");
            
            if(message){

                $("input:checkbox[name=\"get_check\"]:checked").each(function(){

                    rows		= $(this).attr("value");
                    SetDelete(con_name, rows);

                });
                
            } else {
                
                alert("취소 되었습니다.");
                return false;
                
            }
            
        } else {
            
            alert("1개 이상을 선택해야 합니다.");
            return false;
            
        }
       
    });
    
    
});


///////////////////////////////////////


function SetDelete(con_name, no){

    var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/voted_remove";
    var ajax_type			= "post";
    var ajax_return_type	= "json";
    var ajax_param			= [];
    var ajax_return_data;

    ajax_param.push("con_name=" + con_name);
    ajax_param.push("no=" + no);

    call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

    if(call_back.return_code == "0000"){

        console.log("<?=KO_SUCCESS?>");
        location.reload();

    } else if(call_back.return_code == "9999"){

        alert("<?=KO_UNSUCCESS?>");

    } else {

        alert(call_back.return_code);

    }

}
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">추천관리</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_list" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>게시판목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/board/board_add" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>게시판추가
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">

				<div class="card">
					<div class="card-header">
						<h3 class="card-title">추천/비추천 현황(총:<?=@$voted_count->num_rows()?>개)</h3>
					</div>

					
					<div class="card-body">
						<div class="table-responsive">
							<table id="table" class="table table-striped text-nowrap">
								<thead>
									<tr>
                                        <th>
                                            <input type="checkbox" id="set_check">
                                        </th>
										<th>
											#
										</th>
										<th>
											추천인
										</th>
										<th>
											제목
										</th>
										<th>
											추천/비추천
										</th>
										<th>
											아이피
										</th>
										<th>
											작성일
										</th>
									</tr>
								</thead>
								<tbody>

									<?php
									if($voted_count->num_rows() >= 1){

										foreach($voted_result as $voted){
									?>
											
											<tr>
                                                <td>
                                                    <input type="checkbox" name="get_check" class="get_check" value="<?=$voted['vo_no']?>">
                                                </td>
												<td>
													<span class="text-muted">
														<?=$num++?>
													</span>
												</td>
												<td>
													<?=$voted['member_name']?>
												</td>
												<td>
													<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/board/contents/<?=$board_array['request_con_name']?>/no/<?=$voted['bbs_no']?>" target="_blank"><?=$voted['bbs_title']?></a>
												</td>
												<td>
													<?php
													if($voted['vo_like'] == 1){

														echo "추천";

													} else if($voted['vo_dislike'] == 1){

														echo "비추천";

													}
													?>
												</td>
												<td>
													<?=$voted['vo_ip']?>
												</td>
												<td>
													<?=$voted['vo_date']?>
												</td>
											</tr>

									<?php
										}


									} else {
									?>

										<tr>
											<td>
												내용이 없습니다.
											</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>

									<?php
									}
									?>
									
								</tbody>
							</table>
						</div>
					</div>

					<div class="card-footer">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<?=$this->common->GetPaging("dashboard/board/custom_voted_list/" . $board_array['request_con_name'], $voted_count->num_rows(), $id, $page, @$search_array['search_type'], @$search_array['search_name'], $start, $end)?>
								</div>
							</div>
						</div>
					</div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <button id="SetVotedDelete" class="btn btn-danger">삭제</button>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>

			</div>

		</div>
	</div>
</div>