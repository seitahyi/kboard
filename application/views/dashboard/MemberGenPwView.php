<?php
if(!defined('BASEPATH')) exit;
?>


<script>
$(document).ready(function(){

	var search_type, search_name;

	$("#SubmitOk").submit(function(){

		search_type		= $("select[name=\"search_type\"]").val();
		search_name		= $(".search_name").val();

		if(search_type == ""){

			alert("검색 찾기를 선택하세요.");
			$(".search_type").focus();
			return false;

		} else if(search_name == ""){

			alert("검색어를 입력하세요.");
			$(".search_name").focus();
			return false;

		}

	});

});
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">회원 비밀번호 발급</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/members" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_add" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원추가
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_pw" class="list-group-item list-group-item-action d-flex align-items-center active">
							<span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>회원 비밀번호 발급
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/member/member_gen_tk" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회원 토큰 발급
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">비밀번호 발급 목록 (총:<?=$get_member_gen_pw_count->num_rows()?>)</h3>
					</div>

					<div class="card-body">
						<div class="table-responsive">
							<table class="table card-table table-vcenter text-nowrap">
								<thead>
									<tr>
										<th>
											번호
										</th>
										<th>
											아이디
										</th>
										<th>
											발급 된 비밀번호
										</th>
										<th>
											아이피
										</th>
										<th>
											발급일
										</th>
									</tr>
								</thead>
								<tbody>

									<?php
									foreach($result as $print){
									?>
										<tr>
											<td>
												<?=$num++?>
											</td>
											<td>
												<?=$print['member_id']?>
											</td>
											<td>
												<?=$this->common->GetDecrypt($print['lo_password'], @$secret_key, @$secret_iv)?>
											</td>
											<td>
												<?=$print['lo_ip']?>
											</td>
											<td>
												<?=$print['lo_date']?>
											</td>
										</tr>
									<?php
									}
									?>
									
								</tbody>
							</table>
						</div>
					</div>

					<div class="card-footer text-left">

						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<?=$this->common->GetPaging("dashboard/member/member_gen_pw", $get_member_gen_pw_count->num_rows(), $id, @$search_array['page'], @$search_array['search_type'], @$search_array['search_name'], $start, $end)?>
								</div>
							</div>
						</div>

					</div>

					<div class="card-footer text-right">

						<div class="row">
							<div class="col-md-8">
								<div class="form-group">

									<form id="SubmitOk" action="/dashboard/member/member_gen_pw/" method="post">
										<div class="page-options d-flex">
											<select name="search_type" class="form-control search_type custom-select w-auto">
												<option <?=($search_array['search_type']=="")?"selected":""?> value="">선택 찾기</option>
												<option <?=($search_array['search_type']=="member_id")?"selected":""?> value="member_id">아이디</option>
												<option <?=($search_array['search_type']=="member_name")?"selected":""?> value="member_name">이름</option>
											</select>
											<div class="input-icon ml-2">
												<span class="input-icon-addon">
													<i class="fe fe-search"></i>
												</span>
												<input type="text" name="search_name" class="form-control search_name w-10" value="<?=$search_array['search_name']?>">
											</div>
											<div class="input-icon ml-2">
												<input type="submit" class="btn btn-primary" value="검색">
											</div>
										</div>
									</form>
								
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>