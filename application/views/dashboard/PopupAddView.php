<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/summernote/summernote.min.js"></script>
<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/summernote/summernote-ko-KR.js"></script>
<link href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/summernote/summernote.css" rel="stylesheet">


<script>
$(document).ready(function(){

    var member_id       = "<?=$session_array['sess_member_id']?>";
    var popup_title, popup_name, popup_content, popup_layout;

    $("#SubmitOk").submit(function(){

        popup_title      = $(".popup_title").val();
        popup_name       = $(".popup_name").val();
        popup_content    = $(".popup_content").val();
        popup_layout     = $("select[name=\"popup_layout\"]").val();

        message          = confirm("페이지를 추가 할까요?");

        if(message) {

            if(popup_title == "") {

                alert("페이지 이름을 입력하세요.");
                $(".popup_title").focus();
                return false;

            } else if (popup_name == "") {

                alert("페이지 테이블명을 입력하세요.");
                $(".popup_name").focus();
                return false;

            } else if (popup_content == "") {

                alert("페이지 내용을 입력하세요.");
                $(".popup_content").focus();
                return false;

            } else if (popup_layout == "") {

                alert("페이지 레이아웃을 선택하세요.");
                $(".popup_layout").focus();
                return false;

            }

        } else {

            alert("취소 되었습니다.");
            return false;

        }

    });


    //////////////////////////////////


    $(".summernote").summernote({

        height : 400,

        lang : 'ko-KR',

        popover : {
            image : [],
            link : [],
            air : []
        },

        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture2', 'video']],
            ['view', ['codeview']]
        ]

    });


    //////////////////////////////////


    $(".note-icon-picture").click(function(){

        popup_title			= $(".popup_title").val();
        popup_name			= $(".popup_name").val();
        popup_content       = $(".popup_content").val();
        
        message				= confirm("임시 저장 후 사용 가능 합니다. 계속 진행 할까요?");

        if(message){

            SetTmpContent(popup_title, popup_name, popup_content, member_id);

        } else {

            alert("취소 되었습니다.");
            
        }

    });


    ///////////////////////////////////


    $(".popup_name").keyup(function(event){
        if (!(event.keyCode >=37 && event.keyCode<=40)) {
            var inputVal = $(this).val();
            $(this).val(inputVal.replace(/[^a-z0-9_-]/gi,''));
        }
    });


    ///////////////////////////////////


    $(".MoveList").click(function(){

        location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_list";

    });


});


///////////////////////////////////


function SetTmpContent(title, name, content, member_id){


    var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/tmp_popup_add_ok/";
    var ajax_type			= "post";
    var ajax_return_type	= "json";
    var ajax_param			= [];
    var ajax_return_data;

    ajax_param.push("popup_title=" + title);
    ajax_param.push("popup_name=" + name);
    ajax_param.push("popup_content=" + content);
    ajax_param.push("member_id=" + member_id);

    call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

    if(call_back.return_code == "0000"){

        console.log("<?=KO_SUCCESS?>");
        location.href = '<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_mod/' + call_back.popup_name;

    } else if(call_back.return_code == "9999"){

        alert("<?=KO_UNSUCCESS?>");

    } else if(call_back.return_code == "9997"){

        alert("<?=KO_DUPLICATED?>");

    } else {

        alert(call_back.return_code);

    }

}
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">페이지설정</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<!--
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>메뉴설정
						</a>
                        -->
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/page_list" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>페이지목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_list" class="list-group-item list-group-item-action d-flex align-items-center active">
							<span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>팝업목록
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">팝업추가</h3>
					</div>
					
					<form id="SubmitOk" action="/dashboard/pager/popup_add_ok" method="post">
						
						<input type="hidden" name="member_id" class="member_id" value="<?=$session_array['sess_member_id']?>">
						<div class="card-body">
							
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">팝업 이름</label>
										<input type="text" name="popup_title" class="form-control popup_title" value="" placeholder="팝업 이름을 입력하세요." required>
									</div>
								</div>
							</div>
							
							<div class="alert alert-primary"></div>
							
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">팝업명 (영어만 가능)</label>
										<input type="text" name="popup_name" class="form-control popup_name" value="" placeholder="팝업 테이블명을 입력하세요." required>
									</div>
								</div>
							</div>
							
							<div class="alert alert-primary"></div>
							
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">팝업 내용</label>
										<textarea name="popup_content" class="form-control summernote popup_content" value="" placeholder="팝업 내용을 입력하세요." required></textarea>
									</div>
								</div>
							</div>
						</div>
						
						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="submit" class="btn btn-primary btn-space">저장</button>
								<button type="button" class="btn btn-secondary MoveList">목록</button>
							</div>
						</div>
					</form>
				
				</div>
			</div>
		</div>
	</div>
</div>