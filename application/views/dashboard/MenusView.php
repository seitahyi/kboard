<?php
if(!defined('BASEPATH')) exit;
?>


<link rel="stylesheet" href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/css/menu_list.css">
<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>


<script>
$(document).ready(function(){

	var row				= -1;	// Attribute 에 조부모, 부모, 자식의 상속 관계를 입증키 위한 넘버링을 인덱스로 잡는다.
	var menu_no, menu_grandparent, menu_parent, menu_child, menu_name, menu_url, menu_target, message, data_row;

	$(".AddMenu").click(function(){

		menu_grandparent	= $(".menu_grandparent").val();
		menu_parent			= $(".menu_parent").val();
		menu_child			= $(".menu_child").val();
		menu_name			= $(".menu_name").val();
		menu_url			= $(".menu_url").val();
		menu_target			= $(".menu_target").val();

		message			= confirm("메뉴를 추가 할까요?");

		if(message){

			if(menu_name == ""){

				alert("메뉴명을 입력하세요.");
				$(".menu_name").focus();
				return false;
				
			} else if(menu_url == ""){

				alert("주소를 입력하세요.");
				$(".menu_url").focus();
				return false;
				
			} else {

				CreateMenu(menu_grandparent, menu_parent, menu_name, menu_url, menu_target);
				
			}
			
		} else {

			alert("취소 되었습니다.");
			return false;
			
		}
		
	});


	///////////////////////////////////


	$(".ModMenu").click(function(){
		
		data_row			= $(this).attr("data");

		menu_no				= $(".menu_no_mod_" + data_row).val();
		menu_grandparent	= $(".menu_grandparent_mod_" + data_row).val();
		menu_parent			= $(".menu_parent_mod_" + data_row).val();
		menu_child			= $(".menu_child_mod_" + data_row).val();
		menu_name			= $(".menu_name_mod_" + data_row).val();
		menu_url			= $(".menu_url_mod_" + data_row).val();
		menu_target			= $(".menu_target_mod_" + data_row).val();

		message				= confirm("메뉴를 수정 할까요?");

		if(message){

			if(menu_name == ""){

				alert("메뉴명을 입력하세요.");
				$(".menu_name_mod_" + data_row).focus();
				return false;
				
			} else if(menu_url == ""){

				alert("주소를 입력하세요.");
				$(".menu_url_mod_" + data_row).focus();
				return false;
				
			} else {

				ModifyMenu(menu_no, menu_grandparent, menu_parent, menu_child, menu_name, menu_url, menu_target);
				
			}
			
		} else {

			alert("취소 되었습니다.");
			return false;
			
		}
		
	});


	//////////////////////////////////


	$(".ModMenu2").click(function(){
		
		data_row			= $(this).attr("data");

		menu_no				= $(".menu_no_mod2_" + data_row).val();
		menu_grandparent	= $(".menu_grandparent_mod2_" + data_row).val();
		menu_parent			= $(".menu_parent_mod2_" + data_row).val();
		menu_child			= $(".menu_child_mod2_" + data_row).val();
		menu_name			= $(".menu_name_mod2_" + data_row).val();
		menu_url			= $(".menu_url_mod2_" + data_row).val();
		menu_target			= $(".menu_target_mod2_" + data_row).val();

		message				= confirm("메뉴를 수정 할까요?");

		if(message){

			if(menu_name == ""){

				alert("메뉴명을 입력하세요.");
				$(".menu_name_mod2_" + data_row).focus();
				return false;
				
			} else if(menu_url == ""){

				alert("주소를 입력하세요.");
				$(".menu_url_mod2_" + data_row).focus();
				return false;
				
			} else {

				ModifyMenu2(menu_no, menu_grandparent, menu_parent, menu_child, menu_name, menu_url, menu_target);
				
			}
			
		} else {

			alert("취소 되었습니다.");
			return false;
			
		}
		
	});


	//////////////////////////////////
	
	
	$(".DelMenu").click(function(){

		data_row		= $(this).attr("data");

		message			= confirm("메뉴를 삭제 할까요?");

		if(message){

			RemoveMenu(data_row);
			
		} else {

			alert("취소 되었습니다.");
			return false;
			
		}
		
	});


	//////////////////////////////////


	$(".DelMenu2").click(function(){

		data_row		= $(this).attr("data");

		message			= confirm("메뉴를 삭제 할까요?");

		if(message){

			RemoveMenu2(data_row);
			
		} else {

			alert("취소 되었습니다.");
			return false;
			
		}
		
	});


	//////////////////////////////////


	$(".AddPar").click(function(){

		data_row			= $(this).attr("data");

		menu_grandparent	= $(".menu_grandparent_added_" + data_row).val();
		menu_parent			= $(".menu_parent_added_" + data_row).val();
		menu_child			= $(".menu_child_added_" + data_row).val();
		menu_name			= $(".menu_name_added_" + data_row).val();
		menu_url			= $(".menu_url_added_" + data_row).val();
		menu_target			= $(".menu_target_added_" + data_row).val();

		message			= confirm("메뉴를 추가 할까요?");

		if(message){

			if(menu_name == ""){

				alert("메뉴명을 입력하세요.");
				$(".menu_name_added_" + data_row).focus();
				return false;

			} else if(menu_url == ""){

				alert("주소를 입력하세요.");
				$(".menu_url_added_" + data_row).focus();
				return false;

			} else if(menu_target == ""){

				alert("주소를 입력하세요.");
				$(".menu_target_added_" + data_row).focus();
				return false;

			} else {
		
				CreateSubMenu(menu_grandparent, menu_parent, menu_child, menu_name, menu_url, menu_target);

			}

		} else {

			alert("취소 되었습니다.");
			return false;
			
		}


	});

	
});


/////////////////////////////////////


function CreateMenu(menu_grandpar, menu_parent, menu_name, menu_url, menu_target){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus_gd_add";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("menu_grandparent=" + menu_grandpar);
	ajax_param.push("menu_parent=0");
	ajax_param.push("menu_child=0");
	ajax_param.push("menu_name=" + menu_name);
	ajax_param.push("menu_url=" + menu_url);
	ajax_param.push("menu_target=" + menu_target);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9998"){

		alert("<?=KO_DUPLICATED?>");
		location.reload();

	} else {

		alert(call_back.return_code);
		location.reload();

	}
	
}


/////////////////////////////////////


function CreateSubMenu(menu_grandpar, menu_parent, menu_child, menu_name, menu_url, menu_target){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus_pa_add";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("menu_grandparent=" + menu_grandpar);
	ajax_param.push("menu_parent=" + menu_parent);
	ajax_param.push("menu_child=0");
	ajax_param.push("menu_name=" + menu_name);
	ajax_param.push("menu_url=" + menu_url);
	ajax_param.push("menu_target=" + menu_target);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9998"){

		alert("<?=KO_DUPLICATED?>");
		location.reload();

	} else {

		alert(call_back.return_code);
		location.reload();

	}
	
}


/////////////////////////////////////


function ModifyMenu(menu_no, menu_grandparent, menu_parent, menu_child, menu_name, menu_url, menu_target){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus_mod_add";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("menu_no=" + menu_no);
	ajax_param.push("menu_grandparent=" + menu_grandparent);
	ajax_param.push("menu_parent=" + menu_parent);
	ajax_param.push("menu_child=" + menu_child);
	ajax_param.push("menu_name=" + menu_name);
	ajax_param.push("menu_url=" + menu_url);
	ajax_param.push("menu_target=" + menu_target);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9998"){

		alert("<?=KO_CANTMODIFY?>");
		location.reload();

	} else {

		alert(call_back.return_code);
		location.reload();

	}
	
}


/////////////////////////////////////


function ModifyMenu2(menu_no, menu_grandparent, menu_parent, menu_child, menu_name, menu_url, menu_target){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus_mod_add";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("menu_no=" + menu_no);
	ajax_param.push("menu_grandparent=" + menu_grandparent);
	ajax_param.push("menu_parent=" + menu_parent);
	ajax_param.push("menu_child=" + menu_child);
	ajax_param.push("menu_name=" + menu_name);
	ajax_param.push("menu_url=" + menu_url);
	ajax_param.push("menu_target=" + menu_target);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9998"){

		alert("<?=KO_CANTMODIFY?>");
		location.reload();

	} else {

		alert(call_back.return_code);
		location.reload();

	}
	
}


/////////////////////////////////////


function RemoveMenu(menu_no){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus_remove";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("menu_no=" + menu_no);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");
		location.reload();

	} else {

		alert(call_back.return_code);
		location.reload();

	}
	
}


/////////////////////////////////////


function RemoveMenu2(menu_no){

	var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus_remove";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("menu_no=" + menu_no);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("<?=KO_SUCCESS?>");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("<?=KO_UNSUCCESS?>");
		location.reload();

	} else {

		alert(call_back.return_code);
		location.reload();

	}
	
}
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
		
			<div class="col-md-3">
				<h3 class="page-title mb-5">페이지설정</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
                        <!--
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus" class="list-group-item list-group-item-action d-flex align-items-center active">
							<span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>메뉴설정
						</a>
                        -->
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/pager" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>페이지설정
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>팝업설정
						</a>
					</div>
				</div>
			</div>
			
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">메뉴 구성</h3>
					</div>
					
					<form>

						<div class="card-body">
							<div id="menulist" class="tbl_head01">
								<table>
									<caption>메뉴설정 목록</caption>
									<thead>
										<tr>
											<th scope="col">순서</th>
											<th scope="col">이름</th>
											<th scope="col">주소</th>
											<th scope="col">타겟</th>
											<th scope="col">관리</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i		= 0;
										foreach($menu as $print){


											/** 리팩토링 구간 전체 행 가져온 후 각 행의 고유 menu_grandparent 값과 menu_parent 값을 가져오기 **/
											$gd_max				= $this->PagerModel->GetGDMaximum($print['menu_grandparent']);
											$gd_max_print		= $gd_max->row_array();
											$max_gd				= $gd_max_print['max_gd'];
											
											$par_max			= $this->PagerModel->GetParentMaximum($print['menu_grandparent']);
											$par_max_print		= $par_max->row_array();
											$max_parent			= ($par_max_print['max_par']==0)? 1 : $par_max_print['max_par']+1;
											/** 리팩토링 구간 전체 행 가져온 후 각 행의 고유 menu_grandparent 값과 menu_parent 값을 가져오기 **/

										
											if($print['menu_parent'] == 0){
											?>
											
												<tr class="bg<?=$i++?>">
													<td class="td_num">
														<input type="text" name="menu_grandparent" class="form-control form-control-sm" value="<?=$print['menu_grandparent']?>" readonly>
													</td>
													<td class="td_category">
														<input type="text" name="menu_name" class="form-control form-control-sm" value="<?=$print['menu_name']?>" readonly>
													</td>
													<td>
														<input type="text" name="menu_url" class="form-control form-control-sm" value="<?=$print['menu_url']?>" readonly>
													</td>
													<td class="td_mng">
														<select name="menu_target" class="form-control form-control-sm" disabled>
															<?php
															if($print['menu_target'] == "_self"){
															?>
																<option value="_self" <?=($print['menu_target']=="_self")?"selected=\"selected\"":""?> >현재창</option>
															<?php
															} else if($print['menu_target'] == "_blank"){
															?>
																<option value="_blank" <?=($print['menu_target']=="_blank")?"selected=\"selected\"":""?> >새창</option>
															<?php
															}
															?>
														</select>
													</td>
													<td class="td_mng">
														<div class="item-action dropdown">
															<a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>
															<div class="dropdown-menu dropdown-menu-right">
																<a href="javascript:void(0)" class="dropdown-item" data-toggle="modal" data-target=".myModal_added_<?=$print['menu_grandparent']?>"><i class="dropdown-icon fe fe-plus"></i>추가</a>

																<?php
																if($par_max_print['max_par'] <= 0){
																?>
																	<a href="javascript:void(0)" class="dropdown-item" data-toggle="modal" data-target=".myModal_Mod_<?=$print['menu_no']?>"><i class="dropdown-icon fe fe-edit-2"></i>수정</a>
																	<a href="javascript:void(0)" class="dropdown-item DelMenu" data="<?=$print['menu_no']?>"><i class="dropdown-icon fe fe-trash-2"></i>삭제</a>
																<?php
																}
																?>
															</div>
														</div>
													</td>
												</tr>
												
											<?php
											}
											
											if($print['menu_parent'] != 0){
											?>
											
												<tr class="bg<?=$i++?>">
													<td class="td_num sub_menu_class">
														<input type="text" name="menu_parent" class="form-control form-control-sm" value="<?=$print['menu_parent']?>" readonly>
													</td>
													<td class="td_category">
														<input type="text" name="menu_name" class="form-control form-control-sm" value="<?=$print['menu_name']?>" readonly>
													</td>
													<td>
														<input type="text" name="menu_url" class="form-control form-control-sm" value="<?=$print['menu_url']?>" readonly>
													</td>
													<td class="td_mng">
														<select name="menu_target" class="form-control form-control-sm" disabled>
															<?php
															if($print['menu_target'] == "_self"){
															?>
																<option value="_self" <?=($print['menu_target']=="_self")?"selected=\"selected\"":""?> >현재창</option>
															<?php
															} else if($print['menu_target'] == "_blank"){
															?>
																<option value="_blank" <?=($print['menu_target']=="_blank")?"selected=\"selected\"":""?> >새창</option>
															<?php
															}
															?>
														</select>
													</td>
													<td class="td_mng">
														<div class="item-action dropdown">
															<a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>
															<div class="dropdown-menu dropdown-menu-right">
																<a href="javascript:void(0)" class="dropdown-item" data-toggle="modal" data-target=".myModal_Mod2_<?=$print['menu_no']?>"><i class="dropdown-icon fe fe-edit-2"></i>수정</a>
																<a href="javascript:void(0)" class="dropdown-item DelMenu2" data="<?=$print['menu_no']?>"><i class="dropdown-icon fe fe-trash-2"></i>삭제</a>
															</div>
														</div>
													</td>
												</tr>
												
											<?php
											}
											?>
											
											<!-- modify row modal start -->
											<div class="modal fade myModal_Mod_<?=$print['menu_no']?>" role="dialog">
												<div class="modal-dialog modal-lg">
													
													<form>
														<div class="modal-content">
															<div class="modal-header">
																<h4>1차메뉴 수정</h4>
																<button type="button" class="close" data-dismiss="modal"></button>
															</div>
															<div class="modal-body">
																<div class="my-3 my-md-5">
																	<div class="container">
																		<div class="card">
																			<div class="card-body">
																				<div class="row">
																				
																					<input type="hidden" name="menu_no" class="menu_no_mod_<?=$print['menu_no']?>" value="<?=$print['menu_no']?>">
																					<input type="hidden" name="menu_grandparent" class="menu_grandparent_mod_<?=$print['menu_no']?>" value="<?=$print['menu_grandparent']?>">
																					
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 이름</label>
																							<input type="text" name="menu_name" class="form-control menu_name_mod_<?=$print['menu_no']?>" value="<?=$print['menu_name']?>" placeholder="메뉴 이름">
																						</div>
																					</div>
																					
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 URL</label>
																							<input type="text" name="menu_url" class="form-control menu_url_mod_<?=$print['menu_no']?>" value="<?=$print['menu_url']?>" placeholder="메뉴 URL" >
																						</div>
																					</div>
																						
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 타겟</label>
																							<select name="menu_target" class="form-control custom-select menu_target_mod_<?=$print['menu_no']?>">
																								<option value="_self" <?=($print['menu_target']=="_self")?"selected=\"selected\"":""?> >현재창</option>
																								<option value="_blank" <?=($print['menu_target']=="_blank")?"selected=\"selected\"":""?> >새창</option>
																							</select>
																						</div>
																					</div>
																
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
																<button type="button" class="btn btn-primary ModMenu" data="<?=$print['menu_no']?>">저장</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											<!-- modify row modal end -->


											<!-- modify2 row modal start -->
											<div class="modal fade myModal_Mod2_<?=$print['menu_no']?>" role="dialog">
												<div class="modal-dialog modal-lg">
													
													<form>
														<div class="modal-content">
															<div class="modal-header">
																<h4>2차메뉴 수정</h4>
																<button type="button" class="close" data-dismiss="modal"></button>
															</div>
															<div class="modal-body">
																<div class="my-3 my-md-5">
																	<div class="container">
																		<div class="card">
																			<div class="card-body">
																				<div class="row">
																				
																					<input type="hidden" name="menu_no" class="menu_no_mod2_<?=$print['menu_no']?>" value="<?=$print['menu_no']?>">
																					<input type="hidden" name="menu_grandparent" class="menu_grandparent_mod2_<?=$print['menu_no']?>" value="<?=$print['menu_grandparent']?>">
																					<input type="hidden" name="menu_parent" class="menu_parent_mod2_<?=$print['menu_no']?>" value="<?=$print['menu_parent']?>">
																					
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 이름</label>
																							<input type="text" name="menu_name" class="form-control menu_name_mod2_<?=$print['menu_no']?>" value="<?=$print['menu_name']?>" placeholder="메뉴 이름">
																						</div>
																					</div>
																					
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 URL</label>
																							<input type="text" name="menu_url" class="form-control menu_url_mod2_<?=$print['menu_no']?>" value="<?=$print['menu_url']?>" placeholder="메뉴 URL" >
																						</div>
																					</div>
																						
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 타겟</label>
																							<select name="menu_target" class="form-control custom-select menu_target_mod2_<?=$print['menu_no']?>">
																								<option value="_self" <?=($print['menu_target']=="_self")?"selected=\"selected\"":""?> >현재창</option>
																								<option value="_blank" <?=($print['menu_target']=="_blank")?"selected=\"selected\"":""?> >새창</option>
																							</select>
																						</div>
																					</div>
																
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
																<button type="button" class="btn btn-primary ModMenu2" data="<?=$print['menu_no']?>">저장</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											<!-- modify row modal end -->
											
											
											<!-- added row modal start -->
											<div class="modal fade myModal_added_<?=$print['menu_grandparent']?>" role="dialog">
												<div class="modal-dialog modal-lg">
													
													<form>
														<div class="modal-content">
															<div class="modal-header">
																<h4>2차메뉴 추가</h4>
																<button type="button" class="close" data-dismiss="modal"></button>
															</div>
															<div class="modal-body">
																<div class="my-3 my-md-5">
																	<div class="container">
																		<div class="card">
																			<div class="card-body">
																				<div class="row">
																				
																					<input type="hidden" name="menu_grandparent" class="menu_grandparent_added_<?=$print['menu_grandparent']?>" value="<?=$print['menu_grandparent']?>">
																					<input type="hidden" name="menu_child" class="menu_child_added_<?=$print['menu_grandparent']?>" value="<?=$print['menu_child']?>"><br>
																				
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 순서 (<?=@$max_parent?> = 원하는 위치 입력)</label>
																							<input type="text" name="menu_parent" class="form-control menu_parent_added_<?=$print['menu_grandparent']?>" value="<?=@$max_parent?>" placeholder="메뉴 순서" required>
																						</div>
																					</div>
																					
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 이름</label>
																							<input type="text" name="menu_name" class="form-control menu_name_added_<?=$print['menu_grandparent']?>" value="<?=$print['menu_name']?>" placeholder="메뉴 이름" required>
																						</div>
																					</div>
																					
																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 URL</label>
																							<input type="text" name="menu_url" class="form-control menu_url_added_<?=$print['menu_grandparent']?>" value="<?=$print['menu_url']?>" placeholder="메뉴 URL" required>
																						</div>
																					</div>

																					<div class="col-sm-12 col-md-12">
																						<div class="form-group">
																							<label class="form-label">메뉴 타겟</label>
																							<select name="menu_target" class="form-control custom-select menu_target_added_<?=$print['menu_grandparent']?>">
																								<option value="_self" <?=($print['menu_target']=="_self")?"selected=\"selected\"":""?> >현재창</option>
																								<option value="_blank" <?=($print['menu_target']=="_blank")?"selected=\"selected\"":""?> >새창</option>
																							</select>
																						</div>
																					</div>
																
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
																<button type="button" class="btn btn-primary AddPar" data="<?=$print['menu_grandparent']?>">저장</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											<!-- add row modal end -->
											
										<?php
										}
										?>
									</tbody>
									
									
									<!-- add row modal start -->
									<div class="modal fade myModal" role="dialog">
										<div class="modal-dialog modal-lg">
											
											<form>
												<div class="modal-content">
													<div class="modal-header">
														<h4>메뉴 추가</h4>
														<button type="button" class="close" data-dismiss="modal"></button>
													</div>
													<div class="modal-body">
														<div class="my-3 my-md-5">
															<div class="container">
																<div class="card">
																	<div class="card-body">
																		<div class="row">
																			
																			<div class="col-sm-12 col-md-12">
																				<div class="form-group">
																					<label class="form-label">메뉴 순서 (<?=@$max_gd+1?> = 원하는 위치 입력)</label>
																					<input type="text" name="menu_grandparent" class="form-control menu_grandparent" value="<?=@$max_gd+1?>" placeholder="메뉴 순서" required />
																				</div>
																			</div>

																			<div class="col-sm-12 col-md-12">
																				<div class="form-group">
																					<label class="form-label">메뉴 이름</label>
																					<input type="text" name="menu_name" class="form-control menu_name" value="" placeholder="메뉴 이름" required />
																				</div>
																			</div>

																			<div class="col-sm-12 col-md-12">
																				<div class="form-group">
																					<label class="form-label">메뉴 URL</label>
																					<input type="text" name="menu_url" class="form-control menu_url" value="" placeholder="메뉴 URL" required />
																				</div>
																			</div>

																			<div class="col-sm-12 col-md-12">
																				<div class="form-group">
																					<label class="form-label">메뉴 타겟</label>
																					<select name="menu_target" class="form-control custom-select menu_target">
																						<option value="_self" selected="selected">현재창</option>
																						<option value="_blank">새창</option>
																					</select>
																				</div>
																			</div>
																
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
														<button type="button" class="btn btn-primary AddMenu">저장</button>
													</div>
												</div>
											</form>
										</div>
									</div>
									<!-- add row modal end -->

									
								</table>
							</div>
						</div>
	
						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="button" class="btn btn-warning btn-space" data-toggle="modal" data-target=".myModal"><i class="fe fe-plus"></i>새로 추가</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>