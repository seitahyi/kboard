<?php
if(!defined('BASEPATH')) exit;
?>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">라이브러리 목록</h3>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table card-table table-vcenter text-nowrap">
								<thead>
									<tr>
										<th>
											번호
										</th>
										<th>
											제목
										</th>
										<th>
											링크
										</th>
										<th>
											정보
										</th>
										<th>
											생성일
										</th>
										<th>
											최근 업데이트
										</th>
									</tr>
								</thead>
        
								<tbody>
                                    <?php
									for($i = 0; $i < count($set_json); $i++){
									?>
										<tr>
											<td>
												<span class="text-muted">
													<?=$set_json[$i]['lib_no']?>
												</span>
											</td>
											<td>
												<?=$set_json[$i]['lib_title']?>
											</td>
											<td>
												<?=$set_json[$i]['lib_name']?>
											</td>
											<td>
												<?=$set_json[$i]['lib_desc']?>
											</td>
											<td>
												<?=$set_json[$i]['lib_created_date']?>
											</td>
											<td>
												<?=$set_json[$i]['lib_latest_date']?>
											</td>
										</tr>
									<?php
									}
									?>
                                    
                                    <br>
								</tbody>
        
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>