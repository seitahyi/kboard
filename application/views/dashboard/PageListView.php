<?php
if(!defined('BASEPATH')) exit;
?>


<script>
$(document).ready(function(){

    var search_type, search_name;

    $("#SubmitOk").submit(function(){

        search_type		= $("select[name=\"search_type\"]").val();
        search_name		= $(".search_name").val();

        if(search_type == ""){

            alert("검색 찾기를 선택하세요.");
            $(".search_type").focus();
            return false;

        } else if(search_name == ""){

            alert("검색어를 입력하세요.");
            $(".search_name").focus();
            return false;

        }

    });


//////////////////////////////////////////


    $(".AddPage").click(function(){

        location.href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/page_add";

    });

});
</script>


<div class="my-3 my-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h3 class="page-title mb-5">페이지설정</h3>
                <div>
                    <div class="list-group list-group-transparent mb-0">
                        <!--
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>메뉴설정
						</a>
                        -->
                        <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/page_list" class="list-group-item list-group-item-action d-flex align-items-center active">
                            <span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>페이지목록
                        </a>
                        <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_list" class="list-group-item list-group-item-action d-flex align-items-center">
                            <span class="icon mr-3"></span>팝업목록
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">페이지목록</h3>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table card-table table-vcenter text-nowrap">
                                <thead>
                                    <tr>
                                        <th>
                                            번호
                                        </th>
                                        <th>
                                            이름
                                        </th>
                                        <th>
                                            테이블명
                                        </th>
                                        <th>
                                            생성자
                                        </th>
                                        <th>
                                            아이피
                                        </th>
                                        <th>
                                            생성일자
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
								
                                    <?php
                                    foreach($result as $print){
                                    ?>
                                        <tr>
                                            <td>
                                                <span class="text-muted">
                                                    <?=$print['page_no']?>
                                                </span>
                                            </td>
                                            <td>
                                                <?=$print['page_title']?>
                                            </td>
                                            <td>
                                                <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/page_mod/<?=$print['page_name']?>">
                                                    <?=$print['page_name']?>
                                                </a>
                                            </td>
                                            <td>
                                                <?=$print['member_id']?>
                                            </td>
                                            <td>
                                                <?=$print['page_ip']?>
                                            </td>
                                            <td>
                                                <?=$print['page_date']?>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card-footer text-left">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
									<?php
                                    echo $this->common->GetPaging("dashboard/pager/page_list/", $page_count->num_rows(), $id, @$search_array['page'], @$search_array['search_type'], @$search_array['search_name'], $start, $end);
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer text-right">

                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">

                                    <form id="SubmitOk" action="/dashboard/pager/page_list/" method="post">
                                        <div class="page-options d-flex">
                                            <select name="search_type" class="form-control search_type custom-select w-auto">
                                                <option <?=($search_array['search_type']=="")?"selected":""?> value="">선택 찾기</option>
                                                <option <?=($search_array['search_type']=="con_title")?"selected":""?> value="con_title">이름</option>
                                                <option <?=($search_array['search_type']=="con_name")?"selected":""?> value="con_name">테이블명</option>
                                            </select>
                                            <div class="input-icon ml-2">
												<span class="input-icon-addon">
													<i class="fe fe-search"></i>
												</span>
                                                <input type="text" name="search_name" class="form-control search_name w-10" value="<?=$search_array['search_name']?>">
                                            </div>
                                            <div class="input-icon ml-2">
                                                <button type="submit" class="btn btn-primary">검색</button>
                                                <button type="button" class="btn btn-success AddPage">페이지 추가</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>