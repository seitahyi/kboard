<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/summernote/summernote.min.js"></script>
<script type="text/javascript" src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/summernote/summernote-ko-KR.js"></script>
<link href="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/summernote/summernote.css" rel="stylesheet">


<script>
$(document).ready(function(){

    var no              = "<?=$print['popup_no']?>";
    var member_id       = "<?=$session_array['sess_member_id']?>";
    var file_count      = "<?=$file_count?>";
    var file, popup_title, popup_name, popup_content, popup_layout;

    $("#SubmitOk").submit(function(){
    	
        popup_title     = $(".popup_title").val();
        popup_name      = $(".popup_name").val();
        popup_content   = $(".popup_content").val();
        popup_layout    = $("select[name=\"popup_layout\"]").val();

        message         = confirm("페이지를 수정 할까요?");

        if(message) {

            if(popup_title == "") {

                alert("페이지 이름을 입력하세요.");
                $(".popup_title").focus();
                return false;

            } else if (popup_name == "") {

                alert("페이지 테이블명을 입력하세요.");
                $(".popup_name").focus();
                return false;

            } else if (popup_content == "") {

                alert("페이지 내용을 입력하세요.");
                $(".popup_content").focus();
                return false;

            } else if (popup_layout == "") {

                alert("페이지 레이아웃을 선택하세요.");
                $(".popup_layout").focus();
                return false;

            }

        } else {

            alert("취소 되었습니다.");
            return false;

        }

    });


    //////////////////////////////////


    $(".RemoveFile").click(function(){

        file			= $(this).attr("data");
        message			= confirm("파일을 삭제하면 브라우저를 새로고침 합니다. 삭제를 계속 진행 할까요?");

        $(".RemoveFileRows_" + file).each(function(){

            if(message){

                RemoveFile(file, no);

            } else {

                alert("취소 되었습니다.");
                return false;

            }

        });

    });


    //////////////////////////////////


    $(".summernote").summernote({

        callbacks : {
            onImageUpload : function(files){
                SendFile(no, member_id, files, file_count);
            }
        },

        height : 400,

        lang : 'ko-KR',

        popover : {
            image : [],
            link : [],
            air : []
        },

        maximumImageFileSize : 5242880,

        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['codeview']]
        ]

    });


    ///////////////////////////////////


    $(".GetRemove").click(function () {

        message = confirm("삭제 할까요?");

        if (message) {

            RemoveContent(popup_name);

        } else {

            alert("<?=KO_CANCELED?>");
            return false;

        }

    });


    ///////////////////////////////////
    

    $(".MoveList").click(function(){

        location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_list";

    });

});


///////////////////////////////////


function SendFile(no, member_id, files, file_count){

    if(files.length < (6-file_count)){	// 썸머노트가 배열을 던질 때 기본적으로 1을 더해서 던져주는듯; 2019-07-01

        for(var i = 0; i < files.length; i++){

            (function(i){

                var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_insert_files/";
                var ajax_type			= "post";
                var ajax_return_type	= "json";
                var ajax_param			= new FormData();
                var ajax_return_data;

                ajax_param.append("no", no);
                ajax_param.append("file_name", files[i]);
                ajax_param.append("count", files.length);
                ajax_param.append("dir", "popup");

                call_back				= ajaxSendFile(ajax_url, ajax_param, ajax_type, ajax_return_type);


                if(call_back.return_code == "0000"){

                    var image	= $("<img>").attr("src", "<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/popup/" + no + "/" + call_back.encrypt_name);
                    $(".summernote").summernote("insertNode", image[0]);

                } else if(call_back.return_code == "9997"){

                    alert(call_back.return_code + ": 파일이 크거나 파일 형식이 올바르지 않습니다.");

                } else if(call_back.return_code == "9998"){

                    alert(call_back.return_code + ": 1개 이상의 파일이 크거나 파일 형식이 올바르지 않습니다.");

                } else {

                    alert(call_back.return_code + ": <?=KO_UNSUCCESS?>");

                }


            })(i);

        }

    } else {

        alert("추가 할 수 없습니다.");
 
    }

}


///////////////////////////////////


function RemoveFile(file, no){

    var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_file_remove/" + no;
    var ajax_type			= "post";
    var ajax_return_type	= "json";
    var ajax_param			= [];
    var ajax_return_data;

    ajax_param.push("file_no=" + file);
    ajax_param.push("popup_no=" + no);
    ajax_param.push("dir=popup")

    call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);


    if(call_back.return_code == "0000"){

        alert("삭제 되었습니다.");
        location.reload();

    } else if(call_back.return_code == "9999"){

        alert("삭제 할 수 없습니다." + call_back.return_code);
        //location.reload();

    } else {

        alert(call_back.return_code);

    }

}


///////////////////////////////////


function RemoveContent(popup_name){

    var ajax_url			= "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_remove/" + popup_name;
    var ajax_type			= "post";
    var ajax_return_type	= "json";
    var ajax_param			= [];
    var ajax_return_data;

    ajax_param.push("popup_name=" + popup_name);

    call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

    if(call_back.return_code == "0000"){

        alert("<?=KO_SUCCESS?>");
        location.href = "<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_list";

    } else if(call_back.return_code == "9999"){

        alert("<?=KO_UNSUCCESS?>");

    } else {

        alert(call_back.return_code);

    }

}
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">페이지설정</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<!--
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>메뉴설정
						</a>
                        -->
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/page_list" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>페이지목록
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup_list" class="list-group-item list-group-item-action d-flex align-items-center active">
							<span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>팝업목록
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">팝업수정</h3>
					</div>
					
					<form id="SubmitOk" method="post" action="/dashboard/pager/popup_mod_ok/popup_name/<?=$print['popup_name']?>" enctype="multipart/form-data">
						
						<input type="hidden" name="member_id" class="member_id" value="<?=$session_array['sess_member_id']?>">
						<div class="card-body">
							
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">팝업 이름</label>
										<input type="text" name="popup_title" class="form-control popup_title" value="<?=$print['popup_title']?>" placeholder="팝업 이름을 입력하세요." required>
									</div>
								</div>
							</div>
							
							<div class="alert alert-primary"></div>
							
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">팝업명 (수정 불가)</label>
										<input type="text" name="popup_name" class="form-control popup_name" value="<?=$print['popup_name']?>" required readonly>
									</div>
								</div>
							</div>
							
							<div class="alert alert-primary"></div>
							
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">팝업 내용</label>
										<textarea name="popup_content" class="form-control summernote popup_content" value="" placeholder="팝업 내용을 입력하세요." required><?=$print['popup_content']?></textarea>
									</div>
								</div>
							</div>
							
							
							<?php
							if($file_count >= 1) {
								?>
								
								<div class="alert alert-primary"></div>
								
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="form-group">
											<label class="form-label">첨부파일 (<?=($file_count)?>)</label>
											<div>
												<?php
												$i = 1;
												foreach ($file_result as $files) {
													?>
													<button type="button" class="btn btn-secondary RemoveFile" data="<?= $files['file_no'] ?>">
														<i class="fe fe-paperclip RemoveFileRows_<?= $files['file_no'] ?>"></i>파일삭제<?=($i++)?>
													</button>
													<?php
												}
												?>
											</div>
										</div>
									</div>
								</div>
								
								<?php
							}
							?>
						
						</div>
						
						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="submit" class="btn btn-primary btn-space">저장</button>
                                <button type="button" class="btn btn-danger GetRemove">삭제</button>
								<button type="button" class="btn btn-secondary MoveList">목록</button>
							</div>
						</div>
					</form>
				
				</div>
			</div>
		</div>
	</div>
</div>