<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script>
$(document).ready(function(){

	var message, home_no, home_title, home_logo, home_icon, home_layout, member_skin, home_id_blocks, home_name_blocks, home_email_1, home_email_2, home_email_password;

	$("#SubmitOk").submit(function(){

		message				= confirm("작성을 완료할까요?");
		home_title			= $(".home_title").val();
        home_layout			= $(".home_layout").val();
        member_skin         = $(".member_skin").val();
        home_id_blocks      = $(".home_id_blocks").val();
        home_name_blocks    = $(".home_name_blocks").val();


		if(message){

			if(home_title == ""){

				alert("홈페이지 제목이 비었습니다.");
				$(".home_title").focus();
				return false;

			} else if(home_layout == ""){

				alert("레이아웃을 선택 하세요.");
				$(".home_layout").focus();
				return false;

			} else if(member_ksin == ""){
			    
			    alert("회원 스킨을 선택 하세요.");
			    $(".member_skin").focus();
			    return false;
			    
            } else if(home_id_blocks == ""){
				
				alert("아이디 사용 불가 목록을 작성 하세요.");
			    $(".home_id_blocks").focus();
			    return false;
			    
			} else if(home_name_blocks == ""){
				
				alert("닉네임 사용 불가 목록을 작성 하세요.");
			    $(".home_name_blocks").focus();
			    return false;
			    
			}


		} else {

			return false;

		}

	});

});
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">환경설정</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home/configs" class="list-group-item list-group-item-action d-flex align-items-center active">
							<span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>기본환경설정
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home/companies" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>회사정보
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">

				<div class="card">
					<div class="card-header">
						<h3 class="card-title">기본환경설정</h3>
					</div>

					<form id="SubmitOk" action="/dashboard/home/configs_ok/" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">홈페이지 제목</label>
										<input type="text" name="home_title" class="form-control home_title" placeholder="홈페이지 제목을 입력하세요." value="<?=$config['home_title']?>" required />
									</div>
								</div>
                            </div>

                            <div class="alert alert-primary"></div>

                            <div class="row">
                                <div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label">레이아웃</label>
										<select name="home_layout" class="form-control custom-select home_layout" required />
											<option value="">레이아웃 선택</option>
											<?php
											$i	= "";	// 변수 리셋

											for($i = 0; $i <= count($get_layout) - 1; $i++){

												if($config['home_layout'] == $get_layout[$i]){

													echo "<option value=\"" . $get_layout[$i] . "\" selected=\"selected\">" . $get_layout[$i] . "</option>";

												} else {

													echo "<option value=\"" . $get_layout[$i] . "\">" . $get_layout[$i] . "</option>";

												}

											}

											unset($i);	// 변수 리셋1
											?>
										</select>
									</div>
								</div>
	                            
	                            <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">회원 스킨</label>
                                        <select name="member_skin" class="form-control custom-select member_skin" required />
                                        <option value="">스킨 선택</option>
										<?php
										$i	= "";	// 변수 리셋
										
										for($i = 0; $i <= count($get_skin) - 1; $i++){
											
											if($config['member_skin'] == $get_skin[$i]){
												
												echo "<option value=\"" . $get_skin[$i] . "\" selected=\"selected\">" . $get_skin[$i] . "</option>";
												
											} else {
												
												echo "<option value=\"" . $get_skin[$i] . "\">" . $get_skin[$i] . "</option>";
												
											}
											
										}
										
										unset($i);	// 변수 리셋2
										?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-primary"></div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">아이디 사용 불가 목록 (, 기준)</label>
                                        <textarea name="home_id_blocks" class="form-control home_id_blocks" placeholder="홈페이지 가입 불가 아이디" required /><?=$config['home_id_blocks']?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-primary"></div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">닉네임 사용 불가 목록 (, 기준)</label>
                                        <textarea name="home_name_blocks" class="form-control home_name_blocks" placeholder="홈페이지 가입 불가 이름" required /><?=$config['home_name_blocks']?></textarea>
                                    </div>
                                </div>
                            </div>
							
						</div>


						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="submit" class="btn btn-primary btn-space">저장</button>
							</div>
						</div>
					</form>
				</div>
				
				
				<?php
				if($config_count >= 1){
				?>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">SMTP 설정</h3>
						</div>
	
						<form action="/dashboard/home/configs_smtp_ok/" method="post" enctype="multipart/form-data">
							<div class="card-body">
								<div class="row">
	                                <div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label class="form-label">SMTP 이메일</label>
	
											<div class="input-group">
	
												<?php
												$get_home_email		= explode("@", $config['home_email']);
												?>
												<input type="text" name="home_email_1" class="form-control home_email_1" placeholder="SMTP 이메일 주소를 입력하세요." value="<?=$this->common->GetDecrypt(@$get_home_email['0'], @$secret_key, @$secret_iv)?>" required />
	
												<span class="input-group-prepend" id="basic-addon3">
													<span class="input-group-text">@</span>
												</span>
	
												<input type="text" name="home_email_2" class="form-control home_email_2" placeholder="SMTP 이메일 주소를 입력하세요." value="<?=$this->common->GetDecrypt(@$get_home_email['1'], @$secret_key, @$secret_iv)?>" required />
											</div>
	
										</div>
									</div>
	
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label class="form-label">SMTP 이메일 비밀번호</label>
											<input type="password" name="home_email_password" class="form-control home_email_password" placeholder="SMTP 이메일 주소의 비밀번호를 입력하세요." value="<?=$this->common->GetDecrypt(@$config['home_email_password'], @$secret_key, @$secret_iv)?>" required />
										</div>
									</div>
	
									<div class="col-sm-6 col-md-6">
										<div class="form-group">
											<label class="form-label">SMTP 이메일 호스트</label>
											<input type="text" name="home_email_host" class="form-control home_email_host" placeholder="SMTP 이메일 주소의 호스트를 입력하세요." value="<?=$this->common->GetDecrypt(@$config['home_email_host'], @$secret_key, @$secret_iv)?>" required />
										</div>
									</div>
								</div>
							</div>
	
							<div class="card-footer">
								<div class="btn-list mt-4 text-right">
									<button type="submit" class="btn btn-primary btn-space">저장</button>
								</div>
							</div>
						</form>
	
					</div>
					
					
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">네이버 메타 태그</h3>
						</div>
	
						<form action="/dashboard/home/configs_meta_ok/" method="post">
							<div class="card-body">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="form-group">
											<label class="form-label">네이버 메타 태그</label>
											<input type="hidden" name="type" value="naver" readonly />
											<input type="text" name="home_naver" class="form-control home_naver" placeholder="네이버 메타 태그를 입력하세요." value="<?=htmlspecialchars(@$config['home_naver'])?>" />
										</div>
									</div>
								</div>
							</div>
	
							<div class="card-footer">
								<div class="btn-list mt-4 text-right">
									<button type="submit" class="btn btn-primary btn-space">저장</button>
								</div>
							</div>
						</form>
	
					</div>
					
					
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">구글 메타 태그</h3>
						</div>
	
						<form action="/dashboard/home/configs_meta_ok/" method="post">
							<div class="card-body">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="form-group">
											<label class="form-label">구글 메타 태그</label>
											<input type="hidden" name="type" value="google" readonly />
											<input type="text" name="home_google" class="form-control home_google" placeholder="구글 메타 태그를 입력하세요." value="<?=htmlspecialchars(@$config['home_google'])?>" />
										</div>
									</div>
								</div>
							</div>
	
							<div class="card-footer">
								<div class="btn-list mt-4 text-right">
									<button type="submit" class="btn btn-primary btn-space">저장</button>
								</div>
							</div>
						</form>
	
					</div>
					
	
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">로고 업로드</h3>
						</div>
	
						<form action="/dashboard/home/configs_logo_ok/" method="post" enctype="multipart/form-data">
							<div class="card-body">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="form-group">
											<label class="form-label">로고 (타입 : *.jpg/*.jpeg/*.png/*.bmp)</label>
											<input type="file" name="home_logo" class="form-control home_logo" value="<?=$config['home_logo']?>" required />
	
											<?php
											if(!empty($config['home_logo'])){
											?>
												<div class="tags">
													<span class="tag">
														<span class="ajax_home_logo">
															<a href="#" data-toggle="modal" data-target=".myModal_1">
																<?=$config['home_logo']?>
															</a>
														</span>
													</span>
												</div>
	
												<div class="modal fade myModal_1" role="dialog">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal"></button>
															</div>
	
															<div class="modal-body">
																<div class="my-3 my-md-5">
																	<div class="container">
																		<div class="card">
	
																			<div class="card-body">
																				<div class="row">
																					<img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_logo']?>">
																				</div>
	
																				<div class="alert alert-icon alert-primary" role="alert">
																					<label class="form-label">로고 URL</label>
																					<i class="fe fe-bell mr-2" aria-hidden="true"></i>
																					<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_logo']?>
																				</div>
																			</div>
	
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											<?php
											}
											?>
	
										</div>
									</div>
								</div>
							</div>
	
							<div class="card-footer">
								<div class="btn-list mt-4 text-right">
									<button type="submit" class="btn btn-outline-primary"><i class="fe fe-upload mr-2"></i>로고 업로드</button>
								</div>
							</div>
						</form>
	
					</div>
	
	
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">아이콘 업로드</h3>
						</div>
	
						<form action="/dashboard/home/configs_icon_ok/" method="post" enctype="multipart/form-data">
							<div class="card-body">
								<div class="row">
									<div class="col-sm-12 col-md-12">
										<div class="form-group">
											<label class="form-label">아이콘 (타입 : *.ico/ 크기 : 16px x 16px)</label>
											<input type="file" name="home_icon" class="form-control home_icon" value="<?=$config['home_icon']?>" required />
	
											<?php
											if(!empty($config['home_icon'])){
											?>
												<div class="tags">
													<span class="tag">
														<span class="ajax_home_icon">
															<a href="#" data-toggle="modal" data-target=".myModal_2">
																<?=$config['home_icon']?>
															</a>
														</span>
													</span>
												</div>
	
												<div class="modal fade myModal_2" role="dialog">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal"></button>
															</div>
	
															<div class="modal-body">
																<div class="my-3 my-md-5">
																	<div class="container">
																		<div class="card">
	
																			<div class="card-body">
																				<div class="row">
																					<img src="<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_icon']?>">
																				</div>
	
																				<div class="alert alert-icon alert-primary" role="alert">
																					<label class="form-label">아이콘 URL</label>
																					<i class="fe fe-bell mr-2" aria-hidden="true"></i>
																					<?=PROTOCOLS?><?=HTTP_HOST?>/uploaded/configs/<?=$config['home_icon']?>
																				</div>
																			</div>
	
																		</div>
																	</div>
																</div>
															</div>
	
														</div>
													</div>
												</div>
											<?php
											}
											?>
	
										</div>
									</div>
								</div>
							</div>
	
							<div class="card-footer">
								<div class="btn-list mt-4 text-right">
									<button type="submit" class="btn btn-outline-primary"><i class="fe fe-upload mr-2"></i>아이콘 업로드</button>
								</div>
							</div>
						</form>
	
					</div>
				<?php
				}
				?>

			</div>
		</div>
	</div>
</div>