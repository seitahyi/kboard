<?php
if(!defined('BASEPATH')) exit;
?>


<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script>
    $(document).ready(function(){

        var com_name, com_owner, com_rotary_1, com_rotary_2, com_rotary_3, com_fax_1, com_fax_2, com_fax_3, com_zip_1, com_zip_2, com_address_1, com_address_2, com_manager, com_manager_email_1, com_manager_email_2, message;

        $("#SubmitOk").submit(function(){

            message				= confirm("작성을 완료할까요?");
            com_name			= $(".com_name").val();
            com_owner			= $(".com_owner").val();
            com_rotary_1		= $(".com_rotary_1").val();
            com_rotary_2		= $(".com_rotary_2").val();
            com_rotary_3		= $(".com_rotary_3").val();
            com_fax_1			= $(".com_fax_1").val();
            com_fax_2			= $(".com_fax_2").val();
            com_fax_3			= $(".com_fax_3").val();
            com_zip_1			= $(".com_zip_1").val();
            com_zip_2			= $(".com_zip_2").val();
            com_address_1		= $(".com_address_1").val();
            com_address_2		= $(".com_address_2").val();
            com_manager			= $(".com_manager").val();
            com_manager_email_1	= $(".com_manager_email_1").val();
            com_manager_email_2	= $(".com_manager_email_2").val();

            if(message){

                if(com_name == ""){

                    alert("회사명을 입력하세요.");
                    $(".com_name").focus();
                    return false;

                } else if(com_owner == ""){

                    alert("대표자명을 입력하세요.");
                    $(".com_owner").focus();
                    return false;

                } else if(com_rotary_1 == ""){

                    alert("대표번호 첫째자리를 입력하세요.");
                    $(".com_rotary_1").focus();
                    return false;

                } else if(com_rotary_2 == ""){

                    alert("대표번호 둘째자리를 입력하세요.");
                    $(".com_rotary_2").focus();
                    return false;

                } else if(com_rotary_3 == ""){

                    alert("대표번호 셋째자리를 입력하세요.");
                    $(".com_rotary_3").focus();
                    return false;

                } else if(com_fax_1 == ""){

                    alert("팩스번호 첫째자리를 입력하세요.");
                    $(".com_fax_1").focus();
                    return false;

                } else if(com_fax_2 == ""){

                    alert("팩스번호 둘째자리를 입력하세요.");
                    $(".com_fax_2").focus();
                    return false;

                } else if(com_fax_3 == ""){

                    alert("팩스번호 셋째자리를 입력하세요.");
                    $(".com_fax_3").focus();
                    return false;

                } else if(com_zip_1	== ""){

                    alert("사업장 우편번호를 입력하세요.");
                    $(".com_zip_1").focus();
                    return false;

                } else if(com_zip_2 == ""){

                    alert("사업장 우편번호를 입력하세요.");
                    $(".com_zip_2").focus();
                    return false;

                } else if(com_address_1 == ""){

                    alert("사업장주소를 입력하세요.");
                    $(".com_address_1").focus();
                    return false;

                } else if(com_address_2 == ""){

                    alert("사업장 상세주소를 입력하세요.");
                    $(".com_address_2").focus();
                    return false;

                } else if(com_manager == ""){

                    alert("정보관리 책임자명을 입력하세요.");
                    $(".com_manager").focus();
                    return false;

                } else if(com_manager_email_1 == ""){

                    alert("정보관리 책임자 이메일 아이디를 입력하세요.");
                    $(".com_manager_email_1").focus();
                    return false;

                } else if(com_manager_email_2 == ""){

                    alert("정보관리 책임자 이메일 호스트를 입력하세요.");
                    $(".com_manager_email_2").focus();
                    return false;

                }

            } else {

                return false;

            }

        });

    });


    /////////////////////////////////////////////


    function openDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {

                document.getElementById('com_zip_1').value		= data.postcode1;
                document.getElementById('com_zip_2').value		= data.postcode2;
                document.getElementById('com_address_1').value	= data.address;

                document.getElementById('com_address_2').focus();

            }
        }).open();
    }
</script>


<div class="my-3 my-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h3 class="page-title mb-5">환경설정</h3>
                <div>
                    <div class="list-group list-group-transparent mb-0">
                        <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home/configs" class="list-group-item list-group-item-action d-flex align-items-center">
                            <span class="icon mr-3"></span>기본환경설정
                        </a>
                        <a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/home/companies" class="list-group-item list-group-item-action d-flex align-items-center active">
                            <span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>회사정보
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">회사정보</h3>
                    </div>

                    <form id="SubmitOk" action="/dashboard/home/companies_ok/" method="post">
                        <div class="card-body">
                            <div class="row">

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">회사명</label>
                                        <input type="text" name="com_name" class="form-control com_name" placeholder="회사명을 입력하세요." value="<?=$this->common->GetDecrypt(@$company['com_name'], @$secret_key, @$secret_iv)?>" />
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">대표자명</label>
                                        <input type="text" name="com_owner" class="form-control com_owner" placeholder="대표자명을 입력하세요." value="<?=$this->common->GetDecrypt(@$company['com_owner'], @$secret_key, @$secret_iv)?>" />
                                    </div>
                                </div>

                            </div>


                            <div class="alert alert-primary"></div>


                            <div class="row">

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">대표번호</label>
                                        <div class="input-group">

                                            <?php
                                            $get_rotary_num	= explode("-", $company['com_rotary']);
                                            ?>

                                            <input type="text" name="com_rotary_1" class="form-control com_rotary_1" placeholder="첫째자리" maxlength="4"
                                                   value="<?=$this->common->GetDecrypt(@$get_rotary_num['0'], @$secret_key, @$secret_iv)?>" />

                                            <span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">-</span>
											</span>

                                            <input type="text" name="com_rotary_2" class="form-control com_rotary_2" placeholder="둘째자리" maxlength="4" value="<?=$this->common->GetDecrypt(@$get_rotary_num['1'], @$secret_key, @$secret_iv)?>" />

                                            <span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">-</span>
											</span>

                                            <input type="text" name="com_rotary_3" class="form-control com_rotary_3" placeholder="셋째자리" maxlength="4" value="<?=$this->common->GetDecrypt(@$get_rotary_num['2'], @$secret_key, @$secret_iv)?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">팩스번호</label>
                                        <div class="input-group">

                                            <?php
                                            $get_fax_num	= explode("-", $company['com_fax']);
                                            ?>

                                            <input type="text" name="com_fax_1" class="form-control com_fax_1" placeholder="첫째자리" maxlength="4" value="<?=$this->common->GetDecrypt(@$get_fax_num['0'], @$secret_key, @$secret_iv)?>" />

                                            <span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">-</span>
											</span>

                                            <input type="text" name="com_fax_2" class="form-control com_fax_2" placeholder="둘째자리" maxlength="4" value="<?=$this->common->GetDecrypt(@$get_fax_num['1'], @$secret_key, @$secret_iv)?>" />

                                            <span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">-</span>
											</span>

                                            <input type="text" name="com_fax_3" class="form-control com_fax_3" placeholder="셋째자리" maxlength="4" value="<?=$this->common->GetDecrypt(@$get_fax_num['2'], @$secret_key, @$secret_iv)?>" />
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="alert alert-primary"></div>


                            <div class="row">

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">사업장 우편번호</label>
                                        <div class="input-group">
                                            <input type="text" name="com_zip_1" id="com_zip_1" class="form-control com_zip_1" placeholder="사업장 우편번호" value="<?=$this->common->GetDecrypt(@$company['com_zip_1'], @$secret_key, @$secret_iv)?>" />
                                            <span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">-</span>
											</span>
                                            <input type="text" name="com_zip_2" id="com_zip_2" class="form-control com_zip_2" placeholder="사업장 우편번호" value="<?=$this->common->GetDecrypt(@$company['com_zip_2'], @$secret_key, @$secret_iv)?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">사업장 주소</label>
                                        <input type="text" name="com_address_1" id="com_address_1" class="form-control com_address_1" placeholder="사업장주소를 입력하세요." value="<?=$this->common->GetDecrypt(@$company['com_address_1'], @$secret_key, @$secret_iv)?>" />
                                    </div>
                                </div>

                                <div class="col-sm-10 col-md-10">
                                    <div class="form-group">
                                        <label class="form-label">사업장 상세주소</label>
                                        <input type="text" name="com_address_2" id="com_address_2" class="form-control com_address_2" placeholder="사업장주소를 입력하세요." value="<?=$this->common->GetDecrypt(@$company['com_address_2'], @$secret_key, @$secret_iv)?>" />
                                    </div>
                                </div>

                                <div class="col-sm-2 col-md-2">
                                    <div class="form-group">
                                        <label class="form-label">&nbsp;</label>
                                        <button type="button" onclick="openDaumPostcode();" class="btn btn-outline-primary"><i class="fe fe-plus mr-2"></i>주소 찾기</button>
                                    </div>
                                </div>

                            </div>


                            <div class="alert alert-primary"></div>


                            <div class="row">

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">정보관리 책임자명</label>
                                        <input type="text" name="com_manager" class="form-control com_manager" placeholder="정보관리 책임자명을 입력하세요." value="<?=$this->common->GetDecrypt(@$company['com_manager'], @$secret_key, @$secret_iv)?>" />
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">정보관리 책임자 이메일</label>
                                        <div class="input-group">

                                            <?php
                                            $get_manager_email	= explode("@", $company['com_manager_email']);
                                            ?>

                                            <input type="text" name="com_manager_email_1" class="form-control com_manager_email_1" placeholder="정보관리 책임자 이메일 아이디를 입력하세요." value="<?=$this->common->GetDecrypt(@$get_manager_email['0'], @$secret_key, @$secret_iv)?>" />

                                            <span class="input-group-prepend" id="basic-addon3">
												<span class="input-group-text">@</span>
											</span>

                                            <input type="text" name="com_manager_email_2" class="form-control com_manager_email_2" placeholder="정보관리 책임자 이메일 호스트를 입력하세요." value="<?=$this->common->GetDecrypt(@$get_manager_email['1'], @$secret_key, @$secret_iv)?>" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="btn-list mt-4 text-right">
                                <button type="submit" class="btn btn-primary btn-space">저장</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>