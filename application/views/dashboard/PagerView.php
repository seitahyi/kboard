<?php
if(!defined('BASEPATH')) exit;
?>


<script src="<?=PROTOCOLS?><?=HTTP_HOST?>/skin/dashboard/assets/js/json_return.js"></script>

<script>
$(document).ready(function(){

	var message, home_no, home_title, home_logo, home_icon, home_skin;

	$("#SubmitOk").click(function(){

		message		= confirm("작성을 완료할까요?");
		home_title	= $(".home_title").val();
		home_skin	= $(".home_skin").val();

		
		if(message){

			if(home_title == ""){

				alert("홈페이지 제목이 비었습니다.");
				$(".home_title").focus();
				return false;

			} else if(home_skin == ""){

				alert("레이아웃을 선택 하세요.");
				$(".home_skin").focus();
				return false;

			}

		} else {

			return false;

		}

	});


	///////////////////////////////////////////////////

	
	$(".RemoveHomeLogo").click(function(){

		home_no		= 1;
		home_logo	= $(".ajax_home_logo").text();

		message		= confirm("파일을 삭제 할까요?");

		if(message){

			RemoveHomeLogo(home_no, home_logo.trim());

		} else {

			alert("취소 되었습니다.");
			return false;

		}

	});


	///////////////////////////////////////////////////


	$(".RemoveHomeIcon").click(function(){
		
		home_no		= 1;
		home_icon	= $(".ajax_home_icon").text();

		message		= confirm("파일을 삭제 할까요?");

		if(message){

			RemoveHomeIcon(home_no, home_icon.trim());

		} else {

			alert("취소 되었습니다.");
			return false;

		}

	});

});


///////////////////////////////////////////////////////


function RemoveHomeLogo(home_no, home_logo){

	var ajax_url			= "configs_file_remove";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("type=configs");
	ajax_param.push("home_no=" + home_no);
	ajax_param.push("home_logo=" + home_logo);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("파일을 삭제 했습니다.");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("파일 삭제가 실패 했습니다.");

	} else {

		alert(call_back.return_code);

	}

}


///////////////////////////////////////////////////////


function RemoveHomeIcon(home_no, home_icon){

	var ajax_url			= "configs_file_remove";
	var ajax_type			= "post";
	var ajax_return_type	= "json";
	var ajax_param			= [];
	var ajax_return_data;

	ajax_param.push("type=configs");
	ajax_param.push("home_no=" + home_no);
	ajax_param.push("home_icon=" + home_icon);

	call_back				= ajaxSend(ajax_url, ajax_param, ajax_type, ajax_return_type);

	if(call_back.return_code == "0000"){

		alert("파일을 삭제 했습니다.");
		location.reload();

	} else if(call_back.return_code == "9999"){

		alert("파일 삭제가 실패 했습니다.");

	} else {

		alert(call_back.return_code);

	}

}
</script>


<div class="my-3 my-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h3 class="page-title mb-5">페이지설정</h3>
				<div>
					<div class="list-group list-group-transparent mb-0">
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/menus" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>메뉴설정
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/pager" class="list-group-item list-group-item-action d-flex align-items-center active">
							<span class="icon mr-3"><i class="fe fe-chevrons-right"></i></span>페이지설정
						</a>
						<a href="<?=PROTOCOLS?><?=HTTP_HOST?>/dashboard/pager/popup" class="list-group-item list-group-item-action d-flex align-items-center">
							<span class="icon mr-3"></span>팝업설정
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">페이지설정</h3>
					</div>
					
					<form id="SubmitOk" action="/dashboard/member/member_update/" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 col-md-12">
									<div class="form-group">
										<label class="form-label">홈페이지 제목</label>
										<input type="text" name="home_title" class="form-control home_title" placeholder="홈페이지 제목을 입력하세요." value="222" required />
									</div>
								</div>
							</div>
						</div>
						
						<div class="card-footer">
							<div class="btn-list mt-4 text-right">
								<button type="click" class="btn btn-primary btn-space">저장</button>
							</div>
						</div>
					
					</form>
				
				</div>
				
			</div>
		</div>
	</div>
</div>