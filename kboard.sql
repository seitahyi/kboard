-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.0.35-MariaDB - MariaDB Server
-- 서버 OS:                        Linux
-- HeidiSQL 버전:                  11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- kboard 데이터베이스 구조 내보내기
CREATE DATABASE IF NOT EXISTS `kboard` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kboard`;

-- 테이블 kboard.admin_board 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_board` (
  `con_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '게시판 생성자',
  `con_title` varchar(100) NOT NULL COMMENT '게시판 이름',
  `con_name` varchar(100) NOT NULL COMMENT '게시판 테이블명',
  `con_category` varchar(100) NOT NULL COMMENT '게시판 카테고리',
  `con_skin` varchar(150) NOT NULL COMMENT '게시판 스킨',
  `con_layout` varchar(150) NOT NULL COMMENT '레이아웃',
  `con_thumb_filter` text NOT NULL COMMENT '첨부 허용 확장자',
  `con_diff` mediumint(1) unsigned NOT NULL COMMENT '작성 시간차',
  `con_list_level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '목록 권한',
  `con_content_level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '본문 권한',
  `con_write_level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '글쓰기 권한',
  `con_reply_level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '댓글 권한',
  `con_ip` varchar(16) NOT NULL COMMENT '게시판 생성 IP',
  `con_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '생성일', /* string 으로 저장 할 것 */
  `ext_title_1` varchar(20) NOT NULL COMMENT '여분필드 제목 1',
  `ext_title_2` varchar(20) NOT NULL COMMENT '여분필드 제목 2',
  `ext_title_3` varchar(20) NOT NULL COMMENT '여분필드 제목 3',
  `ext_title_4` varchar(20) NOT NULL COMMENT '여분필드 제목 4',
  `ext_title_5` varchar(20) NOT NULL COMMENT '여분필드 제목 5',
  PRIMARY KEY (`con_no`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 테이블 kboard.admin_company 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_company` (
  `com_no` smallint(6) unsigned NOT NULL,
  `com_name` varchar(100) NOT NULL COMMENT '회사명 암호화',
  `com_owner` varchar(100) NOT NULL COMMENT '대표자명 암호화',
  `com_rotary` varchar(100) NOT NULL COMMENT '대표번호 암호화',
  `com_fax` varchar(100) NOT NULL COMMENT '팩스번호 암호화',
  `com_zip_1` varchar(100) NOT NULL COMMENT '사업장 우편번호 1 암호화',
  `com_zip_2` varchar(100) NOT NULL COMMENT '사업장 우편번호 2 암호화',
  `com_address_1` varchar(255) NOT NULL COMMENT '사업장주소 암호화',
  `com_address_2` varchar(255) NOT NULL COMMENT '사업장 상세주소 암호화',
  `com_manager` varchar(100) NOT NULL COMMENT '정보관리 책임자명 암호화',
  `com_manager_email` varchar(200) NOT NULL COMMENT '청보관리 책임자 이메일 암호화',
  `com_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '최근수정일',
  PRIMARY KEY (`com_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 kboard.admin_config 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_config` (
  `home_no` smallint(6) unsigned NOT NULL,
  `home_title` varchar(100) NOT NULL COMMENT '홈페이지 타이틀',
  `home_logo` varchar(255) NOT NULL COMMENT '홈페이지 로고',
  `home_icon` varchar(255) NOT NULL COMMENT '홈페이지 아이콘',
  `home_layout` varchar(200) NOT NULL COMMENT '홈페이지 레이아웃',
  `member_skin` varchar(200) NOT NULL COMMENT '회원 스킨',
  `home_id_blocks` tinytext NOT NULL COMMENT '홈페이지 가입 불가 아이디',
  `home_name_blocks` tinytext NOT NULL COMMENT '홈페이지 가입 불가 이름',
  `home_email` varchar(255) NOT NULL COMMENT 'smtp용 이메일 암호화',
  `home_email_password` varchar(255) NOT NULL COMMENT 'smtp용 이메일 비밀번호 암호화',
  `home_email_host` varchar(255) NOT NULL COMMENT 'smtp용 이메일 호스트 암호화',
  `home_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '홈페이지 최근수정일',
  PRIMARY KEY (`home_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 kboard.admin_lost_password 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_lost_password` (
  `lo_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '회원아이디',
  `lo_password` varchar(255) NOT NULL COMMENT '새로받은 비밀번호 암호화',
  `lo_ip` varchar(16) NOT NULL,
  `lo_date` varchar(16) NOT NULL,
  PRIMARY KEY (`lo_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.admin_lost_password:~0 rows (대략적) 내보내기
DELETE FROM `admin_lost_password`;
/*!40000 ALTER TABLE `admin_lost_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_lost_password` ENABLE KEYS */;

-- 테이블 kboard.admin_member 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_member` (
  `member_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL,
  `member_password` varchar(255) NOT NULL COMMENT '비밀번호 암호화',
  `member_name` varchar(255) NOT NULL COMMENT '이름 암호화',
  `member_email` varchar(255) NOT NULL COMMENT '이메일 암호화',
  `member_level` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '레벨',
  `member_profile` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '0:비공개/1:공개',
  `member_formal` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0:정지/1:승인',
  `member_signup_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '가입일',
  PRIMARY KEY (`member_no`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.admin_member:~0 rows (대략적) 내보내기
DELETE FROM `admin_member`;
/*!40000 ALTER TABLE `admin_member` DISABLE KEYS */;
INSERT INTO `admin_member` (`member_no`, `member_id`, `member_password`, `member_name`, `member_email`, `member_level`, `member_profile`, `member_formal`, `member_signup_date`) VALUES
	(1, 'admin', 'bjluZlpnblB6SzE2YmZ3ekVkNiswQT09', 'UHJOWEE2bTFUeFlQODVWL21mak03Zz09', 'naver@naver.com', 10, 1, 1, '2019-10-01 16:23:52');
/*!40000 ALTER TABLE `admin_member` ENABLE KEYS */;

-- 테이블 kboard.admin_menu 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_menu` (
  `menu_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_grandparent` int(11) unsigned NOT NULL COMMENT '메뉴 상속 조부모',
  `menu_parent` int(11) unsigned NOT NULL COMMENT '메뉴 상속 부모',
  `menu_child` int(11) unsigned NOT NULL COMMENT '메뉴 상속 자식',
  `menu_name` varchar(50) NOT NULL COMMENT '메뉴 이름',
  `menu_url` varchar(255) NOT NULL COMMENT '메뉴 주소',
  `menu_target` varchar(10) NOT NULL COMMENT '메뉴 타겟',
  `menu_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '메뉴 생성일',
  PRIMARY KEY (`menu_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.admin_menu:~0 rows (대략적) 내보내기
DELETE FROM `admin_menu`;
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;

-- 테이블 kboard.admin_page 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_page` (
  `page_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '페이지 생성자',
  `page_title` varchar(100) NOT NULL COMMENT '페이지 이름',
  `page_name` varchar(100) NOT NULL COMMENT '페이지 테이블명',
  `page_content` text NOT NULL COMMENT '페이지 내용',
  `page_layout` varchar(150) NOT NULL COMMENT '페이지 레이아웃',
  `page_ip` varchar(16) NOT NULL COMMENT '페이지 생성 IP',
  `page_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '페이지 생성일',
  PRIMARY KEY (`page_no`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- 테이블 kboard.admin_page_file 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_page_file` (
  `file_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_no` int(11) unsigned NOT NULL COMMENT '페이지 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `file_encrypt_name` varchar(255) NOT NULL COMMENT '암호화 파일명',
  `file_real_name` varchar(255) NOT NULL COMMENT '원래 파일명',
  `file_width` smallint(10) unsigned NOT NULL COMMENT '넓이값',
  `file_height` smallint(10) unsigned NOT NULL COMMENT '길이값',
  `file_type` varchar(20) NOT NULL COMMENT '파일 형식',
  `file_ip` varchar(16) NOT NULL COMMENT '아이피',
  `file_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '첨부일자',
  PRIMARY KEY (`file_no`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 테이블 kboard.admin_permission 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_permission` (
  `perm_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '생성자',
  `perm_member_id` varchar(50) NOT NULL,
  `perm_page` varchar(100) NOT NULL COMMENT '권한줄 페이지',
  `perm_name` varchar(150) NOT NULL COMMENT '권한줄 페이지명',
  `perm_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`perm_no`),
  KEY `member_permission_FK` (`perm_member_id`),
  CONSTRAINT `member_permission_FK` FOREIGN KEY (`perm_member_id`) REFERENCES `admin_member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.admin_permission:~0 rows (대략적) 내보내기
DELETE FROM `admin_permission`;
/*!40000 ALTER TABLE `admin_permission` DISABLE KEYS */;
INSERT INTO `admin_permission` (`perm_no`, `member_id`, `perm_member_id`, `perm_page`, `perm_name`, `perm_date`) VALUES
	(1, 'admin', 'admin', 'dummy_row', '슈퍼어드민 전용 행', '2019-04-05 20:56:56');
/*!40000 ALTER TABLE `admin_permission` ENABLE KEYS */;

-- 테이블 kboard.admin_popup 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_popup` (
  `popup_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '팝업 생성자',
  `popup_title` varchar(100) NOT NULL COMMENT '팝업 이름',
  `popup_name` varchar(100) NOT NULL COMMENT '팝업 테이블명',
  `popup_content` text NOT NULL COMMENT '팝업 내용',
  `popup_layout` varchar(150) NOT NULL COMMENT '팝업 레이아웃',
  `popup_ip` varchar(16) NOT NULL COMMENT '팝업 생성 IP',
  `popup_date` datetime NOT NULL COMMENT '팝업 생성일',
  PRIMARY KEY (`popup_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.admin_popup:~0 rows (대략적) 내보내기
DELETE FROM `admin_popup`;
/*!40000 ALTER TABLE `admin_popup` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_popup` ENABLE KEYS */;

-- 테이블 kboard.admin_popup_file 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_popup_file` (
  `file_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `popup_no` int(11) unsigned NOT NULL COMMENT '팝업 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `file_encrypt_name` varchar(255) NOT NULL COMMENT '암호화 파일명',
  `file_real_name` varchar(255) NOT NULL COMMENT '원래 파일명',
  `file_width` smallint(10) unsigned NOT NULL COMMENT '넓이값',
  `file_height` smallint(10) unsigned NOT NULL COMMENT '길이값',
  `file_type` varchar(20) NOT NULL COMMENT '파일 형식',
  `file_ip` varchar(16) NOT NULL COMMENT '아이피',
  `file_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '첨부일자',
  PRIMARY KEY (`file_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.admin_popup_file:~0 rows (대략적) 내보내기
DELETE FROM `admin_popup_file`;
/*!40000 ALTER TABLE `admin_popup_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_popup_file` ENABLE KEYS */;

-- 테이블 kboard.admin_sessions 구조 내보내기
CREATE TABLE IF NOT EXISTS `admin_sessions` (
  `no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`no`),
  UNIQUE KEY `ci_sessions_id_ip` (`id`,`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=977 DEFAULT CHARSET=utf8;

-- 테이블 kboard.board_counseling 구조 내보내기
CREATE TABLE IF NOT EXISTS `board_counseling` (
  `bbs_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `member_name` varchar(50) NOT NULL COMMENT '회원 이름',
  `bbs_title` varchar(255) NOT NULL COMMENT '제목',
  `bbs_category` varchar(100) NOT NULL COMMENT '카테고리',
  `bbs_content` text NOT NULL COMMENT '본문',
  `bbs_secret` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '비밀글',
  `bbs_readed` mediumint(8) unsigned NOT NULL COMMENT '조회수',
  `bbs_ip` varchar(16) NOT NULL COMMENT '아이피',
  `bbs_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  `ext_desc_1` varchar(30) NOT NULL COMMENT '여분필드 본문 1',
  `ext_desc_2` varchar(30) NOT NULL COMMENT '여분필드 본문 2',
  `ext_desc_3` varchar(30) NOT NULL COMMENT '여분필드 본문 3',
  `ext_desc_4` varchar(30) NOT NULL COMMENT '여분필드 본문 4',
  `ext_desc_5` varchar(30) NOT NULL COMMENT '여분필드 본문 5',
  PRIMARY KEY (`bbs_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.board_counseling:~0 rows (대략적) 내보내기
DELETE FROM `board_counseling`;
/*!40000 ALTER TABLE `board_counseling` DISABLE KEYS */;
/*!40000 ALTER TABLE `board_counseling` ENABLE KEYS */;

-- 테이블 kboard.board_notice 구조 내보내기
CREATE TABLE IF NOT EXISTS `board_notice` (
  `bbs_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `member_name` varchar(50) NOT NULL COMMENT '회원 이름',
  `bbs_title` varchar(255) NOT NULL COMMENT '제목',
  `bbs_category` varchar(100) NOT NULL COMMENT '카테고리',
  `bbs_content` text NOT NULL COMMENT '본문',
  `bbs_secret` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '비밀글',
  `bbs_readed` mediumint(8) unsigned NOT NULL COMMENT '조회수',
  `bbs_ip` varchar(16) NOT NULL COMMENT '아이피',
  `bbs_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  `ext_desc_1` varchar(30) NOT NULL COMMENT '여분필드 본문 1',
  `ext_desc_2` varchar(30) NOT NULL COMMENT '여분필드 본문 2',
  `ext_desc_3` varchar(30) NOT NULL COMMENT '여분필드 본문 3',
  `ext_desc_4` varchar(30) NOT NULL COMMENT '여분필드 본문 4',
  `ext_desc_5` varchar(30) NOT NULL COMMENT '여분필드 본문 5',
  PRIMARY KEY (`bbs_no`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 테이블 kboard.board_test_1 구조 내보내기
CREATE TABLE IF NOT EXISTS `board_test_1` (
  `bbs_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `member_name` varchar(50) NOT NULL COMMENT '회원 이름',
  `bbs_title` varchar(255) NOT NULL COMMENT '제목',
  `bbs_category` varchar(100) NOT NULL COMMENT '카테고리',
  `bbs_content` text NOT NULL COMMENT '본문',
  `bbs_secret` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '비밀글',
  `bbs_readed` mediumint(8) unsigned NOT NULL COMMENT '조회수',
  `bbs_ip` varchar(16) NOT NULL COMMENT '아이피',
  `bbs_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  `ext_desc_1` varchar(30) NOT NULL COMMENT '여분필드 본문 1',
  `ext_desc_2` varchar(30) NOT NULL COMMENT '여분필드 본문 2',
  `ext_desc_3` varchar(30) NOT NULL COMMENT '여분필드 본문 3',
  `ext_desc_4` varchar(30) NOT NULL COMMENT '여분필드 본문 4',
  `ext_desc_5` varchar(30) NOT NULL COMMENT '여분필드 본문 5',
  PRIMARY KEY (`bbs_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.board_test_1:~0 rows (대략적) 내보내기
DELETE FROM `board_test_1`;
/*!40000 ALTER TABLE `board_test_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `board_test_1` ENABLE KEYS */;

-- 테이블 kboard.file_board_counseling 구조 내보내기
CREATE TABLE IF NOT EXISTS `file_board_counseling` (
  `file_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `file_encrypt_name` varchar(255) NOT NULL COMMENT '암호화 파일명',
  `file_real_name` varchar(255) NOT NULL COMMENT '원래 파일명',
  `file_width` smallint(10) unsigned NOT NULL COMMENT '넓이값',
  `file_height` smallint(10) unsigned NOT NULL COMMENT '길이값',
  `file_type` varchar(16) NOT NULL COMMENT '파일 형식',
  `file_ip` varchar(16) NOT NULL COMMENT '아이피',
  `file_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '첨부일자',
  PRIMARY KEY (`file_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.file_board_counseling:~0 rows (대략적) 내보내기
DELETE FROM `file_board_counseling`;
/*!40000 ALTER TABLE `file_board_counseling` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_board_counseling` ENABLE KEYS */;

-- 테이블 kboard.file_board_notice 구조 내보내기
CREATE TABLE IF NOT EXISTS `file_board_notice` (
  `file_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `file_encrypt_name` varchar(255) NOT NULL COMMENT '암호화 파일명',
  `file_real_name` varchar(255) NOT NULL COMMENT '원래 파일명',
  `file_width` smallint(10) unsigned NOT NULL COMMENT '넓이값',
  `file_height` smallint(10) unsigned NOT NULL COMMENT '길이값',
  `file_type` varchar(16) NOT NULL COMMENT '파일 형식',
  `file_ip` varchar(16) NOT NULL COMMENT '아이피',
  `file_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '첨부일자',
  PRIMARY KEY (`file_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.file_board_notice:~0 rows (대략적) 내보내기
DELETE FROM `file_board_notice`;
/*!40000 ALTER TABLE `file_board_notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_board_notice` ENABLE KEYS */;

-- 테이블 kboard.file_board_test_1 구조 내보내기
CREATE TABLE IF NOT EXISTS `file_board_test_1` (
  `file_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `file_encrypt_name` varchar(255) NOT NULL COMMENT '암호화 파일명',
  `file_real_name` varchar(255) NOT NULL COMMENT '원래 파일명',
  `file_width` smallint(10) unsigned NOT NULL COMMENT '넓이값',
  `file_height` smallint(10) unsigned NOT NULL COMMENT '길이값',
  `file_type` varchar(16) NOT NULL COMMENT '파일 형식',
  `file_ip` varchar(16) NOT NULL COMMENT '아이피',
  `file_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '첨부일자',
  PRIMARY KEY (`file_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.file_board_test_1:~0 rows (대략적) 내보내기
DELETE FROM `file_board_test_1`;
/*!40000 ALTER TABLE `file_board_test_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_board_test_1` ENABLE KEYS */;

-- 테이블 kboard.reply_board_counseling 구조 내보내기
CREATE TABLE IF NOT EXISTS `reply_board_counseling` (
  `rep_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rep_parent_no` int(11) unsigned NOT NULL,
  `rep_child_no` int(11) unsigned NOT NULL,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `member_name` varchar(50) NOT NULL COMMENT '회원 이름',
  `rep_content` varchar(255) NOT NULL COMMENT '댓글',
  `rep_ip` varchar(16) NOT NULL COMMENT '아이피',
  `rep_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  PRIMARY KEY (`rep_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.reply_board_counseling:~0 rows (대략적) 내보내기
DELETE FROM `reply_board_counseling`;
/*!40000 ALTER TABLE `reply_board_counseling` DISABLE KEYS */;
/*!40000 ALTER TABLE `reply_board_counseling` ENABLE KEYS */;

-- 테이블 kboard.reply_board_notice 구조 내보내기
CREATE TABLE IF NOT EXISTS `reply_board_notice` (
  `rep_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rep_parent_no` int(11) unsigned NOT NULL,
  `rep_child_no` int(11) unsigned NOT NULL,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `member_name` varchar(50) NOT NULL COMMENT '회원 이름',
  `rep_content` varchar(255) NOT NULL COMMENT '댓글',
  `rep_ip` varchar(16) NOT NULL COMMENT '아이피',
  `rep_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  PRIMARY KEY (`rep_no`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 테이블 kboard.reply_board_test_1 구조 내보내기
CREATE TABLE IF NOT EXISTS `reply_board_test_1` (
  `rep_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rep_parent_no` int(11) unsigned NOT NULL,
  `rep_child_no` int(11) unsigned NOT NULL,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `member_name` varchar(50) NOT NULL COMMENT '회원 이름',
  `rep_content` varchar(255) NOT NULL COMMENT '댓글',
  `rep_ip` varchar(16) NOT NULL COMMENT '아이피',
  `rep_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  PRIMARY KEY (`rep_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.reply_board_test_1:~0 rows (대략적) 내보내기
DELETE FROM `reply_board_test_1`;
/*!40000 ALTER TABLE `reply_board_test_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `reply_board_test_1` ENABLE KEYS */;

-- 테이블 kboard.vote_board_counseling 구조 내보내기
CREATE TABLE IF NOT EXISTS `vote_board_counseling` (
  `vo_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `vo_like` tinyint(1) unsigned NOT NULL COMMENT '추천수',
  `vo_dislike` tinyint(1) unsigned NOT NULL COMMENT '비추천수',
  `vo_ip` varchar(16) NOT NULL COMMENT '아이피',
  `vo_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  PRIMARY KEY (`vo_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.vote_board_counseling:~0 rows (대략적) 내보내기
DELETE FROM `vote_board_counseling`;
/*!40000 ALTER TABLE `vote_board_counseling` DISABLE KEYS */;
/*!40000 ALTER TABLE `vote_board_counseling` ENABLE KEYS */;

-- 테이블 kboard.vote_board_notice 구조 내보내기
CREATE TABLE IF NOT EXISTS `vote_board_notice` (
  `vo_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `vo_like` tinyint(1) unsigned NOT NULL COMMENT '추천수',
  `vo_dislike` tinyint(1) unsigned NOT NULL COMMENT '비추천수',
  `vo_ip` varchar(16) NOT NULL COMMENT '아이피',
  `vo_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  PRIMARY KEY (`vo_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.vote_board_notice:~0 rows (대략적) 내보내기
DELETE FROM `vote_board_notice`;
/*!40000 ALTER TABLE `vote_board_notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `vote_board_notice` ENABLE KEYS */;

-- 테이블 kboard.vote_board_test_1 구조 내보내기
CREATE TABLE IF NOT EXISTS `vote_board_test_1` (
  `vo_no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bbs_no` int(11) unsigned NOT NULL COMMENT '게시판 PK',
  `member_id` varchar(50) NOT NULL COMMENT '회원 아이디',
  `vo_like` tinyint(1) unsigned NOT NULL COMMENT '추천수',
  `vo_dislike` tinyint(1) unsigned NOT NULL COMMENT '비추천수',
  `vo_ip` varchar(16) NOT NULL COMMENT '아이피',
  `vo_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '작성일',
  PRIMARY KEY (`vo_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 kboard.vote_board_test_1:~0 rows (대략적) 내보내기
DELETE FROM `vote_board_test_1`;
/*!40000 ALTER TABLE `vote_board_test_1` DISABLE KEYS */;
/*!40000 ALTER TABLE `vote_board_test_1` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
