function ajaxSend(url, param, type, return_type){

    var rtn;

    $.ajax({
        type        : type,
        url         : url,
        data        : param.join("&"),
        dataType    : return_type,
        async       : false,
        success     : function(data) {
            rtn = data;
        },
        error       : function(e) {
            alert('Exception:'+e);
        }
    });

    return rtn;
}


function ajaxSendForm(url, param, type, return_type){

    var rtn;

    $.ajax({
        type        : type,
        url         : url,
        data        : param,
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
        dataType    : return_type,
        async       : false,
        success     : function(data) {
            rtn = data;
        },
        error       : function(e) {
            alert('Exception:'+e);
        }
    });

    return rtn;
}


function ajaxSendFile(url, param, type, return_type){

	var rtn;

	$.ajax({

		type		: type,
		url			: url,
		cache		: false,
		contentType	: false,
		data		: param,
		enctype		: 'multipart/form-data',
		dataType	: return_type,
		processData	: false,
		async		: false,
		beforeSend	: function(xhr){
			$("#loader").html("<img src=\"http://smaks1.sch.id/images/ajax-loader.gif\">");
		},
		success		: function(data){
			rtn	= data;
		},
		error		: function(e){
			alert('Exception:' + e);
		}

	});

	return rtn;

}


//replaceAll prototype 선언
String.prototype.replaceAll = function(org, dest) {
    return this.split(org).join(dest);
}


function AltMsg(type, message, await){

	var type, message, returns, alert_type;
	var awaiting	= Number(await);


	if(type == "0000"){

		alert_type	= "alert-success";

	} else if(type == "9999"){

		alert_type	= "alert-danger";

	} else if(type == "9998"){

		alert_type	= "alert-danger";

	} else if(type == "9997"){

		alert_type	= "alert-danger";

	}

	alert_var		=	"";
	alert_var		+=	"<div class=\"alert " + alert_type + " alert-dismissable\">";
	alert_var		+=		message;
	alert_var		+=	"</div>";

	object			= $("#alert").html(alert_var);
	returns			= $("#alert").fadeOut(awaiting);

	return returns;

}